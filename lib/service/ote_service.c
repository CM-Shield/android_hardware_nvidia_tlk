/*
 * Copyright (c) 2013-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <sys/ioccom.h>

#include <common/ote_command.h>
#include <common/ote_common.h>
#include <service/ote_service.h>
#include <service/ote_memory.h>
#include <service/ote_task_load.h>

static void * ote_instance_data;

ta_event_handler_t __event_handler = NULL;

/*
 * Stubs for TA init/destroy handlers.
 */
__attribute__((weak)) te_error_t te_init(int argc, char **argv)
{
	(void)argc;
	(void)argv;
	return 0;
}
__attribute__((weak)) void te_destroy(void)
{
	; // do nothing
}

/*
 * Routines managing the TA entry inputs and return values.
 */
static void te_get_next_entry_point_msg(te_entry_point_message_t *ep)
{
	int retval;

	retval = read(TE_INTERFACE, ep, sizeof(te_entry_point_message_t));
	if (retval < 0) {
		exit(-1);
	}
}

static void te_handle_create_instance(te_entry_point_message_t *ep)
{
	ep->result = te_create_instance_iface();
	write(TE_RESULT, ep, sizeof(te_entry_point_message_t));
}

static void te_create_operation_wrapper(
		te_entry_point_message_t *ep,
		te_operation_t* te_op)
{
	te_oper_param_t *req_params;
	uint32_t i;

	te_op->status = OTE_SUCCESS;
	te_op->command = ep->command_id;
	te_op->interface_side = 1;

	if (!ep->params_size) {
		te_op->list_count = ep->params_size;
		te_op->list_head = ep->params;
		te_op->list_tail = (cmnptr_t)(uintptr_t)NULL;
		return;
	}

	te_op->list_count = ep->params_size;
	te_op->list_head = ep->params;
	req_params = (te_oper_param_t *)(uintptr_t)ep->params;

	for (i = 0; i < te_op->list_count-1; i++) {
		req_params[i].next = (cmnptr_t)(uintptr_t)&req_params[i+1];
	}
	req_params[te_op->list_count-1].next = (cmnptr_t)(uintptr_t)NULL;
	te_op->list_tail = (cmnptr_t)(uintptr_t)req_params+(ep->params_size-1);
	return;
}

static void te_handle_operation(te_entry_point_message_t *ep) {
	te_error_t result;
	te_operation_t op_te;

	if (ep->params_size) {
		te_oper_param_t *req_params;

		req_params = te_mem_alloc(sizeof(te_oper_param_t) * ep->params_size);
		if (req_params == NULL) {
			ep->result = OTE_ERROR_OUT_OF_MEMORY;
			goto err;
		}
		ep->params = (cmnptr_t)(uintptr_t)req_params;
		te_get_next_entry_point_msg(ep);
	} else {
		ep->params = (cmnptr_t)(uintptr_t)NULL;
	}

	te_create_operation_wrapper(ep, &op_te);
	result = te_receive_operation_iface((void *)(uintptr_t)ep->context, &op_te);
	ep->result = result;

err:
	write(TE_RESULT, ep, sizeof(te_entry_point_message_t));
	te_mem_free((void *)(uintptr_t)ep->params);
}

static void te_handle_close_session(te_entry_point_message_t *ep)
{
	te_close_session_iface((void *)(uintptr_t)ep->context);
	ep->result = OTE_SUCCESS;
	write(TE_RESULT, ep, sizeof(te_entry_point_message_t));
}

static void te_handle_destroy_instance(te_entry_point_message_t *ep)
{
	te_destroy_instance_iface();
	ep->result = OTE_SUCCESS;
	write(TE_RESULT, ep, sizeof(te_entry_point_message_t));
}

static void te_handle_open_session(te_entry_point_message_t *ep)
{
	te_error_t result;
	te_operation_t op_te;

	if (ep->params_size) {
		te_oper_param_t *req_params;

		req_params = te_mem_alloc(sizeof(te_oper_param_t) * ep->params_size);
		if (req_params == NULL) {
			te_fprintf(TE_ERR, "OUT OF MEMORY\n");
			ep->result = OTE_ERROR_OUT_OF_MEMORY;
			goto err;
		}
		ep->params = (cmnptr_t)(uintptr_t)req_params;
		te_get_next_entry_point_msg(ep);
	} else {
		ep->params = (cmnptr_t)(uintptr_t)NULL;
	}

	te_create_operation_wrapper(ep, &op_te);
	result = te_open_session_iface((void **)(uintptr_t)&ep->context, &op_te);
	ep->result = result;

err:
	write(TE_RESULT, ep, sizeof(te_entry_point_message_t));
	te_mem_free((void *)(uintptr_t)ep->params);
}

static void te_handle_ta_event(te_entry_point_message_t *ep)
{
	/*
	 * NOTE: Current implementation does not support
	 * real argument passing from *ep.
	 */
	ta_event_args_t args;

	args.event_id = ep->command_id;

	if (__event_handler == NULL) {
		te_fprintf(TE_ERR, "%s: No ta event handler for this task\n",
				__func__);
		return;
	}

	ep->result = __event_handler(&args);
}

static void te_schedule_ta_entry_point(void)
{
	te_entry_point_message_t ep;

	while (1) {
		memset(&ep, 0, sizeof(te_entry_point_message_t));
		te_get_next_entry_point_msg(&ep);

		switch (ep.type) {
		case CREATE_INSTANCE:
			te_handle_create_instance(&ep);
			break;

		case DESTROY_INSTANCE:
			te_handle_destroy_instance(&ep);
			break;

		case OPEN_SESSION:
			te_handle_open_session(&ep);
			break;

		case CLOSE_SESSION:
			te_handle_close_session(&ep);
			break;

		case LAUNCH_OPERATION:
			te_handle_operation(&ep);
			break;

		case TA_EVENT:
			te_handle_ta_event(&ep);
			break;

		default:
			te_fprintf(TE_ERR, "received unrecognized command (%d)\n", ep.type);
			exit(-1);
		}
	}
}

/* set a log prefix based on task manifest name */
static void te_lib_set_printf_prefix()
{
	te_error_t err = OTE_SUCCESS;
	char	   prefix[OTE_TE_FPRINTF_PREFIX_MAX_LENGTH];
	uint32_t   name_len = sizeof(prefix) - 4;

	prefix[0] = '[';
	err = te_task_get_name_self(&prefix[1], &name_len);
	if (err == OTE_SUCCESS && name_len > 0) {
		prefix[1 + name_len + 0] = ']';
		prefix[1 + name_len + 1] = ' ';
		prefix[1 + name_len + 2] = '\000';
		te_fprintf_set_prefix(prefix);
	}
}

static void te_lib_initialize(void)
{
	return;
}

int main(int argc, char** argv, char** env)
{
	(void)argc;
	(void)argv;
	(void)env;

	te_lib_set_printf_prefix();

	if (te_init(argc, argv) == OTE_SUCCESS) {
		te_lib_initialize();
		te_schedule_ta_entry_point();
		te_destroy();
		return 0;
	}
	return -1;
}

void ote_set_instance_data(void *instanceData)
{
	ote_instance_data = instanceData;
}

void *ote_get_instance_data(void)
{
	return ote_instance_data;
}

te_error_t te_get_current_ta_uuid(te_service_id_t *value)
{
	te_get_property_args_t args;

	memset(&args, 0, sizeof(args));

	args.prop = TE_PROPERTY_CURRENT_TA;
	args.data_type = TE_PROP_DATA_TYPE_UUID;
	args.value_size = sizeof(te_service_id_t);

	if (ioctl(1, OTE_IOCTL_GET_PROPERTY, &args) < 0) {
		te_fprintf(TE_ERR, "%s: failed to launch operation\n", __func__);
		return OTE_ERROR_GENERIC;
	}

	if (value)
		te_mem_move(value, &args.value.uuid, sizeof(*value));

	return args.result;
}

te_error_t te_get_client_identity(te_identity_t *value)
{
	te_get_property_args_t args;

	memset(&args, 0, sizeof(args));

	args.prop = TE_PROPERTY_CURRENT_CLIENT;
	args.data_type = TE_PROP_DATA_TYPE_IDENTITY;
	args.value_size = sizeof(te_identity_t);

	if (ioctl(1, OTE_IOCTL_GET_PROPERTY, &args) < 0) {
		if (args.result == OTE_ERROR_NOT_IMPLEMENTED)
			return args.result;
		te_fprintf(TE_ERR, "%s: failed to launch operation\n", __func__);
		return OTE_ERROR_GENERIC;
	}

	if (value)
		te_mem_move(value, &args.value.identity, sizeof(*value));

	return args.result;
}

te_error_t te_get_client_ta_identity(te_identity_t *value)
{
	te_error_t status;

	status = te_get_client_identity(value);
	if (status != OTE_SUCCESS)
		return status;

	/* client must be a ta */
	if (value->login != TE_LOGIN_TA) {
		te_fprintf(TE_ERR, "%s: client not a TA\n", __func__);
		te_mem_fill(value, 0, sizeof(*value));
		return OTE_ERROR_NOT_IMPLEMENTED;
	}

	return OTE_SUCCESS;
}

te_error_t te_get_device_unique_id(te_device_unique_id *uid)
{
	if (!uid)
		return OTE_ERROR_BAD_PARAMETERS;

	if (ioctl(1, OTE_IOCTL_GET_DEVICE_ID, uid) < 0) {
		te_fprintf(TE_ERR, "%s: failed to get device id\n", __func__);
		return OTE_ERROR_GENERIC;
	}

	return OTE_SUCCESS;
}

te_error_t te_register_ta_event_handler(ta_event_handler_t handler,
		uint32_t events_mask)
{
	te_fprintf(TE_ERR, "%s: registering ta event handler %p "
			"events_mask: 0x%x\n", __func__, handler, events_mask);

	if (ioctl(1, OTE_IOCTL_REGISTER_TA_EVENT, events_mask) < 0) {
		te_fprintf(TE_ERR, "%s: failed to register for ta events\n",
				__func__);
		return OTE_ERROR_GENERIC;
	}

	__event_handler = handler;

	return OTE_SUCCESS;
}

void te_panic(char *msg)
{
	te_panic_args_t args;
	uint32_t len = 0;

	memset(&args, 0, sizeof(args));

	if (msg) {
		if ((len = strlen(msg)) > OTE_PANIC_MSG_MAX_SIZE)
			len = OTE_PANIC_MSG_MAX_SIZE;
		memcpy(args.msg, msg, len);
		args.msg[len] = '\0';
	}

	if (ioctl(1, OTE_IOCTL_TASK_PANIC, &args) < 0) {
		te_fprintf(TE_ERR, "%s: failed to panic\n", __func__);
	}

	// Silence the compiler, the ioctl does not return
	while(1);
}

static char printf_prefix[OTE_TE_FPRINTF_PREFIX_MAX_LENGTH];
static int  printf_prefix_len;

void te_fprintf_set_prefix(const char *prefix)
{
	printf_prefix[0] = '\000';
	printf_prefix_len = 0;

	if (prefix && *prefix) {
		uint32_t i = 0;
	        for (i = 0; i < (sizeof(printf_prefix) - 1); i++) {
			if (prefix[i] < ' ' || prefix[i] > '~') {
				/* stop on first non-printable char */
				printf_prefix[i] = '\000';
				break;
			}

			printf_prefix[i] = prefix[i];
			printf_prefix_len++;
		}
	}

	printf_prefix[sizeof(printf_prefix) - 1] = '\000';
}

#define TE_PRINTF_MAX_SIZE	512
static va_list ap;

int te_fprintf(int fd, char *fmt, ...)
{
	int ret;
	char *str = NULL;

	/* Sanity check */
	if (fd < 0 || fd > TE_SECURE)
		return -1;

	/*
	 * Depending upon the debug level of the builds
	 * the function will call the write syscall.
	 * This will prevent unnecessary calling of the syscall
	 */
	if (fd > DEBUG_LEVEL)
		return 0;

	str = (char *)te_mem_alloc(TE_PRINTF_MAX_SIZE);
	if (!str)
		return -1;

	if (printf_prefix_len > 0)
		memcpy(str, printf_prefix, printf_prefix_len);

	va_start(ap, fmt);
	ret = vsnprintf((str+printf_prefix_len), TE_PRINTF_MAX_SIZE - 1 - printf_prefix_len, fmt, ap);
	va_end(ap);

	/* Error in writing to string */
	if (ret < 0)
		goto exit;

	str[TE_PRINTF_MAX_SIZE - 1] = '\0';
	write(fd, str, strlen(str));

exit:
	te_mem_free(str);
	return ret;
}

void te_oper_dump_param(te_oper_param_t *param)
{
	if (!param)
		return;

	switch (param->type) {
	case TE_PARAM_TYPE_NONE:
		break;
	case TE_PARAM_TYPE_INT_RO:
		te_fprintf(TE_CRITICAL, "oper_param: index: %d, INT RO value: %d(0x%x)\n",
			param->index, param->u.Int.val, param->u.Int.val);
		break;
	case TE_PARAM_TYPE_INT_RW:
		te_fprintf(TE_CRITICAL, "oper_param: index: %d, INT RW value: %d(0x%x)\n",
			param->index, param->u.Int.val, param->u.Int.val);
		break;
	case TE_PARAM_TYPE_MEM_RO:
		te_fprintf(TE_CRITICAL, "oper_param: index: %d, MEM RO base: %p len: %d\n",
			param->index, (void *)(uintptr_t)param->u.Mem.base, param->u.Mem.len);
		break;
	case TE_PARAM_TYPE_MEM_RW:
		te_fprintf(TE_CRITICAL, "oper_param: index: %d, MEM RW base: %p len: %d\n",
			param->index, (void *)(uintptr_t)param->u.Mem.base, param->u.Mem.len);
		break;
	default:
		te_fprintf(TE_CRITICAL, "Unknown param type 0x%x\n", param->type);
		break;
	}
}

void te_oper_dump_param_list(te_operation_t *te_op)
{
	te_oper_param_t *p;

	if (!te_op)
		return;

	p = (te_oper_param_t *)(uintptr_t)te_op->list_head;
	while (p) {
		te_oper_dump_param(p);
		p = (te_oper_param_t *)(uintptr_t)p->next;
	}
}
