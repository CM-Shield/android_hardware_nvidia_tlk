/*
 * Copyright (c) 2013-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <sys/types.h>
#include <sys/ioccom.h>
#include <errno.h>

#include <common/ote_command.h>
#include <common/ote_common.h>
#include <service/ote_service.h>
#include <service/ote_memory.h>
#include <service/ote_task_load.h>

/* Service calls related to task loading and management. These calls
 * are used by secure tasks authorized to communicate with TLK for
 * this purpose, i.e. they have the install privilege set in their
 * manifest. Calls from any other tasks are rejected.
 *
 * TLK communication is implemented by an IOCTL call (OTE_IOCTL_TASK_REQUEST)
 * for all routines below.
 */

te_error_t te_app_request_memory(u_int app_size,
				 uintptr_t *app_addr,
				 uint32_t *app_handle)
{
	te_task_request_args_t args;

	if (app_size <= 0 || !app_addr || !app_handle)
		return OTE_ERROR_BAD_PARAMETERS;

	te_mem_fill(&args, 0, sizeof(args));
	args.ia_opcode = OTE_TASK_OP_MEMORY_REQUEST;

	args.ia_load_memory_request.app_size = app_size;

	if (ioctl(1, OTE_IOCTL_TASK_REQUEST, &args) < 0) {
		if (errno == EACCES)
			return OTE_ERROR_ACCESS_DENIED;
		if (errno == ENOMEM)
			return OTE_ERROR_OUT_OF_MEMORY;
		return OTE_ERROR_GENERIC;
	}

	/* installer virtual address (for loading app) */
	*app_addr   = args.ia_load_memory_request.app_addr;
	*app_handle = args.ia_load_memory_request.app_handle;

	return OTE_SUCCESS;
}

te_error_t te_app_prepare(uint32_t app_handle, te_task_info_t *app_task_info)
{
	te_task_request_args_t args;

	if (!app_handle)
		return OTE_ERROR_BAD_PARAMETERS;

	te_mem_fill(&args, 0, sizeof(args));

	args.ia_opcode = OTE_TASK_OP_PREPARE;
	args.ia_prepare.app_handle = app_handle;

	if (ioctl(1, OTE_IOCTL_TASK_REQUEST, &args) < 0) {
		if (errno == EACCES)
			return OTE_ERROR_ACCESS_DENIED;
		return OTE_ERROR_GENERIC;
	}

	if (app_task_info)
		te_mem_move(app_task_info, &args.ia_prepare.te_task_info,
			    sizeof(te_task_info_t));

	return OTE_SUCCESS;
}

te_error_t te_app_start(uint32_t app_handle,
			install_type_t install_type,
			te_task_restrictions_t *app_restrictions)
{
	te_task_request_args_t args;

	if (!app_handle)
		return OTE_ERROR_BAD_PARAMETERS;

	te_mem_fill(&args, 0, sizeof(args));
	args.ia_start.app_handle = app_handle;
	args.ia_start.app_install_type = install_type;

	args.ia_opcode = OTE_TASK_OP_START;

	if (app_restrictions)
		te_mem_move(&args.ia_start.app_restrictions, app_restrictions,
			    sizeof(te_task_restrictions_t));

	if (ioctl(1, OTE_IOCTL_TASK_REQUEST, &args) < 0) {
		if (errno == EACCES)
			return OTE_ERROR_ACCESS_DENIED;
		return OTE_ERROR_GENERIC;
	}

	return OTE_SUCCESS;
}

te_error_t te_get_pending_task_mapping(uint32_t app_handle,
				       te_memory_mapping_t *app_map)
{
	te_task_request_args_t args;

	if (!app_handle || !app_map)
		return OTE_ERROR_BAD_PARAMETERS;

	te_mem_fill(&args, 0, sizeof(args));
	args.ia_pending_mapping.pm_handle        = app_handle;
	args.ia_pending_mapping.pm_map.map_index = app_map->map_index;

	args.ia_opcode = OTE_TASK_OP_PENDING_MAPPING;

	if (ioctl(1, OTE_IOCTL_TASK_REQUEST, &args) < 0) {
		if (errno == EACCES)
			return OTE_ERROR_ACCESS_DENIED;
		if (errno == ESRCH)
			return OTE_ERROR_ITEM_NOT_FOUND;
		return OTE_ERROR_GENERIC;
	}

	app_map->map_id     = args.ia_pending_mapping.pm_map.map_id;
	app_map->map_offset = args.ia_pending_mapping.pm_map.map_offset;
	app_map->map_size   = args.ia_pending_mapping.pm_map.map_size;

	return OTE_SUCCESS;
}

te_error_t te_list_apps(te_list_apps_t *app_list)
{
	te_task_request_args_t args;
	int rval = 0;

	if (!app_list)
		return OTE_ERROR_BAD_PARAMETERS;

	te_mem_fill(&args, 0, sizeof(args));

	args.ia_opcode = OTE_TASK_OP_LIST;
	args.ia_list.app_index = app_list->al_index;

	app_list->al_type = 0;
	app_list->al_state = 0;
	te_mem_fill(&app_list->al_info, 0, sizeof(args.ia_list.app_info));

	rval = ioctl(1, OTE_IOCTL_TASK_REQUEST, &args);
	if (rval < 0) {
		if (errno == ENOENT)
			return OTE_ERROR_NO_DATA;
		if (errno == ESRCH)
			return OTE_ERROR_ITEM_NOT_FOUND;
		if (errno == EACCES)
			return OTE_ERROR_ACCESS_DENIED;
		return OTE_ERROR_GENERIC;
	}

	/* copy results */
	app_list->al_type = args.ia_list.app_type;
	app_list->al_state = args.ia_list.app_state;

	te_mem_move(&app_list->al_info, &args.ia_list.app_info,
		    sizeof(args.ia_list.app_info));

	return OTE_SUCCESS;
}

te_error_t te_task_get_info(te_get_task_info_t *task_info)
{
	te_task_request_args_t args;
	int rval = 0;

	if (!task_info)
		return OTE_ERROR_BAD_PARAMETERS;

	te_mem_fill(&args, 0, sizeof(args));

	args.ia_opcode = OTE_TASK_OP_GET_TASK_INFO;
	args.ia_get_task_info.gti_request_type = task_info->gti_request_type;

	/* clear response fields */
	task_info->gti_state = 0;
	te_mem_fill(&task_info->gti_info, 0, sizeof(task_info->gti_info));

	switch (task_info->gti_request_type) {
	case OTE_GET_TASK_INFO_REQUEST_INDEX:
		args.ia_get_task_info.gtiu_index = task_info->gtiu_index;
		break;
	case OTE_GET_TASK_INFO_REQUEST_UUID:
		te_mem_move(&args.ia_get_task_info.gtiu_uuid,
			    &task_info->gtiu_uuid,
			    sizeof(te_service_id_t));
		break;
	case OTE_GET_TASK_INFO_REQUEST_SELF:
		break;
	default:
		return OTE_ERROR_BAD_PARAMETERS;
	}

	rval = ioctl(1, OTE_IOCTL_TASK_REQUEST, &args);
	if (rval < 0) {
		if (errno == ENOENT)
			return OTE_ERROR_NO_DATA;
		if (errno == ESRCH)
			return OTE_ERROR_ITEM_NOT_FOUND;
		if (errno == EACCES)
			return OTE_ERROR_ACCESS_DENIED;
		return OTE_ERROR_GENERIC;
	}

	task_info->gti_state = args.ia_get_task_info.gti_state;
	te_mem_move(&task_info->gti_info,
		    &args.ia_get_task_info.gti_info,
		    sizeof(args.ia_get_task_info.gti_info));

	return OTE_SUCCESS;
}

te_error_t te_task_get_mapping(te_get_task_mapping_t *task_mapping)
{
	te_task_request_args_t args;
	int rval = 0;

	if (!task_mapping)
		return OTE_ERROR_BAD_PARAMETERS;

	te_mem_fill(&args, 0, sizeof(args));

	args.ia_opcode = OTE_TASK_OP_GET_MAPPING;
	args.ia_get_task_mapping.gmt_request_type = task_mapping->gmt_request_type;

	switch (task_mapping->gmt_request_type) {
	case OTE_GET_TASK_INFO_REQUEST_INDEX:
		args.ia_get_task_mapping.gmtu_index = task_mapping->gmtu_index;
		break;
	case OTE_GET_TASK_INFO_REQUEST_UUID:
		te_mem_move(&args.ia_get_task_mapping.gmtu_uuid,
			    &task_mapping->gmtu_uuid,
			    sizeof(te_service_id_t));
		break;
	case OTE_GET_TASK_INFO_REQUEST_SELF:
		break;
	default:
		return OTE_ERROR_BAD_PARAMETERS;
	}

	args.ia_get_task_mapping.gmt_map.map_index = task_mapping->gmt_map.map_index;

	/* clear response fields */
	task_mapping->gmt_map.map_id	= 0;
	task_mapping->gmt_map.map_offset= 0;
	task_mapping->gmt_map.map_size	= 0;

	rval = ioctl(1, OTE_IOCTL_TASK_REQUEST, &args);
	if (rval < 0) {
		if (errno == ENOENT)
			return OTE_ERROR_NO_DATA;
		if (errno == ESRCH)
			return OTE_ERROR_ITEM_NOT_FOUND;
		if (errno == EACCES)
			return OTE_ERROR_ACCESS_DENIED;
		return OTE_ERROR_GENERIC;
	}

	task_mapping->gmt_map.map_id	= args.ia_get_task_mapping.gmt_map.map_id;
	task_mapping->gmt_map.map_offset= args.ia_get_task_mapping.gmt_map.map_offset;
	task_mapping->gmt_map.map_size	= args.ia_get_task_mapping.gmt_map.map_size;

	return OTE_SUCCESS;
}

te_error_t te_task_get_name_self(char *name, uint32_t *len_p)
{
	te_error_t err = OTE_SUCCESS;
	te_get_task_info_t self;
	uint32_t name_max_len = 0;

	if (!name || !len_p || !*len_p) {
		err = OTE_ERROR_BAD_PARAMETERS;
		goto exit;
	}

	te_mem_fill(&self, 0, sizeof(self));

	self.gti_request_type = OTE_GET_TASK_INFO_REQUEST_SELF;
	err = te_task_get_info(&self);
	if (err != OTE_SUCCESS) {
		goto exit;
	}

	name_max_len = *len_p;
	*len_p  = 0;
	name[0] = '\000';

	if (self.gti_info.task_name[0]) {
		uint32_t i = 0;

		if (name_max_len > sizeof(self.gti_info.task_name))
			name_max_len = sizeof(self.gti_info.task_name);

		for (i=0; i < name_max_len - 1; i++) {
			if (!self.gti_info.task_name[i])
				break;
			name[i] = self.gti_info.task_name[i];
		}
		*len_p  = i;
		name[i] = '\000';
	}
exit:
	return err;
}

te_error_t te_task_system_info(uint32_t type)
{
	te_task_request_args_t args;
	int rval = 0;

	te_mem_fill(&args, 0, sizeof(args));

	args.ia_opcode = OTE_TASK_OP_SYSTEM_INFO;
	args.ia_system_info.si_type = type;

	rval = ioctl(1, OTE_IOCTL_TASK_REQUEST, &args);
	if (rval < 0) {
		if (errno == EACCES)
			return OTE_ERROR_ACCESS_DENIED;
		return OTE_ERROR_GENERIC;
	}

	return OTE_SUCCESS;
}

te_error_t te_app_unload(te_app_unload_t *arg_unload)
{
	te_task_request_args_t args;

	if (! arg_unload)
		return OTE_ERROR_BAD_PARAMETERS;

#ifdef DEBUG
	te_fprintf(TE_INFO, "TE TASK UNLOAD: request type%d index %d\n",
		   arg_unload->aid_type,
		   arg_unload->aid_index);
#endif

	args.ia_app_unload.tid_type = arg_unload->aid_type;
	switch (arg_unload->aid_type) {
	case OTE_APP_ID_INDEX:
		args.ia_app_unload.tid_index = arg_unload->aid_index;
		break;
	case OTE_APP_ID_UUID:
		te_mem_move(&args.ia_app_unload.tid_uuid, &arg_unload->aid_uuid,
			    sizeof(te_service_id_t));
		break;
	default:
		return OTE_ERROR_BAD_PARAMETERS;
	}

	args.ia_opcode = OTE_TASK_OP_UNLOAD;

	if (ioctl(1, OTE_IOCTL_TASK_REQUEST, &args) < 0) {
		if (errno == ENOENT)
			return OTE_ERROR_NO_DATA;
		if (errno == ESRCH)
			return OTE_ERROR_ITEM_NOT_FOUND;
		if (errno == EACCES)
			return OTE_ERROR_ACCESS_DENIED;
		return OTE_ERROR_GENERIC;
	}

	return OTE_SUCCESS;
}

te_error_t te_app_block(te_app_block_t *barg)
{
	te_task_request_args_t args;

	if (! barg)
		return OTE_ERROR_BAD_PARAMETERS;

#ifdef DEBUG
	te_fprintf(TE_INFO, "TE TASK BLOCK: request type%d index %d\n",
		   barg->aid_type,
		   barg->aid_index);
#endif

	args.ia_app_block.tid_type = barg->aid_type;
	switch (barg->aid_type) {
	case OTE_APP_ID_INDEX:
		args.ia_app_block.tid_index = barg->aid_index;
		break;
	case OTE_APP_ID_UUID:
		te_mem_move(&args.ia_app_block.tid_uuid, &barg->aid_uuid,
			    sizeof(te_service_id_t));
		break;
	default:
		return OTE_ERROR_BAD_PARAMETERS;
	}

	args.ia_opcode = OTE_TASK_OP_BLOCK;

	if (ioctl(1, OTE_IOCTL_TASK_REQUEST, &args) < 0) {
		if (errno == ENOENT)
			return OTE_ERROR_NO_DATA;
		if (errno == ESRCH)
			return OTE_ERROR_ITEM_NOT_FOUND;
		if (errno == EACCES)
			return OTE_ERROR_ACCESS_DENIED;
		return OTE_ERROR_GENERIC;
	}

	return OTE_SUCCESS;
}

te_error_t te_app_unblock(te_app_block_t *barg)
{
	te_task_request_args_t args;

	if (! barg)
		return OTE_ERROR_BAD_PARAMETERS;

#ifdef DEBUG
	te_fprintf(TE_INFO, "TE TASK UNBLOCK: request type%d index %d\n",
		   barg->aid_type,
		   barg->aid_index);
#endif

	args.ia_app_block.tid_type = barg->aid_type;
	switch (barg->aid_type) {
	case OTE_APP_ID_INDEX:
		args.ia_app_block.tid_index = barg->aid_index;
		break;
	case OTE_APP_ID_UUID:
		te_mem_move(&args.ia_app_block.tid_uuid, &barg->aid_uuid,
			    sizeof(te_service_id_t));
		break;
	default:
		return OTE_ERROR_BAD_PARAMETERS;
	}

	args.ia_opcode = OTE_TASK_OP_UNBLOCK;

	if (ioctl(1, OTE_IOCTL_TASK_REQUEST, &args) < 0) {
		if (errno == ENOENT)
			return OTE_ERROR_NO_DATA;
		if (errno == ESRCH)
			return OTE_ERROR_ITEM_NOT_FOUND;
		if (errno == EACCES)
			return OTE_ERROR_ACCESS_DENIED;
		return OTE_ERROR_GENERIC;
	}

	return OTE_SUCCESS;
}
