/*
 * Copyright (c) 2013-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>

#include <service/ote_memory.h>

void *te_mem_alloc(uint32_t size)
{
	void *ptr = malloc(size);

	return ptr;
}

void *te_mem_calloc(uint32_t size)
{
	void *ptr = te_mem_alloc(size);
	if (ptr)
		te_mem_fill(ptr, 0, size);

	return ptr;
}

void *te_mem_realloc(void *buffer, uint32_t size)
{
	void *new_ptr = realloc(buffer, size);

	return new_ptr;
}

void te_mem_free(void *buffer)
{
	if (buffer)
		free(buffer);
}

void te_mem_fill(void *buffer, uint32_t value, uint32_t size)
{
	memset(buffer, value, size);
}

void te_mem_move(void *dest, const void *src, uint32_t size)
{
	memmove(dest, src, size);
}

int te_mem_compare(const void *buffer1, const void *buffer2, uint32_t size)
{
	int err = 0;
	uint32_t i;

	for (i = 0; i < size; i++) {
		if (*((uint8_t *)buffer1 + i) != *((uint8_t *)buffer2 + i))
			err = -1;
	}

	return err;
}
