/*
 * Copyright (c) 2013-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>

#include <common/ote_common.h>
#include <common/ote_command.h>
#include <common/ote_nv_uuid.h>

#include <service/ote_service.h>
#include <service/ote_nvcrypto.h>

#define CRYPTO_SERVICE_GET_KEYBOX	1
#define CRYPTO_SERVICE_GET_STORAGE_KEY	2
#define CRYPTO_SERVICE_GET_ROLLBACK_KEY	3

static te_session_t nvcrypto_session;
static uint32_t nvcrypto_session_open_count;

te_error_t ote_nvcrypto_init(void)
{
	te_service_id_t uuid =  SERVICE_NVCRYPTO_UUID;
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;

	if (nvcrypto_session_open_count > 0) {
		nvcrypto_session_open_count++;
		return OTE_SUCCESS;
	}

	te_init_operation(&operation);

	/* open a NVCRYPTO service session */
	err = te_open_session(&nvcrypto_session, &uuid, &operation);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s failed with err (%d)\n", __func__, err);
		goto exit;
	}

	nvcrypto_session_open_count++;

exit:
	te_operation_reset(&operation);
	return err;
}

te_error_t ote_nvcrypto_deinit(void)
{
	if (nvcrypto_session_open_count <= 0)
		return OTE_ERROR_BAD_STATE;

	if (nvcrypto_session_open_count > 1) {
		nvcrypto_session_open_count--;
		return OTE_SUCCESS;
	}

	nvcrypto_session_open_count--;
	te_close_session(&nvcrypto_session);
	return OTE_SUCCESS;
}

te_error_t ote_nvcrypto_get_keybox(uint32_t keybox_index, void *buf, uint32_t *len)
{
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;

	if (nvcrypto_session_open_count <= 0)
		return OTE_ERROR_BAD_STATE;

	if (!buf)
		return OTE_ERROR_BAD_PARAMETERS;

	te_init_operation(&operation);
	te_oper_set_command(&operation, CRYPTO_SERVICE_GET_KEYBOX);
	te_oper_set_param_mem_rw(&operation, 0, buf, *len);
	te_oper_set_param_int_ro(&operation, 1, keybox_index);

	err = te_launch_operation(&nvcrypto_session, &operation);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s:%d failed with err (%d)\n", __func__, __LINE__, err);
		goto cleanup;
	}

	err = te_oper_get_param_mem_rw(&operation, 0, (void **)&buf, len);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s:%d failed with err (%d)\n", __func__, __LINE__, err);
		goto cleanup;
	}

	te_fprintf(TE_SECURE, "wv keybox size %d\n", *len);

cleanup:
	te_operation_reset(&operation);
	return err;
}

te_error_t ote_nvcrypto_get_storage_key(uint8_t *key, uint32_t key_size)
{
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;

	if (nvcrypto_session_open_count <= 0)
		return OTE_ERROR_BAD_STATE;

	if (!key)
		return OTE_ERROR_BAD_PARAMETERS;

	te_init_operation(&operation);
	te_oper_set_command(&operation, CRYPTO_SERVICE_GET_STORAGE_KEY);
	te_oper_set_param_mem_rw(&operation, 0, key, key_size);

	err = te_launch_operation(&nvcrypto_session, &operation);
	if (err != OTE_SUCCESS)
		te_fprintf(TE_ERR, "%s failed with err (%d)\n", __func__, err);

	te_operation_reset(&operation);

	return err;
}

te_error_t ote_nvcrypto_get_rollback_key(uint8_t *key, uint32_t key_size)
{
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;

	if (nvcrypto_session_open_count <= 0)
		return OTE_ERROR_BAD_STATE;

	if (!key)
		return OTE_ERROR_BAD_PARAMETERS;

	te_init_operation(&operation);
	te_oper_set_command(&operation, CRYPTO_SERVICE_GET_ROLLBACK_KEY);
	te_oper_set_param_mem_rw(&operation, 0, key, key_size);

	err = te_launch_operation(&nvcrypto_session, &operation);
	if (err != OTE_SUCCESS)
		te_fprintf(TE_ERR, "%s failed with err (%d)\n", __func__, err);

	te_operation_reset(&operation);

	return err;
}
