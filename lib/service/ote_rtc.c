/*
 * Copyright (c) 2013-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>

#include <common/ote_common.h>
#include <common/ote_command.h>
#include <common/ote_nv_uuid.h>

#include <service/ote_rtc.h>
#include <service/ote_service.h>

#define RTC_GET_TIME		1

static te_session_t rtc_session;
static int rtc_session_valid;

te_error_t ote_rtc_init(void)
{
	te_service_id_t uuid = SERVICE_RTC_UUID;
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;

	if (rtc_session_valid)
		return OTE_ERROR_BAD_STATE;

	te_init_operation(&operation);

	/* open a rtc service session */
	err = te_open_session(&rtc_session, &uuid, &operation);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s: te_open_session failed with err (%d)\n",
				__func__, err);
		goto exit;
	}
	rtc_session_valid = 1;

exit:
	te_operation_reset(&operation);
	return err;
}

te_error_t ote_rtc_deinit(void)
{
	if (!rtc_session_valid)
		return OTE_ERROR_BAD_STATE;

	te_close_session(&rtc_session);
	rtc_session_valid = 0;
	return OTE_SUCCESS;
}

te_error_t ote_rtc_get_time(uint32_t *rtc)
{
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;

	if (!rtc_session_valid)
		return OTE_ERROR_BAD_STATE;

	te_init_operation(&operation);
	te_oper_set_command(&operation, RTC_GET_TIME);
	te_oper_set_param_int_rw(&operation, 0, 0);
	err = te_launch_operation(&rtc_session, &operation);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s: te_launch_operation failed with err (%d)\n",
				__func__, err);
	}

	err = te_oper_get_param_int(&operation, 0, rtc);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s: te_oper_get_param_int failed with err (%d)\n",
				__func__, err);
	}

	te_operation_reset(&operation);
	return err;
}
