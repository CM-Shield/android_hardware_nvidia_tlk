/*
 * Copyright (c) 2013-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>

#include <openssl/aes.h>
#include <openssl/evp.h>
#include <openssl/sha.h>

#include <common/ote_error.h>
#include <service/ote_attrs.h>
#include <service/ote_crypto.h>
#include <service/ote_openssl.h>
#include "ote_crypto_internal.h"


/* Allocate memory for te_crypto_object.  */
te_error_t te_allocate_object(te_crypto_object_t *obj)
{
	te_crypto_object_t o = malloc(sizeof(*o));
	if (o == NULL) {
		return OTE_ERROR_OUT_OF_MEMORY;
	}
	memset(o, 0, sizeof(*o));
	*obj = o;
	return OTE_SUCCESS;
}

/* Populate obj with attributes from attrs.
 * Array for obj->attrs is allocated.
 * Attributes are copied and internal memory needed is allocated. */
te_error_t te_populate_object(te_crypto_object_t obj, te_attribute_t *attrs,
		uint32_t attr_count)
{
	uint32_t i;
	int ret = OTE_SUCCESS;

	if (obj->attrs != NULL) {
		/* Object previously populated.  */
		return OTE_ERROR_BAD_STATE;
	}

	/* Allocate array for attributes.  */
	obj->attrs = malloc(sizeof(*obj->attrs) * attr_count);
	if (obj->attrs == NULL) {
		return OTE_ERROR_OUT_OF_MEMORY;
	}

	/* Copy attributes. New memory will be allocated in obj attributes.  */
	for (i = 0; i < attr_count; ++i) {
		ret = te_copy_attribute(&obj->attrs[i], &attrs[i]);
		if (ret != OTE_SUCCESS) {
			goto fail_copy;
		}
	}
	obj->attr_count = attr_count;

	return OTE_SUCCESS;

fail_copy:
	/* Free any internal attribute buffers allocated before failure.  */
	for (; i > 0; --i) {
		te_free_internal_attribute(&obj->attrs[i - 1]);
	}

	/* Free array created for attributes.  */
	free(obj->attrs);
	return ret;
}

void te_free_object(te_crypto_object_t obj)
{
	/* Free individual attrs then the array.  */
	uint32_t i;
	for (i = 0; i < obj->attr_count; ++i) {
		te_free_internal_attribute(&obj->attrs[i]);
	}
	free(obj->attrs);
	free(obj);
}

/* Allocate memory for a te_crypto_operation_t and initialize
 * for algo/mode.  */
te_error_t te_allocate_operation(te_crypto_operation_t *oper,
		te_oper_crypto_algo_t algorithm,
		te_oper_crypto_algo_mode_t mode)

{
	te_error_t ret = OTE_SUCCESS;

	te_crypto_operation_t new_op = malloc(sizeof(*new_op));
	if (new_op == NULL) {
		return OTE_ERROR_OUT_OF_MEMORY;
	}
	memset(new_op, 0, sizeof(*new_op));

	new_op->info.algorithm = algorithm;
	new_op->info.mode = mode;

	// Determine which algorithm to set operations too
	switch (algorithm) {
		case OTE_ALG_AES_CTR:
		case OTE_ALG_AES_CTS:
		case OTE_ALG_AES_CBC_NOPAD:
		case OTE_ALG_AES_ECB_NOPAD:
		case OTE_ALG_AES_CBC:
		case OTE_ALG_AES_ECB:
			new_op->init = openssl_aes_init;
			new_op->update = openssl_aes_update;
			new_op->do_final = openssl_aes_do_final;
			new_op->free = openssl_aes_free;
			break;

		case OTE_ALG_AES_CMAC_128:
		case OTE_ALG_AES_CMAC_192:
		case OTE_ALG_AES_CMAC_256:
		case OTE_ALG_SHA_HMAC_224:
		case OTE_ALG_SHA_HMAC_256:
		case OTE_ALG_SHA_HMAC_384:
		case OTE_ALG_SHA_HMAC_512:
		case OTE_ALG_SHA_HMAC_1:
			new_op->init = openssl_mac_init;
			new_op->update = openssl_mac_update;
			new_op->free = openssl_mac_free;
			break;

		case OTE_ALG_RSA_PKCS_OAEP:
		case OTE_ALG_RSA_PSS:
		case OTE_ALG_PKCS1_Block1:
			new_op->init = openssl_rsa_init;
			new_op->handle_req = openssl_rsa_handle_request;
			new_op->free = openssl_rsa_free;
			break;

		default:
			free(new_op);
			ret = OTE_ERROR_NOT_SUPPORTED;
			break;
	}

	*oper = new_op;

	return ret;
}

/* Extract key{s} (multiple keys unsupported at this time) from obj.
 * Allocate buffer for key and store in oper.  */
te_error_t te_set_operation_key(te_crypto_operation_t oper,
		te_crypto_object_t obj)
{
	void *buffer;
	size_t size;
	te_error_t ret;
	te_attribute_t *key;

	if (oper->key) {
		free(oper->key);
		oper->key = NULL;
	}

	/* Get the key attribute from the obj.  */
	ret = te_get_attribute_by_id(obj,
			OTE_ATTR_SECRET_VALUE, &key);
	if (ret != OTE_SUCCESS) {
		return ret;
	}

	/* Get the key size so we can allocate buffer.  */
	ret = te_get_mem_attribute_size(key, &size);
	if (ret != OTE_SUCCESS) {
		return ret;
	}

	buffer = malloc(size);
	if (buffer == NULL) {
		return OTE_ERROR_OUT_OF_MEMORY;
	}

	/* Assume key size has not changed!  */
	te_copy_mem_attribute(buffer, key);

	oper->info.key_size = size;
	oper->key = buffer;

	return OTE_SUCCESS;
}

te_error_t te_cipher_init(te_crypto_operation_t oper, void *iv,
		size_t iv_len)
{
	if (!oper || !oper->init) {
		return OTE_ERROR_BAD_PARAMETERS;
	}
	oper->iv = iv; //TODO: Perhaps should use memcpy
	oper->iv_len = iv_len; //TODO: Perhaps should use memcpy
	return oper->init(oper);
}

te_error_t te_cipher_update(te_crypto_operation_t oper, const void *src_data,
		size_t src_size, void *dst_data, size_t *dst_size)
{
	if (!oper || !oper->update) {
		return OTE_ERROR_BAD_PARAMETERS;
	}
	return oper->update(oper, src_data, src_size, dst_data, dst_size);
}

te_error_t te_cipher_do_final(te_crypto_operation_t oper, const void *src_data,
		size_t src_size, void *dst_data, size_t *dst_size)
{
	if (!oper || !oper->do_final) {
		return OTE_ERROR_BAD_PARAMETERS;
	}
	return oper->do_final(oper, src_data, src_size, dst_data, dst_size);
}

te_error_t te_rsa_init(te_crypto_operation_t oper)
{
	if (!oper || !oper->init) {
		return OTE_ERROR_BAD_PARAMETERS;
	}

	return oper->init(oper);
}

te_error_t te_rsa_handle_request(te_crypto_operation_t oper, const void *src_data,
		size_t src_size, void *dst_data, size_t *dst_size)
{
	if (!oper || !oper->handle_req) {
		return OTE_ERROR_BAD_PARAMETERS;
	}
	return oper->handle_req(oper, src_data, src_size, dst_data, dst_size);
}

void te_free_operation(te_crypto_operation_t oper)
{
	if (oper == NULL) {
		return;
	}

	/* cleanup any cipher resources */
	if (oper->free) {
		oper->free(oper);
	}

	if (oper->key) {
		free(oper->key);
		oper->key = NULL;
	}

	/*
	 * The oper->iv was linked to the operation in te_cipher_init
	 * and is assumed to be freed by the caller (as it could be used
	 * else where). But, we will NULL out the field in the struct.
	 */
	oper->iv = NULL;

	if (oper->imp_obj) {
		free(oper->imp_obj);
		oper->imp_obj = NULL;
	}

	free(oper);
}

void te_generate_random(void *buffer, size_t size)
{
	(void)openssl_rand_bytes(buffer, size);
}

// TODO: Decide if this should be static to this file.
/* Search through attributes of an object, returns the first attr
 * with matching id.  */
te_error_t te_get_attribute_by_id(te_crypto_object_t object,
		te_attribute_id_t id, te_attribute_t **ret)
{
	uint32_t i;
	if (object == NULL || ret == NULL) {
		return OTE_ERROR_BAD_PARAMETERS;
	}
	for (i = 0; i < object->attr_count; ++i) {
		if (object->attrs[i].id == id) {
			*ret = &object->attrs[i];
			return OTE_SUCCESS;
		}
	}
	return OTE_ERROR_ITEM_NOT_FOUND;
}
