/*
 * Copyright (c) 2012-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <unistd.h>
#include <malloc.h>
#include <sys/ioccom.h>

#include <openssl/aes.h>
#include <openssl/evp.h>
#include <openssl/cmac.h>
#include <openssl/rand.h>
#include <openssl/rsa.h>
#include <openssl/bn.h>
#include <openssl/err.h>
#include <openssl/sha.h>
#include <openssl/hmac.h>
#include <openssl/x509.h>

#include <common/ote_error.h>

#include <service/ote_memory.h>
#include <service/ote_crypto.h>
#include <service/ote_service.h>
#include <service/ote_openssl.h>

#define DEBUG_OPENSSL_SPEW	0

static uint reseed_rng;
extern uint32_t platform_get_rand32(void);

typedef struct {
	te_device_unique_id dev_id;
	pid_t tid[2];
	uint32_t time_us[4];
} rng_seed_t;

te_error_t openssl_rand_bytes(void *buffer, size_t size)
{
	rng_seed_t seed;
	te_error_t error = OTE_SUCCESS;
	uint32_t rnd_32;
	int i;

	if (!buffer || !size) {
		error = OTE_ERROR_BAD_PARAMETERS;
		goto fail;
	}

	/* generate seed for the RNG */
	seed.tid[0] = gettid();
	seed.tid[1] = gettid();

	error = te_get_device_unique_id(&seed.dev_id);
	if (error != OTE_SUCCESS)
		goto fail;

	for (i = 0; i < 4; i++) {
		if (ioctl(1, OTE_IOCTL_GET_TIME_US, &seed.time_us[i]) < 0) {
			error = OTE_ERROR_GENERIC;
			goto fail;
		}

		if (ioctl(1, OTE_IOCTL_GET_RAND32, &rnd_32) < 0) {
			error = OTE_ERROR_GENERIC;
			goto fail;
		}

		seed.time_us[i] ^= rnd_32;
	}

	if (reseed_rng)
		RAND_add((const void *)&seed, sizeof(seed), 0.50f * sizeof(seed));
	else
		RAND_seed((const void *)&seed, sizeof(seed));

	reseed_rng = 1;

	/* get random bytes */
	if (!RAND_bytes(buffer, size))
		error = OTE_ERROR_GENERIC;

fail:

	if (error != OTE_SUCCESS)
#if DEBUG_OPENSSL_SPEW
		te_fprintf(TE_ERR, "%s: failed to get random bytes (%d)\n", __func__,
			ERR_GET_REASON(ERR_get_error()));
#else
		te_fprintf(TE_ERR, "failed to get random bytes\n");
#endif

	return error;
}

te_error_t openssl_aes_init(te_crypto_operation_t operation)
{
	te_error_t err = OTE_SUCCESS;
	EVP_CIPHER *evp_cipher;
	EVP_CIPHER_CTX *ctx;

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_INFO, "%s: enter\n", __func__);
#endif

	ctx = calloc(1, sizeof(*ctx));
	if (ctx == NULL) {
		te_fprintf(TE_ERR, "%s: unable to allocate cipher ctx\n", __func__);
		return OTE_ERROR_OUT_OF_MEMORY;
	}

	//TODO: Use operation->info.keySize to determine key size
	switch (operation->info.algorithm) {
	case OTE_ALG_AES_ECB:
	case OTE_ALG_AES_ECB_NOPAD:
		evp_cipher = (EVP_CIPHER*)EVP_aes_128_ecb();
		break;
	case OTE_ALG_AES_CTS:
	case OTE_ALG_AES_CBC:
	case OTE_ALG_AES_CBC_NOPAD:
		evp_cipher = (EVP_CIPHER*)EVP_aes_128_cbc();
		break;
	case OTE_ALG_AES_CTR:
		evp_cipher = (EVP_CIPHER*)EVP_aes_128_ctr();
		break;
	default:
		te_fprintf(TE_ERR, "%s: unable to determine cipher algorithm\n",
				__func__);
		err = OTE_ERROR_BAD_PARAMETERS;
		goto err;
	}

#if DEBUG_OPENSSL_SPEW
	int i;
	te_fprintf(TE_SECURE, "%s, setting key:", __func__);
	for (i = 0; i < operation->info.key_size; ++i) {
		te_fprintf(TE_SECURE, " %02x", *((uint8_t*)operation->key+i));
	}
	te_fprintf(TE_SECURE, "\n");
	te_fprintf(TE_SECURE, "%s, setting iv:", __func__);
	for (i = 0; i < operation->iv_len; ++i) {
		te_fprintf(TE_SECURE, " %02x", *((uint8_t*)operation->iv+i));
	}
	te_fprintf(TE_SECURE, "\n");
#endif

	int init_success = 0;
	switch (operation->info.mode) {
	case OTE_ALG_MODE_ENCRYPT:
		init_success = EVP_EncryptInit_ex(ctx, evp_cipher,
				NULL, operation->key, operation->iv);
		break;
	case OTE_ALG_MODE_DECRYPT:
		init_success = EVP_DecryptInit_ex(ctx, evp_cipher,
				NULL, operation->key, operation->iv);
		break;
	default:
		te_fprintf(TE_ERR, "%s: unable to determine cipher mode\n",
				__func__);
		err = OTE_ERROR_BAD_PARAMETERS;
		goto err;
	}

	if (!init_success) {
		te_fprintf(TE_ERR, "%s: EVP Init for mode %s failed\n", __func__,
				(operation->info.mode == OTE_ALG_MODE_ENCRYPT) ?
				"encrypt" : "decrypt");
		err = OTE_ERROR_GENERIC;
		goto err;
	}

	switch (operation->info.algorithm) {
	case OTE_ALG_AES_ECB_NOPAD:
	case OTE_ALG_AES_CBC_NOPAD:
	case OTE_ALG_AES_CTS:
		EVP_CIPHER_CTX_set_padding(ctx, 0);
		break;
	}

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_INFO, "%s: exit\n", __func__);
#endif
	operation->imp_obj = (void *)ctx;
	return err;

err:
	EVP_CIPHER_CTX_cleanup(ctx);
	free(ctx);
	return err;
}

static size_t aes_cts_encrypt(te_crypto_operation_t operation,
		const unsigned char *in, unsigned char *out, size_t len)
{
	size_t dst_len = 0, nblocks, src_len;
	uint8_t temp[AES_BLOCK_SIZE];
	uint8_t *src, *dst, *end;
	te_error_t error;

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_INFO, "%s: enter\n", __func__);
#endif

	EVP_CIPHER_CTX *ctx = (EVP_CIPHER_CTX*)operation->imp_obj;

	if (!in || !out)
		return 0;

	nblocks = (len / AES_BLOCK_SIZE) + 1;
	src_len = AES_BLOCK_SIZE * nblocks;
	src = malloc(src_len);
	dst = malloc(src_len);
	if (!src || !dst) {
		len = 0;
		goto exit;
	}

	/* set the source buffer to zeros */
	memset(src, 0, src_len);

	/* pad the last partial plaintext block using zeros */
	te_mem_move(src, in, len);

	if (len < AES_BLOCK_SIZE) {
		/*
		 * With more than one block, bits from from Cn-1 are used as
		 * the rightmost padding of Pn on encryption, and Cn-1 / Cn
		 * text blocks are swapped on output.
		 *
		 * On lengths < AES_BLOCK_SIZE, the iv is used as the Cn-1
		 * ciphertext. It is swapped, by returning it as the truncated
		 * output C0 and the encryption with P0 is returned as the
		 * iv for decryption.
		 *
		 * Note: this assumes coordination with the caller in knowing
		 * it's a short buffer and to use the written back iv in
		 * operation->iv for decryption.
		 */
		error = openssl_rand_bytes(temp, EVP_CIPHER_CTX_iv_length(ctx));
		if (error != OTE_SUCCESS) {
			len = 0;
			goto exit;
		}

		/* create a Cn-1 block from random data */
		memset(ctx->iv, 0, EVP_CIPHER_CTX_iv_length(ctx));
		EVP_EncryptUpdate(ctx, dst, (int *)&dst_len, temp, AES_BLOCK_SIZE);

		if (dst_len != AES_BLOCK_SIZE) {
			len = 0;
			goto exit;
		}

		/* which is input to Cn CBC encryption from P0 input below */
		memcpy(ctx->iv, dst, EVP_CIPHER_CTX_iv_length(ctx));

		/* ... and is "swapped" by becoming our truncated C0 output */
		te_mem_move(out, dst, len);
	}

	/* encrypt the whole padded plaintext using standard non-padding CBC mode */
	dst_len = 0;
	EVP_EncryptUpdate(ctx, dst, (int *)&dst_len, src, src_len);

	if (dst_len != src_len) {
		len = 0;
		goto exit;
	}

	if (len < AES_BLOCK_SIZE) {
		/* complete ciphertext swap, by returning dst as the iv */
		te_mem_move(operation->iv, dst, EVP_CIPHER_CTX_iv_length(ctx));
	} else {
		/* swap the last 2 ciphertext blocks */
		end = dst + src_len;
		te_mem_move(temp, end - AES_BLOCK_SIZE, AES_BLOCK_SIZE);
		te_mem_move(end - AES_BLOCK_SIZE, end - (AES_BLOCK_SIZE * 2), AES_BLOCK_SIZE);
		te_mem_move(end - (AES_BLOCK_SIZE * 2), temp, AES_BLOCK_SIZE);

		/* truncate ciphertext to length of original plaintext */
		te_mem_move(out, dst, len);
	}

exit:
	te_mem_free(src);
	te_mem_free(dst);

	return len;
}

static size_t aes_cts_decrypt(te_crypto_operation_t operation,
		const unsigned char *in, unsigned char *out, size_t len)
{
	size_t residue, dst_len, src_len;
	uint8_t d[AES_BLOCK_SIZE];
	uint8_t *src, *end, *dst;
	int i;
	EVP_CIPHER_CTX *ctx = (EVP_CIPHER_CTX*)operation->imp_obj;

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_INFO, "%s: enter\n", __func__);
#endif

	if (!in || !out)
		return 0;

	residue = len % AES_BLOCK_SIZE;
	dst_len = AES_BLOCK_SIZE;

	/* decrypt the second to last ciphertext block */
	if (len < AES_BLOCK_SIZE) {
		/*
		 * For short buffers, the input iv represents the Cn-1
		 * text which is decrypted to get the residue bits.
		 */
		memset(ctx->iv, 0, EVP_CIPHER_CTX_iv_length(ctx));

		/* use iv as second to last ciphertext block */
		EVP_DecryptUpdate(ctx, d, (int *)&dst_len, operation->iv, AES_BLOCK_SIZE);
		src_len = AES_BLOCK_SIZE;
	} else {
		uint8_t temp[AES_BLOCK_SIZE];
		size_t last_full_blk = len - (AES_BLOCK_SIZE + residue);

		/* zero IV must be used to decipher this CBC block */
		memcpy(temp, ctx->iv, AES_BLOCK_SIZE);
		memset(ctx->iv, 0, AES_BLOCK_SIZE);
		EVP_DecryptUpdate(ctx, d, (int *)&dst_len, in + last_full_blk, AES_BLOCK_SIZE);
		memcpy(ctx->iv, temp, AES_BLOCK_SIZE);
		src_len = len - residue + AES_BLOCK_SIZE;
	}

	src = te_mem_alloc(src_len);
	dst = te_mem_alloc(src_len);
	if (!src || !dst) {
		len = 0;
		goto exit;
	}

	/* pad ciphertext using the last B-M bytes from previous output */
	te_mem_move(src, in, len);
	te_mem_move(src + len, d + residue, AES_BLOCK_SIZE - residue);

	if (len < AES_BLOCK_SIZE) {
		uint8_t x1[AES_BLOCK_SIZE];

		/* XOR with input Cn src, to complete decryption */
		for (i = 0; i < AES_BLOCK_SIZE; i++) {
			x1[i] = d[i] ^ src[i];
		}

		/*
		 * Note: at this point we have the Pn (P0) plaintext and don't
		 * need to finish the decryption to produce the Pn-1 text, which
		 * would be the random data used in encryption, so we're done.
		 */
		te_mem_move(out, x1, len);
	} else {
		/* pad the last block with zeros */
		te_mem_fill(d + residue, 0, AES_BLOCK_SIZE - residue);

		/* swap the last 2 ciphertext blocks */
		end = src + src_len;
		te_mem_move(d, end - AES_BLOCK_SIZE, AES_BLOCK_SIZE);
		te_mem_move(end - AES_BLOCK_SIZE, end - (AES_BLOCK_SIZE * 2), AES_BLOCK_SIZE);
		te_mem_move(end - (AES_BLOCK_SIZE * 2), d, AES_BLOCK_SIZE);

		/* restore iv */
		memcpy(ctx->iv, operation->iv, EVP_CIPHER_CTX_iv_length(ctx));

		/* decrypt this "modified" buffer */
		dst_len = src_len;
		EVP_DecryptUpdate(ctx, dst, (int *)&dst_len, src, src_len);

		/* truncate the plaintext to the length of the original */
		te_mem_move(out, dst, len);
	}

exit:
	te_mem_free(src);
	te_mem_free(dst);

	return len;
}

te_error_t openssl_aes_update(te_crypto_operation_t operation, const void *src,
		size_t src_len, void *dst, size_t *dst_len)
{
	void *oper_data;

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_INFO, "%s: enter\n", __func__);
#endif

	EVP_CIPHER_CTX *ctx = (EVP_CIPHER_CTX*)operation->imp_obj;
	if (!ctx) {
		te_fprintf(TE_ERR, "%s: invalid key\n", __func__);
		return OTE_ERROR_GENERIC;
	}

	if (src_len > *dst_len) {
		/* use temporary (large enough) dest buffer */
		oper_data = te_mem_alloc(src_len);
		if (!oper_data) {
			return OTE_ERROR_SHORT_BUFFER;
		}
		te_fprintf(TE_SECURE, "%s: dest buffer too small src:%zu dest:%zu"
				"(using temp buffer)\n",
				__func__, src_len, *dst_len);
	} else {
		oper_data = dst;
	}

#if DEBUG_OPENSSL_SPEW
	size_t i;
	te_fprintf(TE_SECURE, "enc data : %zu %zu\n", src_len, src_len % AES_BLOCK_SIZE);
	for (i = 0; i < src_len; i++) {
		te_fprintf(TE_SECURE, " %02x", *((uint8_t*)(src)+i));
	}
	te_fprintf(TE_SECURE, "\n");
#endif

	/*
	 * We use AES-CBC if the source length is a multiple of AES_BLOCK_SIZE.
	 * At least WV expects it this way.
	 */
	if ((operation->info.algorithm == OTE_ALG_AES_CTS) &&
			(src_len % AES_BLOCK_SIZE == 0))
		operation->info.algorithm = OTE_ALG_AES_CBC;

	/* Handle CTS algorithm */
	if (operation->info.algorithm == OTE_ALG_AES_CTS) {
		switch (operation->info.mode) {
		case OTE_ALG_MODE_ENCRYPT:
			*dst_len = aes_cts_encrypt(operation, src, oper_data,
					src_len);
			break;
		case OTE_ALG_MODE_DECRYPT:
			*dst_len = aes_cts_decrypt(operation, src, oper_data,
					src_len);
			break;
		default:
			te_fprintf(TE_ERR, "%s: unable to determine cipher mode\n",
					__func__);
			goto err;
		}
	} else {
		switch(operation->info.mode) {
		case OTE_ALG_MODE_ENCRYPT:
			EVP_EncryptUpdate(ctx, oper_data, (int *)dst_len, src,
					(int)src_len);
			break;
		case OTE_ALG_MODE_DECRYPT:
			EVP_DecryptUpdate(ctx, oper_data, (int *)dst_len, src,
					(int)src_len);
			break;
		default:
			te_fprintf(TE_ERR, "%s: unable to determine cipher mode\n",
					__func__);
			goto err;
		}
	}

	if (oper_data != dst) {
		memcpy(dst, oper_data, *dst_len);
		te_mem_free(oper_data);
	}

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_SECURE, "dec data : %zu\n", *dst_len);
	for (i = 0; i < *dst_len; i++) {
		te_fprintf(TE_SECURE, " %02x", *((uint8_t*)dst+i));
	}
	te_fprintf(TE_SECURE, "\n");

	te_fprintf(TE_INFO, "%s: exit\n", __func__);
#endif

	return OTE_SUCCESS;

err:

	if (oper_data != dst)
		te_mem_free(oper_data);

	return OTE_ERROR_GENERIC;
}

//DOFINAL is needed when:
// - doing encryption with padding, extra dest data is produced
te_error_t openssl_aes_do_final(te_crypto_operation_t  operation, const void *src,
		size_t src_len, void *dst, size_t *dst_len)
{
	size_t i;

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_INFO, "%s: enter\n", __func__);
#endif

	AES_KEY *ctx = (AES_KEY*)operation->imp_obj;
	if (!ctx) {
		te_fprintf(TE_ERR, "%s: invalid key\n", __func__);
		return OTE_ERROR_GENERIC;
	}
	if (src_len > *dst_len) {
		te_fprintf(TE_ERR, "%s: dest buffer too small src:%zu dest:%zu\n", __func__,
				src_len, *dst_len);
		return OTE_ERROR_SHORT_BUFFER;
	}

	te_fprintf(TE_SECURE, "enc data: %zu\n", src_len);
	for (i = 0; i < src_len; i++) {
		te_fprintf(TE_SECURE, " %02x", *((uint8_t*)src+i));
	}
	te_fprintf(TE_SECURE, "\n");

	EVP_DecryptFinal_ex((EVP_CIPHER_CTX*)operation->imp_obj, dst,
			(int *)dst_len);

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_SECURE, "dec data: %zu\n", *dst_len);
	for (i = 0; i < *dst_len; i++) {
		te_fprintf(TE_SECURE, " %02x", *((uint8_t*)dst+i));
	}
	te_fprintf(TE_SECURE, "\n");

	te_fprintf(TE_INFO, "%s: exit\n", __func__);
#endif

	return OTE_SUCCESS;
}

void openssl_aes_free(te_crypto_operation_t operation)
{
#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_INFO, "%s: enter\n", __func__);
#endif

	if (operation->imp_obj)
		EVP_CIPHER_CTX_cleanup((EVP_CIPHER_CTX*)operation->imp_obj);

	CRYPTO_cleanup_all_ex_data();
	ERR_free_strings();
	ERR_remove_thread_state(NULL);
	EVP_cleanup();

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_INFO, "%s: exit\n", __func__);
#endif
}

te_error_t openssl_mac_init(te_crypto_operation_t operation)
{
	CMAC_CTX *cmac_ctx;
	HMAC_CTX *hmac_ctx;
	int ret = 0;
	const EVP_CIPHER *cipher = NULL;
	const EVP_MD *md = NULL;

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_INFO, "%s: enter\n", __func__);
#endif

	if (!operation)
		return OTE_ERROR_BAD_PARAMETERS;

	switch (operation->info.algorithm) {
	case OTE_ALG_AES_CMAC_128:
		cipher = EVP_aes_128_cbc();
		break;
	case OTE_ALG_AES_CMAC_192:
		cipher = EVP_aes_192_cbc();
		break;
	case OTE_ALG_AES_CMAC_256:
		cipher = EVP_aes_256_cbc();
		break;
	case OTE_ALG_SHA_HMAC_224:
		md = EVP_sha224();
		break;
	case OTE_ALG_SHA_HMAC_256:
		md = EVP_sha256();
		break;
	case OTE_ALG_SHA_HMAC_384:
		md = EVP_sha384();
		break;
	case OTE_ALG_SHA_HMAC_512:
		md = EVP_sha512();
		break;
	case OTE_ALG_SHA_HMAC_1:
		md = EVP_sha1();
		break;
	default:
		te_fprintf(TE_ERR, "%s: unknown algorithm(%d)\n", __func__,
			operation->info.algorithm);
		return OTE_ERROR_BAD_PARAMETERS;
	}

	switch (operation->info.algorithm) {
	case OTE_ALG_AES_CMAC_128:
	case OTE_ALG_AES_CMAC_192:
	case OTE_ALG_AES_CMAC_256:
		cmac_ctx = CMAC_CTX_new();
		if (!cmac_ctx) {
			te_fprintf(TE_ERR, "%s: CMAC_CTX_new fail\n", __func__);
			return OTE_ERROR_OUT_OF_MEMORY;
		}

		ret = CMAC_Init(cmac_ctx, operation->key,
				operation->info.key_size, cipher, NULL);
		if (ret == 0) {
			te_fprintf(TE_ERR, "%s: CMAC_Init fail (%d)\n", __func__, ret);
			CMAC_CTX_free(cmac_ctx);
			return OTE_ERROR_NO_DATA;
		}

		operation->imp_obj = (void *)cmac_ctx;
		break;

	case OTE_ALG_SHA_HMAC_224:
	case OTE_ALG_SHA_HMAC_256:
	case OTE_ALG_SHA_HMAC_384:
	case OTE_ALG_SHA_HMAC_512:
	case OTE_ALG_SHA_HMAC_1:
		hmac_ctx = te_mem_calloc(sizeof(HMAC_CTX));
		if (!hmac_ctx) {
			te_fprintf(TE_ERR, "%s: mem alloc fail\n", __func__);
			return OTE_ERROR_OUT_OF_MEMORY;
		}

		HMAC_CTX_init(hmac_ctx);

		ret = HMAC_Init_ex(hmac_ctx, operation->key,
				   operation->info.key_size, md, NULL);
		if (ret == 0) {
			HMAC_CTX_cleanup(hmac_ctx);
			te_mem_free(hmac_ctx);
			return OTE_ERROR_NO_DATA;
		}

		operation->imp_obj = (void *)hmac_ctx;
		break;

	default:
		return OTE_ERROR_BAD_PARAMETERS;
	}

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_INFO, "%s: exit\n", __func__);
#endif

	return OTE_SUCCESS;
}

te_error_t openssl_mac_update(te_crypto_operation_t operation, const void *src,
		size_t src_size, void *dst, size_t *dst_size)
{
	int ret = 0;

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_INFO, "%s: enter\n", __func__);
#endif

	if (!operation || !operation->imp_obj)
		return OTE_ERROR_BAD_PARAMETERS;

	*dst_size = 0;

	switch (operation->info.algorithm) {
	case OTE_ALG_AES_CMAC_128:
	case OTE_ALG_AES_CMAC_192:
	case OTE_ALG_AES_CMAC_256:
		ret = CMAC_Update((CMAC_CTX *)operation->imp_obj, src, src_size);
		if (ret == 0) {
			te_fprintf(TE_ERR, "%s: CMAC_Update fail (%d)\n", __func__, ret);
			return OTE_ERROR_NO_DATA;
		}

		ret = CMAC_Final((CMAC_CTX *)operation->imp_obj, dst, dst_size);
		if (ret == 0 || *dst_size != 16) {
			te_fprintf(TE_ERR, "%s: CMAC_Final fail (%d)\n", __func__, ret);
			return OTE_ERROR_NO_DATA;
		}
		break;

	case OTE_ALG_SHA_HMAC_224:
	case OTE_ALG_SHA_HMAC_256:
	case OTE_ALG_SHA_HMAC_384:
	case OTE_ALG_SHA_HMAC_512:
	case OTE_ALG_SHA_HMAC_1:
		ret = HMAC_Update((HMAC_CTX *)operation->imp_obj, src, src_size);
		if (ret == 0) {
			te_fprintf(TE_ERR, "%s: HMAC_Update fail (%d)\n", __func__, ret);
			return OTE_ERROR_NO_DATA;
		}

		ret = HMAC_Final((HMAC_CTX *)operation->imp_obj, dst, (uint32_t *)dst_size);
		if (ret == 0) {
			te_fprintf(TE_ERR, "%s: HMAC_Final fail (%d)\n", __func__, ret);
			return OTE_ERROR_NO_DATA;
		}
		break;

	default:
		te_fprintf(TE_ERR, "%s: unknown algorithm(%d)\n", __func__,
			operation->info.algorithm);
		return OTE_ERROR_BAD_PARAMETERS;
	}

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_INFO, "%s: exit\n", __func__);
#endif

	return OTE_SUCCESS;
}

void openssl_mac_free(te_crypto_operation_t operation)
{
#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_INFO, "%s: enter\n", __func__);
#endif

	if (operation && operation->imp_obj) {
		switch (operation->info.algorithm) {
		case OTE_ALG_AES_CMAC_128:
		case OTE_ALG_AES_CMAC_192:
		case OTE_ALG_AES_CMAC_256:
			CMAC_CTX_free((CMAC_CTX *)operation->imp_obj);
			break;
		case OTE_ALG_SHA_HMAC_224:
		case OTE_ALG_SHA_HMAC_256:
		case OTE_ALG_SHA_HMAC_384:
		case OTE_ALG_SHA_HMAC_512:
		case OTE_ALG_SHA_HMAC_1:
			HMAC_CTX_cleanup((HMAC_CTX *)operation->imp_obj);
			te_mem_free(operation->imp_obj);
			break;

		default:
			te_fprintf(TE_ERR, "%s: unknown algorithm(%d)\n", __func__,
				operation->info.algorithm);
			return;
		}

		operation->imp_obj = NULL;
	}

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_INFO, "%s: exit\n", __func__);
#endif
}

te_error_t openssl_rsa_init(te_crypto_operation_t operation)
{
	RSA *rsa = NULL;

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_INFO, "%s: enter\n", __func__);
#endif

	if (!operation)
		return OTE_ERROR_BAD_PARAMETERS;

	if (operation->info.algorithm != OTE_ALG_RSA_PKCS_OAEP &&
		operation->info.algorithm != OTE_ALG_RSA_PSS &&
		operation->info.algorithm != OTE_ALG_PKCS1_Block1)
		return OTE_ERROR_NOT_SUPPORTED;

#if DEBUG_OPENSSL_SPEW
	uint i;
	te_fprintf(TE_SECURE, "set key: %u\n", operation->info.key_size);
	for (i = 0; i < operation->info.key_size; i++) {
		te_fprintf(TE_SECURE, " %02x", *((uint8_t*)operation->key+i));
	}
	te_fprintf(TE_SECURE, "\n");
#endif

	uint8_t *p = operation->key;

	if (operation->info.mode == OTE_ALG_MODE_ENCRYPT ||
	    operation->info.mode == OTE_ALG_MODE_VERIFY) {
		rsa = d2i_RSAPublicKey(NULL, (const unsigned char **)&p,
				operation->info.key_size);
	} else if (operation->info.mode == OTE_ALG_MODE_DECRYPT ||
			operation->info.mode == OTE_ALG_MODE_SIGN) {
		BIO *bio = BIO_new_mem_buf(p, operation->info.key_size);
		if( bio == NULL ) {
			te_fprintf(TE_ERR, "[LoadRSAKey(): Could not allocate bio buffer]");
			return OTE_ERROR_GENERIC;
		}

		int success = 1;
		PKCS8_PRIV_KEY_INFO *pkcs8_pki = (PKCS8_PRIV_KEY_INFO *)d2i_PKCS8_PRIV_KEY_INFO_bio(bio, NULL);
		if (pkcs8_pki == NULL) {
			te_fprintf(TE_INFO, "d2i_PKCS8_PRIV_KEY_INFO_bio returned NULL.");
			success = 0;
		}

		EVP_PKEY *evp = NULL;
		if (success) {
			evp = (EVP_PKEY *)EVP_PKCS82PKEY(pkcs8_pki);
			if (evp == NULL) {
				te_fprintf(TE_INFO, "EVP_PKCS82PKEY returned NULL.");
				success = 0;
			}
		}

		if (success) {
			rsa = EVP_PKEY_get1_RSA(evp);
			if (rsa == NULL) {
				te_fprintf(TE_INFO, "PrivateKeyInfo did not contain an RSA key.");
				success = 0;
			}
		}

		if (evp != NULL) {
			EVP_PKEY_free(evp);
		}

		if (pkcs8_pki != NULL) {
			PKCS8_PRIV_KEY_INFO_free(pkcs8_pki);
		}

		BIO_free(bio);
	}

	if (!rsa) {
		te_fprintf(TE_ERR, "%s: RSA_new fail\n", __func__);
		return OTE_ERROR_OUT_OF_MEMORY;
	}

	operation->imp_obj = (void *)rsa;

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_INFO, "%s: exit\n", __func__);
#endif

	return OTE_SUCCESS;
}

te_error_t openssl_rsa_handle_request(te_crypto_operation_t operation, const void *src,
		size_t src_size, void *dst, size_t *dst_size)
{
	int num;
	uint i;
	int ret = 0;
	RSA *rsa;
	int saltlen = 20;
	uint8_t *pad_out = NULL;

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_INFO, "%s: enter\n", __func__);
#endif

	if (!operation || !operation->imp_obj || !src || !dst || !dst_size)
		return OTE_ERROR_BAD_PARAMETERS;

	if (operation->info.algorithm != OTE_ALG_RSA_PKCS_OAEP &&
		operation->info.algorithm != OTE_ALG_RSA_PSS &&
		operation->info.algorithm != OTE_ALG_PKCS1_Block1)
		return OTE_ERROR_NOT_SUPPORTED;

	*dst_size = 0;
	rsa = (RSA *)operation->imp_obj;

	if (operation->info.algorithm == OTE_ALG_RSA_PKCS_OAEP)
		rsa->flags |= RSA_FLAG_NO_CONSTTIME;

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_SECURE, "src data: %d\n", src_size);
	for (i = 0; i < src_size; i++) {
		te_fprintf(TE_SECURE, " %02x", *((uint8_t*)src+i));
	}
	te_fprintf(TE_SECURE, "\n");
#endif

	switch (operation->info.mode) {
	case OTE_ALG_MODE_ENCRYPT:
		ERR_clear_error();
		num = RSA_public_encrypt(src_size, (const unsigned char *)src,
			(unsigned char *)dst, rsa, RSA_PKCS1_OAEP_PADDING);
		if (num < 0) {
			te_fprintf(TE_ERR, "%s: failed (%d, %d)\n", __func__,
				num, ERR_GET_REASON(ERR_get_error()));
			return OTE_ERROR_NOT_SUPPORTED;
		}
		*dst_size = num;
		break;

	case OTE_ALG_MODE_DECRYPT:
		ERR_clear_error();
		num = RSA_private_decrypt(RSA_size(rsa), (const unsigned char *)src,
			(unsigned char *)dst, rsa, RSA_PKCS1_OAEP_PADDING);
		if (num < 0) {
			te_fprintf(TE_ERR, "%s: failed (%d, %d)\n", __func__,
				num, ERR_GET_REASON(ERR_get_error()));
			return OTE_ERROR_NO_DATA;
		}
		*dst_size = num;
		break;

	case OTE_ALG_MODE_SIGN:
		if(operation->info.algorithm == OTE_ALG_PKCS1_Block1) {
			/* Pad the message with PKCS1 padding, and then encrypt.*/
			ERR_clear_error();
			num = RSA_private_encrypt(src_size, src, dst, rsa,
					RSA_PKCS1_PADDING);
			if (num < 0) {
				te_fprintf(TE_ERR, "%s: enc failed (%d, %d)\n", __func__,
					num, ERR_GET_REASON(ERR_get_error()));
				return OTE_ERROR_NO_DATA;
			}
			*dst_size = num;
		} else {
			/* Add PSS padding */
			pad_out = te_mem_calloc(RSA_size(rsa));
			if (!pad_out)
				return OTE_ERROR_OUT_OF_MEMORY;

			ERR_clear_error();
			ret = RSA_padding_add_PKCS1_PSS(rsa, pad_out, src, EVP_sha1(),
					saltlen);
			if (ret != 1) {
				te_fprintf(TE_ERR, "%s: pad failed (%d, %d)\n", __func__,
					ret, ERR_GET_REASON(ERR_get_error()));
				te_mem_free(pad_out);
				return OTE_ERROR_BAD_FORMAT;
			}

			/* Encrypt PSS padded digest */
			ERR_clear_error();
			num = RSA_private_encrypt(RSA_size(rsa), pad_out, dst, rsa,
					RSA_NO_PADDING);
			if (num < 0) {
				te_fprintf(TE_ERR, "%s: enc failed (%d, %d)\n", __func__,
					num, ERR_GET_REASON(ERR_get_error()));
				te_mem_free(pad_out);

				return OTE_ERROR_NO_DATA;
			}
			*dst_size = num;
			te_mem_free(pad_out);
		}
		break;

	case OTE_ALG_MODE_VERIFY:
		if (src_size != SHA_DIGEST_LENGTH)
			return OTE_ERROR_BAD_PARAMETERS;

		pad_out = te_mem_calloc(RSA_size(rsa));
		if (!pad_out)
			return OTE_ERROR_OUT_OF_MEMORY;

		ERR_clear_error();
		num = RSA_public_decrypt(RSA_size(rsa), dst, pad_out, rsa,
				RSA_NO_PADDING);
		if (num < 0) {
#if DEBUG_OPENSSL_SPEW
			te_fprintf(TE_ERR, "%s: dec failed (%d, %d)\n", __func__,
				num, ERR_GET_REASON(ERR_get_error()));
#endif
			te_mem_free(pad_out);
			return OTE_ERROR_NO_DATA;
		}

		ERR_clear_error();
		ret = RSA_verify_PKCS1_PSS(rsa, src, EVP_sha1(), pad_out,
				SHA_DIGEST_LENGTH);
		if (ret != 1) {
#if DEBUG_OPENSSL_SPEW
			te_fprintf(TE_ERR, "%s: ver failed (%d, %d)\n", __func__,
				ret, ERR_GET_REASON(ERR_get_error()));
#endif
			te_mem_free(pad_out);
			return OTE_ERROR_BAD_FORMAT;
		}
		te_mem_free(pad_out);
		break;

	default:
		return OTE_ERROR_NOT_SUPPORTED;
	}

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_SECURE, "dst data: %d\n", *dst_size);
	for (i = 0; i < *dst_size; i++) {
		te_fprintf(TE_SECURE, " %02x", *((uint8_t*)dst+i));
	}
	te_fprintf(TE_SECURE, "\n");

	te_fprintf(TE_INFO, "%s: exit\n", __func__);
#endif

	return OTE_SUCCESS;
}

void openssl_rsa_free(te_crypto_operation_t operation)
{
#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_INFO, "%s: enter\n", __func__);
#endif

	if (operation && operation->imp_obj) {
		RSA_free((RSA *)operation->imp_obj);
		operation->imp_obj = NULL;
	}

	CRYPTO_cleanup_all_ex_data();
	ERR_free_strings();
	ERR_remove_thread_state(NULL);
	EVP_cleanup();

#if DEBUG_OPENSSL_SPEW
	te_fprintf(TE_INFO, "%s: exit\n", __func__);
#endif
}
