/*
 * Copyright (c) 2013-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stddef.h>
#include <stdlib.h>
#include <common/ote_command.h>

static te_error_t te_oper_find_param(te_operation_t *te_op, uint32_t index,
		te_oper_param_t **param);

te_operation_t *te_create_operation(void)
{
	return (te_operation_t*)malloc(sizeof(te_operation_t));
}

void te_init_operation(te_operation_t *te_op)
{
	memset(te_op, 0, sizeof(te_operation_t));
	te_op->status = OTE_ERROR_NO_ERROR;
}

/* Free internal memory used for te_operation_t.  */
void te_deinit_operation(te_operation_t *te_op)
{
	te_oper_param_t *tmp, *head;

	if (te_op == NULL) {
		return;
	}

	head = (te_oper_param_t *)(uintptr_t)te_op->list_head;
	while (head != NULL) {
		tmp = head;
		head = (te_oper_param_t *)(uintptr_t)head->next;
		free(tmp);
	}
	memset(te_op, 0, sizeof(*te_op));
	return;
}

void te_oper_set_command(te_operation_t *te_op, uint32_t command)
{
	te_op->command = command;
}

static te_oper_param_t *create_oper_param(te_operation_t *te_op)
{
	if (te_op->status != OTE_ERROR_NO_ERROR) {
		//Previous set param operation failed
		return NULL;
	}
	if (te_op->interface_side) {
		te_op->status = OTE_ERROR_BAD_PARAMETERS;
		return NULL;
	}
	te_oper_param_t *param = calloc(1, sizeof(*param));
	if (param == NULL) {
		te_fprintf(TE_ERR, "%s: out of memory\n", __func__);
		te_op->status = OTE_ERROR_OUT_OF_MEMORY;
		return NULL;
	}

	if (!te_op->list_head) {
		te_op->list_head = (cmnptr_t)(uintptr_t)param;
	} else {
		te_oper_param_t *tail_param;

		tail_param = (te_oper_param_t *)(uintptr_t)te_op->list_tail;
		tail_param->next = (cmnptr_t)(uintptr_t)param;
	}
	te_op->list_tail = (cmnptr_t)(uintptr_t)param;
	return param;
}

void te_oper_set_param_int_ro(te_operation_t *te_op, uint32_t index,
		uint32_t Int)
{
	te_error_t err;
	te_oper_param_t *param;

	/* Re-use existing param at index if it exists.  */
	err = te_oper_find_param(te_op, index, &param);
	if (err != OTE_SUCCESS) {
		param = create_oper_param(te_op);
		if (param == NULL) {
			return;
		}
		te_op->list_count++;
	}
	param->type = TE_PARAM_TYPE_INT_RO;
	param->index = index;
	param->u.Int.val = Int;
}

void te_oper_set_param_int_rw(te_operation_t *te_op, uint32_t index,
		uint32_t Int)
{
	te_error_t err;
	te_oper_param_t *param;

	/* Re-use existing param at index if it exists.  */
	err = te_oper_find_param(te_op, index, &param);
	if (err != OTE_SUCCESS) {
		param = create_oper_param(te_op);
		if (param == NULL) {
			return;
		}
		te_op->list_count++;
	}
	param->type = TE_PARAM_TYPE_INT_RW;
	param->index = index;
	param->u.Int.val = Int;
}

static void te_oper_set_param_mem(te_operation_t *te_op, uint32_t type,
				uint32_t index, const void *base, uint32_t len)
{
	te_error_t err;
	te_oper_param_t *param;

	/* Re-use existing param at index if it exists.  */
	err = te_oper_find_param(te_op, index, &param);
	if (err != OTE_SUCCESS) {
		if (base == NULL) {
			te_op->status = OTE_ERROR_BAD_PARAMETERS;
			return;
		}
		param = create_oper_param(te_op);
		if (param == NULL) {
			te_op->status = OTE_ERROR_BAD_PARAMETERS;
			return;
		}
		te_op->list_count++;
	}
	param->type = type;
	param->index = index;
	if (base != NULL) {
		param->u.Mem.base = (cmnptr_t)(uintptr_t)base;
	}
	param->u.Mem.len = len;
}

void te_oper_set_param_mem_ro(te_operation_t *te_op, uint32_t index,
		const void *base, uint32_t len)
{
	return te_oper_set_param_mem(te_op, TE_PARAM_TYPE_MEM_RO,
				index, base, len);
}

void te_oper_set_param_mem_rw(te_operation_t *te_op, uint32_t index,
		void *base, uint32_t len)
{
	return te_oper_set_param_mem(te_op, TE_PARAM_TYPE_MEM_RW,
				index, base, len);
}

void te_oper_set_param_persist_mem_ro(te_operation_t *te_op, uint32_t index,
		const void *base, uint32_t len)
{
	return te_oper_set_param_mem(te_op, TE_PARAM_TYPE_PERSIST_MEM_RO,
				index, base, len);
}

void te_oper_set_param_persist_mem_rw(te_operation_t *te_op, uint32_t index,
		void *base, uint32_t len)
{
	return te_oper_set_param_mem(te_op, TE_PARAM_TYPE_PERSIST_MEM_RW,
				index, base, len);
}

uint32_t te_oper_get_command(te_operation_t *te_op)
{
	return te_op->command;

}

uint32_t te_oper_get_num_params(te_operation_t *te_op)
{
	return te_op->list_count;
}

static te_error_t te_oper_find_param(te_operation_t *te_op, uint32_t index,
		te_oper_param_t **param)
{
	if (!param || !te_op) {
		return OTE_ERROR_BAD_PARAMETERS;
	}

	te_oper_param_t *head = (te_oper_param_t *)(uintptr_t)te_op->list_head;
	while (head) {
		if (head->index == index) {
			*param = head;
			return OTE_SUCCESS;
		}
		head = (te_oper_param_t *)(uintptr_t)head->next;
	}
	return OTE_ERROR_ITEM_NOT_FOUND;
}

te_error_t te_oper_get_param_type(te_operation_t *te_op, uint32_t index,
		te_oper_param_type_t *type)
{
	if (!te_op || !type) {
		return OTE_ERROR_BAD_PARAMETERS;
	}

	te_oper_param_t *param = NULL;
	te_error_t res;
	if ((res = te_oper_find_param(te_op, index, &param)) != OTE_SUCCESS) {
		return res;
	}
	*type = param->type;
	return OTE_SUCCESS;
}

te_error_t te_oper_get_param_int(te_operation_t *te_op, uint32_t index,
		uint32_t *Int)
{
	if (!te_op || !Int) {
		return OTE_ERROR_BAD_PARAMETERS;
	}

	te_oper_param_t *param = NULL;
	te_error_t res;
	if ((res = te_oper_find_param(te_op, index, &param)) != OTE_SUCCESS) {
		return res;
	}
	if (param->type == TE_PARAM_TYPE_INT_RO ||
			param->type == TE_PARAM_TYPE_INT_RW) {
		*Int = param->u.Int.val;
	} else {
		return OTE_ERROR_ITEM_NOT_FOUND;
	}
	return OTE_SUCCESS;
}

te_error_t te_oper_get_param_mem(te_operation_t *te_op, uint32_t index,
		void **base, uint32_t *len)
{
	if (!te_op || !base || !len) {
		return OTE_ERROR_BAD_PARAMETERS;
	}

	te_oper_param_t *param = NULL;
	te_error_t res;
	if ((res = te_oper_find_param(te_op, index, &param)) != OTE_SUCCESS) {
		return res;
	}
	if ((param->type == TE_PARAM_TYPE_MEM_RO) ||
		(param->type == TE_PARAM_TYPE_MEM_RW) ||
		(param->type == TE_PARAM_TYPE_PERSIST_MEM_RO) ||
		(param->type == TE_PARAM_TYPE_PERSIST_MEM_RW)) {
		*base = (void *)(uintptr_t)param->u.Mem.base;
		*len = param->u.Mem.len;
	} else {
		return OTE_ERROR_ITEM_NOT_FOUND;
	}
	return OTE_SUCCESS;
}

static te_error_t te_oper_get_param_mem_type(te_operation_t *te_op,
		uint32_t index, void **base, uint32_t *len,
		te_oper_param_type_t type)
{
	if (!te_op || !base || !len) {
		return OTE_ERROR_BAD_PARAMETERS;
	}

	te_oper_param_t *param = NULL;
	te_error_t res;
	if ((res = te_oper_find_param(te_op, index, &param)) != OTE_SUCCESS) {
		return res;
	}
	if (param->type == type) {
		*base = (void *)(uintptr_t)param->u.Mem.base;
		*len = param->u.Mem.len;
	} else {
		return OTE_ERROR_ITEM_NOT_FOUND;
	}
	return OTE_SUCCESS;
}

te_error_t te_oper_get_param_mem_ro(te_operation_t *te_op, uint32_t index,
		const void **base, uint32_t *len)
{
	return te_oper_get_param_mem_type(te_op, index, (void **)base, len,
	                                  TE_PARAM_TYPE_MEM_RO);
}

te_error_t te_oper_get_param_mem_rw(te_operation_t *te_op, uint32_t index,
		void **base, uint32_t *len)
{
	return te_oper_get_param_mem_type(te_op, index, base, len,
	                                  TE_PARAM_TYPE_MEM_RW);
}

te_error_t te_oper_get_param_persist_mem_ro(te_operation_t *te_op,
		uint32_t index, const void **base, uint32_t *len)
{
{
	return te_oper_get_param_mem_type(te_op, index, (void **)base, len,
	                                  TE_PARAM_TYPE_PERSIST_MEM_RO);
}
}

te_error_t te_oper_get_param_persist_mem_rw(te_operation_t *te_op,
		uint32_t index, void **base, uint32_t *len)
{
	return te_oper_get_param_mem_type(te_op, index, base, len,
	                                  TE_PARAM_TYPE_PERSIST_MEM_RW);
}

void te_operation_reset(te_operation_t *te_op)
{
	te_oper_param_t *tmp, *head;

	if (te_op == NULL) {
		return;
	}

	head = (te_oper_param_t *)(uintptr_t)te_op->list_head;
	while (head != NULL) {
		tmp = head;
		head = (te_oper_param_t *)(uintptr_t)head->next;
		free(tmp);
	}
	memset(te_op, 0, sizeof(*te_op));
	return;
}
