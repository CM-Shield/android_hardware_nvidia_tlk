/*
 * Copyright (c) 2013-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * @file
 * <b> NVIDIA Trusted Little Kernel Interface: NVIDIA Cryptography</b>
 *
 * @b Description: Declares the cryptography APIs in the TLK.
 */

/**
 * @defgroup tlk_services_nvcrypto Crypto Service Manager
 *
 * Defines APIs for managing Trusted Little Kernel (TLK)
 * crypto services.
 *
 * @ingroup tlk_services
 *
 * @{
 */

#ifndef __OTE_NVCRYPTO_H
#define __OTE_NVCRYPTO_H

#include <common/ote_error.h>

/*! Initializes and opens an nvcrypto service session.
 *  This function keeps track of the number of open sessions.
 *
 * \retval OTE_SUCCESS Indicates the operation was successful.
 */
te_error_t ote_nvcrypto_init(void);

/*! Closes an nvcrypto service session.
 *
 *
* \retval OTE_SUCCESS Indicates the operation was successful.
 *
 */
te_error_t ote_nvcrypto_deinit(void);

/*! Gets the keybox.
 *
 *  \param [in]        keybox_index      The index of the keybox.
 *  \param [in,out]     buf              A pointer to the key.
 *  \param [in,out]     len              The length of the buffer in bytes.
 *
 * \retval OTE_SUCCESS Indicates the operation was successful.
 */
te_error_t ote_nvcrypto_get_keybox(uint32_t keybox_index, void *buf, uint32_t *len);

/*! \brief Gets the storage key.
 *
 * \retval OTE_SUCCESS Indicates the operation was successful.
 *
 *  \param [in,out]     key               A pointer to the key.
 *  \param [in]        key_size               The length of the key in bytes.
 */
te_error_t ote_nvcrypto_get_storage_key(uint8_t *key, uint32_t key_size);

/*! \brief Gets the rollback key.
 *
 * \retval OTE_SUCCESS Indicates the operation was successful.
 *
 *  \param [out]       key               A pointer to the key.
 *  \param [in,out]    key_size          The length of the key in bytes.
 */
te_error_t ote_nvcrypto_get_rollback_key(uint8_t *key, uint32_t key_size);

/** @} */
#endif
