#
# Copyright (c) 2015 NVIDIA CORPORATION.  All Rights Reserved.
#
# NVIDIA CORPORATION and its licensors retain all intellectual property
# and proprietary rights in and to this software, related documentation
# and any modifications thereto.  Any use, reproduction, disclosure or
# distribution of this software and related documentation without an express
# license agreement from NVIDIA CORPORATION is strictly prohibited.

LOCAL_DIR := $(GET_LOCAL_DIR)

LIB_ROOT := $(ROOT)/lib

INCLUDES += \
	-I$(LIB_ROOT)/include \
	-I$(LIB_ROOT)/external/libc/include

MODULE_SRCS += \
	$(LOCAL_DIR)/ote_ext_nv.c

MODULE_CFLAGS += -nostdinc -nostdlib -fno-stack-protector

include $(ROOT)/lib/make/module.mk
