/*
 * Copyright (c) 2013, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioccom.h>

#include <common/ote_error.h>
#include <ext_nv/ote_ext_nv.h>

#include <service/ote_service.h>

static te_error_t check_error(int error)
{
	te_error_t ret_error;

	switch (error) {
	case EFAULT:
	case EINVAL:
		ret_error = OTE_ERROR_BAD_PARAMETERS;
		break;
	case ENOMEM:
		ret_error = OTE_ERROR_OUT_OF_MEMORY;
		break;
	default:
		ret_error = OTE_ERROR_GENERIC;
		break;
	}
	return ret_error;
}

te_error_t te_ext_nv_cache_maint(te_ext_nv_cache_maint_op_t op, void *addr,
		uint32_t length)
{
	te_cache_maint_args_t cache_maint_args;
	te_error_t err;

	memset(&cache_maint_args, 0, sizeof(cache_maint_args));

	switch (op) {
	case OTE_EXT_NV_CM_OP_CLEAN:
		cache_maint_args.op = OTE_EXT_NV_CM_OP_CLEAN;
		break;
	case OTE_EXT_NV_CM_OP_INVALIDATE:
		cache_maint_args.op = OTE_EXT_NV_CM_OP_INVALIDATE;
		break;
	case OTE_EXT_NV_CM_OP_FLUSH:
		cache_maint_args.op = OTE_EXT_NV_CM_OP_FLUSH;
		break;
	default:
		te_fprintf(TE_ERR, "%s: bad argument: %d\n", __func__, op);
		return OTE_ERROR_BAD_PARAMETERS;
	}

	cache_maint_args.vaddr = addr;
	cache_maint_args.length = length;

	if (ioctl(1, OTE_IOCTL_CACHE_MAINT, &cache_maint_args) < 0) {
		err = check_error(errno);
		te_fprintf(TE_ERR, "%s: ioctl failure: 0x%x\n", __func__, err);
		return err;
	}

	return OTE_SUCCESS;
}

te_error_t te_ext_nv_virt_to_phys(void *addr, uint64_t *paddr)
{
	te_v_to_p_args_t vtop_args;
	te_error_t err;

	/* parameter checking */
	if (paddr == NULL) {
		te_fprintf(TE_ERR, "%s: paddr argument is null\n", __func__);
		return OTE_ERROR_BAD_PARAMETERS;
	}

	memset(&vtop_args, 0, sizeof(vtop_args));
	vtop_args.vaddr = addr;
	if (ioctl(1, OTE_IOCTL_V_TO_P, &vtop_args) < 0) {
		err = check_error(errno);
		te_fprintf(TE_ERR, "%s: ioctl failure: 0x%x\n", __func__, err);
		return err;
	}

	*paddr = vtop_args.paddr;

	return OTE_SUCCESS;
}

te_error_t te_ext_nv_get_map_addr(uint32_t id, void **addr)
{
	te_map_mem_addr_args_t mapmem_args;
	te_error_t err;

	/* parameter checking */
	if (addr == NULL) {
		te_fprintf(TE_ERR, "%s: addr argument is null\n", __func__);
		return OTE_ERROR_BAD_PARAMETERS;
	}

	memset(&mapmem_args, 0, sizeof(mapmem_args));
	mapmem_args.id = id;
	if (ioctl(1, OTE_IOCTL_GET_MAP_MEM_ADDR, &mapmem_args) < 0) {
		err = check_error(errno);
		te_fprintf(TE_ERR, "%s: ioctl failure: 0x%x\n", __func__, err);
		return err;
	}

	*addr = mapmem_args.addr;

	return OTE_SUCCESS;
}
