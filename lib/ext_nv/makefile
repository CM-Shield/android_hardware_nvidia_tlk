#
# Copyright (c) 2015 NVIDIA CORPORATION.  All Rights Reserved.
#
# NVIDIA CORPORATION and its licensors retain all intellectual property
# and proprietary rights in and to this software, related documentation
# and any modifications thereto.  Any use, reproduction, disclosure or
# distribution of this software and related documentation without an express
# license agreement from NVIDIA CORPORATION is strictly prohibited.

ROOT := ../..

TOOLCHAIN_PREFIX ?= $(ROOT)/tools/arm-linux-androideabi-4.8/bin/arm-linux-androideabi-

MODULE := libtlk_nv_hw_ext

include $(ROOT)/lib/make/macros.mk

DEBUG ?= 2

BUILDOUT ?= $(ROOT)/out/libs
BUILDDIR := $(BUILDOUT)/$(MODULE)
LIB := $(BUILDDIR)/$(MODULE).a

INCLUDES :=
GLOBAL_OPTFLAGS ?= -Os
GLOBAL_COMPILEFLAGS := -g -fno-builtin -finline -W -Wall -Wno-multichar -Wno-unused-parameter -Wno-unused-function
GLOBAL_CFLAGS := --std=c99 -Werror-implicit-function-declaration
GLOBAL_ASMFLAGS := -DASSEMBLY
GLOBAL_LDFLAGS := --gc-sections
GLOBAL_ARFLAGS := crsPD
GLOBAL_COMPILEFLAGS += -ffunction-sections -fdata-sections

all:: $(LIB)

# master object list (for dep generation)
ALLOBJS :=

# a linker script needs to be declared in one of the project/target/platform files
TASK_LINKER_SCRIPT :=

# any rules you put here will also be built by the system before considered being complete
EXTRA_BUILDDEPS :=

# any rules you put here will be depended on in clean builds
EXTRA_CLEANDEPS :=

include rules.mk

# any extra top level build dependencies that someone declared
all:: $(EXTRA_BUILDDEPS)

# debug build?
ifneq ($(DEBUG),)
DEFINES += \
	DEBUG=$(DEBUG)
endif

# allow additional defines from outside the build system
ifneq ($(EXTERNAL_DEFINES),)
DEFINES += $(EXTERNAL_DEFINES)
$(info EXTERNAL_DEFINES = $(EXTERNAL_DEFINES))
endif

DEPS := $(ALLOBJS:%o=%d)

#$(warning DEPS=$(DEPS))

LIBGCC := $(shell $(TOOLCHAIN_PREFIX)gcc $(MODULE_COMPILEFLAGS) -print-libgcc-file-name)
$(info LIBGCC = $(LIBGCC))

# comment out or override if you want to see the full output of each command
NOECHO ?= @

ifneq ($(OBJS),)
$(warning OBJS=$(OBJS))
$(error OBJS is not empty, please convert to new module format)
endif
ifneq ($(CFLAGS),)
$(warning CFLAGS=$(CFLAGS))
$(error CFLAGS is not empty, please use GLOBAL_CFLAGS or MODULE_CFLAGS)
endif

# the logic to compile and link stuff is in here
$(LIB): $(ALLMODULE_OBJS)
	@echo building $(MODULE) $@
	$(NOECHO)$(AR) $(GLOBAL_ARFLAGS) $@ $<

clean: $(EXTRA_CLEANDEPS)
	rm -f $(ALLOBJS) $(DEPS) $(LIB)


