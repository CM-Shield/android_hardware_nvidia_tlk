#######################################
# target static library
include $(CLEAR_VARS)

# The static library should be used in only unbundled apps
# and we don't have clang in unbundled build yet.
LOCAL_SDK_VERSION := 9

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := libtlk_crypto_static
LOCAL_ADDITIONAL_DEPENDENCIES := $(LOCAL_PATH)/android-config.mk $(LOCAL_PATH)/Crypto.mk

include $(LOCAL_PATH)/Crypto-config-tlk.mk
include $(LOCAL_PATH)/android-config.mk

LOCAL_CFLAGS += -nostdinc -nostdlib -fno-stack-protector -Wno-empty-body

LOCAL_C_INCLUDES := $(patsubst external/openssl/%,$(LOCAL_PATH)/%,$(LOCAL_C_INCLUDES))
LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/../libc/include

# Replace cflags with static-specific cflags so we dont build in libdl deps
LOCAL_CFLAGS_32 := $(openssl_cflags_static_32)
LOCAL_CFLAGS_64 := $(openssl_cflags_static_64)
include $(BUILD_STATIC_LIBRARY)
