#
# Copyright (c) 2015 NVIDIA CORPORATION.  All Rights Reserved.
#
# NVIDIA CORPORATION and its licensors retain all intellectual property
# and proprietary rights in and to this software, related documentation
# and any modifications thereto.  Any use, reproduction, disclosure or
# distribution of this software and related documentation without an express
# license agreement from NVIDIA CORPORATION is strictly prohibited.

LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE_DEPS := $(LOCAL_DIR)/android-config.mk $(LOCAL_DIR)/Crypto.mk

include $(LOCAL_DIR)/Crypto-config-tlk.mk
include $(LOCAL_DIR)/android-config.mk

INCLUDES := -I$(patsubst external/openssl/%,$(LOCAL_DIR)/%,$(INCLUDES))

common_c_includes := \
  -I. \
  -I$(LOCAL_DIR)/. \
  -I$(LOCAL_DIR)/crypto \
  -I$(LOCAL_DIR)/crypto/asn1 \
  -I$(LOCAL_DIR)/crypto/evp \
  -I$(LOCAL_DIR)/crypto/modes \
  -I$(LOCAL_DIR)/include \
  -I$(LOCAL_DIR)/include/openssl \
  -I$(LOCAL_DIR)/../libc/include \

INCLUDES += $(common_c_includes)

MODULE_CFLAGS += -nostdinc -nostdlib -fno-stack-protector -Wno-empty-body
MODULE_CFLAGS += $(LOCAL_CFLAGS) \
		$(LOCAL_CFLAGS_arm)

MODULE_SRCS += $(LOCAL_SRC_FILES_arm) \
		$(LOCAL_DIR)/crypto/cms/cms_env.c \
		$(LOCAL_DIR)/crypto/cms/cms_sd.c

include $(MAKE_DIR)/module.mk
