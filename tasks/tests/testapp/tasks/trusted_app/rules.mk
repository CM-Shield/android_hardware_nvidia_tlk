#
# Copyright (c) 2015 NVIDIA CORPORATION.  All Rights Reserved.
#
# NVIDIA CORPORATION and its licensors retain all intellectual property
# and proprietary rights in and to this software, related documentation
# and any modifications thereto.  Any use, reproduction, disclosure or
# distribution of this software and related documentation without an express
# license agreement from NVIDIA CORPORATION is strictly prohibited.

LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR)

LIB_ROOT := $(ROOT)/lib

INCLUDES += \
	-I$(LIB_ROOT)/include \
	-I$(LIB_ROOT)/external/libc/include

BUILD_LIBS := $(ROOT)/out/libs

STATIC_LIBS += \
	$(BUILD_LIBS)/libtlk_libc/libtlk_libc.a \
	$(BUILD_LIBS)/libtlk_service/libtlk_service.a \
	$(BUILD_LIBS)/libtlk_common/libtlk_common.a \
	$(BUILD_LIBS)/libtlk_nv_hw_ext/libtlk_nv_hw_ext.a

MODULE_SRCS += \
	$(LOCAL_DIR)/trusted_app.c \
	$(LOCAL_DIR)/manifest.c

TASK_LINKER_SCRIPT += \
	$(LOCAL_DIR)/$(LIB_ROOT)/task_linker_script.ld

MODULE_CFLAGS += -nostdinc -nostdlib -fno-stack-protector

include $(ROOT)/tasks/make/module.mk
