/*
 * Copyright (c) 2012-2014 NVIDIA CORPORATION.  All rights reserved.
 *
 * NVIDIA Corporation and its licensors retain all intellectual property
 * and proprietary rights in and to this software and related documentation
 * and any modifications thereto.  Any use, reproduction, disclosure or
 * distribution of this software and related documentation without an express
 * license agreement from NVIDIA Corporation is strictly prohibited.
 */

#include <stddef.h>
#include <stdio.h>

#include <common/ote_nv_uuid.h>
#include <service/ote_manifest.h>

OTE_MANIFEST OTE_MANIFEST_ATTRS ote_manifest =
{
    .name = "trusted_app",

	/* UUID : refer to "ote_nv_uuid.h" */
    .uuid = SERVICE_TRUSTEDAPP_UUID,

    .config_options =
	/* optional configuration options here */
	{
		/* request two I/O mappings */
		OTE_CONFIG_MAP_MEM(1, 0x70000000, 0x1000),
		OTE_CONFIG_MAP_MEM(2, 0x70000804, 0x4)
	},
};
