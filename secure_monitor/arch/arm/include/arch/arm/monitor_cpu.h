/*
 * Copyright (c) 2013-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __MONITOR_CPU_H
#define __MONITOR_CPU_H

#ifndef ASSEMBLY

void mon_cpu_save_context(void);
void mon_cpu_copy_context(void *dstptr);
void mon_cpu_gic_setup(void);
void mon_cpu_set_mvbar(uint32_t val);
void mon_cpu_set_monitor_stack(void *stackptr);
void mon_cpu_set_nsacr(void);
void mon_cpu_exit_coherency(void);
void mon_cpu_suspend(uint32_t fn_addr);
__NO_RETURN void mon_cpu_enter_wfi(void);
void mon_cpu_flush_l1(void);
void mon_cpu_inv_l1(void);
void mon_clean_inv_cache_range(addr_t start, size_t len);

#else

#if ARM_WITH_LPAE
#include <arch/arm/mmu_ldesc_macros.h>
#else
#include <arch/arm/mmu_sdesc_macros.h>
#endif

/* copied from Travis' upstream include/asm.h */
#define FUNCTION(x) .global x; .type x,STT_FUNC; x:
#define DATA(x) .global x; .type x,STT_OBJECT; x:

#define LOCAL_FUNCTION(x) .type x,STT_FUNC; x:
#define LOCAL_DATA(x) .type x,STT_OBJECT; x:

/* returns the ID of the current processor */
.macro cpu_id, rd
	mrc	p15, 0, \rd, c0, c0, 5
	and	\rd, \rd, #0xF
.endm

.macro mon_cpu_restore_context, base, enable, tmp, tmp2
	/* restore MMU registers */
	mmu_desc_restore_context \base, \tmp, \tmp2

	/* restore CPU registers */
	ldr	\tmp, [\base, #0]
	mcr	p15, 0, \tmp, c12, c0, 0	@ VBAR
	ldr	\tmp, [\base, #4]
	mcr	p15, 0, \tmp, c13, c0, 3	@ TLS
	ldr	\tmp, [\base, #8]		@ saved SCTLR

	/* should MMU be enabled? */
	cmp	\enable, #0
	biceq	\tmp, #0x1			@ nope, clear the enable
	mcr	p15, 0, \tmp, c1, c0, 0		@ SCTLR
	isb
.endm

#endif /* ASSEMBLY */

#endif /*__MONITOR_SECONDARY_H */
