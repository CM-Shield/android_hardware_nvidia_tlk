/*
 * Copyright (c) 2012-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __MONITOR_VECTORS_H
#define __MONITOR_VECTORS_H

#define SAVED_STATE_WORDS	7

/* save exception SP and VFP state (assumes SCR.NS=0) */
.macro SAVE_STATE, base, tmp
	cps	#0x11			/* fiq */
	str	sp, [\base], #4
	cps	#0x12			/* irq */
	str	sp, [\base], #4
	cps	#0x13			/* svc */
	str	sp, [\base], #4
	str	lr, [\base], #4
	cps	#0x17			/* abt */
	str	sp, [\base], #4
	cps	#0x1b			/* und */
	str	sp, [\base], #4
	cps	#0x1f			/* sys */
	str	sp, [\base], #4

	/* return in monitor mode */
	cps	#0x16			/* mon */
.endm

/* restore exception SP and VFP state (assumes SCR.NS=0) */
.macro RESTORE_STATE, base, tmp
	cps	#0x11			/* fiq */
	ldr	sp, [\base], #4
	cps	#0x12			/* irq */
	ldr	sp, [\base], #4
	cps	#0x13			/* svc */
	ldr	sp, [\base], #4
	ldr	lr, [\base], #4
	cps	#0x17			/* abt */
	ldr	sp, [\base], #4
	cps	#0x1b			/* und */
	ldr	sp, [\base], #4
	cps	#0x1f			/* sys */
	ldr	sp, [\base], #4

	/* return in monitor mode */
	cps	#0x16			/* mon */
.endm

.macro SAVE_SECURE_STATE, base, tmp
	ldr		\base, =secure_state
	SAVE_STATE	\base, \tmp
.endm

.macro RESTORE_SECURE_STATE, base, tmp
	ldr		\base, =secure_state
	RESTORE_STATE	\base, \tmp
.endm

.macro SAVE_NONSECURE_STATE, base, tmp
	ldr		\base, =nonsecure_state
	SAVE_STATE	\base, \tmp
.endm

.macro RESTORE_NONSECURE_STATE, base, tmp
	ldr		\base, =nonsecure_state
	RESTORE_STATE	\base, \tmp
.endm

/* sets SCR.NS bit to 1 (assumes monitor mode) */
.macro SWITCH_SCR_TO_NONSECURE, tmp
	mrc	p15, 0, \tmp, c1, c1, 0
	orr	\tmp, \tmp, #0x1
	mcr	p15, 0, \tmp, c1, c1, 0
	isb
.endm

/* sets SCR.NS bit to 0 (assumes monitor mode) */
.macro SWITCH_SCR_TO_SECURE, tmp
	mrc	p15, 0, \tmp, c1, c1, 0
	bic	\tmp, \tmp, #0x1
	mcr	p15, 0, \tmp, c1, c1, 0
	isb
.endm

.macro SAVE_MON_FRAME_TO_ARG, arg
	push	{ r14 }
	adr	r14, \arg
	ldr	r14, [r14]
	str	r0, [r14, #0x00]
	str	r1, [r14, #0x08]
	str	r2, [r14, #0x10]
	str	r3, [r14, #0x18]
	str	r4, [r14, #0x20]
	str	r5, [r14, #0x28]
	str	r6, [r14, #0x30]
	str	r7, [r14, #0x38]
	str	r8, [r14, #0x40]
	str	r9, [r14, #0x48]
	str	r10, [r14, #0x50]
	str	r11, [r14, #0x58]
	str	r12, [r14, #0x60]
	pop	{ r12 }
	str	r12, [r14, #0x68]
	mrs	r12, spsr
	str	r12, [r14, #0x70]
.endm

.macro RESTORE_MON_FRAME_FROM_ARG, arg
	mov	r14, \arg
	ldr	\arg,  [r14, #0x70]
	msr	spsr_cfsx, \arg
	ldr	r0,  [r14, #0x00]
	ldr	r1,  [r14, #0x08]
	ldr	r2,  [r14, #0x10]
	ldr	r3,  [r14, #0x18]
	ldr	r4,  [r14, #0x20]
	ldr	r5,  [r14, #0x28]
	ldr	r6,  [r14, #0x30]
	ldr	r7,  [r14, #0x38]
	ldr	r8,  [r14, #0x40]
	ldr	r9,  [r14, #0x48]
	ldr	r10, [r14, #0x50]
	ldr	r11, [r14, #0x58]
	ldr	r12, [r14, #0x60]
	ldr	r14, [r14, #0x68]
.endm

.macro SAVE_MON_FRAME_TO_STACK
	sub	sp, sp, #0x74
	str	r14, [sp, #0x68]
	mov	r14, #0
	str	r14, [sp, #0x6C]
	str	r0, [sp, #0x00]
	str	r14, [sp, #0x04]
	str	r1, [sp, #0x08]
	str	r14, [sp, #0x0C]
	str	r2, [sp, #0x10]
	str	r14, [sp, #0x14]
	str	r3, [sp, #0x18]
	str	r14, [sp, #0x1C]
	str	r4, [sp, #0x20]
	str	r14, [sp, #0x24]
	str	r5, [sp, #0x28]
	str	r14, [sp, #0x2C]
	str	r6, [sp, #0x30]
	str	r14, [sp, #0x34]
	str	r7, [sp, #0x38]
	str	r14, [sp, #0x3C]
	str	r8, [sp, #0x40]
	str	r14, [sp, #0x44]
	str	r9, [sp, #0x48]
	str	r14, [sp, #0x4C]
	str	r10, [sp, #0x50]
	str	r14, [sp, #0x54]
	str	r11, [sp, #0x58]
	str	r14, [sp, #0x5C]
	str	r12, [sp, #0x60]
	str	r14, [sp, #0x64]
	mrs	r12, spsr
	str	r12, [sp, #0x70]
	str	r14, [sp, #0x74]
.endm

.macro RESTORE_MON_FRAME_FROM_STACK
	ldr	r14,  [sp, #0x70]
	msr	spsr_cfsx, r14
	ldr	r0,  [sp, #0x00]
	ldr	r1,  [sp, #0x08]
	ldr	r2,  [sp, #0x10]
	ldr	r3,  [sp, #0x18]
	ldr	r4,  [sp, #0x20]
	ldr	r5,  [sp, #0x28]
	ldr	r6,  [sp, #0x30]
	ldr	r7,  [sp, #0x38]
	ldr	r8,  [sp, #0x40]
	ldr	r9,  [sp, #0x48]
	ldr	r10, [sp, #0x50]
	ldr	r11, [sp, #0x58]
	ldr	r12, [sp, #0x60]
	ldr	r14, [sp, #0x68]
	add	sp, sp, #0x78
.endm

#endif
