/*
 * Copyright (c) 2008 Travis Geiselbrecht
 * Copyright (c) 2013, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __ARCH_ARM_MMU_SDESC_H
#define __ARCH_ARM_MMU_SDESC_H

/* C, B and TEX[2:0] encodings without TEX remap (for first level descriptors) */
                                                          /* TEX      |    CB    */
#define MMU_MEMORY_L1_TYPE_STRONGLY_ORDERED              ((0x0 << 12) | (0x0 << 2))
#define MMU_MEMORY_L1_TYPE_DEVICE_SHARED                 ((0x0 << 12) | (0x1 << 2))
#define MMU_MEMORY_L1_TYPE_DEVICE_NON_SHARED             ((0x2 << 12) | (0x0 << 2))
#define MMU_MEMORY_L1_TYPE_NORMAL                        ((0x1 << 12) | (0x0 << 2))
#define MMU_MEMORY_L1_TYPE_NORMAL_WRITE_THROUGH          ((0x0 << 12) | (0x2 << 2))
#define MMU_MEMORY_L1_TYPE_NORMAL_WRITE_BACK_NO_ALLOCATE ((0x0 << 12) | (0x3 << 2))
#define MMU_MEMORY_L1_TYPE_NORMAL_WRITE_BACK_ALLOCATE    ((0x1 << 12) | (0x3 << 2))

/* C, B and TEX[2:0] encodings without TEX remap (for second level descriptors) */
                                                          /* TEX     |    CB    */
#define MMU_MEMORY_L2_TYPE_STRONGLY_ORDERED              ((0x0 << 6) | (0x0 << 2))
#define MMU_MEMORY_L2_TYPE_DEVICE_SHARED                 ((0x0 << 6) | (0x1 << 2))
#define MMU_MEMORY_L2_TYPE_DEVICE_NON_SHARED             ((0x2 << 6) | (0x0 << 2))
#define MMU_MEMORY_L2_TYPE_NORMAL                        ((0x1 << 6) | (0x0 << 2))
#define MMU_MEMORY_L2_TYPE_NORMAL_WRITE_THROUGH          ((0x0 << 6) | (0x2 << 2))
#define MMU_MEMORY_L2_TYPE_NORMAL_WRITE_BACK_NO_ALLOCATE ((0x0 << 6) | (0x3 << 2))
#define MMU_MEMORY_L2_TYPE_NORMAL_WRITE_BACK_ALLOCATE    ((0x1 << 6) | (0x3 << 2))

#define MMU_MEMORY_DOMAIN_MEM    	(0)

#define MMU_MEMORY_L1_AP_P_NA_U_NA	((0x0 << 15) | (0x0 << 10))
#define MMU_MEMORY_L1_AP_P_RW_U_RO	((0x0 << 15) | (0x2 << 10))
#define MMU_MEMORY_L1_AP_P_RW_U_RW	((0x0 << 15) | (0x3 << 10))
#define MMU_MEMORY_L1_AP_P_RW_U_NA	((0x0 << 15) | (0x1 << 10))

#define MMU_MEMORY_L2_AP_P_NA_U_NA	((0x0 << 9) | (0x0 << 4))
#define MMU_MEMORY_L2_AP_P_RW_U_RO	((0x0 << 9) | (0x2 << 4))
#define MMU_MEMORY_L2_AP_P_RW_U_RW	((0x0 << 9) | (0x3 << 4))
#define MMU_MEMORY_L2_AP_P_RW_U_NA	((0x0 << 9) | (0x1 << 4))

#define MMU_MEMORY_L1_NON_SECURE	(1 << 3)

#define MMU_MEMORY_L1_SHAREABLE		(1 << 16)
#define MMU_MEMORY_L1_NON_GLOBAL	(1 << 17)
#define MMU_MEMORY_L2_SHAREABLE		(1 << 10)
#define MMU_MEMORY_L2_NON_GLOBAL	(1 << 11)

#define MMU_MEMORY_L1_CB_SHIFT		2
#define MMU_MEMORY_L1_TEX_SHIFT		12

#define MMU_MEMORY_L2_CB_SHIFT		2
#define MMU_MEMORY_L2_TEX_SHIFT		6

#define MMU_MEMORY_NON_CACHEABLE		0
#define MMU_MEMORY_WRITE_BACK_ALLOCATE		1
#define MMU_MEMORY_WRITE_THROUGH_NO_ALLOCATE	2
#define MMU_MEMORY_WRITE_BACK_NO_ALLOCATE	3

#define MMU_MEMORY_SET_L1_INNER(val)	    (((val) & 0x3) << MMU_MEMORY_L1_CB_SHIFT)
#define MMU_MEMORY_SET_L1_OUTER(val)	    (((val) & 0x3) << MMU_MEMORY_L1_TEX_SHIFT)
#define MMU_MEMORY_SET_L1_CACHEABLE_MEM	    (0x4 << MMU_MEMORY_L1_TEX_SHIFT)

#define MMU_MEMORY_SET_L2_INNER(val)	    (((val) & 0x3) << MMU_MEMORY_L2_CB_SHIFT)
#define MMU_MEMORY_SET_L2_OUTER(val)	    (((val) & 0x3) << MMU_MEMORY_L2_TEX_SHIFT)
#define MMU_MEMORY_SET_L2_CACHEABLE_MEM	    (0x4 << MMU_MEMORY_L2_TEX_SHIFT)

#define MMU_MEMORY_TTBCR_N		7

#define MMU_MEMORY_TTBR0_L1_INDEX_BITS	(((31 - MMU_MEMORY_TTBCR_N) - 20) + 1)
#define MMU_MEMORY_TTBR0_L1_INDEX_MASK	((1 << MMU_MEMORY_TTBR0_L1_INDEX_BITS) - 1)
#define MMU_MEMORY_TTBR0_L1_SIZE	(1 << (MMU_MEMORY_TTBR0_L1_INDEX_BITS + 2))

#define MMU_MEMORY_TTBR_INNER_RGN0_SHIFT	6
#define MMU_MEMORY_TTBR_INNER_RGN1_SHIFT	0
#define MMU_MEMORY_TTBR_OUTER_RGN_SHIFT	3

/* I:wb/alloc,  O:wb/alloc */
#define MMU_MEMORY_TTBR1_IRGN_RGN \
	((1 << MMU_MEMORY_TTBR_INNER_RGN0_SHIFT) | \
	 (1 << MMU_MEMORY_TTBR_OUTER_RGN_SHIFT))

#define MMU_MEMORY_TTBR1_L1_INDEX_BITS	((31 - 20) + 1)
#define MMU_MEMORY_TTBR1_L1_INDEX_MASK	((1 << MMU_MEMORY_TTBR1_L1_INDEX_BITS) - 1)
#define MMU_MEMORY_TTBR1_L1_SIZE	(1 << (MMU_MEMORY_TTBR1_L1_INDEX_BITS + 2))

#define MMU_MEMORY_TTBR_L2_INDEX_BITS	((19 - 12) + 1)
#define MMU_MEMORY_TTBR_L2_INDEX_MASK	((1 << MMU_MEMORY_TTBR_L2_INDEX_BITS) - 1)
#define MMU_MEMORY_TTBR_L2_SIZE		(1 << (MMU_MEMORY_TTBR_L2_INDEX_BITS + 2))

#endif
