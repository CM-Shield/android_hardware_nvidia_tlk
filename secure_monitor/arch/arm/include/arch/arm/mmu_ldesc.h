/*
 * Copyright (c) 2013-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __ARCH_ARM_MMU_LDESC_H
#define __ARCH_ARM_MMU_LDESC_H

/*
 * Initialize banked copy of MAIR0/MAIR1 registers with the memory
 * attributes the kernel will be using:
 *
 * idx0 = strongly-ordered
 * idx1 = outer: writeback/no alloc, inner: writeback/alloc
 *
 * and let the remaining indexes be allocated when a lookup during
 * a page mapping fails to find an existing entry.
 */
#define MMU_MEMORY_ATTR_INDIR_0		0x0000EF00
#define MMU_MEMORY_ATTR_INDIR_1		0x00000000

/* indices into attr indirect regs */
#define MMU_MEMORY_STRONGLY_ORDERED			0
#define MMU_MEMORY_WB_OUTER_NO_ALLOC_INNER_ALLOC	1

#define MMU_MEMORY_SET_ATTR_IDX(val)	(((val) & 0x7) << 2)

#define MMU_MEMORY_WRITE_BACK_NO_ALLOCATE	0xE
#define MMU_MEMORY_WRITE_BACK_ALLOCATE		0xF

/* permissions */
#define MMU_MEMORY_AP_P_RW_U_NA	((0x0 << 7) | (0x0 << 6))
#define MMU_MEMORY_AP_P_RW_U_RW	((0x0 << 7) | (0x1 << 6))
#define MMU_MEMORY_AP_P_RO_U_NA	((0x1 << 7) | (0x0 << 6))
#define MMU_MEMORY_AP_P_RO_U_RO	((0x1 << 7) | (0x1 << 6))

/* shareable */
#define MMU_MEMORY_SH_NON_SHAREABLE	((0x0 << 9) | (0x0 << 8))
#define MMU_MEMORY_SH_OUTER_SHAREABLE	((0x1 << 9) | (0x0 << 8))
#define MMU_MEMORY_SH_INNER_SHAREABLE	((0x1 << 9) | (0x1 << 8))

#define MMU_MEMORY_NON_GLOBAL	(1 << 11)
#define MMU_MEMORY_ACCESS_FLAG	(1 << 10)
#define MMU_MEMORY_NON_SECURE	(1 << 5)

/* ttbcr */
#define MMU_MEMORY_TTBCR_EAE	(1 << 31)
#define MMU_MEMORY_TTBCR_A1	(0 << 22)

#define MMU_MEMORY_RGN_NON_CACHEABLE		0
#define MMU_MEMORY_RGN_WRITE_BACK_ALLOCATE	1
#define MMU_MEMORY_RGN_WRITE_THROUGH		2
#define MMU_MEMORY_RGN_WRITE_BACK_NO_ALLOCATE	3

#define MMU_MEMORY_TTBCR_OUTER_RGN1(val)	(((val) & 0x3) << 26)
#define MMU_MEMORY_TTBCR_OUTER_RGN0(val)	(((val) & 0x3) << 10)
#define MMU_MEMORY_TTBCR_INNER_RGN1(val)	(((val) & 0x3) << 24)
#define MMU_MEMORY_TTBCR_INNER_RGN0(val)	(((val) & 0x3) << 8)

/* 1GB user (TTBR0) and 3GB kernel (TTBR1) */
#define MMU_MEMORY_TTBCR_T0SZ	(2 << 0)
#define MMU_MEMORY_TTBCR_T1SZ	(0 << 16)

#define MMU_ENTRY_SHIFT		(3)

/* table descriptor level 2 */
#define MMU_MEMORY_TTBR_L2_VADDR_SHIFT	21
#define MMU_MEMORY_TTBR_L2_INDEX_BITS	((29 - 21) + 1)
#define MMU_MEMORY_TTBR_L2_INDEX_MASK	((1 << MMU_MEMORY_TTBR_L2_INDEX_BITS) - 1)
#define MMU_MEMORY_TTBR_L2_SIZE		(1 << (MMU_MEMORY_TTBR_L2_INDEX_BITS + 3))

#define MMU_L2_BLOCK_SHIFT      (MMU_MEMORY_TTBR_L2_VADDR_SHIFT)
#define MMU_L2_BLOCK_SIZE       (1 << MMU_L2_BLOCK_SHIFT)

/* table descriptor level 3 */
#define MMU_MEMORY_TTBR_L3_VADDR_SHIFT	12
#define MMU_MEMORY_TTBR_L3_INDEX_BITS	((20 - 12) + 1)
#define MMU_MEMORY_TTBR_L3_INDEX_MASK	((1 << MMU_MEMORY_TTBR_L3_INDEX_BITS) - 1)
#define MMU_MEMORY_TTBR_L3_SIZE		(1 << (MMU_MEMORY_TTBR_L3_INDEX_BITS + 3))

/* L3 is 2^12 bytes in size and maps 2^21 of VA space */
#define MMU_L3_MAP_SHIFT	(MMU_MEMORY_TTBR_L2_VADDR_SHIFT)
#define MMU_L3_SIZE_SHIFT	(MMU_MEMORY_TTBR_L3_VADDR_SHIFT)

/* phys address of next table (for level 2/level 3 tables) */
#define MMU_MEMORY_TABLE_ADDR_SHIFT	12
#define MMU_MEMORY_TABLE_ADDR_ALIGN	(1 << MMU_MEMORY_TABLE_ADDR_SHIFT)
#define MMU_MEMORY_TABLE_ADDR_BITS	((39 - 12) + 1)
#define MMU_MEMORY_TABLE_ADDR_MASK	(((1 << MMU_MEMORY_TABLE_ADDR_BITS) - 1) \
						<< MMU_MEMORY_TABLE_ADDR_SHIFT)
#endif
