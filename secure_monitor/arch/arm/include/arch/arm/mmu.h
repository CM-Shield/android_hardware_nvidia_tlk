/*
 * Copyright (c) 2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __ARCH_ARM_MMU_H
#define __ARCH_ARM_MMU_H

#include <sys/types.h>

#if defined(__cplusplus)
extern "C" {
#endif

#define ALIGN_4KB       (4 * 1024)
#define ALIGN_1MB       (1 * 1024 * 1024)

/* define needed virtaddr alignment for non-secure memory */
#if ARM_WITH_LPAE
#define NS_VIRT_ADDR_ALIGN	ALIGN_4KB
#else
#define NS_VIRT_ADDR_ALIGN	ALIGN_1MB
#endif

/* PAR register: common defines (between short/long desc) */
#define PAR_ATTR_FAULTED	(0x1 <<  0)
#define PAR_ATTR_LPAE		(0x1 << 11)

/* PAR register: short desc defines */
#define PAR_SDESC_ATTR_NON_SECURE	(0x1 << 9)
#define PAR_SDESC_ATTR_SHAREABLE(par)	(((par) >> 7) & 0x1)
#define PAR_SDESC_ATTR_INNER(par)	(((par) >> 4) & 0x7)
#define PAR_SDESC_ATTR_OUTER(par)	(((par) >> 2) & 0x3)

#define PAR_SDESC_ATTR_SSECTION		(0x1 << 1)
#define PAR_SDESC_ALIGNED_PA(par)	\
	(((par) & PAR_SDESC_ATTR_SSECTION) ? 	/* super-section */		\
		(((par) & 0xFF000000ULL) | (((par) & 0x00FF0000ULL) << 16)) : 	\
		((par) & ~PAGE_MASK))	/* section or large/small page */

/* PAR register: long desc defines */
#define PAR_LDESC_ATTR_NON_SECURE	(0x1 << 9)
#define PAR_LDESC_ATTR_SHAREABLE(par)	(((par) >> 7) & 0x3)
#define PAR_LDESC_ATTR_INNER(par)	(((par) >> 56) & 0xF)
#define PAR_LDESC_ATTR_OUTER(par)	(((par) >> 60) & 0xF)

#define PAR_LDESC_ALIGNED_PA(par)	((par) & 0xFFFFFF000ULL)

#define INVALID_PHYSADDR	(-1)

#if ARM_USE_MMU_RELOC
extern uint32_t __load_phys_base;
#define virtual_to_physical(v)	(((vaddr_t)(v) - (vaddr_t)VMEMBASE) + __load_phys_base)
#define physical_to_virtual(p)	((vaddr_t)((((paddr_t)(p)) - __load_phys_base) + VMEMBASE))
#else
#define virtual_to_physical(v)	(v)
#define physical_to_virtual(p)	(p)
#endif

#if defined(__cplusplus)
}
#endif

#endif
