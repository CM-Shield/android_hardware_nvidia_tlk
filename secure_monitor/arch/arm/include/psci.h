/*
 * Copyright (c) 2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __PSCI_H
#define __PSCI_H

#ifndef ASSEMBLY
#include <sys/types.h>
#endif

#define PSCI_FUNC_ID_VERSION		0x84000000
#define PSCI_FUNC_ID_CPU_SUSPEND	0x84000001
#define PSCI_FUNC_ID64_CPU_SUSPEND	0xC4000001
#define PSCI_FUNC_ID_CPU_OFF		0x84000002
#define PSCI_FUNC_ID64_CPU_ON		0xC4000003
#define PSCI_FUNC_ID_CPU_ON		0x84000003
#define PSCI_FUNC_ID64_AFFINITY_INFO	0xC4000004
#define PSCI_FUNC_ID_AFFINITY_INFO	0x84000004
#define PSCI_FUNC_ID_MIGRATE		0xC4000005
#define PSCI_FUNC_ID_SYSTEM_RESET	0x84000009

#define PSCI_RETURN_SUCCESS		(0)
#define PSCI_RETURN_NOT_SUPPORTED	(-1)
#define PSCI_RETURN_INVALID_PARAMS	(-2)
#define PSCI_RETURN_DENIED		(-3)
#define PSCI_RETURN_ALREADY_ON		(-4)
#define PSCI_RETURN_ON_PENDING		(-5)
#define PSCI_RETURN_INTERNAL_FAILURE	(-6)
#define PSCI_RETURN_NOT_PRESENT		(-7)
#define PSCI_RETURN_DISABLED		(-8)

#define PSCI_POWER_STATE_TYPE_STANDBY		0
#define PSCI_POWER_STATE_TYPE_POWER_DOWN	1

#define PSCI_POWER_STATE_ID_MASK	0xffff
#define PSCI_POWER_STATE_ID_SHIFT	0
#define PSCI_POWER_STATE_TYPE_MASK	0x1
#define PSCI_POWER_STATE_TYPE_SHIFT	16
#define PSCI_POWER_STATE_AFFL_MASK	0x3
#define PSCI_POWER_STATE_AFFL_SHIFT	24

/* PSCI version decoding (independent of PSCI version) */
#define PSCI_VERSION_MAJOR_SHIFT	16
#define PSCI_VERSION_MAJOR(x)		(x << PSCI_VERSION_MAJOR_SHIFT)
#define PSCI_VERSION_MINOR(y)		(y)

/* PSCI v0.2 affinity level state returned by AFFINITY_INFO */
#define PSCI_0_2_AFFINITY_LEVEL_ON		0
#define PSCI_0_2_AFFINITY_LEVEL_OFF		1
#define PSCI_0_2_AFFINITY_LEVEL_ON_PENDING	2

#ifndef ASSEMBLY
void platform_psci_start(int cpu_id);
int platform_psci_cpu_suspend(int cpu, uint32_t pwr_state);
void platform_psci_cpu_resume(int cpu);
void platform_psci_cpu_off(int cpu, uint32_t pwr_state);
int platform_psci_cpu_on(int cpu);
#endif

#endif
