LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR)

# default to the regular arm subarch
SUBARCH := arm

DEFINES += \
	ARM_CPU_$(ARM_CPU)=1

# do set some options based on the cpu core
HANDLED_CORE := false

ifeq ($(ARM_CPU),cortex-a15)
DEFINES += \
	ARM_WITH_CP15=1		\
	ARM_WITH_MMU=1		\
	ARM_ISA_ARMv7=1		\
	ARM_ISA_ARMv7A=1	\
	ARM_WITH_VFP=1		\
	ARM_WITH_NEON=1		\
	ARM_WITH_THUMB=1	\
	ARM_WITH_THUMB2=1	\
	ARM_WITH_CACHE=1	\
	ARM_WITH_SCU=0		\
	ARM_WITH_L2=0
HANDLED_CORE := true
#CFLAGS += -mfpu=neon -mfloat-abi=softfp
MODULE_DEPS += $(LOCAL_DIR)/arm/neon
endif
ifeq ($(ARM_CPU),cortex-a9)
DEFINES += \
	ARM_WITH_CP15=1		\
	ARM_WITH_MMU=1		\
	ARM_ISA_ARMv7=1		\
	ARM_ISA_ARMv7A=1	\
	ARM_WITH_VFP=1		\
	ARM_WITH_NEON=1		\
	ARM_WITH_THUMB=1	\
	ARM_WITH_THUMB2=1	\
	ARM_WITH_CACHE=1	\
	ARM_WITH_SCU=1		\
	ARM_WITH_L2=0
HANDLED_CORE := true
#CFLAGS += -mfpu=neon -mfloat-abi=softfp
MODULE_DEPS += $(LOCAL_DIR)/arm/neon
endif

ifeq ($(ARM_CPU),arm926ej-s)
DEFINES += \
	ARM_WITH_CP15=1 \
	ARM_WITH_MMU=1 \
	ARM_ISA_ARMv5E=1 \
	ARM_WITH_THUMB=1 \
	ARM_WITH_CACHE=1 \
	ARM_CPU_ARM9=1 \
	ARM_CPU_ARM926=1
HANDLED_CORE := true
endif

ifneq ($(HANDLED_CORE),true)
$(warning $(LOCAL_DIR)/rules.mk doesnt have logic for arm core $(ARM_CPU))
$(warning this is likely to be broken)
endif

INCLUDES += \
	-I$(LOCAL_DIR)/include \
	-I$(LOCAL_DIR)/$(SUBARCH)/include

ifeq ($(SUBARCH),arm)

# using either long / short MMU desc support
ifeq ($(ARM_WITH_LPAE),true)
DEFINES += \
	ARM_WITH_LPAE=1
endif

DEFINES += \
	ARCH_DEFAULT_STACK_SIZE=4096
endif

# If platform sets ARM_USE_MMU_RELOC the image will be built based on
# VMEMBASE and will create page table entries in start.S to the physmem
# it's been given (avoiding relocation by copying the image).

ifeq ($(ARM_USE_MMU_RELOC),true)
DEFINES += \
	ARM_USE_MMU_RELOC=1
endif

ifeq ($(ARM_USE_CPU_CACHING),true)
DEFINES += \
	ARM_USE_CPU_CACHING=1
endif

# make sure some bits were set up
MEMVARS_SET := 0
ifeq ($(ARM_USE_MMU_RELOC),true)
ifneq ($(VMEMBASE),)
MEMVARS_SET := 1
endif
ifneq ($(VMEMSIZE),)
MEMVARS_SET := 1
endif
ifeq ($(MEMVARS_SET),0)
$(error missing VMEMBASE or VMEMSIZE variable, please set in target rules.mk)
endif
else
ifneq ($(MEMBASE),)
MEMVARS_SET := 1
endif
ifneq ($(MEMSIZE),)
MEMVARS_SET := 1
endif
DEFINES += \
        VMEMBASE=$(MEMBASE)
endif
ifeq ($(MEMVARS_SET),0)
$(error missing MEMBASE or MEMSIZE variable, please set in target rules.mk)
endif

LIBGCC := $(shell $(TOOLCHAIN_PREFIX)gcc $(MODULE_COMPILEFLAGS) -print-libgcc-file-name)
$(info LIBGCC = $(LIBGCC))

$(info ARCH_COMPILEFLAGS = $(MODULE_COMPILEFLAGS))
