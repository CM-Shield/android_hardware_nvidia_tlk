#
# This builds either into a library that's linked with the TLK kernel
# (when MONITOR_MODULE = false, in the case of MODULE_ARCH = arm).
#
# Or, generates a separate monitor.bin binary with its own address space
# (when MONITOR_MODULE = true, in the case of MODULE_ARCH = arm64).
#
LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR)

MONARCH := $(MONITOR_ARCH)
MODULE_ARCH := $(MONARCH)

ifeq ($(MONARCH),arm)

MODULE_COMPILEFLAGS += -mcpu=$(ARM_CPU)

MODULE_SRCS += \
	$(LOCAL_DIR)/arm/monitor_lib.S \
	$(LOCAL_DIR)/arm/monitor_vector.S \
	$(LOCAL_DIR)/arm/monitor_fastcall.S \
	$(LOCAL_DIR)/arm/monitor_suspend.S \
	$(LOCAL_DIR)/arm/monitor_cpu.S \
	$(LOCAL_DIR)/arm/cache_helpers.S

endif

INCLUDES += \
	-I$(LOCAL_DIR)/include \
	-I$(LOCAL_DIR)/$(MONARCH)/include

ifeq ($(MONARCH),arm64)
MODULE_SRCS += \
	$(LOCAL_DIR)/arm64/monitor_start.S \
	$(LOCAL_DIR)/arm64/monitor_vector.S \
	$(LOCAL_DIR)/arm64/monitor_cpu.S \
	$(LOCAL_DIR)/arm64/monitor_fastcall.S \
	$(LOCAL_DIR)/arm64/monitor_lib.S \
	$(LOCAL_DIR)/arm64/monitor_mmu.S \
	$(LOCAL_DIR)/arm64/cache_helpers.S \
	$(LOCAL_DIR)/common/debug.c \
	$(LOCAL_DIR)/common/printf.c

MODULE_CC := $(TOOLCHAIN_PREFIX64)gcc
MODULE_LD := $(TOOLCHAIN_PREFIX64)ld
MON_OBJCOPY := $(TOOLCHAIN_PREFIX64)objcopy
#
# If we're building a secure build include the secure world callbacks
ifeq ($(STANDALONE_MONITOR),false)
MODULE_SRCS += \
	$(LOCAL_DIR)/arm64/secure_callback.S
endif # STANDALONE_MONITOR == false
endif # MONARCH == arm64

# generating separate $(MONARCH) monitor.bin
ifeq ($(MONITOR_BIN),true)
DEFINES += \
	WITH_MONITOR_BIN=1 \
	MONBITS=$(MONBITS) \
	MONBASE=$(MONBASE) \
	MONCPUS=$(MONCPUS) \
	MONTARGET_$(MONTARGET)=1

MONITOR_MODULE := true
MON_LD := $(MODULE_LD)
MON_LINKER_SCRIPT += \
	$(BUILDDIR)/monitor-onesegment.ld

ifeq ($(MONBASE),)
$(error missing MONBASE variable, please set in target rules.mk)
endif
ifeq ($(MONBITS),)
$(error missing MONBITS variable, please set in target rules.mk)
endif

$(BUILDDIR)/monitor-onesegment.ld: $(LOCAL_DIR)/$(MONARCH)/monitor-onesegment.ld $(CONFIGHEADER)
	@echo generating $@
	@$(MKDIR)
	$(NOECHO)sed "s/%MEMBASE%/$(MONBASE)/" < $< > $@
endif

ifeq ($(MONCPUS),)
$(error missing MONCPUS variable, please set in target rules.mk)
endif

include make/module.mk
