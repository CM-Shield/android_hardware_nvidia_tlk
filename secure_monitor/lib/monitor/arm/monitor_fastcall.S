/*
 * Copyright (c) 2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <asm.h>
#include <arch/arm.h>
#include <lib/monitor/monitor_vector.h>

FUNCTION(monitor_fastcall)
	/* load fastcall frame */
	stmfd	sp!, { lr }

	/*
	 * Find index in fastcall_table. If index >= TRUSTED_BASE,
	 * route through TRUSTED_SERVICE (covers OS/APP fastcalls).
	 */
	lsr	r7, r0, #SMC_OWNER_SHIFT
	and	r7, r7, #SMC_OWNER_MASK
	cmp	r7, #SMC_OWNER_TRUSTED_BASE
	blt	1f
	mov	r7, #SMC_OWNER_TRUSTED_SERVICE

	/* args for fastcall handler (frame) */
1:	mov	r0, r1
	mov	r1, #0

	/* call fastcall handler */
	adr	r6, fastcall_table
	ldr	r7, [r6, r7, lsl #2]
	blx	r7

	/* restore fastcall frame (returning results) */
	ldmia	sp!, { pc }

unimp_fastcall:
	b	.

/* fastcall SMCs issued from non-secure */
fastcall_table:
	.long	unimp_fastcall		/* ARM Arch service */
	.long	unimp_fastcall		/* CPU service */
	.long	platform_sip_handler	/* SIP service */
	.long	unimp_fastcall		/* OEM service */
	.long	psci_handler		/* ARM Standard service (currently, only PSCI) */
	.long	unimp_fastcall		/* Trusted (OS/TA) service */
