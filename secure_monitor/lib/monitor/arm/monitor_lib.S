/*
 * Copyright (c) 2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <arch/arm/monitor_cpu.h>

#define	CPU_CONTEXT_INTS	3

/* void mon_atomic_or(uint32_t *ptr, uint32_t bits); */
FUNCTION(mon_atomic_or)
1:	ldrex	r2, [r0]
	orr	r2, r2, r1
	strex	r3, r2, [r0]
	cmp	r3, #0
	bne	1b
	bx	lr

/* void mon_atomic_and(uint32_t *ptr, uint32_t bits); */
FUNCTION(mon_atomic_and)
1:	ldrex	r2, [r0]
	and	r2, r2, r1
	strex	r3, r2, [r0]
	cmp	r3, #0
	bne	1b
	bx	lr

/* uint32_t mon_cpu_id(void); */
FUNCTION(mon_cpu_id)
	cpu_id	r0
	bx	lr

/* void mon_cpu_gic_setup(void); */
FUNCTION(mon_cpu_gic_setup)
	ldr	r0, =ARM_GIC_DIST_BASE
	mvn	r1, #0
	str	r1, [r0, #128]
	ldr	r0, =ARM_GIC_CPU_BASE
	mov	r1, #0xFF
	str	r1, [r0, #4]
	bx	lr

/* void mon_cpu_set_mvbar(uint32_t); */
FUNCTION(mon_cpu_set_mvbar)
	mcr	p15, 0, r0, c12, c0, 1
	isb
	bx	lr

/* void mon_cpu_set_monitor_stack(uint32_t); */
FUNCTION(mon_cpu_set_monitor_stack)
	mrs	r2, cpsr
	cps	#0x16
	mov	sp, r0
	msr	cpsr, r2
	bx	lr

/* void mon_cpu_set_nsacr(void); */
FUNCTION(mon_cpu_set_nsacr)
	mrc	p15, 0, r0, c1, c1, 2
	orr	r0, r0, #0x00000C00
	orr	r0, r0, #0x00060000
	mcr	p15, 0, r0, c1, c1, 2
	isb
	bx	lr

/* void mon_cpu_exit_coherency(void); */
FUNCTION(mon_cpu_exit_coherency)
	dsb
	mrc	p15, 0, r1, c1, c0, 1		@ ACTLR
	bic	r1, r1, #(1<<6) | (1<<0)	@ clear ACTLR.SMP | ACTLR.FW
	mcr	p15, 0, r1, c1, c0, 1		@ ACTLR
	isb

	/*
	 * Issue a Dummy DVM op to make subsequent DSB issue a DVM_SYNC
	 * in A15. This is for a bug where DSB-lite( with no DVM_SYNC
	 * component) doesn't trigger the logic returned to drain all
	 * other DSBs.
	 */
	mrc	p15, 0, r1, c0, c0, 0
	movw	r2, #0xC0F0			@ Cortex a15 part number
	and	r1, r1, r2
	cmp	r1, r2
	moveq	r1, #0
	mcreq	p15, 0, r1, c7, c5, 6
	dsb
	bx	lr

/* __NO_RETURN void mon_cpu_enter_wfi(void); */
FUNCTION(mon_cpu_enter_wfi)
	dsb
wfi_spill:
	.align	5
	wfi
	b	wfi_spill

	/*
	 * 38 nop's, which fills reset of wfe cache line and
	 * 4 more cachelines with nop
	 */
	.rept 38
	nop
	.endr
	b	.				@ should never get here

/* void mon_cpu_save_context(void) */
FUNCTION(mon_cpu_save_context)
	push	{ lr }

	/* save MMU registers */
	adr	r0, _cpu_context
	mmu_desc_save_context r0, r1, r2

	/* save CPU registers */
	mrc	p15, 0, r1, c12, c0, 0		@ VBAR
	str	r1, [r0, #0]
	mrc	p15, 0, r1, c13, c0, 3		@ TLS
	str	r1, [r0, #4]
	mrc	p15, 0, r1, c1, c0, 0		@ SCTLR
	str	r1, [r0, #8]
	dsb

	adr	r0, _cpu_context
	mov	r1, #(CPU_CONTEXT_INTS + MMU_DESC_CONTEXT_INTS)
	bl	mon_clean_inv_cache_range

	pop	{ pc }

/* void mon_cpu_copy_context(void *dstaddr) */
FUNCTION(mon_cpu_copy_context)
	adr	r1, _cpu_context
	ldr	r2, =_cpu_context_end
	cmp	r1, r2
	beq	2f
1:
	/* copy data */
	ldr	r3, [r1], #4
	str	r3, [r0], #4
	cmp	r1, r2
	blt	1b
2:
	bx	lr

DATA(_cpu_context)
	.rept (CPU_CONTEXT_INTS + MMU_DESC_CONTEXT_INTS)
	.long	0
	.endr
DATA(_cpu_context_end)
