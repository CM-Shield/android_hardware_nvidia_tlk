/*
 * Copyright (c) 2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __ARCH_ARM64_ASM_H
#define __ARCH_ARM64_ASM_H

/* data cache operations */
#define DCISW		0	/* invalidate data cache */
#define DCCISW		1	/* clean-invalidate data cache */
#define DCCSW		1	/* clean data cache (Bug 200077334) */

/* Cortex A57/A53 CPU IDs */
#define CORTEX_A57_MP	0xd07
#define CORTEX_A53_MP	0xd03

/* SMP enable bit */
#define CPUECTLR_SMP_BIT	(1 << 6)

/* Double lock control bit */
#define OSDLR_DBL_LOCK_BIT	1

#define MODE_EL(x)	((x) << 2)

/* flag indicating in which mode CPU is returned */
#define MON_CPU_RETURN_64	0x0
#define MON_CPU_RETURN_32	0x1

/* SPSR_EL3 register fields/settings */
#define MON_SPSR_EXC_MASKED	(0x7 << 6)

/* SCR_EL3 register fields/settings */
#define MON_SCR_NS_MODE		(0x1 << 0)
#define MON_SCR_EL3FIQ_EN	(0x1 << 2)
#define MON_SCR_RESV1		(0x3 << 4)
#define MON_SCR_HCE		(0x1 << 8)
#define MON_SCR_32BIT		(0x0 << 10)
#define MON_SCR_64BIT		(0x1 << 10)

/* ARM GIC cpu/dist offsets */
#define ARM_GIC_GICC_CTLR		0x0
#define ARM_GIC_GICC_PMR		0x4
#define ARM_GIC_GICC_IAR		0xC

#define ARM_GIC_GICD_CTLR		0x0
#define ARM_GIC_GICD_TYPER		0x4
#define ARM_GIC_GICD_IGROUPR0		0x80
#define ARM_GIC_GICD_IGROUPR1		0x84
#define ARM_GIC_GICD_ISENABLER0		0x100
#define ARM_GIC_GICD_SGIR		0xF00

/* use highest SGI ID, leaving NS kernel with low IDs */
#define MON_SECURE_FIQ_IPI	0xF

#ifdef ASSEMBLY
/* get CPU index (0 through 3) */
.macro cpuidx, tmp
	mrs	\tmp, mpidr_el1
	and	\tmp, \tmp, #0xF
.endm

/* enter secure EL1 in 32bit mode */
.macro mon_scr_secure_32, tmp
	mrs	\tmp, scr_el3
	bic	\tmp, \tmp, #(0x1 << 10)	// clear RW
	bic	\tmp, \tmp, #(0x1 << 0)		// clear NS
	msr	scr_el3, \tmp
	isb
.endm

.macro mon_scr_non_secure_64, tmp
	mrs	\tmp, scr_el3
	orr	\tmp, \tmp, #(0x1 << 10)	// set RW
	orr	\tmp, \tmp, #(0x1 << 0)		// set NS
	msr	scr_el3, \tmp
	isb
.endm

/*
 * Register aliases.
 */
lr      .req    x30             // link register
#endif

#endif
