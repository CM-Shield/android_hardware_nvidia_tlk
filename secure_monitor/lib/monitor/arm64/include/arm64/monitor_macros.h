/*
 * Copyright (c) 2013-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __MONITOR_MACROS_H
#define __MONITOR_MACROS_H

/* number of regs of each type */
#define NUM_GPR_REGS		28
#define NUM_MMU_REGS		6
#define NUM_SYS_REGS		7

#define NUM_CTX_REGS	(NUM_GPR_REGS + NUM_MMU_REGS + NUM_SYS_REGS)

/* byte offsets with context buffer */
#define CTX_GPR_OFFSET		0
#define CTX_MMU_OFFSET		(NUM_GPR_REGS << 0x3)
#define CTX_SYS_OFFSET		(CTX_MMU_OFFSET + (NUM_MMU_REGS << 0x3))

.macro mon_save_el1_gpr, ctxptr
	/* save general purpose CPU state */
	stp	x4, x5,   [\ctxptr, #CTX_GPR_OFFSET]
	stp	x6, x7,   [\ctxptr, #(CTX_GPR_OFFSET + 0x10)]
	stp	x8, x9,   [\ctxptr, #(CTX_GPR_OFFSET + 0x20)]
	stp	x10, x11, [\ctxptr, #(CTX_GPR_OFFSET + 0x30)]
	stp	x12, x13, [\ctxptr, #(CTX_GPR_OFFSET + 0x40)]
	stp	x14, x15, [\ctxptr, #(CTX_GPR_OFFSET + 0x50)]
	stp	x16, x17, [\ctxptr, #(CTX_GPR_OFFSET + 0x60)]
	stp	x18, x19, [\ctxptr, #(CTX_GPR_OFFSET + 0x70)]
	stp	x20, x21, [\ctxptr, #(CTX_GPR_OFFSET + 0x80)]
	stp	x22, x23, [\ctxptr, #(CTX_GPR_OFFSET + 0x90)]
	stp	x24, x25, [\ctxptr, #(CTX_GPR_OFFSET + 0xA0)]
	stp	x26, x27, [\ctxptr, #(CTX_GPR_OFFSET + 0xB0)]
	mrs	x27, sp_el1
	stp	x28, x27, [\ctxptr, #(CTX_GPR_OFFSET + 0xC0)]
	stp	x29, x30, [\ctxptr, #(CTX_GPR_OFFSET + 0xD0)]
.endm

.macro mon_save_el1_mmu, ctxptr
	/* save MMU related CPU state */

	.if (\ctxptr == x0 || \ctxptr == x1)
	.error "can't use x0 or x1 as ctxptr (used internally)."
	.endif

	mrs	x0, mair_el1
	mrs	x1, tcr_el1
	stp	x0, x1, [\ctxptr, #CTX_MMU_OFFSET]
	mrs	x0, ttbr0_el1
	mrs	x1, ttbr1_el1
	stp	x0, x1, [\ctxptr, #(CTX_MMU_OFFSET + 0x10)]
	mrs	x0, contextidr_el1
	mrs	x1, sctlr_el1		/* for M bit */
	stp	x0, x1, [\ctxptr, #(CTX_MMU_OFFSET + 0x20)]
.endm

.macro mon_save_el1_sys, ctxptr
	/* save SYS related CPU state */

	.if (\ctxptr == x0 || \ctxptr == x1)
	.error "can't use x0 or x1 as ctxptr (used internally)."
	.endif

	mrs	x0, cpacr_el1
	mrs	x1, actlr_el1
	stp	x0, x1, [\ctxptr, #CTX_SYS_OFFSET]
	mrs	x0, vbar_el1
	mrs	x1, tpidr_el1
	stp	x0, x1, [\ctxptr, #(CTX_SYS_OFFSET + 0x10)]
	mrs	x0, spsr_el3
	mrs	x1, elr_el3
	stp	x0, x1, [\ctxptr, #(CTX_SYS_OFFSET + 0x20)]
	mrs	x0, tpidrro_el0
	str	x0, [\ctxptr, #(CTX_SYS_OFFSET + 0x30)]
.endm

.macro mon_save_el1_regs, ctxptr
	mon_save_el1_gpr \ctxptr
	mon_save_el1_mmu \ctxptr
	mon_save_el1_sys \ctxptr
.endm

.macro mon_restore_el1_gpr, ctxptr
	/* restore general purpose CPU state */

	ldp	x4, x5,   [\ctxptr, #CTX_GPR_OFFSET]
	ldp	x6, x7,   [\ctxptr, #(CTX_GPR_OFFSET + 0x10)]
	ldp	x8, x9,   [\ctxptr, #(CTX_GPR_OFFSET + 0x20)]
	ldp	x10, x11, [\ctxptr, #(CTX_GPR_OFFSET + 0x30)]
	ldp	x12, x13, [\ctxptr, #(CTX_GPR_OFFSET + 0x40)]
	ldp	x14, x15, [\ctxptr, #(CTX_GPR_OFFSET + 0x50)]
	ldp	x16, x17, [\ctxptr, #(CTX_GPR_OFFSET + 0x60)]
	ldp	x18, x19, [\ctxptr, #(CTX_GPR_OFFSET + 0x70)]
	ldp	x20, x21, [\ctxptr, #(CTX_GPR_OFFSET + 0x80)]
	ldp	x22, x23, [\ctxptr, #(CTX_GPR_OFFSET + 0x90)]
	ldp	x24, x25, [\ctxptr, #(CTX_GPR_OFFSET + 0xA0)]
	ldp	x26, x27, [\ctxptr, #(CTX_GPR_OFFSET + 0xB0)]
	ldp	x28, x29, [\ctxptr, #(CTX_GPR_OFFSET + 0xC0)]
	msr	sp_el1, x29
	ldp	x29, x30, [\ctxptr, #(CTX_GPR_OFFSET + 0xD0)]
.endm

.macro mon_restore_el1_mmu, ctxptr
	/* restore MMU related CPU state */

	.if (\ctxptr == x0 || \ctxptr == x1)
	.error "can't use x0 or x1 as ctxptr (used internally)."
	.endif

	ldp	x0, x1, [\ctxptr, #CTX_MMU_OFFSET]
	msr	mair_el1, x0
	msr	tcr_el1, x1
	ldp	x0, x1, [\ctxptr, #(CTX_MMU_OFFSET + 0x10)]
	msr	ttbr0_el1, x0
	msr	ttbr1_el1, x1
	ldp	x0, x1, [\ctxptr, #(CTX_MMU_OFFSET + 0x20)]
	msr	contextidr_el1, x0
	msr	sctlr_el1, x1		/* for M bit */
	isb
.endm

.macro mon_restore_el1_sys, ctxptr
	/* save SYS related CPU state */

	.if (\ctxptr == x0 || \ctxptr == x1)
	.error "can't use x0 or x1 as ctxptr (used internally)."
	.endif

	ldp	x0, x1, [\ctxptr, #CTX_SYS_OFFSET]
	msr	cpacr_el1, x0
	msr	actlr_el1, x1
	ldp	x0, x1, [\ctxptr, #(CTX_SYS_OFFSET + 0x10)]
	msr	vbar_el1, x0
	msr	tpidr_el1, x1
	ldp	x0, x1, [\ctxptr, #(CTX_SYS_OFFSET + 0x20)]
	msr	spsr_el3, x0
	msr	elr_el3, x1
	ldr	x0, [\ctxptr, #(CTX_SYS_OFFSET + 0x30)]
	msr	tpidrro_el0, x0
	isb
.endm

.macro mon_restore_el1_regs, ctxptr
	mon_restore_el1_mmu \ctxptr
	mon_restore_el1_sys \ctxptr
	mon_restore_el1_gpr \ctxptr
.endm

#endif
