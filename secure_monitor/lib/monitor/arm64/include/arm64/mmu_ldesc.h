/*
 * Copyright (c) 2013-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __ARCH_ARM_MMU_LDESC_H
#define __ARCH_ARM_MMU_LDESC_H

#include <arch/defines.h>

#define MMU_MEMORY_SET_ATTR_IDX(val)	(((val) & 0x7) << 2)

#define MMU_MEMORY_WRITE_BACK_NO_ALLOCATE	0xE
#define MMU_MEMORY_WRITE_BACK_ALLOCATE		0xF

/* permissions */
#define MMU_MEMORY_AP_P_RW_U_NA	((0x0 << 7) | (0x0 << 6))
#define MMU_MEMORY_AP_P_RW_U_RW	((0x0 << 7) | (0x1 << 6))
#define MMU_MEMORY_AP_P_RO_U_NA	((0x1 << 7) | (0x0 << 6))
#define MMU_MEMORY_AP_P_RO_U_RO	((0x1 << 7) | (0x1 << 6))

/* shareable */
#define MMU_MEMORY_SH_NON_SHAREABLE	((0x0 << 9) | (0x0 << 8))
#define MMU_MEMORY_SH_OUTER_SHAREABLE	((0x1 << 9) | (0x0 << 8))
#define MMU_MEMORY_SH_INNER_SHAREABLE	((0x1 << 9) | (0x1 << 8))

#define MMU_MEMORY_NON_GLOBAL	(1 << 11)
#define MMU_MEMORY_ACCESS_FLAG	(1 << 10)
#define MMU_MEMORY_NON_SECURE	(1 << 5)

/* tcr */
#define MMU_MEMORY_TCR_T0SZ(x)		((64 - (x)) << 0)
#define MMU_MEMORY_TCR_T1SZ(x)		((64 - (x)) << 16)

#define MMU_MEMORY_TCR_SH0_NON_SHARED	(0 << 12)
#define MMU_MEMORY_TCR_TG0_4K		(0 << 14)
#define MMU_MEMORY_TCR_TG0_16K		(2 << 14)
#define MMU_MEMORY_TCR_TG0_64K		(1 << 14)
#define MMU_MEMORY_TCR_PS_40BIT		(2 << 16)
#define MMU_MEMORY_TCR_ASID_IN_TTBR0	(0 << 22)
#define MMU_MEMORY_TCR_SH1_NON_SHARED	(0 << 28)
#define MMU_MEMORY_TCR_TG1_4K		(2 << 30)
#define MMU_MEMORY_TCR_TG1_16K		(1 << 30)
#define MMU_MEMORY_TCR_TG1_64K		(3 << 30)
#define MMU_MEMORY_TCR_IPS_40BIT	(2 << 32)
#define MMU_MEMORY_TCR_ASID_16BIT	(1 << 36)

#define MMU_MEMORY_RGN_NON_CACHEABLE		0
#define MMU_MEMORY_RGN_WRITE_BACK_ALLOCATE	1
#define MMU_MEMORY_RGN_WRITE_THROUGH		2
#define MMU_MEMORY_RGN_WRITE_BACK_NO_ALLOCATE	3

#define MMU_MEMORY_TCR_OUTER_RGN1(val)	(((val) & 0x3) << 26)
#define MMU_MEMORY_TCR_OUTER_RGN0(val)	(((val) & 0x3) << 10)
#define MMU_MEMORY_TCR_INNER_RGN1(val)	(((val) & 0x3) << 24)
#define MMU_MEMORY_TCR_INNER_RGN0(val)	(((val) & 0x3) << 8)

/* page granule (using 4K) */
#define MMU_PAGE_GRANULE_4K	(12)
#define MMU_PAGE_GRANULE_16K	(14)
#define MMU_PAGE_GRANULE_64K	(16)

#define MMU_ENTRY_SHIFT         (3)	/* each entry takes 2^3 (8) bytes */
#define MMU_ENTRY_PHYS_ADDR_MSB (48)	/* block/table holds max 2^48 phys addr */

#define MMU_ENTRIES_PER_LEVEL_BITS	(MMU_PAGE_GRANULE - MMU_ENTRY_SHIFT)
#define MMU_ENTRIES_PER_LEVEL		(1 << MMU_ENTRIES_PER_LEVEL_BITS)
#define MMU_ENTRIES_LEVEL_MASK  	(MMU_ENTRIES_PER_LEVEL - 1)

#define MMU_L3_INDEX_LSB        (MMU_PAGE_GRANULE)
#define MMU_L2_INDEX_LSB        (MMU_L3_INDEX_LSB + MMU_ENTRIES_PER_LEVEL_BITS)
#define MMU_L1_INDEX_LSB        (MMU_L2_INDEX_LSB + MMU_ENTRIES_PER_LEVEL_BITS)
#define MMU_L0_INDEX_LSB        (MMU_L1_INDEX_LSB + MMU_ENTRIES_PER_LEVEL_BITS)

/* table descriptor level 2 */
#define MMU_MEMORY_TTBR_L2_VADDR_SHIFT	MMU_L2_INDEX_LSB
#define MMU_MEMORY_TTBR_L2_INDEX_BITS	MMU_ENTRIES_PER_LEVEL_BITS
#define MMU_MEMORY_TTBR_L2_INDEX_MASK	MMU_ENTRIES_LEVEL_MASK
#define MMU_MEMORY_TTBR_L2_SIZE		(1 << (MMU_MEMORY_TTBR_L2_INDEX_BITS + 3))

#define MMU_L2_BLOCK_SHIFT	(MMU_L2_INDEX_LSB)
#define MMU_L2_BLOCK_SIZE	(1 << MMU_L2_BLOCK_SHIFT)

/* table descriptor level 3 */
#define MMU_MEMORY_TTBR_L3_VADDR_SHIFT	MMU_PAGE_GRANULE
#define MMU_MEMORY_TTBR_L3_INDEX_BITS	MMU_ENTRIES_PER_LEVEL_BITS
#define MMU_MEMORY_TTBR_L3_INDEX_MASK	MMU_ENTRIES_LEVEL_MASK
#define MMU_MEMORY_TTBR_L3_SIZE		(1 << (MMU_MEMORY_TTBR_L3_INDEX_BITS + 3))

/* phys address of next table (for level 2/level 3 tables) */
#define MMU_MEMORY_TABLE_ADDR_ALIGN	(1 << MMU_PAGE_GRANULE)
#define MMU_MEMORY_TABLE_ADDR_BITS	(MMU_ENTRY_PHYS_ADDR_MSB - MMU_PAGE_GRANULE)
#define MMU_MEMORY_TABLE_ADDR_MASK	(((1UL << MMU_MEMORY_TABLE_ADDR_BITS) - 1) \
						<< MMU_PAGE_GRANULE)
#endif
