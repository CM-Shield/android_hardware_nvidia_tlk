/*
 * Copyright (c) 2014-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <config.h>
#include <asm.h>
#include <arch/arm.h>
#include <arm64/asm.h>
#include <psci.h>
#include <arm64/monitor_macros.h>
#include <platform/memmap.h>

#define CORTEX_A53_MP_R0	0x410fd03
#define FLOW_CTRL_DBG_QUAL	0x50
#define nLEGACYFIQ_PPI		(1 << 28)

#define TIMER_TICKS_512_BEFORE_RETENTION	7

/*
 * ARMv8 allows max 16 breakpoints and 16 watchpoints. However,
 * luckily for us, Denver, Cortex-A57, and Cortex-A53 all implements
 * 6 breakpoints and 4 watchpoints.
 */
#define ARM_MAX_BRP_WRP		10

/* called both for cold reset and boot_secondary */
FUNCTION(mon_init_cpu)
	mrs	x4, currentel
	cmp	x4, #MODE_EL(3)
	b.ne	.		// error, if not EL3

	/* initialize SCR to secure state */
	mov	x3, #(MON_SCR_RESV1 | MON_SCR_64BIT)
	msr	scr_el3, x3
	isb

	/* set vbar (with phys, to catch setup errors) */
	adr	x3, _vector_el3
	msr	vbar_el3, x3

	/* Check if ARM Cortex-A53 with revisions R0P0, R0P1, R0P2 */
	mrs	x3, MIDR_EL1
	lsr	x4, x3, #4
	ldr	x12, =CORTEX_A53_MP_R0
	cmp	x4, x12

	b.ne	post_a53_erratum
	and	x4, x3, #0xf
	cmp	x4, #2
	b.gt	post_a53_erratum

	/*
	 * Cortex-A53 erratum 826319: disable evict and writeevict
	 * transactions
	 */
	mrs	x3, s3_1_c15_c0_0
	orr	x3, x3, #8		// disable clean/evict
	mov	x4, #0x4000
	bic	x3, x3, x4		// enable UniqueClean eviction
	msr	s3_1_c15_c0_0, x3

post_a53_erratum:
#if !defined(MONTARGET_DENVER)
	/* set SMPEN for A57/A53 */
	mrs	x0, s3_1_c15_c2_1
	orr	x0, x0, #CPUECTLR_SMP_BIT
	orr	x0, x0, #TIMER_TICKS_512_BEFORE_RETENTION
	msr	s3_1_c15_c2_1, x0

	mrs	x0, s3_1_c11_c0_3
	bic	x0, x0, #0x7
	orr	x0, x0, #TIMER_TICKS_512_BEFORE_RETENTION
	msr	s3_1_c11_c0_3, x0
#endif

	/* enable I cache, disable MMU and alignment checks */
	mrs	x4, sctlr_el3
	bic	x4, x4, #(1 << 25)
	orr	x4, x4, #(1 << 12)
	bic	x4, x4, #((1 << 2) | (1 << 1) | (1 << 0))
	msr	sctlr_el3, x4

	/* set freq for arch general timer */
	ldr	x0, =ARM_SYSTEM_COUNTER_FREQ
	msr	cntfrq_el0, x0

	/* allow non-privileged access to CNTVCT */
	mrs	x0, cntkctl_el1
	orr	x0, x0, #(1 << 1)
	msr	cntkctl_el1, x0

	/* enable the cycle count register */
	mrs	x0, pmcr_el0
	ubfx	x0, x0, #11, #5		// read PMCR.N field
	mov	x4, #1
	lsl	x0, x4, x0
	sub	x0, x0, #1		// mask of event counters
	orr	x0, x0, #0x80000000	// disable overflow intrs
	msr	pmintenclr_el1, x0
	msr	pmuserenr_el0, x4	// enable user mode access

#if !defined(MONTARGET_NO_FLOW_CONTROLLER) && defined(TARGET_T210)
	/* forward nLEGACYFIQ to the GICD */
	ldr	x4, =TEGRA_FLOW_CTRL_BASE
	ldr	w3, [x4, FLOW_CTRL_DBG_QUAL]
	orr	w3, w3, #nLEGACYFIQ_PPI
	str	w3, [x4, FLOW_CTRL_DBG_QUAL]
#endif

	/* mark per-CPU nLEGACYFIQ and FIQ IPI as secure */
	ldr	x4, =ARM_GIC_DIST_BASE
	mov	w3, #~nLEGACYFIQ_PPI
	bic	w3, w3, #(1 << MON_SECURE_FIQ_IPI)
	str	w3, [x4, ARM_GIC_GICD_IGROUPR0]

	/* forward nLEGACYFIQ to GICC */
	mov	w3, #nLEGACYFIQ_PPI
	str	w3, [x4, ARM_GIC_GICD_ISENABLER0]

	/* enable group 0/1 in the distributor */
	ldr	w3, [x4, ARM_GIC_GICD_CTLR]
	orr	w3, w3, #(1 << 1) | (1 << 0)
	str	w3, [x4, ARM_GIC_GICD_CTLR]

	/* enables GROUP0/GROUP1 intrs, signals GROUP0 with FIQ */
	ldr	x4, =ARM_GIC_CPU_BASE
	mov	w3, #((0xF << 5) | (0x1 << 3) | (0x3 << 0))
	str	w3, [x4, ARM_GIC_GICC_CTLR]

	/* init low pri mask, so NS can set its value */
	mov	w3, #0xFF
	str	w3, [x4, ARM_GIC_GICC_PMR]

	/* disable copro traps to EL3 */
	msr	cptr_el3, xzr

	cpuidx	x12

	/* setup per-cpu monitor stack (dividing up single 4K page) */
	msr	spsel, #1
	ldr	x3, =monitor_stack_top
	lsl	x4, x12, #10	// each CPU gets a 1K stack
	sub	x3, x3, x4
	mov	sp, x3

	ret
ENDFUNC(mon_init_cpu)

/*
 * Return to address saved in __mon_cpu_return_addr, in
 * AARCH32 SVC (non-secure) mode.
 */
FUNCTION(mon_return_aarch32_ns)
	/* load return address */
	cpuidx	x1
	adr	x2, __mon_cpu_return_addr
	ldr	x2, [x2, x1, lsl #3]

	msr	elr_el3, x2
	mov	x2, #(MON_SCR_RESV1 | MON_SCR_32BIT | MON_SCR_NS_MODE | \
			MON_SCR_HCE)
	msr	scr_el3, x2
	mov	x2, #(MON_SPSR_EXC_MASKED | MODE_SVC)
	msr	spsr_el3, x2

	eret
ENDFUNC(mon_return_aarch32_ns)

/*
 * Return to address saved in __mon_cpu_return_addr, in
 * AARCH64 EL2 (non-secure) mode.
 */
FUNCTION(mon_return_aarch64_ns)
	/* load return address */
	cpuidx	x1
	adr	x2, __mon_cpu_return_addr
	ldr	x2, [x2, x1, lsl #3]

	msr	elr_el3, x2
	mov	x2, #(MON_SCR_RESV1 | MON_SCR_64BIT | MON_SCR_NS_MODE | \
			MON_SCR_HCE)
	orr	x2, x2, #MON_SCR_EL3FIQ_EN
	msr	scr_el3, x2
	mov	x2, #(MON_SPSR_EXC_MASKED | MODE_EL(2))
	msr	spsr_el3, x2

	eret
ENDFUNC(mon_return_aarch64_ns)

/*
 * Routine to setup secondary CPU state and return, leaving
 * the primary CPU to initialize the secureos.
 */
FUNCTION(boot_secondary)
	bl	mon_init_cpu
	bl	mon_enable_mmu
	/* reload vbar with virt addr */
	adr	x0, _vector_el3
	msr	vbar_el3, x0
	isb

	bl	mon_cpu_dbg_restore

	cpuidx	x0
	bl	platform_psci_cpu_has_reset
	b	mon_return_aarch64_ns

/* get the CPU ID */
FUNCTION(mon_get_cpu_id)
	mrs	x0, midr_el1
	ubfx	x0, x0, #4, #12
	ret
ENDFUNC(boot_secondary)

.ltorg
.align 6
.global __mon_cpu_reset_vector
__mon_cpu_reset_vector:
#if !defined(MONTARGET_DENVER)
	/*
	 * code in this #if block are approved by HW and recommended
	 * by ARM. do NOT touch it.
	 */
	mrs	x4, s3_1_c15_c2_0
	orr	x4, x4, #1
	msr	s3_1_c15_c2_0, x4      /* inv BTB when invalidating icache */
	dsb	sy
	isb
	ic	iallu                  /* really invalidating icache & BTB */
	dsb	sy
	isb
	bic	x4, x4, #1
	msr	s3_1_c15_c2_0, x4      /* restore original cpuactlr_el1 */
	dsb	sy
	isb
	.rept 7
	nop                            /* wait */
	.endr

	mrs	x0, oslsr_el1
	and	x0, x0, #2             /* extract oslk bit */
	mrs	x1, mpidr_el1
	bics	xzr, x0, x1, lsr #7    /* 0 if slow cluster or warm reset */
	b.eq	__restore_oslock
	mov	x0, xzr
	msr	oslar_el1, x0          /* os lock stays off across warm reset */
	mov	x3, #3
	movz	x4, #0x8000, lsl #48
	msr	s3_1_c15_c2_0, x4      /* turn off RCG */
	isb
	msr	rmr_el3, x3            /* request warm reset */
	isb
	dsb	sy
	wfi
	.rept 65
	nop	/* these nops are here so that speculative execution */
		/* won't do harm before we are done warm reset */
		/* do not insert instructions here */
	.endr
__restore_oslock:
	mov	x0, #1
	msr	oslar_el1, x0
	tst	x1, #(1<<8)
	b.eq	boot_secondary
	mrs	x4, revidr_el1
	cmp	x4, #0xfc
	b.eq	boot_secondary
	mrs	x1, s3_1_c15_c2_0
	orr	x1, x1, #(1<<29)
	msr	s3_1_c15_c2_0, x1
	isb
#endif
	b	boot_secondary

.ltorg
.align 3
.global __mon_cpu_return_addr
__mon_cpu_return_addr:
	.rept MONCPUS
	.quad 0
	.endr

.ltorg
.align 3
.global __mon_cpu_return_mode
__mon_cpu_return_mode:
	.rept MONCPUS
	.quad 0
	.endr

/*
 * CPU power down sequence as per A57/A53 TRM
 *
 * x0 - L2 flush?
 *
 */
FUNCTION(mon_cpu_power_down)
	/* Store L2 cache flush request */
	mov	x13, x0

	/* 1. Stop allocations to our data cache */
	mrs	x0, sctlr_el1
	bic	x0, x0, #1 << 2		// clear SCTLR.C
	msr	sctlr_el1, x0
	isb

	mrs	x0, sctlr_el3
	bic	x0, x0, #1 << 2		// clear SCTLR.C
	msr	sctlr_el3, x0
	isb

	mrs	x0, midr_el1
	ubfx	x0, x0, #4, #12
	cmp	x0, #CORTEX_A57_MP
	b.ne	not_a57

	/* 2. Disable L2 prefetch */
	mrs	x0, s3_1_c15_c2_1	// CPUECTLR_EL1
	/* CPUECTLR[38], disable table walk descriptor access L2 prefetch */
	orr	x0, x0, #1 << 38
	/*
	 * CPUECTLR[36:35] L2 instruction fetch prefetch distance
	 * 0 => disable instruction prefetch
	 */
	bic	x0, x0, #3 << 35
	/*
	 * CPUECTLR[33:32] L2 load/store prefetch distance
	 * 0 => disable instruction prefetch
	 */
	bic	x0, x0, #3 << 32
	msr	s3_1_c15_c2_1, x0

	/* 3. ISB to ensure ectlr write is complete */
	isb

	/* 4. DSB to ensure prior prefetches are complete */
	dsb	sy

not_a57:
	/* 5. Clean and invalidate L1 and L2 if X13 == 1 */
	mov	x0, #DCCISW
	cmp	x13, #1
	bne	1f
	bl	dcsw_op_all
	b	2f
1:
	bl	dcsw_op_louis
2:

	/* 6. Leave coherency, clear SMPEN */
	mrs	x0, s3_1_c15_c2_1
	bic	x0, x0, #CPUECTLR_SMP_BIT
	msr	s3_1_c15_c2_1, x0

	/* 7. Ensure that the system does not send interrupts to us */
	ldr	x1, =ARM_GIC_CPU_BASE
	mov	w0, #0x1E0
	str	w0, [x1]

	/* 8. Set the DBGOSDLR.DLK, Double lock control bit */
	mrs	x0, osdlr_el1
	orr	x0, x0, #OSDLR_DBL_LOCK_BIT
	msr	osdlr_el1, x0

	/*
	 * 9. Execute an ISB instruction to ensure that all of the
	 * System register changes from the previous steps have
	 * been committed.
	 */
	isb

	/*
	 * 10. Execute a DSB instruction to ensure that all
	 * instruction cache, TLB, and branch predictor
	 * maintenance operations issued by any processor in the
	 * multiprocessor before the SMPEN bit was cleared have
	 * completed.
	 */
	dsb	sy

	/* 11. wfi */
3:	wfi

	/* we never return here */
	b	3b
ENDFUNC(mon_cpu_power_down)

#if !defined(MONTARGET_DENVER)
.macro saveRP wp, num
	mrs	x0, dbg\wp\()vr\num\()_el1
	str	x0, [x1], #8
	mrs	x0, dbg\wp\()cr\num\()_el1
	str	x0, [x1], #8
.endm

.macro restoreRP wp, num
	ldr	x0, [x1], #8
	msr	dbg\wp\()vr\num\()_el1, x0
	ldr	x0, [x1], #8
	msr	dbg\wp\()cr\num\()_el1, x0
.endm

.ltorg
.align 3
__mon_cpu_dbg_ctx:
	.rept MONCPUS
	.rept 5
	.quad 0
	.endr
	.rept ARM_MAX_BRP_WRP
	.quad 0
	.quad 0
	.endr
	.endr
__mon_cpu_dbg_ctx_end:
#endif

/*
 * CPU breakpoints/watchpoints restore
 */
FUNCTION(mon_cpu_dbg_restore)
#if !defined(MONTARGET_DENVER)
	/* Don't restore if no matching save, e.g on boot */
	cpuidx	x0
	adr	x1, __mon_cpu_dbg_ctx_saved
	lsl	x2, x0, #2
	add	x1, x1, x2
	ldr	w2, [x1]
	cbnz	w2, ctx_exists
	ret
ctx_exists:
	adr	x1, __mon_cpu_dbg_ctx
	adr	x4, __mon_cpu_dbg_ctx_end
	sub	x4, x4, x1
	lsr	x4, x4, #MONCPUS_SHIFT
	madd	x1, x4, x0, x1

	/* this sequence follows recommendation in ARMv8 debug achitecture */
	ldp	w0, w3, [x1], #8
	msr	osdtrrx_el1, x0
	msr	osdtrtx_el1, x3
	ldp	w0, w3, [x1], #8
	msr	mdscr_el1, x0
	msr	oseccr_el1, x3
	ldp	w0, w3, [x1], #8
	msr	mdccint_el1, x0
	msr	dbgclaimset_el1, x3
	ldp	w0, w3, [x1], #8
	msr	dbgvcr32_el2, x0
	msr	sder32_el3, x3
	ldp	w0, w3, [x1], #8
	msr	mdcr_el2, x0
	msr	mdcr_el3, x3

	restoreRP b, 0
	restoreRP b, 1
	restoreRP b, 2
	restoreRP b, 3
	restoreRP b, 4
	restoreRP b, 5
	restoreRP w, 0
	restoreRP w, 1
	restoreRP w, 2
	restoreRP w, 3
	isb
	dsb	sy
#endif
	ret
ENDFUNC(mon_cpu_dbg_restore)
#if !defined(MONTARGET_DENVER)
.ltorg
.align 2
__mon_cpu_dbg_ctx_saved:
	.rept MONCPUS
	.dword 0
	.endr
#endif

/*
 * CPU breakpoints/watchpoints save
 */
FUNCTION(mon_cpu_dbg_save)
#if !defined(MONTARGET_DENVER)
	/* prevent external debugger from modifying debug registers */
	mov	x0, #1
	msr	oslar_el1, x0
	isb

	cpuidx	x0
	adr	x1, __mon_cpu_dbg_ctx
	adr	x4, __mon_cpu_dbg_ctx_end
	sub	x4, x4, x1
	lsr	x4, x4, #MONCPUS_SHIFT
	madd	x1, x4, x0, x1

	/* this sequence follows recommendation in ARMv8 debug achitecture */
	mrs	x0, osdtrrx_el1
	mrs	x3, osdtrtx_el1
	stp	w0, w3, [x1], #8
	mrs	x0, mdscr_el1
	mrs	x3, oseccr_el1
	stp	w0, w3, [x1], #8
	mrs	x0, mdccint_el1
	mrs	x3, dbgclaimclr_el1
	stp	w0, w3, [x1], #8
	mrs	x0, dbgvcr32_el2
	mrs	x3, sder32_el3
	stp	w0, w3, [x1], #8
	mrs	x0, mdcr_el2
	mrs	x3, mdcr_el3
	stp	w0, w3, [x1], #8

	saveRP	b, 0
	saveRP	b, 1
	saveRP	b, 2
	saveRP	b, 3
	saveRP	b, 4
	saveRP	b, 5
	saveRP	w, 0
	saveRP	w, 1
	saveRP	w, 2
	saveRP	w, 3

	isb
	dsb	sy
	/* Note that we are doing a save, so it will actually get restored */
	cpuidx	x0
	adr	x1, __mon_cpu_dbg_ctx_saved
	lsl	x2, x0, #2
	add	x1, x1, x2
	mov	w0, #1
	str	w0, [x1]
#endif
	ret
ENDFUNC(mon_cpu_dbg_save)
