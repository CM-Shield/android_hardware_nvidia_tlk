/*
 * Copyright (c) 2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <asm.h>
#include <arm64/asm.h>

/* uint32_t mon_cpu_id(void); */
FUNCTION(mon_cpu_id)
	mrs	x0, mpidr_el1
	and	x0, x0, #0xf
	ret

/* void mon_atomic_or(uint32_t *ptr, uint32_t bits); */
FUNCTION(mon_atomic_or)
1:
	ldxr	w2, [x0]
	orr	w2, w2, w1
	stxr	w3, w2, [x0]
	cbnz	w3, 1b
	ret

/* void mon_atomic_and(uint32_t *ptr, uint32_t bits); */
FUNCTION(mon_atomic_and)
1:
	ldxr	w2, [x0]
	and	w2, w2, w1
	stxr	w3, w2, [x0]
	cbnz	w3, 1b
	ret

/* void *memset(void *s, int c, size_t n); */
FUNCTION(memset)
	cbz	x0, done

	/* dup 8bit x1 to 8 byte value */
	orr	x1, x1, x1, lsl #8
	orr	x1, x1, x1, lsl #16
	orr	x1, x1, x1, lsl #32

	mov	x7, #0xF
	mov	x8, x0

	/* first write 16 byte chunks */
	bics	xzr, x2, x7
	b.eq	less_than_16		// memset is < 16 bytes

do_16_bytes:
	stp	x1, x1, [x8], #16
	sub	x2, x2, #16

	bics	xzr, x2, x7
	b.ne	do_16_bytes

less_than_16:
	cbz	x2, done		// ended on a 16 byte boundary

do_single_byte:
	/* write trailing bytes */
	strb	w1, [x8], #1
	sub	x2, x2, 1
	cbnz	x2, do_single_byte
done:
	ret
