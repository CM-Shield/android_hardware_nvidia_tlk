/*
 * Copyright (c) 2013-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <config.h>
#include <asm.h>
#include <arm64/asm.h>
#include <arm64/mmu_ldesc.h>

/* MMU params for monitor mode */
#define MMU_PAGE_GRANULE	MMU_PAGE_GRANULE_4K

/* number of first/second level entries */
#define NUM_FIRST_LEVEL		(1 << (MONBITS - MMU_L1_INDEX_LSB))
#define NUM_SECOND_LEVEL	(1 << (MONBITS - MMU_L2_INDEX_LSB))

/* indices into attr indirect regs */
#define MMU_MEMORY_STRONGLY_ORDERED                     0
#define MMU_MEMORY_WB_OUTER_NO_ALLOC_INNER_ALLOC        1
#define MMU_MEMORY_UC_OUTER_UC_INNER                    2

/* mmio (index 0) */
#define MMU_PTE_L2_BLOCK_MMIO_FLAGS	\
	(MMU_MEMORY_SET_ATTR_IDX(0) | MMU_MEMORY_ACCESS_FLAG |	\
	 MMU_MEMORY_AP_P_RW_U_NA | 0x1)

/* mem wb (index 1) */
#define MMU_PTE_L2_BLOCK_MEM_WB_FLAGS	\
	(MMU_MEMORY_SET_ATTR_IDX(1) | MMU_MEMORY_ACCESS_FLAG |	\
	 MMU_MEMORY_SH_INNER_SHAREABLE | \
	 MMU_MEMORY_AP_P_RW_U_NA | 0x1)

/* mem uc (index 2) */
#define MMU_PTE_L2_BLOCK_MEM_UC_FLAGS	\
	(MMU_MEMORY_SET_ATTR_IDX(2) | MMU_MEMORY_ACCESS_FLAG |	\
	 MMU_MEMORY_AP_P_RW_U_NA | 0x1)

/* value for MAIR register:
 *	idx0 = strongly-ordered,
 *	idx1 = outer: writeback/no alloc, inner: writeback/alloc
 *	idx2 = outer: non-cacheable, inner: non-cacheable
 */
#define MMU_MEMORY_ATTR_INDIR	0x0044EF00

#define MMU_TCR_FLAGS_EL3       \
	(MMU_MEMORY_TCR_PS_40BIT | \
	 MMU_MEMORY_TCR_TG0_4K | \
	 MMU_MEMORY_TCR_OUTER_RGN0(MMU_MEMORY_RGN_WRITE_BACK_ALLOCATE) | \
	 MMU_MEMORY_TCR_INNER_RGN0(MMU_MEMORY_RGN_WRITE_BACK_ALLOCATE) | \
	 MMU_MEMORY_TCR_T0SZ(MONBITS))

.macro mmu_phys_align, base, size, tmp
	mov	\tmp, #(MMU_L2_BLOCK_SIZE - 1)
	cbz	\size, .	// size is zero
	tst	\base, \tmp
	b.ne	.		// base not block aligned
	tst	\size, \tmp
	b.ne	.		// size not block aligned
.endm

.macro mmu_load_first_level, level1, level2, count
	orr	\level2, \level2, #0x3
1:
	str	\level2, [\level1], #(1 << MMU_ENTRY_SHIFT)
	add	\level2, \level2, #(1 << MMU_PAGE_GRANULE)
	sub	\count, \count, #0x1
	cbnz	\count, 1b
.endm


/* corrupts \vaddr, \pgt, \paddr, \length \tmp */
.macro mmu_map_virt_phys, vaddr, paddr, length, pgt, flags, tmp, tmp2
	mov	\tmp, #(MMU_L2_BLOCK_SIZE - 1)
	bic	\paddr, \paddr, \tmp		// make aligned
	add	\paddr, \paddr, \flags
	lsr	\vaddr, \vaddr, #MMU_L2_BLOCK_SHIFT
	add	\pgt, \pgt, \vaddr, lsl #MMU_ENTRY_SHIFT
	mov	\tmp2, #(1 << MMU_L2_BLOCK_SHIFT)
1:
	ldr	\tmp, [\pgt]
	cbnz	\tmp, .		/* verify vaddr is not mapped */

	/* write entry and update */
	str	\paddr, [\pgt], #(1 << MMU_ENTRY_SHIFT)
	add	\paddr, \paddr, \tmp2
	sub	\length, \length, \tmp2
	cbnz	\length, 1b
	dsb	sy
.endm

/* corrupts \vaddr, \pgt, \paddr, \length \tmp */
.macro mmu_unmap_virt, vaddr, length, pgt, tmp
	lsr	\vaddr, \vaddr, #MMU_L2_BLOCK_SHIFT
	add	\pgt, \pgt, \vaddr, lsl #MMU_ENTRY_SHIFT
	mov	\tmp, #(1 << MMU_L2_BLOCK_SHIFT)
1:
	/* write entry and update */
	str	xzr, [\pgt], #(1 << MMU_ENTRY_SHIFT)
	sub	\length, \length, \tmp
	cbnz	\length, 1b
	dsb	sy
.endm

.macro mmu_roundup, addr, align
	add	\addr, \addr, \align
	bic	\addr, \addr, \align
.endm

.macro mmu_rounddown, addr, align
	bic	\addr, \addr, \align
.endm

.macro mmu_map_align, vaddr, paddr, length, eaddr, align
	mov	\align, #(MMU_L2_BLOCK_SIZE - 1)
	add	\eaddr, \length, \vaddr

	/* return aligned vaddr, paddr and length */
	mmu_roundup \eaddr, \align
	mmu_rounddown \vaddr, \align
	mmu_rounddown \paddr, \align
	sub	\length, \eaddr, \vaddr
.endm

.macro mmu_unmap_align, vaddr, length, eaddr, align
	mov	\align, #(MMU_L2_BLOCK_SIZE - 1)
	add	\eaddr, \length, \vaddr

	/* return aligned vaddr, paddr and length */
	mmu_roundup \eaddr, \align
	mmu_rounddown \vaddr, \align
	sub	\length, \eaddr, \vaddr
.endm

/* int mon_mmu_map_mmio(vaddr_t vaddr, paddr_t paddr, uint64_t length) */
FUNCTION(mon_mmu_map_mmio)
	cbz	x0, 1f

	/* align vaddr/paddr/length to BLOCK size params */
	mmu_map_align x0, x1, x2, x9, x10

	/* phys address of mon_second_level */
	adr     x3, mon_second_level	// virt addr
	ldr	x4, __mon_phys_offset
	sub	x3, x3, x4		// phys addr
	ldr	x4, =MMU_PTE_L2_BLOCK_MMIO_FLAGS

	/* create mapping (x5, x6 are scratch) */
	mmu_map_virt_phys x0, x1, x2, x3, x4, x5, x6
	dsb	sy
	isb
	tlbi	alle3
	dsb	sy
	isb

	mov	x0, xzr
1:
	ret

/* int mon_mmu_map_uncached(vaddr_t vaddr, paddr_t paddr, uint64_t length) */
FUNCTION(mon_mmu_map_uncached)
	/* align vaddr/paddr/length to BLOCK size params */
	mmu_map_align x0, x1, x2, x9, x10

	/* phys address of mon_second_level */
	adr     x3, mon_second_level	// virt addr
	ldr	x4, __mon_phys_offset
	sub	x3, x3, x4		// phys addr
	ldr	x4, =MMU_PTE_L2_BLOCK_MEM_UC_FLAGS

	/* create mapping (x5, x6 are scratch) */
	mmu_map_virt_phys x0, x1, x2, x3, x4, x5, x6
	dsb	sy
	isb
	tlbi	alle3
	dsb	sy
	isb

	mov	x0, xzr
	ret

/* int mon_mmu_unmap(vaddr_t vaddr, uint64_t length) */
FUNCTION(mon_mmu_unmap)
	/* align vaddr/length to BLOCK size params */
	mmu_unmap_align x0, x1, x9, x10

	/* phys address of mon_second_level */
	adr     x3, mon_second_level	// virt addr
	ldr	x4, __mon_phys_offset
	sub	x3, x3, x4		// phys addr

	/* unmap vaddr for length (x4 is scratch) */
	mmu_unmap_virt x0, x1, x3, x4
	dsb	sy
	isb
	tlbi	alle3
	dsb	sy
	isb
	ret

/* uint64_t mon_virt_phys_el3(uint64_t vaddr) */
FUNCTION(mon_virt_phys_el3)
	at	s1e3r, x0
	mrs	x0, par_el1
	ret

/* void mon_enable_mmu() */
FUNCTION(mon_enable_mmu)
spin_wait:
	ldr	x1, mon_pagetable_done
	cbz	x1, spin_wait		// wait for pagetable done

	ldr	x0, __mon_phys_offset
	add	lr, lr, x0		// convert phys LR to virt

	ldr	x1, =MMU_MEMORY_ATTR_INDIR
	msr	mair_el3, x1
	ldr	x1, =MMU_TCR_FLAGS_EL3
	msr	tcr_el3, x1

	adr	x4, mon_first_level	// phys addr
	msr	ttbr0_el3, x4

	tlbi	alle3
	dsb	sy
	isb

	mrs	x1, sctlr_el3
	orr	x1, x1, #1
	orr	x1, x1, #(1 << 2)
	msr	sctlr_el3, x1		// enable MMU and D cache
	isb

	ret

/* void mon_setup_pagetable(uint64_t pbase, uint64 poff, uint64 psize) */
FUNCTION(mon_setup_pagetable)
	/* check base/size alignment of carveout */
	mmu_phys_align	x0, x2, x11

	/* phys address of mon_second_level */
	ldr	x3, =mon_second_level	// virt addr
	sub	x3, x3, x1		// phys addr

	/* clear mon_second_level */
	mov	x4, #NUM_SECOND_LEVEL
	mov	x13, x3		// copy mon_second_level
1:
	stp	xzr, xzr, [x13], #16
	sub	x4, x4, #2	// 2 entries at a time
	cbnz	x4, 1b

	adr	x4, mon_first_level	// phys addr
	mov	x5, #NUM_FIRST_LEVEL
	mov	x13, x3			// ptr to mon_second_level
	mmu_load_first_level x4, x13, x5

	/* map MONBASE -> carveout in mon_second_level */
	ldr	x4, =MONBASE		// virt
	ldr	x5, =MMU_PTE_L2_BLOCK_MEM_WB_FLAGS
	mov	x10, x0			// phys (carveout base)
	mov	x12, x2			// size
	mov	x13, x3			// phys pgt
	mmu_map_virt_phys x4, x10, x12, x13, x5, x6, x7

	/* identity map carveout in mon_second_level */
	mov     x4, x0			// virt
	mov	x10, x0			// phys (carveout base)
	mov	x12, x2			// size
	mov	x13, x3			// phys pgt
	mmu_map_virt_phys x4, x10, x12, x13, x5, x6, x7
	dsb	sy
	isb

	/* indicate pagetables are done */
	adr	x14, mon_pagetable_done
	mov	x15, 0x1
	str	x15, [x14]
	ret

/* uint64_t mon_virt_to_phys(uint64_t vaddr) */
FUNCTION(mon_virt_to_phys)
	ldr	x1, __mon_phys_offset
	sub	x0, x0, x1
	ret

/* uint64_t mon_phys_to_virt(uint64_t paddr) */
FUNCTION(mon_phys_to_virt)
	ldr	x1, __mon_phys_offset
	add	x0, x0, x1
	ret

.align MMU_PAGE_GRANULE
mon_second_level:
	.rept	NUM_SECOND_LEVEL
	.quad	0
	.endr

.align MMU_PAGE_GRANULE
mon_first_level:
	.rept	NUM_FIRST_LEVEL
	.quad	0
	.endr

mon_pagetable_done:
	.quad	0

.align 3
.global __mon_phys_offset
__mon_phys_offset:
	.quad 0
