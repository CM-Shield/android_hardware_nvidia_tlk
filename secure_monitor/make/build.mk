# comment out or override if you want to see the full output of each command
NOECHO ?= @

$(MONBIN): $(MONELF)
	@echo generating image: $@
	$(NOECHO)$(MON_OBJCOPY) -O binary $< $@

# This target builds the barebones tos.img from monitor.bin
$(TOSIMAGE): $(MONBIN)
	@echo generating image: $@
	tools/gen_tos_part_img.py $< $@

$(MONELF): $(ALLMONITOR_OBJS) $(MON_LINKER_SCRIPT) $(VRRLIB)
	@echo linking $@
	$(NOECHO)$(MON_LD) $(GLOBAL_LDFLAGS) -T $(MON_LINKER_SCRIPT) --whole-archive $(VRRLIB) --no-whole-archive $(ALLMONITOR_OBJS) $(LIBGCC) -o $@

# This builds a static library from platform/monitor for ote/tlk to link in
$(MONLIB): $(ALLMODULE_OBJS)
	@echo building platform monitor static library $@
	$(AR) $(GLOBAL_ARFLAGS) $@ $(ALLMODULE_OBJS)
