#
# Copyright (c) 2013-2014, NVIDIA CORPORATION. All rights reserved
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

# tos.img is considered to be 32-bit
ifneq (,$(TARGET_2ND_ARCH))
LOCAL_2ND_ARCH_VAR_PREFIX := $(TARGET_2ND_ARCH_VAR_PREFIX)
endif

ifeq (tlk,$(SECURE_OS_BUILD))

ifeq (t124,$(TARGET_TEGRA_VERSION))

$(warning SECURE_OS_BUILD. Making libmonitor.a)
# building a monitor library
LOCAL_MODULE := libmonitor
LOCAL_MODULE_CLASS := STATIC_LIBRARIES
LOCAL_UNINSTALLABLE_MODULE := true
LOCAL_BUILT_MODULE_STEM := libmonitor.a
STANDALONE_MONITOR := false
MONITOR_LIBRARY := true
OUTFILE_EXTENSION := .a

else # TARGET_TEGRA_VERSION != t124

$(warning SECURE_OS_BUILD. Making monitor.bin)
# building a monitor binary
LOCAL_MODULE := monitor.bin
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_UNINSTALLABLE_MODULE := true
LOCAL_BUILT_MODULE_STEM := monitor.bin
STANDALONE_MONITOR := false
MONITOR_LIBRARY := false

endif # TARGET_TEGRA_VERSION == t124

else # SECURE_OS_BUILD != tlk

ifeq (t124,$(TARGET_TEGRA_VERSION))

$(warning Non SECURE_OS_BUILD for T124 - Nothing to do)
# t124, non secure: No nothing
LOCAL_MODULE :=
LOCAL_MODULE_CLASS :=
LOCAL_UNINSTALLABLE_MODULE :=
LOCAL_BUILT_MODULE_STEM :=
STANDALONE_MONITOR := false
MONITOR_LIBRARY := false

else # TARGET_TEGRA_VERSION != t124

$(warning Non SECURE_OS_BUILD. Making monitor.bin and tos.img)
# building a monitor binary and tos.img
LOCAL_MODULE := monitor.bin
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_UNINSTALLABLE_MODULE := true
LOCAL_BUILT_MODULE_STEM := monitor.bin
STANDALONE_MONITOR := true
MONITOR_LIBRARY := false

endif # TARGET_TEGRA_VERSION == t124

endif # SECURE_OS_BUILD == tlk

ifneq (,$(LOCAL_BUILT_MODULE_STEM))

ifeq ($(LOCAL_MODULE_CLASS),EXECUTABLES)
# monitor.bin_intermediates
MODULE_INTERMEDIATES := $(call intermediates-dir-for,$(LOCAL_MODULE_CLASS),$(LOCAL_BUILT_MODULE_STEM),,,$(LOCAL_2ND_ARCH_VAR_PREFIX))
else
# libmonitor_intermediates
MODULE_INTERMEDIATES := $(call intermediates-dir-for,$(LOCAL_MODULE_CLASS),$(LOCAL_MODULE),,,$(LOCAL_2ND_ARCH_VAR_PREFIX))
endif

PROJECT := tegra
OUTFILE := $(MODULE_INTERMEDIATES)/$(LOCAL_BUILT_MODULE_STEM)

# if building a monitor binary, if available, link against a VRR lib
ifeq ($(LOCAL_MODULE), monitor.bin)
ifneq ($(wildcard $(LOCAL_PATH)/../ote/vrr/Android.mk),)
VRRLIB := $(call intermediates-dir-for,STATIC_LIBRARIES,libvrr)/libvrr.a
endif
endif

ifeq ($(STANDALONE_MONITOR),true)
TOSIMAGE := $(PRODUCT_OUT)/tos.img
ALL_MODULES.$(LOCAL_MODULE).INSTALLED := $(TOSIMAGE)
endif

ifeq ($(TARGET_ARCH),arm64)
LK_TOOLCHAIN_PREFIX := prebuilts/gcc/$(HOST_PREBUILT_TAG)/arm/arm-eabi-4.8/bin/arm-eabi-
LK_TOOLCHAIN_PREFIX64 := $(TARGET_TOOLS_PREFIX)
else
LK_TOOLCHAIN_PREFIX := $(ARM_EABI_TOOLCHAIN)/arm-eabi-
LK_TOOLCHAIN_PREFIX64 := $(ARM_EABI_TOOLCHAIN)/../../../aarch64/aarch64-linux-android-4.8/bin/aarch64-linux-android-
endif

$(OUTFILE): PRIVATE_CUSTOM_TOOL_ARGS := PROJECT=$(PROJECT) \
		TARGET=$(TARGET_TEGRA_VERSION) \
		TARGET_TEGRA_FAMILY=$(TARGET_TEGRA_FAMILY) \
		TOOLCHAIN_PREFIX=$(abspath $(LK_TOOLCHAIN_PREFIX)) \
		TOOLCHAIN_PREFIX64=$(abspath $(LK_TOOLCHAIN_PREFIX64)) \
		PREFIX=$(abspath $(MODULE_INTERMEDIATES)) \
		STANDALONE_MONITOR=$(STANDALONE_MONITOR) \
		MONITOR_LIBRARY=$(MONITOR_LIBRARY) \
		VRRLIB=$(abspath $(VRRLIB)) \
		TOSIMAGE=$(abspath $(TOSIMAGE)) \
		-C $(LOCAL_PATH)

$(OUTFILE): PRIVATE_MODULE := $(LOCAL_MODULE)
# Depend on tasks when we are doing a full build.
# For one shot builds, (mm, mmm) do not.
ifeq (,$(ONE_SHOT_MAKEFILE))
$(OUTFILE): $(VRRLIB)
endif
$(OUTFILE):
	@echo "target Generated: $(PRIVATE_MODULE)"
	@mkdir -p $(dir $@)
	$(hide) $(MAKE) $(PRIVATE_CUSTOM_TOOL_ARGS)

$(TOSIMAGE): $(OUTFILE)

.PHONY: $(OUTFILE)

# Needed to clean tos.img
PRIVATE_CLEAN_FILES := $(TOSIMAGE)

ifeq ($(LOCAL_2ND_ARCH_VAR_PREFIX),)
ALL_NVIDIA_MODULES += $(LOCAL_MODULE)
else
ALL_NVIDIA_MODULES += $(LOCAL_MODULE)_32
endif
include $(BUILD_SYSTEM)/base_rules.mk

# Clean variables
PROJECT :=
OUTFILE :=
TOSIMAGE :=
LK_TOOLCHAIN_PREFIX :=
LK_TOOLCHAIN_PREFIX64 :=
STANDALONE_MONITOR :=
MONITOR_LIBRARY :=
OUTFILE_EXTENSION :=
VRRLIB :=

endif
