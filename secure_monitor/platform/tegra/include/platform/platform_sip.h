/*
 * Copyright (c) 2012-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* Silicon Partner SMCs */
#define SMC_SIP_PROGRAM_VPR			0x82000003
#define SMC_SIP_REGISTER_FIQ_GLUE		0x82000005
#define SMC_SIP_DEEP_SLEEP_ENTRY		0x82000010
#define SMC_SIP_VRR_SET_BUF			0x82000011
#define SMC_SIP_VRR_SEC 			0x82000012
#define SMC_SIP_REGISTER_LP0_IRAM_VECTOR	0x82000013

#define SMC_SIP_DEVICE_SUSPEND			0x84000001
#define SMC_SIP_CPU_RESET_VECTOR		0x84000003

/* SIP SMCs for SOC-specific services */
#define SMC_SIP_SOC_PRIVATE_BEGIN		0x82FFFF00
#define SMC_SIP_SOC_PRIVATE_END			0x82FFFFFF
