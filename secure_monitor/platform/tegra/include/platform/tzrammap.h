/*
 * Copyright (c) 2012-2013, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __PLATFORM_TEGRA_TZRAMMAP_H
#define __PLATFORM_TEGRA_TZRAMMAP_H

#define CPU_NOT_IN_LP		0
#define CPU_IN_LP0		1
#define CPU_IN_LP1		2
#define CPU_IN_LP2		3

/*
 * These offsets describe the layout of TZRAM as setup for LP1 resume.
 *
 * When resuming from LP1 the code at TZRAM_LP1_RESUME_HANDLER is
 * executed.  While executing this code, DRAM will be in self-refresh
 * and so all required state must be first copied into TZRAM
 * prior to entering LP1 so that it is present for later resume from LP1.
 */

/* stores DRAM address of non-secure resume routine */
#define TZRAM_NS_RESUME_ADDR		(TEGRA_TZRAM_BASE + 0x1000)

/* stores DRAM address of boot_secondary_cpu routine restored by LP1 resume */
#define TZRAM_BOOT_SECONDARY_CPU_ADDR	(TEGRA_TZRAM_BASE + 0x1004)

/* indicates if we should avoid switch to CLKM during LP1 resume */
#define TZRAM_CPU_AVOID_CLKM_SWITCH	(TEGRA_TZRAM_BASE + 0x1008)

/* stores DRAM address of top of monitor stack */
#define TZRAM_MON_STACK_TOP		(TEGRA_TZRAM_BASE + 0x1010)

/* stores DRAM address of monitor vector base (MVBAR) */
#define TZRAM_MVBAR		(TEGRA_TZRAM_BASE + 0x1020)

/* stores contents of cpu_context for use in in LP1 resume */
#define TZRAM_CPU_CONTEXT		(TEGRA_TZRAM_BASE + 0x1030)

/* TZRAM offset of temporary SVC stack used in LP1 resume */
#define TZRAM_TEMP_SVC_SP		(TEGRA_TZRAM_BASE + 0x1080)

/* TZRAM offset of LP1 resume handler (needs LP1_RESUME_HANDLER_SIZE bytes) */
#define TZRAM_LP1_RESUME_HANDLER	(TEGRA_TZRAM_BASE + 0x2000)

#define TZRAM_STORE(offset,value)	(*(volatile uint32_t *)(offset) = value)

#endif /*__PLATFORM_TEGRA_TZRAMMAP_H */
