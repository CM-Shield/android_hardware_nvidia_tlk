/*
 * Copyright (c) 2008 Travis Geiselbrecht
 * Copyright (c) 2012-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __PLATFORM_MONITOR_H
#define __PLATFORM_MONITOR_H

/*
 * Exported monitor data structures and functions which can be
 * referenced by routines also linked into the monitor binary.
 */
extern uint64_t __mon_cpu_return_addr[MONCPUS];
extern uintptr_t __mon_cpu_reset_vector;
extern uintptr_t __mon_phys_base;
extern uintptr_t __mon_phys_size;

#define PMC_SECURE_DISABLE2		0x2c4
#define PMC_SECURE_DISABLE2_WRITE22_ON	(1 << 28)

#define PMC_SECURE_DISABLE3		0x2d8
#define PMC_SECURE_DISABLE3_WRITE34_ON	(1 << 20)
#define PMC_SECURE_DISABLE3_WRITE35_ON	(1 << 22)

#define PMC_SECURE_SCRATCH22		0x338
#define PMC_SECURE_SCRATCH34		0x368
#define PMC_SECURE_SCRATCH35		0x36c

#define EVP_CPU_RESET_VECTOR		0x100

/* CPU reset vector */
#define SB_AA64_RESET_LOW		0x30	// width = 31:0
#define SB_AA64_RESET_HI		0x34	// width = 11:0

#define SB_CSR				0x0
#define SB_CSR_NS_RST_VEC_WR_DIS	(1 << 1)

struct fastcall_frame;

void platform_init_memory(uint32_t sec_base, uint32_t sec_size);
void platform_restore_memory();
status_t platform_program_vpr(uint32_t vpr_base, uint32_t vpr_size);
void platform_init_mc_sid(void);
void platform_config_interrupts(void);
void platform_secure_dram_aperture(void);
void platform_map_mmio(void);
status_t platform_vrr_fastcall(uint64_t cmd, uint64_t arg);
int soc_sip_handler(struct fastcall_frame *frame, uint32_t cpu);
#endif
