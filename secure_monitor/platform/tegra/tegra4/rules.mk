# ROMBASE, VMEMBASE, and VMEMSIZE are required for the linker script
VMEMBASE := 0x48000000

ARM_CPU := cortex-a15
ARM_WITH_LPAE := true

DEFINES += \
	WITH_PADDR_T_64BIT=1
