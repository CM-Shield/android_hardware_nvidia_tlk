/*
 * Copyright (c) 2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <reg.h>
#include <psci.h>
#include <assert.h>
#include <platform/memmap.h>
#include <platform/platform_monitor.h>
#include <lib/monitor/monitor_vector.h>
#include <printf.h>

#define MAX_CPUS	4

#define MC_SECURITY_CFG1_0		0x74

#define PMC_SECURE_DISABLE3		0x2d8
#define PMC_SECURE_DISABLE3_WRITE34_ON	(1 << 20)
#define PMC_SECURE_DISABLE3_WRITE35_ON	(1 << 22)

#define PMC_SECURE_SCRATCH22		0x338
#define PMC_SECURE_SCRATCH34		0x368
#define PMC_SECURE_SCRATCH35		0x36c

#define FLOW_CTRL_HALT_CPU0_EVENTS	0x0
#define FLOW_CTRL_WAITEVENT		(2 << 29)
#define FLOW_CTRL_WAIT_FOR_INTERRUPT	(4 << 29)
#define FLOW_CTRL_JTAG_RESUME		(1 << 28)
#define FLOW_CTRL_HALT_SCLK		(1 << 27)
#define FLOW_CTRL_HALT_LIC_IRQ		(1 << 11)
#define FLOW_CTRL_HALT_LIC_FIQ		(1 << 10)
#define FLOW_CTRL_HALT_GIC_IRQ		(1 << 9)
#define FLOW_CTRL_HALT_GIC_FIQ		(1 << 8)
#define FLOW_CTLR_HALT_COP_EVENTS	0x4
#define FLOW_CTRL_CPU0_CSR		0x8
#define FLOW_CTRL_CSR_PWR_OFF_STS	(1 << 16)
#define FLOW_CTRL_CSR_INTR_FLAG		(1 << 15)
#define FLOW_CTRL_CSR_EVENT_FLAG	(1 << 14)
#define FLOW_CTRL_CSR_IMMEDIATE_WAKE	(1 << 3)
#define FLOW_CTRL_CSR_ENABLE		(1 << 0)
#define FLOW_CTRL_HALT_CPU1_EVENTS	0x14
#define FLOW_CTRL_CPU1_CSR		0x18
#define FLOW_CTLR_CC4_HVC_CONTROL	0x60
#define FLOW_CTRL_CC4_HVC_ENABLE	(1 << 0)
#define FLOW_CTRL_CC4_RETENTION_CONTROL	0x64
#define FLOW_CTRL_CC4_CORE0_CTRL	0x6c
#define FLOW_CTRL_WAIT_WFI_BITMAP	0x100
#define FLOW_CTRL_WAIT_WFE_BITMASK	(0xF << 4)
#define FLOW_CTRL_WAIT_WFI_BITMASK	(0xF << 8)
#define FLOW_CTRL_CC4_HVC_RETRY		0x8c
#define FLOW_CTLR_L2FLUSH_CONTROL	0x94
#define FLOW_CTLR_BPMP_CLUSTER_CONTROL	0x98
#define FLOW_CTLR_BPMP_CLUSTER_LOCK	(1 << 2)

#define FLOW_CTLR_ENABLE_EXT		12
#define FLOW_CTLR_PG_CPU_NONCPU		0x1
#define FLOW_CTLR_TURNOFF_CPURAIL	0x2

#define PMC_PWRGATE_STATUS		0x38
#define PMC_PWRGATE_TOGGLE		0x30
#define PMC_TOGGLE_START		0x100
#define PMC_SCRATCH39			0x138

#define CLK_RST_DEV_L_SET		0x300
#define CLK_RST_DEV_L_CLR		0x304
#define CLK_COP_RST			(1 << 1)

#define CLK_RST_CPU_CMPLX_CLR		0x344
 #define CLK_RST_RESET_CPU1		(1 << 1)
 #define CLK_RST_DERESET_CPU1		(1 << 5)
 #define CLK_RST_DBGRESET_CPU1		(1 << 13)
 #define CLK_RST_CORERESET_CPU1		(1 << 17)

#define EVP_COP_RESET_VECTOR	0x200

#define MSELECT_CONFIG		0x0

#define ERR_RESP_EN_SLAVE1	(1 << 24)
#define ERR_RESP_EN_SLAVE2	(1 << 25)
#define WRAP_TO_INCR_SLAVE0	(1 << 27)
#define WRAP_TO_INCR_SLAVE1	(1 << 28)
#define WRAP_TO_INCR_SLAVE2	(1 << 29)

/* Core state 0-9 */
#define TEGRA_CPUIDLE_C4	4
#define TEGRA_CPUIDLE_C7	7

/* Cluster states 10-19 */
#define TEGRA_CPUIDLE_CC4	14
#define TEGRA_CPUIDLE_CC6	16
#define TEGRA_CPUIDLE_CC7	17

/* SoC states 20-29 */
#define TEGRA_CPUIDLE_SC2	22
#define TEGRA_CPUIDLE_SC3	23
#define TEGRA_CPUIDLE_SC4	24
#define TEGRA_CPUIDLE_SC7	27

#define TEGRA_PREPARE_CLUSTER_SWITCH	30
#define TEGRA_CLUSTER_SWITCH	31

#define NO_CLUSTER_SWITCH_PENDING	-1

#define NO_FLUSH_L2		0
#define FLUSH_L2		1

extern volatile uint32_t cpu_affinity_info[MONCPUS];
extern volatile int enter_deep_sleep;

static paddr_t flowctrl_offset_cpu_csr[MAX_CPUS] = {
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_CPU0_CSR),
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_CPU1_CSR),
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_CPU1_CSR + 8),
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_CPU1_CSR + 16)
};

static paddr_t flowctrl_offset_halt_cpu[MAX_CPUS] = {
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_HALT_CPU0_EVENTS),
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_HALT_CPU1_EVENTS),
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_HALT_CPU1_EVENTS + 8),
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_HALT_CPU1_EVENTS + 16)
};

static paddr_t flowctrl_offset_cc4_ctrl[MAX_CPUS] = {
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_CC4_CORE0_CTRL),
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_CC4_CORE0_CTRL + 4),
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_CC4_CORE0_CTRL + 8),
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_CC4_CORE0_CTRL + 12)
};

static int cpu_powergate_mask[MAX_CPUS];
static int pmc_cpu_powergate_id[MAX_CPUS] = {0 , 9, 10, 11};
static int cluster_last_man_id;

extern void psci_program_reset_vectors(int cpu);

static inline void flowctrl_write_cpu_csr(int cpu_id, uint32_t val)
{
	writel(val, flowctrl_offset_cpu_csr[cpu_id]);
	val = readl(flowctrl_offset_cpu_csr[cpu_id]);
}

static inline void flowctrl_write_halt_cpu(int cpu_id, uint32_t val)
{
	writel(val, flowctrl_offset_halt_cpu[cpu_id]);
	val = readl(flowctrl_offset_halt_cpu[cpu_id]);
}

static inline void flowctrl_write_cc4_ctrl(int cpu_id, uint32_t val)
{
	writel(val, flowctrl_offset_cc4_ctrl[cpu_id]);
	val = readl(flowctrl_offset_cc4_ctrl[cpu_id]);
}

/* Turn off CPU using flow controller */
static void plat_cpu_off(int cpu_id)
{
	uint32_t val = FLOW_CTRL_CSR_INTR_FLAG | FLOW_CTRL_CSR_EVENT_FLAG |
		FLOW_CTRL_CSR_ENABLE | (FLOW_CTRL_WAIT_WFI_BITMAP << cpu_id);

	flowctrl_write_cpu_csr(cpu_id, val);
	flowctrl_write_halt_cpu(cpu_id, FLOW_CTRL_WAITEVENT);
	flowctrl_write_cc4_ctrl(cpu_id, 0);

	return;
}

/* Turn on CPU using flow controller or PMC */
static int plat_cpu_on(int cpu_id)
{
	uint32_t val;

	if (cpu_powergate_mask[cpu_id] == 0) {
		/* use PMC */
		/* fill in the CPU powergate mask */
		cpu_powergate_mask[cpu_id] = 1;

		/* check if CPU is already power ungated */
		val = readl(TEGRA_PMC_BASE + PMC_PWRGATE_STATUS);
		if (val & (1 << pmc_cpu_powergate_id[cpu_id]))
			return PSCI_RETURN_SUCCESS;

		val = pmc_cpu_powergate_id[cpu_id] | PMC_TOGGLE_START;
		writel(val, TEGRA_PMC_BASE + PMC_PWRGATE_TOGGLE);

		/*
		 * The PMC deasserts the START bit when it starts the power
		 * ungate process. Loop till powergate START bit is asserted.
		 */
		do {
			val = readl(TEGRA_PMC_BASE + PMC_PWRGATE_TOGGLE);
		} while (val & (1 << 8));

		/* loop till the CPU is power ungated */
		do {
			val = readl(TEGRA_PMC_BASE + PMC_PWRGATE_STATUS);
		} while ((val & (1 << pmc_cpu_powergate_id[cpu_id])) == 0);
	} else {
		/* use FC */
		flowctrl_write_cpu_csr(cpu_id, FLOW_CTRL_CSR_ENABLE);
		flowctrl_write_halt_cpu(cpu_id, FLOW_CTRL_WAITEVENT |
			FLOW_CTRL_HALT_SCLK);
	}

	return PSCI_RETURN_SUCCESS;
}

/* prepare for SC7 entry */
static int plat_prepare_sc7(int cpu_id)
{
	uint32_t val;

	flowctrl_write_halt_cpu(cpu_id, FLOW_CTRL_WAITEVENT);

	writel(1, TEGRA_FLOW_CTRL_BASE + FLOW_CTLR_L2FLUSH_CONTROL);

	val = FLOW_CTRL_CSR_INTR_FLAG | FLOW_CTRL_CSR_EVENT_FLAG |
		FLOW_CTRL_CSR_ENABLE | (FLOW_CTRL_WAIT_WFI_BITMAP << cpu_id) |
		(FLOW_CTLR_TURNOFF_CPURAIL << FLOW_CTLR_ENABLE_EXT);
	flowctrl_write_cpu_csr(cpu_id, val);

	return PSCI_RETURN_SUCCESS;
}

/* prepare for CC6/CC7 entry */
static int plat_prepare_cc6_cc7(int cpu_id, uint32_t csr)
{
	uint32_t val;
	uint32_t cluster;

	__asm__ volatile ("mrs	%0, mpidr_el1\n"
			  "ubfx	%0, %0, #8, #4"
			  : "=r" (cluster)
			  :
			  : "cc", "memory");

	val = FLOW_CTRL_HALT_GIC_IRQ | FLOW_CTRL_HALT_GIC_FIQ |
	      FLOW_CTRL_HALT_LIC_IRQ | FLOW_CTRL_HALT_LIC_FIQ |
	      FLOW_CTRL_WAITEVENT;
	flowctrl_write_halt_cpu(cpu_id, val);

	/* fast==0 => use SW L2 flush, slow==1 => use HW L2 flush */
	writel(!!cluster, TEGRA_FLOW_CTRL_BASE + FLOW_CTLR_L2FLUSH_CONTROL);

	val = FLOW_CTRL_CSR_INTR_FLAG | FLOW_CTRL_CSR_EVENT_FLAG |
	      FLOW_CTRL_CSR_ENABLE |
	      (FLOW_CTRL_WAIT_WFI_BITMAP << cpu_id);
	flowctrl_write_cpu_csr(cpu_id, val | csr);

	return cluster ? NO_FLUSH_L2 : FLUSH_L2;
}

/* runs on CPUs other than the last standing one */
static int plat_prepare_cluster_switch(int cpu_id)
{
	uint32_t val = (FLOW_CTRL_WAIT_WFI_BITMAP << cpu_id) |
		FLOW_CTRL_CSR_ENABLE | FLOW_CTRL_CSR_IMMEDIATE_WAKE;

	flowctrl_write_cpu_csr(cpu_id, val);
	return NO_FLUSH_L2;
}

/* runs on the last standing CPU */
static int plat_cluster_switch(int cpu_id, int afflvl)
{
	int i;
	uint32_t val = (FLOW_CTRL_WAIT_WFI_BITMAP << cpu_id) |
		FLOW_CTRL_CSR_ENABLE | FLOW_CTRL_CSR_IMMEDIATE_WAKE;

	if (afflvl == 0) {
		flowctrl_write_cpu_csr(cpu_id, val);
		return NO_FLUSH_L2;
	}

	/* Affinity level == cluster, then PG the non CPU part */
	val |= FLOW_CTLR_PG_CPU_NONCPU << FLOW_CTLR_ENABLE_EXT;
	flowctrl_write_cpu_csr(cpu_id, val);

	/* store last man standing */
	cluster_last_man_id = cpu_id;

	for (i = 0; i < MAX_CPUS; i++) {
		if (i == cpu_id)
			continue;

		/* skip CPU if it has never been booted */
		if (i && !cpu_powergate_mask[i])
			continue;

		val = readl(flowctrl_offset_cpu_csr[i]);

		/* wait till current cpu is power gated by FC */
		while ((val & FLOW_CTRL_CSR_PWR_OFF_STS) == 0)
			val = readl(flowctrl_offset_cpu_csr[i]);
	}

	val = readl(TEGRA_FLOW_CTRL_BASE + FLOW_CTLR_BPMP_CLUSTER_CONTROL);
	val &= ~FLOW_CTLR_BPMP_CLUSTER_LOCK;
	writel(val, TEGRA_FLOW_CTRL_BASE + FLOW_CTLR_BPMP_CLUSTER_CONTROL);

	writel(0, TEGRA_FLOW_CTRL_BASE + FLOW_CTLR_L2FLUSH_CONTROL);

	return FLUSH_L2;
}

static int plat_cpu_suspend(int cpu_id, int afflvl, int psci_state_id)
{
	uint32_t val;

	switch (psci_state_id) {
	case TEGRA_CPUIDLE_SC7:
		flowctrl_write_cc4_ctrl(cpu_id, 0);
		return plat_prepare_sc7(cpu_id);

	case TEGRA_CPUIDLE_CC6:
		flowctrl_write_cc4_ctrl(cpu_id, 0);
		val = FLOW_CTLR_PG_CPU_NONCPU << FLOW_CTLR_ENABLE_EXT;
		return plat_prepare_cc6_cc7(cpu_id, val);

	case TEGRA_CPUIDLE_CC7:
		flowctrl_write_cc4_ctrl(cpu_id, 0);
		val = FLOW_CTLR_TURNOFF_CPURAIL << FLOW_CTLR_ENABLE_EXT;
		return plat_prepare_cc6_cc7(cpu_id, val);

	case TEGRA_PREPARE_CLUSTER_SWITCH:
		flowctrl_write_cc4_ctrl(cpu_id, 0);
		return plat_prepare_cluster_switch(cpu_id);

	case TEGRA_CLUSTER_SWITCH:
		flowctrl_write_cc4_ctrl(cpu_id, 0);
		return plat_cluster_switch(cpu_id, afflvl);

	case TEGRA_CPUIDLE_C7:
		val = FLOW_CTRL_HALT_GIC_IRQ | FLOW_CTRL_HALT_GIC_FIQ |
		      FLOW_CTRL_HALT_LIC_IRQ | FLOW_CTRL_HALT_LIC_FIQ |
		      FLOW_CTRL_WAITEVENT;
		flowctrl_write_halt_cpu(cpu_id, val);

		val = FLOW_CTRL_CSR_INTR_FLAG | FLOW_CTRL_CSR_EVENT_FLAG |
		      FLOW_CTRL_CSR_ENABLE |
		      (FLOW_CTRL_WAIT_WFI_BITMAP << cpu_id);
		flowctrl_write_cpu_csr(cpu_id, val);
		return NO_FLUSH_L2;

	default:
		break;
	}

	return PSCI_RETURN_INVALID_PARAMS;
}

void platform_psci_init(int cpu_id)
{
	uint32_t val;

	if (cluster_last_man_id == NO_CLUSTER_SWITCH_PENDING ||
	    cluster_last_man_id != cpu_id)
		return;

	val = readl(TEGRA_FLOW_CTRL_BASE + FLOW_CTLR_BPMP_CLUSTER_CONTROL);
	val |= FLOW_CTLR_BPMP_CLUSTER_LOCK;
	writel(val, TEGRA_FLOW_CTRL_BASE + FLOW_CTLR_BPMP_CLUSTER_CONTROL);
	val = readl(TEGRA_FLOW_CTRL_BASE + FLOW_CTLR_BPMP_CLUSTER_CONTROL);

	cluster_last_man_id = NO_CLUSTER_SWITCH_PENDING;
}

void platform_psci_cpu_resume(int cpu_id)
{
	uint32_t val;
	uintptr_t evp_base = TEGRA_EXCEPTION_VECTORS_BASE;

	flowctrl_write_cpu_csr(cpu_id, 0);
	flowctrl_write_halt_cpu(cpu_id, 0);

	/*
	 * Avoid relying on having seen an LP0 enter SMC.
	 *
	 * If MC_SECURITY_CFG1 has gone back zero (its POR value) then LP0
	 * has occurred (as it's not part of BL's warmboot restore) and system
	 * registers need to be reloaded.
	 */
	val = readl(TEGRA_MC_BASE + MC_SECURITY_CFG1_0);
	if (val == (__mon_phys_size >> 20))
		return;

	/*
	 * disable error mechanism, enable WRAP type conversion
	 * SW WAR for bug 1495753
	 */
	val = readl(TEGRA_MSELECT_BASE + MSELECT_CONFIG);
	val &= ~(ERR_RESP_EN_SLAVE1 | ERR_RESP_EN_SLAVE2);
	val |= (WRAP_TO_INCR_SLAVE0 | WRAP_TO_INCR_SLAVE1 |
				      WRAP_TO_INCR_SLAVE2);
	writel(val, TEGRA_MSELECT_BASE + MSELECT_CONFIG);

	/*
	 * Restore BPMP reset address from SCRATCH39 and Reset BPMP.
	 */

	/* 1. Halt BPMP */
	writel(FLOW_CTRL_WAITEVENT, TEGRA_FLOW_CTRL_BASE +
		FLOW_CTLR_HALT_COP_EVENTS);

	/* 2. Assert BPMP reset */
	writel(CLK_COP_RST, TEGRA_CLK_RESET_BASE + CLK_RST_DEV_L_SET);

	/* 3. Restore reset address (stored in PMC_SCRATCH39) */
	val = readl(TEGRA_PMC_BASE + PMC_SCRATCH39);
	writel(val, evp_base + EVP_COP_RESET_VECTOR);
	while (val != readl(evp_base + EVP_COP_RESET_VECTOR))
		; // wait till value reaches EVP_COP_RESET_VECTOR

	val = readl(TEGRA_TMRUS_BASE);
	val += 2;
	while (val > readl(TEGRA_TMRUS_BASE));

	/* 4. De-assert BPMP reset */
	writel(CLK_COP_RST, TEGRA_CLK_RESET_BASE + CLK_RST_DEV_L_CLR);

	/* 5. Un-halt BPMP */
	writel(0, TEGRA_FLOW_CTRL_BASE + FLOW_CTLR_HALT_COP_EVENTS);

	printf("el3_monitor: sc7 exit\n");
}

int platform_psci_cpu_suspend(int cpu_id, uint32_t psci_state)
{
	uint32_t state = (psci_state >> PSCI_POWER_STATE_TYPE_SHIFT) &
		PSCI_POWER_STATE_TYPE_MASK;
	uint32_t afflvl = (psci_state >> PSCI_POWER_STATE_AFFL_SHIFT) &
		PSCI_POWER_STATE_AFFL_MASK;
	int ret;

	if (state != PSCI_POWER_STATE_TYPE_POWER_DOWN)
		return PSCI_RETURN_DENIED;

	ret = plat_cpu_suspend(cpu_id, afflvl,
			psci_state & PSCI_POWER_STATE_ID_MASK);

	if (ret < 0)
		return ret;

	mon_cpu_dbg_save();
	mon_cpu_power_down(ret);

	return PSCI_RETURN_SUCCESS;
}

void platform_psci_cpu_off(int cpu_id, uint32_t psci_state)
{
	plat_cpu_off(cpu_id);
	mon_cpu_dbg_save();
	mon_cpu_power_down(NO_FLUSH_L2);
}

int platform_psci_cpu_on(int cpu_id)
{
	return plat_cpu_on(cpu_id);
}

void platform_psci_init_reset_vector(uint32_t cpu)
{
	uint64_t phys_cpu_reset;
	uint32_t reg;

	/* both SECURE_SCRATCH34/SCRATCH35 should be writable */
	reg  = readl(TEGRA_PMC_BASE + PMC_SECURE_DISABLE3);
	reg &= (PMC_SECURE_DISABLE3_WRITE34_ON | PMC_SECURE_DISABLE3_WRITE35_ON);
	ASSERT(!reg);

	/* set exception vector to be used to resume from suspend */
	phys_cpu_reset = mon_virt_to_phys(&__mon_cpu_reset_vector);
	writel(phys_cpu_reset & 0xFFFFFFFF, TEGRA_PMC_BASE + PMC_SECURE_SCRATCH34);
	phys_cpu_reset >>= 32;
	writel(phys_cpu_reset & 0x7FF, TEGRA_PMC_BASE + PMC_SECURE_SCRATCH35);

	psci_program_reset_vectors(cpu);
}

static void psci_lock_reset_registers(void)
{
	uint32_t reg;

	/* ensure SECURE_SCRATCH34/35 are write locked */
	reg  = readl(TEGRA_PMC_BASE + PMC_SECURE_DISABLE3);
	reg |= (PMC_SECURE_DISABLE3_WRITE34_ON |
		PMC_SECURE_DISABLE3_WRITE35_ON);
	writel(reg, TEGRA_PMC_BASE + PMC_SECURE_DISABLE3);	/* lock */

	/* set secure boot control (read to flush) */
	reg  = readl(TEGRA_SB_BASE + SB_CSR);
	reg |= SB_CSR_NS_RST_VEC_WR_DIS;
	writel(reg, TEGRA_SB_BASE + SB_CSR);
	readl(TEGRA_SB_BASE + SB_CSR);
}

void psci_program_reset_vectors(int cpu)
{
	uint64_t phys_cpu_reset;

	phys_cpu_reset = mon_virt_to_phys(&__mon_cpu_reset_vector);

	/* write lower 32 bits first, then the upper 11 bits */
	writel((phys_cpu_reset & 0xFFFFFFFF) | 1, TEGRA_SB_BASE + SB_AA64_RESET_LOW);
	phys_cpu_reset >>= 32;
	writel(phys_cpu_reset & 0x7FF, TEGRA_SB_BASE + SB_AA64_RESET_HI);

	psci_lock_reset_registers();
}
