/*
 * Copyright (c) 2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <err.h>
#include <debug.h>
#include <assert.h>
#include <compiler.h>
#include <arch/ops.h>
#include <platform.h>
#include <platform/memmap.h>
#include <reg.h>
#include <string.h>
#include <psci.h>
#include <platform/platform_monitor.h>
#include <lib/monitor/monitor_vector.h>

/* referenced APBDEV_PMC_SECURE registers */
#define MC_SECURITY_CFG1_0		0x74

volatile uint32_t cpu_affinity_info[MONCPUS];

__WEAK void platform_map_mmio(void)
{
	; // do nothing
}

__WEAK void platform_psci_init(int cpu_id)
{
	; // do nothing
}

__WEAK int platform_psci_cpu_on(int cpu_id)
{
	return 0;
}

__WEAK void platform_psci_cpu_off(int cpu_id, uint32_t psci_state)
{
	; // do nothing
}

__WEAK int platform_psci_cpu_suspend(int cpu_id, uint32_t psci_state)
{
	return 0;
}

__WEAK void platform_psci_cpu_resume(int cpu_id)
{
	; // do nothing
}

__WEAK void platform_psci_init_reset_vector(uint32_t cpu)
{
	; // do nothing
}

__WEAK __NO_RETURN void platform_psci_system_reset(void)
{
	PANIC_UNIMPLEMENTED;
}

__WEAK void psci_program_reset_vectors(int cpu)
{
	; // do nothing
}

/*
 * One-time init called during cold boot from primary CPU
 */
void psci_init(uint32_t cpu)
{
	/* mark the CPU as ON */
	cpu_affinity_info[cpu] = PSCI_0_2_AFFINITY_LEVEL_ON;

	/* init reset vector */
	platform_psci_init_reset_vector(cpu);

	platform_psci_init(cpu);
}

/*
 * One time init call during end of cold boot from primary CPU
 */
void platform_psci_coldboot_epilog(void)
{
	/* mark entire TLK carveout as secure in the MC */
	platform_secure_dram_aperture();
}

#if WITH_MONITOR_BIN
/*
 * Routine is called when a CPU goes through reset, either a secondary
 * CPU during cold boot, or all CPUs during system suspend.
 */
void platform_psci_cpu_has_reset(uint32_t cpu)
{
	uint32_t reg;

	/* mark the CPU as ON */
	cpu_affinity_info[cpu] = PSCI_0_2_AFFINITY_LEVEL_ON;

	platform_psci_cpu_resume(cpu);
	platform_monitor_init_cpu();
	platform_psci_init(cpu);

	/*
	 * Avoid relying on having seen an LP0 enter SMC.
	 *
	 * If MC_SECURITY_CFG1 has gone back zero (its POR value) then LP0
	 * has occurred (as it's not part of BL's warmboot restore) and system
	 * registers need to be reloaded.
	 */
	reg = readl(TEGRA_MC_BASE + MC_SECURITY_CFG1_0);
	if (reg == (__mon_phys_size >> 20))
		return;

	psci_program_reset_vectors(cpu);
	platform_restore_memory();
	platform_config_interrupts();
}
#endif

void psci_handler(struct fastcall_frame *frame)
{
	uint64_t ret = PSCI_RETURN_SUCCESS;
	uint32_t curr_cpu, target_cpu;
	uint32_t reg;

	curr_cpu = mon_cpu_id();

	switch (frame->r[0]) {
	/*
	 * Runs on the actual CPU which is being suspended.
	 */
	case PSCI_FUNC_ID_CPU_SUSPEND:
	case PSCI_FUNC_ID64_CPU_SUSPEND:

		/* save NS entry point */
		__mon_cpu_return_addr[curr_cpu] = frame->r[2];

		ret = platform_psci_cpu_suspend(curr_cpu, frame->r[1]);
		break;

	/*
	 * Runs on a CPU other than the one we want to get online.
	 */
	case PSCI_FUNC_ID_CPU_ON:
	case PSCI_FUNC_ID64_CPU_ON:
		target_cpu = frame->r[1];

		/* sanity check */
		if (target_cpu > (MONCPUS - 1)) {
			ret = PSCI_RETURN_INVALID_PARAMS;
			break;
		}

		__mon_cpu_return_addr[target_cpu] = frame->r[2];

		/* The CPU will be marked as ON during its boot sequence */
		cpu_affinity_info[target_cpu] = PSCI_0_2_AFFINITY_LEVEL_ON_PENDING;

		ret = platform_psci_cpu_on(target_cpu);
		break;

	/*
	 * Runs on the actual CPU which is being powered off.
	 */
	case PSCI_FUNC_ID_CPU_OFF:
		cpu_affinity_info[curr_cpu] = PSCI_0_2_AFFINITY_LEVEL_OFF;

		platform_psci_cpu_off(curr_cpu,
			PSCI_POWER_STATE_TYPE_POWER_DOWN);
		break;

	/* support PSCIv0.2 */
	case PSCI_FUNC_ID_VERSION:
		ret = PSCI_VERSION_MAJOR(0) | PSCI_VERSION_MINOR(2);
		break;

	/*
	 * Implement 'affinity info' that was introduced in PSCIv0.2. The
	 * caller expects to get the state of the target CPU.
	 */
	case PSCI_FUNC_ID64_AFFINITY_INFO:
	case PSCI_FUNC_ID_AFFINITY_INFO:
		target_cpu = frame->r[1];
		if (target_cpu > (MONCPUS - 1))
			ret = PSCI_RETURN_NOT_SUPPORTED;
		else
			ret = cpu_affinity_info[target_cpu];
		break;

	/*
	 * Implement 'system reset' that was introduced in PSCI v0.2. This
	 * function ID never returns.
	 */
	case PSCI_FUNC_ID_SYSTEM_RESET:
		reg = readl(TEGRA_PMC_BASE);
		reg |= 0x10;
		writel(reg, TEGRA_PMC_BASE);

		break;

	default:
		ret = PSCI_RETURN_NOT_SUPPORTED;
	}

	frame->r[0] = ret;
}
