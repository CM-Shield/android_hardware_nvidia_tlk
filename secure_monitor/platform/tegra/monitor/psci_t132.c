/*
 * Copyright (c) 2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <reg.h>
#include <psci.h>
#include <assert.h>
#include <platform/memmap.h>
#include <platform/platform_monitor.h>
#include <lib/monitor/monitor_vector.h>
#include <printf.h>

#define MAX_CPUS			2

#define PMC_SECURE_DISABLE2		0x2c4
#define PMC_SECURE_DISABLE2_WRITE22_ON	(1 << 28)

#define PMC_SECURE_DISABLE3		0x2d8
#define PMC_SECURE_DISABLE3_WRITE34_ON	(1 << 20)
#define PMC_SECURE_DISABLE3_WRITE35_ON	(1 << 22)

#define PMC_SECURE_SCRATCH22		0x338
#define PMC_SECURE_SCRATCH34		0x368
#define PMC_SECURE_SCRATCH35		0x36c

#define FLOW_CTRL_HALT_CPU0_EVENTS	0x0
#define FLOW_CTRL_WAITEVENT		(2 << 29)
#define FLOW_CTRL_WAIT_FOR_INTERRUPT	(4 << 29)
#define FLOW_CTRL_JTAG_RESUME		(1 << 28)
#define FLOW_CTRL_HALT_SCLK		(1 << 27)
#define FLOW_CTRL_HALT_LIC_IRQ		(1 << 11)
#define FLOW_CTRL_HALT_LIC_FIQ		(1 << 10)
#define FLOW_CTRL_HALT_GIC_IRQ		(1 << 9)
#define FLOW_CTRL_HALT_GIC_FIQ		(1 << 8)
#define FLOW_CTLR_HALT_COP_EVENTS	0x4
#define FLOW_CTRL_CPU0_CSR		0x8
#define FLOW_CTRL_CSR_PWR_OFF_STS	(1 << 16)
#define FLOW_CTRL_CSR_INTR_FLAG		(1 << 15)
#define FLOW_CTRL_CSR_EVENT_FLAG	(1 << 14)
#define FLOW_CTRL_CSR_IMMEDIATE_WAKE	(1 << 3)
#define FLOW_CTRL_CSR_ENABLE		(1 << 0)
#define FLOW_CTRL_HALT_CPU1_EVENTS	0x14
#define FLOW_CTRL_CPU1_CSR		0x18
#define FLOW_CTLR_CC4_HVC_CONTROL	0x60
#define FLOW_CTRL_CC4_HVC_ENABLE	(1 << 0)
#define FLOW_CTRL_CC4_RETENTION_CONTROL	0x64
#define FLOW_CTRL_CC4_CORE0_CTRL	0x6c
#define FLOW_CTRL_WAIT_WFI_BITMAP	0x100
#define FLOW_CTRL_WAIT_WFE_BITMASK	(0xF << 4)
#define FLOW_CTRL_WAIT_WFI_BITMASK	(0xF << 8)

#define PMC_PWRGATE_STATUS		0x38
#define PMC_PWRGATE_TOGGLE		0x30
#define PMC_TOGGLE_START		0x100

#define CLK_RST_CPU_CMPLX_CLR		0x344
 #define CLK_RST_RESET_CPU1		(1 << 1)
 #define CLK_RST_DERESET_CPU1		(1 << 5)
 #define CLK_RST_DBGRESET_CPU1		(1 << 13)
 #define CLK_RST_CORERESET_CPU1		(1 << 17)

extern volatile uint32_t cpu_affinity_info[MONCPUS];
extern volatile int enter_deep_sleep;

static int cpu_powergate_mask[MAX_CPUS];
static int pmc_cpu_powergate_id[MAX_CPUS] = {0 , 9};

/*
 * For T132, CPUs reset to AARCH32, so the reset vector is first
 * armv8_trampoline and which does a warm reset to AARCH64 and
 * starts execution at the address in SCRATCH34/SCRATCH35.
 */
uint32_t armv8_trampoline[] __ALIGNED(8) = {
	0xE3A00003,		// mov	r0, #3
	0xEE0C0F50,		// mcr	p15, 0, r0, c12, c0, 2
	0xEAFFFFFE,		// b	.
};

static paddr_t flowctrl_offset_cpu_csr[MAX_CPUS] = {
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_CPU0_CSR),
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_CPU1_CSR)
};

static paddr_t flowctrl_offset_halt_cpu[MAX_CPUS] = {
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_HALT_CPU0_EVENTS),
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_HALT_CPU1_EVENTS)
};

static inline void flowctrl_write_cpu_csr(int cpu_id, uint32_t val)
{
	writel(val, flowctrl_offset_cpu_csr[cpu_id]);
	val = readl(flowctrl_offset_cpu_csr[cpu_id]);
}

static inline void flowctrl_write_halt_cpu(int cpu_id, uint32_t val)
{
	writel(val, flowctrl_offset_halt_cpu[cpu_id]);
	val = readl(flowctrl_offset_halt_cpu[cpu_id]);
}

static void __disable_background_ops(int cpu_id)
{
	uint64_t val64;

	/* turn off MTS ops running in the background */
	val64 = (1 << cpu_id) << 16;
	__asm__ volatile(
		"msr	s3_0_c15_c0_2, %0	\n"
		"isb	\n"
		: : "r" (val64));

		/* wait till MTS background work turns off */
	do {
	__asm__ volatile(
			"mrs	%0, s3_0_c15_c0_2	\n"
			: "=r" (val64) : );
		val64 = (val64 >> 32) & 0xffff;
	} while (!!(val64 & (1 << cpu_id)));
}

void platform_psci_cpu_off(int cpu_id, uint32_t psci_state)
{
	uint32_t val;
	int denver_pm_state_c7 = 3;
	int denver_pm_state_c6 = 2;

	if (enter_deep_sleep) {

		__disable_background_ops(cpu_id);

		/* flow controller programming for C7 */
		val = readl(flowctrl_offset_cpu_csr[cpu_id]);
		val &= ~FLOW_CTRL_WAIT_WFE_BITMASK;
		val &= ~FLOW_CTRL_WAIT_WFI_BITMASK;
		val |= FLOW_CTRL_CSR_INTR_FLAG | FLOW_CTRL_CSR_EVENT_FLAG |
		       FLOW_CTRL_CSR_ENABLE |
		       (FLOW_CTRL_WAIT_WFI_BITMAP << cpu_id);
		flowctrl_write_cpu_csr(cpu_id, val);

		val = cpu_id ? FLOW_CTRL_WAITEVENT :
			       (FLOW_CTRL_WAIT_FOR_INTERRUPT |
				FLOW_CTRL_HALT_LIC_IRQ |
				FLOW_CTRL_HALT_LIC_FIQ);
		flowctrl_write_halt_cpu(cpu_id, val);

		enter_deep_sleep = 0;

		/* MTS handles the entry into C7 */
		val = denver_pm_state_c7;
		__asm__ volatile (
			"isb			\n"
			"msr	actlr_el1, %0	\n"
			"wfi			\n"
			: : "r" (val));
	} else {

		/* MTS handles the entry into C6 */
		val = denver_pm_state_c6;
		__asm__ volatile (
			"msr	actlr_el1, %0	\n"
			"wfi			\n"
			: : "r" (val));
	}

	/* We exit from C7/C6 state at this point, so mark the CPU as ON */
	cpu_affinity_info[cpu_id] = PSCI_0_2_AFFINITY_LEVEL_ON;
}

/* Turn on CPU using flow controller or PMC */
int platform_psci_cpu_on(int cpu_id)
{
	uint32_t val;

	if (cpu_powergate_mask[cpu_id] == 0) {

		/* use PMC */

		/* Take CPU1 out of reset */
		flowctrl_write_halt_cpu(cpu_id, 0);
		val = CLK_RST_RESET_CPU1 | CLK_RST_DERESET_CPU1 |
			CLK_RST_DBGRESET_CPU1 | CLK_RST_CORERESET_CPU1;
		writel(val, TEGRA_CLK_RESET_BASE + CLK_RST_CPU_CMPLX_CLR);

		/* delay of 2us for CPU power up */
		val = readl(TEGRA_TMRUS_BASE);
		val += 2;
		while (val > readl(TEGRA_TMRUS_BASE));

		/* fill in the CPU powergate mask */
		cpu_powergate_mask[cpu_id] = 1;

		/* check if CPU is already power ungated */
		val = readl(TEGRA_PMC_BASE + PMC_PWRGATE_STATUS);
		if (val & (1 << pmc_cpu_powergate_id[cpu_id]))
			return PSCI_RETURN_SUCCESS;

		val = pmc_cpu_powergate_id[cpu_id] | PMC_TOGGLE_START;
		writel(val, TEGRA_PMC_BASE + PMC_PWRGATE_TOGGLE);

		/*
		 * The PMC deasserts the START bit when it starts the power
		 * ungate process. Loop till powergate START bit is asserted.
		 */
		do {
			val = readl(TEGRA_PMC_BASE + PMC_PWRGATE_TOGGLE);
		} while (val & (1 << 8));

		/* loop till the CPU is power ungated */
		do {
			val = readl(TEGRA_PMC_BASE + PMC_PWRGATE_STATUS);
		} while ((val & (1 << pmc_cpu_powergate_id[cpu_id])) == 0);
	}

	return PSCI_RETURN_SUCCESS;
}

static void plat_cpu_suspend(int cpu_id, int afflvl, int psci_state_id)
{
	uint32_t val;

	/* LP0 */

	__disable_background_ops(cpu_id);

	/* Flow controller settings for LP0 */
	val = FLOW_CTRL_CSR_INTR_FLAG | FLOW_CTRL_CSR_EVENT_FLAG |
	      FLOW_CTRL_CSR_ENABLE |
	      (FLOW_CTRL_WAIT_WFI_BITMAP << cpu_id);
	flowctrl_write_cpu_csr(cpu_id, val);

	/* Wait till other CPUs enter C7 */
	for (int i = 0; i < MONCPUS; i++) {
		if (i == cpu_id)
			continue;

		val = readl(flowctrl_offset_cpu_csr[i]);
		val |= (FLOW_CTRL_CSR_EVENT_FLAG | FLOW_CTRL_CSR_INTR_FLAG);
		flowctrl_write_cpu_csr(i, val);
	}

	/* disable GIC */
	writel(0x1E0, ARM_GIC_CPU_BASE);
	val = readl(ARM_GIC_CPU_BASE);

	val = FLOW_CTRL_WAIT_FOR_INTERRUPT | FLOW_CTRL_HALT_LIC_IRQ |
	      FLOW_CTRL_HALT_LIC_FIQ;
	flowctrl_write_halt_cpu(cpu_id, val);

	/* allow restarting CPU #1 on LP0 exit */
	cpu_powergate_mask[1] = 0;

	/* MTS handles the entry into SC7 (aka LP0) state */
	val = 0xD;
	__asm__ volatile (
		"isb			\n"
		"msr	actlr_el1, %0	\n"
		"wfi			\n"
		: : "r" (val)
	);
}

int platform_psci_cpu_suspend(int cpu_id, uint32_t psci_state)
{
	uint32_t state = (psci_state >> PSCI_POWER_STATE_TYPE_SHIFT) &
		PSCI_POWER_STATE_TYPE_MASK;
	uint32_t afflvl = (psci_state >> PSCI_POWER_STATE_AFFL_SHIFT) &
		PSCI_POWER_STATE_AFFL_MASK;

	if (state != PSCI_POWER_STATE_TYPE_POWER_DOWN)
		return PSCI_RETURN_DENIED;

	plat_cpu_suspend(cpu_id, afflvl, psci_state & PSCI_POWER_STATE_ID_MASK);

	return PSCI_RETURN_SUCCESS;
}

static void psci_lock_reset_registers(void)
{
	uint32_t reg;

	/* ensure SECURE_SCRATCH34/35 are write locked */
	reg  = readl(TEGRA_PMC_BASE + PMC_SECURE_DISABLE3);
	reg |= (PMC_SECURE_DISABLE3_WRITE34_ON |
		PMC_SECURE_DISABLE3_WRITE35_ON);
	writel(reg, TEGRA_PMC_BASE + PMC_SECURE_DISABLE3);	/* lock */

	/* set secure boot control (read to flush) */
	reg  = readl(TEGRA_SB_BASE + SB_CSR);
	reg |= SB_CSR_NS_RST_VEC_WR_DIS;
	writel(reg, TEGRA_SB_BASE + SB_CSR);
	readl(TEGRA_SB_BASE + SB_CSR);

	/* ensure SECURE_SCRATCH22 is write locked */
	reg  = readl(TEGRA_PMC_BASE + PMC_SECURE_DISABLE2);
	reg |= PMC_SECURE_DISABLE2_WRITE22_ON;
	writel(reg, TEGRA_PMC_BASE + PMC_SECURE_DISABLE2);	/* lock */
}

void psci_program_reset_vectors(int cpu)
{
	uint64_t phys_cpu_reset;

	phys_cpu_reset = mon_virt_to_phys(armv8_trampoline);

	/* set exception vector (read to flush) */
	writel(phys_cpu_reset, TEGRA_EXCEPTION_VECTORS_BASE + EVP_CPU_RESET_VECTOR);
	readl(TEGRA_EXCEPTION_VECTORS_BASE + EVP_CPU_RESET_VECTOR);

	psci_lock_reset_registers();
}

void platform_psci_init_reset_vector(uint32_t cpu)
{
	uint64_t phys_cpu_reset;
	uint32_t reg;

	/* SECURE_SCRATCH22 should be writable */
	reg  = readl(TEGRA_PMC_BASE + PMC_SECURE_DISABLE2);
	ASSERT(!(reg & PMC_SECURE_DISABLE2_WRITE22_ON));

	/* initial AARCH32 reset address */
	phys_cpu_reset = mon_virt_to_phys(armv8_trampoline);
	writel(phys_cpu_reset, TEGRA_PMC_BASE + PMC_SECURE_SCRATCH22);

	/* both SECURE_SCRATCH34/SCRATCH35 should be writable */
	reg  = readl(TEGRA_PMC_BASE + PMC_SECURE_DISABLE3);
	reg &= (PMC_SECURE_DISABLE3_WRITE34_ON | PMC_SECURE_DISABLE3_WRITE35_ON);
	ASSERT(!reg);

	/* set exception vector to be used to resume from suspend */
	phys_cpu_reset = mon_virt_to_phys(&__mon_cpu_reset_vector);
	writel(phys_cpu_reset & 0xFFFFFFFF, TEGRA_PMC_BASE + PMC_SECURE_SCRATCH34);
	phys_cpu_reset >>= 32;
	writel(phys_cpu_reset & 0x7FF, TEGRA_PMC_BASE + PMC_SECURE_SCRATCH35);

	psci_program_reset_vectors(cpu);
}
