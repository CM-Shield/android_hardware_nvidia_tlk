/*
 * Copyright (c) 2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <err.h>
#include <debug.h>
#include <platform.h>
#include <platform/memmap.h>
#include <reg.h>
#include <string.h>

#include <lib/monitor/monitor_vector.h>
#include <platform/platform_monitor.h>

#define SZ_1MB 0x00100000

#define MC_SMMU_CONFIG_0			0x10
#define MC_SMMU_CONFIG_0_SMMU_ENABLE_DISABLE	0
#define MC_SMMU_CONFIG_0_SMMU_ENABLE_ENABLE	1

#define CLK_RST_CONTROLLER_RST_DEVICES_X_0	0x28c
#define GPU_RESET_MASK				(1 << 24)

#define MC_SMMU_TLB_CONFIG_0			0x14
#define MC_SMMU_TLB_CONFIG_0_RESET_VAL		0x20000010

#define MC_SMMU_PTC_CONFIG_0			0x18
#define MC_SMMU_PTC_CONFIG_0_RESET_VAL		0x2000003f

#define MC_SMMU_TLB_FLUSH_0			0x30
#define TLB_FLUSH_VA_MATCH_ALL		0
#define TLB_FLUSH_ASID_MATCH_DISABLE	0
#define TLB_FLUSH_ASID_MATCH_SHIFT	31
#define MC_SMMU_TLB_FLUSH_ALL			\
	(TLB_FLUSH_VA_MATCH_ALL | 		\
	(TLB_FLUSH_ASID_MATCH_DISABLE << TLB_FLUSH_ASID_MATCH_SHIFT))

#define MC_SMMU_PTC_FLUSH_0			0x34
#define MC_SMMU_PTC_FLUSH_ALL			0

#define MC_SMMU_ASID_SECURITY_0			0x38
#define ASID_SECURITY		(0)

#define MC_SECURITY_CFG0_0			0x70
#define MC_SECURITY_CFG1_0			0x74

#define MC_SMMU_TRANSLATION_ENABLE_0_0		0x228
#define MC_SMMU_TRANSLATION_ENABLE_1_0		0x22c
#define MC_SMMU_TRANSLATION_ENABLE_2_0		0x230
#define MC_SMMU_TRANSLATION_ENABLE_3_0		0x234
#define MC_SMMU_TRANSLATION_ENABLE_4_0		0xb98
#define TRANSLATION_ENABLE	(~0)

#define MC_VIDEO_PROTECT_BOM			0x648
#define MC_VIDEO_PROTECT_SIZE_MB		0x64c

static uint32_t   platform_sec_base;
static uint32_t   platform_sec_size;
static uintptr_t  save_vpr_base;
static uintptr_t  save_vpr_size;

__WEAK void platform_init_mc_sid(void)
{
	; // do nothing
}

static void init_smmu_hw()
{
	uintptr_t mc_base = TEGRA_MC_BASE;

	/* allow translations for all MC engines */
	*REG32(mc_base + MC_SMMU_TRANSLATION_ENABLE_0_0) = TRANSLATION_ENABLE;
	*REG32(mc_base + MC_SMMU_TRANSLATION_ENABLE_1_0) = TRANSLATION_ENABLE;
	*REG32(mc_base + MC_SMMU_TRANSLATION_ENABLE_2_0) = TRANSLATION_ENABLE;
	*REG32(mc_base + MC_SMMU_TRANSLATION_ENABLE_3_0) = TRANSLATION_ENABLE;
#ifdef TARGET_T210
	*REG32(mc_base + MC_SMMU_TRANSLATION_ENABLE_4_0) = TRANSLATION_ENABLE;
#endif

	*REG32(mc_base + MC_SMMU_ASID_SECURITY_0) = ASID_SECURITY;

	*REG32(mc_base + MC_SMMU_TLB_CONFIG_0) = MC_SMMU_TLB_CONFIG_0_RESET_VAL;
	*REG32(mc_base + MC_SMMU_PTC_CONFIG_0) = MC_SMMU_PTC_CONFIG_0_RESET_VAL;

	/* flush PTC and TLB */
	*REG32(mc_base + MC_SMMU_PTC_FLUSH_0) = MC_SMMU_PTC_FLUSH_ALL;
	(void) *REG32(mc_base + MC_SMMU_CONFIG_0);	/* read to flush writes */
	*REG32(mc_base + MC_SMMU_TLB_FLUSH_0) = MC_SMMU_TLB_FLUSH_ALL;

	/* enable SMMU */
	*REG32(mc_base + MC_SMMU_CONFIG_0) = MC_SMMU_CONFIG_0_SMMU_ENABLE_ENABLE;
	(void) *REG32(mc_base + MC_SMMU_CONFIG_0);	/* read to flush writes */
}

void platform_secure_dram_aperture()
{
	*REG32(TEGRA_MC_BASE + MC_SECURITY_CFG0_0) = platform_sec_base;
	*REG32(TEGRA_MC_BASE + MC_SECURITY_CFG1_0) = (platform_sec_size >> 20);
}

status_t platform_program_vpr(uint32_t vpr_base, uint32_t vpr_size)
{
	uintptr_t save_vpr_end, vpr_end;
	uint32_t val;

	/* if GPU is not in reset mode, then return error */
	val = readl(TEGRA_CLK_RESET_BASE + CLK_RST_CONTROLLER_RST_DEVICES_X_0);
	val &= GPU_RESET_MASK;
	if (!val) {
		dprintf(CRITICAL, "%s: GPU is not in reset mode\n", __func__);
		return ERR_NOT_ALLOWED;
	}

	if (!save_vpr_base && !save_vpr_size) {
		save_vpr_base = *REG32(TEGRA_MC_BASE + MC_VIDEO_PROTECT_BOM);
		save_vpr_size = *REG32(TEGRA_MC_BASE + MC_VIDEO_PROTECT_SIZE_MB);

		/* apply nvtboot WAR as initial carveout may include HIVEC_BASE */
		if ((uint64_t)save_vpr_base + (save_vpr_size * SZ_1MB) > NV_ARM_CORE_HIVEC_BASE) {
			save_vpr_end = ROUNDDOWN(NV_ARM_CORE_HIVEC_BASE, SZ_1MB);
			save_vpr_size = (save_vpr_end - save_vpr_base) / SZ_1MB;
		}
	}

	/* check if vpr_size and vpr_base are MB align */
	if ((vpr_size & (SZ_1MB-1)) || (vpr_base & (SZ_1MB-1))  ) {
		dprintf(CRITICAL, "%s: vpr base/size not MB align\n", __func__);
		return ERR_GENERIC;
	}

	vpr_end = vpr_base + vpr_size;
	vpr_size = vpr_size / SZ_1MB;
	save_vpr_end = save_vpr_base + (save_vpr_size * SZ_1MB);

	/* check if there was a previous VPR region */
	if (save_vpr_size) {
#if defined(WITH_MONITOR_BIN)
		mon_mmu_map_uncached(save_vpr_base, save_vpr_base, (save_vpr_size * SZ_1MB));
#endif
		/* clear old regions now exposed in the new region */
		if (vpr_base > save_vpr_end || save_vpr_base > vpr_end) {
			/* clear whole old region (no overlap with new region) */
			memset((void*)save_vpr_base, 0, (save_vpr_size * SZ_1MB));
		} else {
			if (save_vpr_base < vpr_base) {
				/* clear old sub-region below new base */
				memset((void*)save_vpr_base, 0, vpr_base-save_vpr_base);
			}
			if (save_vpr_end > vpr_end) {
				/* clear old sub-region above new end */
				memset((void*)vpr_end, 0, save_vpr_end-vpr_end);
			}
		}
#if defined(WITH_MONITOR_BIN)
		mon_mmu_unmap(save_vpr_base, (save_vpr_size * SZ_1MB));
#endif
	}

	*REG32(TEGRA_MC_BASE + MC_VIDEO_PROTECT_BOM) = vpr_base;
	*REG32(TEGRA_MC_BASE + MC_VIDEO_PROTECT_SIZE_MB) = vpr_size;
	save_vpr_base = vpr_base;
	save_vpr_size = vpr_size;

	return NO_ERROR;
}

void platform_init_memory(uint32_t sec_base, uint32_t sec_size)
{
	platform_sec_base = sec_base;
	platform_sec_size = sec_size;

	init_smmu_hw();
}

void platform_restore_memory()
{
	init_smmu_hw();
	platform_secure_dram_aperture();

	/* Program VPR during LP0 exit, if we have the most current value */
	if (save_vpr_base && save_vpr_size) {
		*REG32(TEGRA_MC_BASE + MC_VIDEO_PROTECT_BOM) = save_vpr_base;
		*REG32(TEGRA_MC_BASE + MC_VIDEO_PROTECT_SIZE_MB) = save_vpr_size;
	}
}
