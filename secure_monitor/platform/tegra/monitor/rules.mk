LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR).platform

ifeq ($(MONITOR_BIN),true)

MONARCH := $(MONITOR_ARCH)
MODULE_ARCH := $(MONARCH)

# linked into the monitor build (monitor.bin)
MONITOR_MODULE := true

MODULE_SRCS += \
	$(LOCAL_DIR)/memory.c \
	$(LOCAL_DIR)/interrupts.c \
	$(LOCAL_DIR)/platform.c \
	$(LOCAL_DIR)/debug.c \
	$(LOCAL_DIR)/psci.c \
	$(LOCAL_DIR)/psci_$(TARGET).c

MODULE_SRCS += $(TARGET_SRCS)

INCLUDES += \
	-I$(LOCAL_DIR)/include \
	-I$(LOCAL_DIR)/$(MONARCH)/include

DEFINES += \
	WITH_MONITOR_BIN=1 \
	MONCPUS_SHIFT=$(MONCPUS_SHIFT) \
	MONCPUS=$(MONCPUS)

ifeq ($(MONARCH),arm64)
MODULE_CC := $(TOOLCHAIN_PREFIX64)gcc
MODULE_LD := $(TOOLCHAIN_PREFIX64)ld
MON_OBJCOPY := $(TOOLCHAIN_PREFIX64)objcopy
endif

else

MODULE_COMPILEFLAGS += -mcpu=$(ARM_CPU)

# linked into secureos build (lk.bin)
MODULE_SRCS += \
	$(LOCAL_DIR)/memory.c \
	$(LOCAL_DIR)/interrupts.c \
	$(LOCAL_DIR)/platform.c \
	$(LOCAL_DIR)/psci.c \
	$(LOCAL_DIR)/psci_$(TARGET).c

INCLUDES += \
	-I$(LOCAL_DIR)/include

DEFINES += \
	MONCPUS=$(MONCPUS)

endif

include make/module.mk
