/*
 * Copyright (c) 2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <err.h>
#include <assert.h>
#include <malloc.h>
#include <string.h>
#include <reg.h>
#include <psci.h>
#include <compiler.h>
#include <arch/ops.h>
#include <arch/arm.h>
#include <arch/arm/mmu.h>
#include <arch/arm/monitor_cpu.h>
#include <platform.h>
#include <platform/memmap.h>
#include <platform/tzrammap.h>
#include <platform/platform_monitor.h>
#include <platform/platform_sip.h>
#include <lib/monitor/monitor_vector.h>

extern void platform_enable_debug_intf(void);
extern void platform_disable_debug_intf(void);

extern unsigned int monitor_vector_base;
extern unsigned long mon_stack_top[MONCPUS];
extern unsigned long boot_secondary_cpu_addr;
extern unsigned long mon_stdcall_frame_addr;

extern volatile uint32_t cpu_affinity_info[MONCPUS];

#define MONITOR_MODE_STACK_SZ		4096

/* referenced APBDEV_PMC_SECURE registers */
#define PMC_SCRATCH38			0x134
#define PMC_SECURE_DISABLE2		0x2c4
#define PMC_SECURE_SCRATCH22		0x338

#define PMC_PWRGATE_STATUS		0x38
#define PMC_PWRGATE_TOGGLE		0x30
#define PMC_TOGGLE_START		0x100

#define FLOW_CTRL_HALT_CPU0_EVENTS	0x0
#define  FLOW_CTRL_WAITEVENT		(2 << 29)
#define  FLOW_CTRL_WAIT_FOR_INTERRUPT	(4 << 29)
#define  FLOW_CTRL_JTAG_RESUME		(1 << 28)
#define  FLOW_CTRL_HALT_SCLK		(1 << 27)
#define  FLOW_CTRL_HALT_LIC_IRQ		(1 << 11)
#define  FLOW_CTRL_HALT_LIC_FIQ		(1 << 10)
#define  FLOW_CTRL_HALT_GIC_IRQ		(1 << 9)
#define  FLOW_CTRL_HALT_GIC_FIQ		(1 << 8)

#define FLOW_CTRL_CPU0_CSR		0x8
#define  FLOW_CTRL_CSR_PWR_OFF_STS	(1 << 16)
#define  FLOW_CTRL_CSR_INTR_FLAG	(1 << 15)
#define  FLOW_CTRL_CSR_EVENT_FLAG	(1 << 14)
#define  FLOW_CTRL_CSR_IMMEDIATE_WAKE	(1 << 3)
#define  FLOW_CTRL_CSR_ENABLE		(1 << 0)

#define FLOW_CTRL_HALT_CPU1_EVENTS	0x14
#define FLOW_CTRL_CPU1_CSR		0x18

#define FLOW_CTRL_WAIT_WFI_BITMAP	0x100
#define FLOW_CTRL_WAIT_WFE_BITMASK	(0xF << 4)
#define FLOW_CTRL_WAIT_WFI_BITMASK	(0xF << 8)

static int cpu_powergate_mask[MONCPUS];
static int pmc_cpu_powergate_id[MONCPUS] = {0 , 9, 10, 11};

static uintptr_t flowctrl_offset_cpu_csr[MONCPUS] = {
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_CPU0_CSR),
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_CPU1_CSR),
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_CPU1_CSR + 8),
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_CPU1_CSR + 16)
};

static uintptr_t flowctrl_offset_halt_cpu[MONCPUS] = {
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_HALT_CPU0_EVENTS),
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_HALT_CPU1_EVENTS),
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_HALT_CPU1_EVENTS + 8),
	(TEGRA_FLOW_CTRL_BASE + FLOW_CTRL_HALT_CPU1_EVENTS + 16)
};

static inline void flowctrl_write_cpu_csr(int cpu_id, uint32_t val)
{
	writel(val, flowctrl_offset_cpu_csr[cpu_id]);
	DSB;
	val = readl(flowctrl_offset_cpu_csr[cpu_id]);
}

static inline void flowctrl_write_halt_cpu(int cpu_id, uint32_t val)
{
	writel(val, flowctrl_offset_halt_cpu[cpu_id]);
	DSB;
	val = readl(flowctrl_offset_halt_cpu[cpu_id]);
}

/*
 * The PSCI spec from ARM states the following for power mgmt:
 * SMC         (r0) - CPU_SUSPEND  = 0x84000001
 * power_state (r1) - Bits [0:15]  = StateID
 *                    Bit  [16]    = StateType <0=Stdby, 1=PwrDn>
 *                    Bits [17:23] = MBZ
 *                    Bits [24:25] = AffinityLevel <0=cpu, 1=cluster>
 *                    Bits [26:31] = MBZ
 * entry_addr  (r2) - CPU wake up addr
 * context_id  (r3) - args in r0 when cpu wakes up from pwrdn state and enters
 *                    exception level of caller
 * returns     (r0) - SUCCESS/INVALID PARAM
 */
#define LP0				((1 << 16) | (1 << 24) | 1)
#define LP1				((1 << 16) | (1 << 24) | 2)
#define LP1_MC_CLK_STOP			((1 << 16) | 3)
#define LP2_CLUSTER_PWR_DN		((1 << 16) | (1 << 24) | 4)
#define LP2_NO_FLUSH			((1 << 16) | 5)

/* tracks if we need to load resume handlers into tzram */
static bool load_tzram_lp1_resume_handler = true;

/* LP1 resume code */
extern uint32_t _lp1_resume;
extern uint32_t _lp1_resume_end;

#define LP1_RESUME_HANDLER_SIZE \
	((uint32_t)&_lp1_resume_end - (uint32_t)&_lp1_resume)

/* store the IRAM vector addresses for CPU suspend */
uint32_t iram_stop_mc_clk_fn_pa, iram_sleep_core_fn_pa;

static void allocate_percpu_monitor_stacks(void)
{
	void *stack_top_mon;
	int stack_size = MONITOR_MODE_STACK_SZ;

	for (int i = 0; i < MONCPUS; i++) {
		stack_top_mon = calloc(1, stack_size);
		if (!stack_top_mon)
			panic("no memory available for monitor stack");

		mon_stack_top[i] = (unsigned long)(stack_top_mon +
			(stack_size << 1));
	}
}

static void set_reset_vector(unsigned long vector_addr)
{
	uint32_t evp_cpu_reset = TEGRA_EXCEPTION_VECTORS_BASE + 0x100;
	uint32_t sb_ctrl = TEGRA_SB_BASE;
	uint32_t reg;

	/* set new reset vector */
	*(volatile uint32_t *)evp_cpu_reset = (uint32_t)vector_addr;

	/* dummy read to ensure successful write */
	reg = *(volatile uint32_t *)evp_cpu_reset;

	/* lock reset vector */
	reg = *(volatile uint32_t *)sb_ctrl;
	reg |= 2;
	*(volatile uint32_t *)sb_ctrl = reg;
}

void pm_early_init(void)
{
	int curr_cpu = mon_cpu_id();

	flowctrl_write_cpu_csr(curr_cpu, 0);
	flowctrl_write_halt_cpu(curr_cpu, 0);

	/* set monitor vector base address (use vaddr, since MMU's enabled) */
	mon_cpu_set_mvbar((unsigned int)&monitor_vector_base);

	/* populate the reset vector to boot all the secondary cpus */
	set_reset_vector(virtual_to_physical(boot_secondary_cpu_addr));

	/* mark the CPU as ON */
	cpu_affinity_info[curr_cpu] = PSCI_0_2_AFFINITY_LEVEL_ON;
}

void platform_psci_init(int cpu_id)
{
	extern uint32_t __load_phys_size;

	/* allocate space for std call monitor frames */
	mon_stdcall_frame_addr = (unsigned long)calloc(1, sizeof(uint64_t) * 15);
	ASSERT(mon_stdcall_frame_addr);

	/* allocate monitor vector stacks */
	allocate_percpu_monitor_stacks();

	/* set monitor vector stack */
	mon_cpu_set_monitor_stack((void *)mon_stack_top[cpu_id]);

	pm_early_init();

	/* set normal world access in NSACR */
	mon_cpu_set_nsacr();

	/* setup GIC */
	mon_cpu_gic_setup();

	/* save CPU context to bring up secondary cpus */
	mon_cpu_save_context();

	platform_init_memory(__load_phys_base, __load_phys_size);

	platform_config_interrupts();
}

void platform_psci_init_reset_vector(uint32_t cpu)
{
	extern unsigned long mon_p2v_offset;
	extern uint32_t _boot_secondary_phys_base;
	uint32_t reg;

	/* save off values to help with v-to-p operations */
	_boot_secondary_phys_base = __load_phys_base;
	mon_p2v_offset = (VMEMBASE - __load_phys_base);

	/* install the cpu resume handler to PMC_SEC_SCRATCH22 */
	reg = readl(TEGRA_PMC_BASE + PMC_SECURE_DISABLE2);
	writel(reg & ~(1 << 28), TEGRA_PMC_BASE + PMC_SECURE_DISABLE2);	/* unlock */

	writel(virtual_to_physical(boot_secondary_cpu_addr),
		TEGRA_PMC_BASE + PMC_SECURE_SCRATCH22);

	reg = readl(TEGRA_PMC_BASE + PMC_SECURE_DISABLE2);
	writel(reg | 1 << 28, TEGRA_PMC_BASE + PMC_SECURE_DISABLE2);	/* lock */
}

static void save_lp1_context(void)
{
	uint32_t cpu = mon_cpu_id();

	/* store any state needed for lp1 resume to tzram */
	TZRAM_STORE(TZRAM_BOOT_SECONDARY_CPU_ADDR,
		virtual_to_physical(boot_secondary_cpu_addr));
	TZRAM_STORE(TZRAM_MON_STACK_TOP, virtual_to_physical(mon_stack_top[cpu]));
	TZRAM_STORE(TZRAM_NS_RESUME_ADDR, __mon_cpu_return_addr[cpu]);
	TZRAM_STORE(TZRAM_MVBAR, virtual_to_physical(&monitor_vector_base));

	mon_cpu_copy_context((void *)TZRAM_CPU_CONTEXT);
}

/*
 * Suspend-related SMCs.
 */
static void plat_cpu_enter_lp0(void)
{
	/*
	 * This CPU is power-ungated by the warmboot code, so
	 * clear this flag to avoid querying the FC during LP0
	 * resume.
	 */
	cpu_powergate_mask[mon_cpu_id()] = 0;

	/* set our LP0 reset vector */
	set_reset_vector(virtual_to_physical(boot_secondary_cpu_addr));

	/* save off current state */
	mon_cpu_save_context();

	/* need to reload LP1 handler into tzram before next LP1 suspend */
	load_tzram_lp1_resume_handler = true;

	platform_disable_debug_intf();

	/* flush/disable dcache */
	arch_disable_cache(DCACHE);

	/*
	 * The new NS world driver sends the SMC to register the iRAM vector
	 * address. Check if the NS driver is in sync, before proceeding.
	 */
	if (!iram_sleep_core_fn_pa)
		return;

	/* exit coherency */
	mon_cpu_exit_coherency();

	/*
	 * Switch to NS world and execute the suspend vector from IRAM
	 */
	mon_cpu_suspend(iram_sleep_core_fn_pa);
}

static void plat_cpu_enter_lp1(uint stop_mc_clk)
{
	uint32_t suspend_vector = (stop_mc_clk ? iram_stop_mc_clk_fn_pa :
		iram_sleep_core_fn_pa);
	ASSERT(LP1_RESUME_HANDLER_SIZE < TEGRA_TZRAM_SIZE);

	/* set our LP1 reset vector */
	set_reset_vector(TZRAM_LP1_RESUME_HANDLER);

	/* stop MC clock during suspend entry? */
	TZRAM_STORE(TZRAM_CPU_AVOID_CLKM_SWITCH, stop_mc_clk);

	/* save off current state */
	mon_cpu_save_context();

	save_lp1_context();

	/* load LP1 resume handler to TZRAM if necessary */
	if (load_tzram_lp1_resume_handler) {
		memcpy((void *)(TZRAM_LP1_RESUME_HANDLER), (void *)&_lp1_resume,
			LP1_RESUME_HANDLER_SIZE);
		load_tzram_lp1_resume_handler = false;
	}

	/* flush/disable dcache */
	arch_disable_cache(DCACHE);

	/*
	 * The new NS world driver sends the SMC to register the iRAM vector
	 * address. Check if the NS driver is in sync, before proceeding.
	 */
	if (!suspend_vector)
		return;

	/* exit coherency */
	mon_cpu_exit_coherency();

	/*
	 * Switch to NS world and execute the LP1 vector from IRAM
	 */
	mon_cpu_suspend(suspend_vector);
}

static void plat_cpu_powerdn_cluster(int cpu_id)
{
	uint32_t val;

	/* save off current CPU/MMU state */
	if (cpu_id == 0)
		mon_cpu_save_context();

	/* flush/disable dcache */
	arch_disable_cache(DCACHE);

	/*
	 * Check if the NS driver is in sync, before proceeding.
	 */
	if (!iram_sleep_core_fn_pa && !iram_stop_mc_clk_fn_pa)
		return;

	/* exit coherency */
	mon_cpu_exit_coherency();

	/* Program the Flow Controller's CSR and HALT registers */
	val = readl(flowctrl_offset_cpu_csr[cpu_id]);
	val |= (FLOW_CTRL_CSR_INTR_FLAG | FLOW_CTRL_CSR_EVENT_FLAG |
		FLOW_CTRL_CSR_ENABLE);
	flowctrl_write_cpu_csr(cpu_id, val);

	if (val & FLOW_CTRL_CSR_IMMEDIATE_WAKE)
		val = FLOW_CTRL_WAIT_FOR_INTERRUPT;
	else
		val = FLOW_CTRL_WAITEVENT;

	val |= (FLOW_CTRL_HALT_LIC_IRQ | FLOW_CTRL_HALT_LIC_FIQ);
	flowctrl_write_halt_cpu(cpu_id, val);

	mon_cpu_enter_wfi();
}

static void plat_cpu_enter_lp2(int cpu_id)
{
	uint32_t val;

	/* save off current CPU/MMU state */
	if (cpu_id == 0)
		mon_cpu_save_context();

	/*
	 * Check if the NS driver is in sync, before proceeding.
	 */
	if (!iram_sleep_core_fn_pa && !iram_stop_mc_clk_fn_pa)
		return;

	/* flush L1 $ */
	mon_cpu_flush_l1();

	/* exit coherency */
	mon_cpu_exit_coherency();

	/* Program the Flow Controller's CSR and HALT registers */
	val = FLOW_CTRL_CSR_INTR_FLAG | FLOW_CTRL_CSR_EVENT_FLAG |
		FLOW_CTRL_CSR_ENABLE | (FLOW_CTRL_WAIT_WFI_BITMAP << cpu_id);
	flowctrl_write_cpu_csr(cpu_id, val);

	val = readl(flowctrl_offset_cpu_csr[cpu_id]);
	flowctrl_write_cpu_csr(cpu_id, val);

	val = FLOW_CTRL_WAIT_FOR_INTERRUPT;
	flowctrl_write_halt_cpu(cpu_id, val);

	mon_cpu_enter_wfi();
}

int platform_psci_cpu_suspend(int cpu_id, uint32_t psci_state)
{
	switch (psci_state) {
	case LP2_CLUSTER_PWR_DN:
		plat_cpu_powerdn_cluster(cpu_id);
		break;

	case LP2_NO_FLUSH:
		plat_cpu_enter_lp2(cpu_id);
		break;

	case LP1_MC_CLK_STOP:
	case LP1:
		plat_cpu_enter_lp1(psci_state == LP1_MC_CLK_STOP);
		break;

	case LP0:
		plat_cpu_enter_lp0();
		break;

	default:
		return PSCI_RETURN_NOT_SUPPORTED;
	}

	return PSCI_RETURN_SUCCESS;
}

int platform_psci_cpu_on(int cpu_id)
{
	uint32_t val;

	if (cpu_powergate_mask[cpu_id] == 0) {
		/* use PMC */

		/* fill in the CPU powergate mask */
		cpu_powergate_mask[cpu_id] = 1;

		/* check if CPU is already power ungated */
		val = readl(TEGRA_PMC_BASE + PMC_PWRGATE_STATUS);
		if (val & (1 << pmc_cpu_powergate_id[cpu_id]))
			return PSCI_RETURN_SUCCESS;

		val = pmc_cpu_powergate_id[cpu_id] | PMC_TOGGLE_START;
		writel(val, TEGRA_PMC_BASE + PMC_PWRGATE_TOGGLE);

		/*
		 * The PMC deasserts the START bit when it starts the power
		 * ungate process. Loop till powergate START bit is asserted.
		 */
		do {
			val = readl(TEGRA_PMC_BASE + PMC_PWRGATE_TOGGLE);
		} while (val & (1 << 8));

		/* loop till the CPU is power ungated */
		do {
			val = readl(TEGRA_PMC_BASE + PMC_PWRGATE_STATUS);
		} while ((val & (1 << pmc_cpu_powergate_id[cpu_id])) == 0);
	} else {
		/* use FC */
		flowctrl_write_cpu_csr(cpu_id, FLOW_CTRL_CSR_ENABLE);
		flowctrl_write_halt_cpu(cpu_id, FLOW_CTRL_WAITEVENT |
			FLOW_CTRL_HALT_SCLK);
	}

	return PSCI_RETURN_SUCCESS;
}

void platform_psci_cpu_off(int cpu_id, uint32_t psci_state)
{
	uint32_t val;

	/* flush/disable dcache last */
	arch_disable_cache(DCACHE);

	/* exit coherency */
	mon_cpu_exit_coherency();

	/* program flow controller for CPU power down */
	val = FLOW_CTRL_CSR_INTR_FLAG | FLOW_CTRL_CSR_EVENT_FLAG |
		FLOW_CTRL_CSR_ENABLE | (FLOW_CTRL_WAIT_WFI_BITMAP << cpu_id);
	flowctrl_write_cpu_csr(cpu_id, val);

	val = readl(flowctrl_offset_cpu_csr[cpu_id]);
	flowctrl_write_cpu_csr(cpu_id, val);

	/* program Flow Controller to halt CPU */
	val = FLOW_CTRL_WAITEVENT;
	flowctrl_write_halt_cpu(cpu_id, val);

	mon_cpu_enter_wfi();
}
