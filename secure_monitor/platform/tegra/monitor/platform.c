/*
 * Copyright (c) 2014-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <err.h>
#include <debug.h>
#include <target/debugconfig.h>
#include <platform.h>
#include <platform/platform_monitor.h>
#include <platform/platform_sip.h>
#include <lib/monitor/monitor_vector.h>
#include <platform/memmap.h>

#if WITH_MONITOR_BIN

#define CORTEX_A57	0xD07
#define CORTEX_A53	0xD03

#define L2ACTLR		(1 << 6)
#define L2ECTLR		(1 << 5)
#define L2CTLR		(1 << 4)
#define CPUECTLR	(1 << 1)
#define CPUACTLR	(1 << 0)
#define ACTLR_MODE	(L2ACTLR | L2ECTLR | L2CTLR | CPUECTLR | CPUACTLR)

/* sets of MMIO ranges setup */
#define MMIO_RANGE_0_ADDR	0x50000000
#define MMIO_RANGE_1_ADDR	0x60000000
#define MMIO_RANGE_2_ADDR	0x70000000
#define MMIO_RANGE_3_ADDR	0x54200000
#define MMIO_RANGE_SIZE		0x200000

extern uint64_t __mon_cpu_fiq_glue;
volatile int enter_deep_sleep;

/* Implementation specific CPU init */
void platform_monitor_init_cpu(void)
{
	uint32_t val = ACTLR_MODE;
	int cpu = mon_get_cpu_id();

	/* enable L2 and CPU ECTLR RW access from non-secure world */
	if (cpu == CORTEX_A57 || cpu == CORTEX_A53) {
		__asm__ volatile (
			"msr	actlr_el3, %0	\n"
			"msr	actlr_el2, %0	\n"
			:: "r" (val)
		);
	}
}

__WEAK void platform_map_mmio(void)
{
	/* identity map MMIO ranges for register access */
	mon_mmu_map_mmio(MMIO_RANGE_0_ADDR, MMIO_RANGE_0_ADDR, MMIO_RANGE_SIZE);
	mon_mmu_map_mmio(MMIO_RANGE_1_ADDR, MMIO_RANGE_1_ADDR, MMIO_RANGE_SIZE);
	mon_mmu_map_mmio(MMIO_RANGE_2_ADDR, MMIO_RANGE_2_ADDR, MMIO_RANGE_SIZE);
	mon_mmu_map_mmio(MMIO_RANGE_3_ADDR, MMIO_RANGE_3_ADDR, MMIO_RANGE_SIZE);
}

void platform_init_common(int cpu)
{
	platform_map_mmio();
	platform_init_debug_port(DEFAULT_DEBUG_PORT);
	platform_monitor_init_cpu();
	platform_init_memory(__mon_phys_base, __mon_phys_size);
	platform_init_mc_sid();
	platform_config_interrupts();
}

__WEAK status_t platform_vrr_fastcall(uint64_t cmd, uint64_t arg)
{
	return NO_ERROR;
}

#else
extern uint32_t iram_stop_mc_clk_fn_pa, iram_sleep_core_fn_pa;
#endif

__WEAK int soc_sip_handler(struct fastcall_frame *frame, uint32_t cpu)
{
	return 0;
}

void platform_sip_handler(struct fastcall_frame *frame, uint32_t cpu)
{
	uint32_t func_id = frame->r[0];

	if (func_id >= SMC_SIP_SOC_PRIVATE_BEGIN &&
		func_id <= SMC_SIP_SOC_PRIVATE_END) {
		frame->r[0] = soc_sip_handler(frame, cpu);
		return;
	}

	switch (func_id) {
	case SMC_SIP_PROGRAM_VPR:
		frame->r[0] = platform_program_vpr(frame->r[1], frame->r[2]);
		break;

#if WITH_MONITOR_BIN
	case SMC_SIP_REGISTER_FIQ_GLUE:
		__mon_cpu_fiq_glue = frame->r[1];
		frame->r[0] = NO_ERROR;
		break;

	case SMC_SIP_DEEP_SLEEP_ENTRY:
		enter_deep_sleep = frame->r[1];
		break;

	case SMC_SIP_VRR_SET_BUF:
	case SMC_SIP_VRR_SEC:
		frame->r[0] = platform_vrr_fastcall(frame->r[0], frame->r[1]);
		break;
#else
	case SMC_SIP_REGISTER_LP0_IRAM_VECTOR:
		/*
		 * Register IRAM vector addresses for the MC-CLK-CTOP and
		 * CORE-SLEEP case.
		 */
		iram_stop_mc_clk_fn_pa = frame->r[1];
		iram_sleep_core_fn_pa = frame->r[2];
		break;
#endif

	default:
		frame->r[0] = ERR_NOT_SUPPORTED;
	}
}
