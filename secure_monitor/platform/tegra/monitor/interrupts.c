/*
 * Copyright (c) 2014-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <sys/types.h>
#include <assert.h>
#include <debug.h>
#include <err.h>
#include <reg.h>
#include <string.h>
#include <platform/interrupts.h>
#include <arch/ops.h>
#include <platform/memmap.h>
#include <platform/platform_monitor.h>
#include <lib/monitor/monitor_vector.h>

#define GIC_CPU_CTRL		0x0
#define GIC_CPU_PMR		0x4

#define GIC_DIST_CTRL		0x0
#define GIC_DIST_TYPER		0x4
#define GIC_DIST_IGROUPR0	0x80
#define GIC_DIST_ISENABLER	0x110
#define GIC_DIST_TARGETSR	0x800
#define GIC_DIST_INTCFG		0xC00
#define GIC_DIST_PRIORITY	0x400

#if !defined(WITH_MONITOR_BIN)
#define ICTLR_CPU_IEP_VFIQ	0x08
#define ICTLR_CPU_IEP_FIR	0x14
#define ICTLR_CPU_IEP_FIR_CLR	0x1c

#define ICTLR_CPU_IER_SET	0x24
#define ICTLR_CPU_IER_CLR	0x28
#define ICTLR_CPU_IEP_CLASS	0x2C

#define ICTLR_COP_IER_CLR	0x38

static uint32_t num_ictrlrs;

static uintptr_t ictlr_reg_base[] = {
	TEGRA_PRIMARY_ICTLR_BASE,
	TEGRA_SECONDARY_ICTLR_BASE,
	TEGRA_TERTIARY_ICTLR_BASE,
	TEGRA_QUATERNARY_ICTLR_BASE,
	TEGRA_QUINARY_ICTLR_BASE,
};

void platform_init_interrupts(void)
{
	uint32_t i;

	num_ictrlrs = readl(ARM_GIC_DIST_BASE + GIC_DIST_CTRL);
	num_ictrlrs &= 0x1f;

	for (i = 0; i < num_ictrlrs; i++) {
		uintptr_t ictrl = ictlr_reg_base[i];
		writel(~0, ictrl + ICTLR_CPU_IER_CLR);
		writel(0, ictrl + ICTLR_CPU_IEP_CLASS);
		writel(~0, ictrl + ICTLR_CPU_IEP_FIR_CLR);
		writel(~0, ictrl + ICTLR_CPU_IER_SET);
		writel(~0, ictrl + ICTLR_COP_IER_CLR);
	}
}
#endif

void platform_config_interrupts(void)
{
#if !defined(MONTARGET_ARM64)
	uintptr_t gicd_groupr_base = ARM_GIC_DIST_BASE + GIC_DIST_IGROUPR0;
	uintptr_t gicd_prio_base = ARM_GIC_DIST_BASE + GIC_DIST_PRIORITY;
	uintptr_t gicd_targetsr_base = ARM_GIC_DIST_BASE + GIC_DIST_TARGETSR;
	uintptr_t gicd_intconfig_base = ARM_GIC_DIST_BASE + GIC_DIST_INTCFG;
	uint32_t wdt_cpu = (4 * 32) + 27;
#endif
	int num_irqs =  readl(ARM_GIC_DIST_BASE + GIC_DIST_TYPER);
	num_irqs = (num_irqs & 0x1f) + 1;

	/* set priority value to 128 for SPIs before making them NS */
	uintptr_t base = ARM_GIC_DIST_BASE + GIC_DIST_PRIORITY;
	for (int i = 8; i < num_irqs * 8; i++)
		writel(0x80808080, base + i * 4);

	/* mark SPIs as NS, skip per-CPU GROUPR0 SGIs (see mon_init_cpu) */
	base = ARM_GIC_DIST_BASE + GIC_DIST_IGROUPR0;
	for (int i = 1; i < num_irqs; i++)
		writel(~0, base + i * 4);

#if !defined(MONTARGET_ARM64)
	/* enable fiqs, group 0/1, irq/fiq bypass fields */
	uint32_t val = (1 << 0 | 1 << 1 | 1 << 3 | 0xF << 5);
	writel(val, ARM_GIC_CPU_BASE + GIC_CPU_CTRL);

	/* priority mask max, accept ints at all priority levels */
	writel(0xFF, ARM_GIC_CPU_BASE + GIC_CPU_PMR);

	/* enable group 0/1 in the distributor */
	writel((1 << 1) | (1 << 0), ARM_GIC_DIST_BASE + GIC_DIST_CTRL);

	/* enable path for WDT_CPU in GIC */
	val = readl(ARM_GIC_DIST_BASE + GIC_DIST_ISENABLER);
	val |= (1 << 27);
	writel(val, ARM_GIC_DIST_BASE + GIC_DIST_ISENABLER);

	/* map WDT_CPU to CPU 0-3 */
	val = readl(gicd_targetsr_base + ((wdt_cpu / 4) * 4));
	val &= ~(0xFF << ((wdt_cpu % 4) * 8));
	val |= (0xF << ((wdt_cpu % 4) * 8));
	writel(val, gicd_targetsr_base + ((wdt_cpu / 4) * 4));

	/* configure WDT_CPU level-triggered */
	val = readl(gicd_intconfig_base + ((wdt_cpu / 16) * 4));
	val &= ~(3 << ((wdt_cpu % 16) * 2));
	writel(val, gicd_intconfig_base + ((wdt_cpu / 16) * 4));

	/* configure CPU WDT as FIQ (grp 0) */
	val = readl(gicd_groupr_base + ((wdt_cpu / 32) * 4));
	val &= ~(1 << (wdt_cpu % 32));
	writel(val, gicd_groupr_base + ((wdt_cpu / 32) * 4));

	/* CPU WDT is high priority */
	val = readl(gicd_prio_base + ((wdt_cpu / 4) * 4));
	val &= ~(0xFF << ((wdt_cpu % 4) * 8));
	writel(val, gicd_prio_base + ((wdt_cpu / 4) * 4));
#endif
}
