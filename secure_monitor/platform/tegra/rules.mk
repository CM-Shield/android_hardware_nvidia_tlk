LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR).platform

ENABLE_THUMB := false

ARCH := arm

INCLUDES += \
	-I$(LOCAL_DIR)/include \
	-I$(LOCAL_DIR)/include/platform/$(PLATFORM_SOC) \
	-I$(LOCAL_DIR)/common \
	-I../lib

COMMON_DIR := $(LOCAL_DIR)/common
PLATFORM_SOC_DIR := $(LOCAL_DIR)/$(PLATFORM_SOC)

# The code within the monitor dir builds to a lib that's either
# linked into a mon.bin (when MONITOR_BIN is true) or into the
# secureos when it's not.
#
# Routines in the monitor dir are those that restore HW state.
# In particular, SOC level state is restored there, while CPU
# state was handled by lib/monitor or boot_secondary.S,
#
# In platform/tegra/common, there are still memory and interrupt
# routines, but they're only called for secureos handling
# (i.e. aren't part of the monitor or for restoring HW state).

MODULE_DEPS += \
	$(LOCAL_DIR)/monitor

include $(PLATFORM_SOC_DIR)/rules.mk

# Disable all prints for release builds, only CRITICAL prints will be
# printed in release builds. Change the value of DEBUG in else block
# to 1->INFO and 2->SPEW prints in debug builds.
ifeq ($(TARGET_BUILD_TYPE), release)
	DEBUG := 0
else
	DEBUG := 1
endif

# relocate image to provided physaddr via MMU
ARM_USE_MMU_RELOC := true

# enable use of CPU caching
ARM_USE_CPU_CACHING := true

DEFINES += VMEMBASE=$(VMEMBASE) \
	VMEMSIZE=$(VMEMSIZE) \
	DEBUG=$(DEBUG) \
	WITH_CPU_EARLY_INIT=1

# use a two segment memory layout, where all of the read-only sections
# of the binary reside in rom, and the read/write are in memory. The
# ROMBASE, VMEMBASE, and VMEMSIZE make variables are required to be set
# for the linker script to be generated properly.
#
LINKER_SCRIPT += \
	$(BUILDDIR)/system-onesegment.ld
