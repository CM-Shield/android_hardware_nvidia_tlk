LOCAL_DIR := $(GET_LOCAL_DIR)

INCLUDES += \
	-I$(LOCAL_DIR)/include

PLATFORM := tegra
PLATFORM_SOC := tegra4

# using phys (MAP_NOR_BASE -> MAP_NOR_LIMIT) as VA space
VMEMSIZE := 0x8000000	# 128MB

#
# As part of the build, also generate a separate mon.bin from
# lib/monitor of the specified architecture. This binary will
# an address space of 2^MONBITS in size linked at MONBASE.
#
MONITOR_ARCH := arm64
MONITOR_BIN := true

MODULE_DEPS += lib/monitor

MONBASE := 0x40000000
MONBITS := 33
MONCPUS := 4
MONCPUS_SHIFT := 2
MONTARGET := ARM64

# base of GIC dist/cpu registers
ARM_GIC_DIST_BASE := 0x50041000
ARM_GIC_CPU_BASE  := 0x50042000

# frequency of ARM generic timer
ARM_SYSTEM_COUNTER_FREQ := 19200000

DEFINES += \
	ARM_GIC_DIST_BASE=$(ARM_GIC_DIST_BASE) \
	ARM_GIC_CPU_BASE=$(ARM_GIC_CPU_BASE) \
	ARM_SYSTEM_COUNTER_FREQ=$(ARM_SYSTEM_COUNTER_FREQ) \
	WITH_PLATFORM_IDLE=1 \
	WITH_AA64_CPU_RESET_VECTORS=1 \
	TARGET_T210=1

ifeq ($(STANDALONE_MONITOR), true)
DEFINES += \
	WITH_EL3_MONITOR_ONLY=1
endif
