# comment out or override if you want to see the full output of each command
NOECHO ?= @

$(OUTELF): $(ALLMODULE_OBJS) $(EXTRA_OBJS) $(LINKER_SCRIPT) $(MONLIB)
	@echo linking $@
	$(NOECHO)$(SIZE) -t $(ALLMODULE_OBJS)
	$(NOECHO)$(LD) $(GLOBAL_LDFLAGS) -T $(LINKER_SCRIPT) $(ALLMODULE_OBJS) $(EXTRA_OBJS) $(LIBGCC) $(MONLIB) -o $@

$(OUTTASK): $(OUTSYS) $(TASK_OBJ) $(LINKER_SCRIPT)
	@echo linking $@
	$(LD) $(GLOBAL_LDFLAGS) --no-warn-mismatch -T $(LINKER_SCRIPT) $(OUTSYS) $(TASK_OBJ) $(LIBGCC) -o $@

$(OUTBIN): $(OUTTASK)
	@echo generating image: $@
	$(NOECHO)$(OBJCOPY) -O binary $< $@

$(IMGBIN): $(MONBIN) $(OUTBIN)
	@echo combining monitor / task binaries
	cat $^ > $@

$(TOSIMAGE): $(IMGBIN)
	@echo generating image: $@
	tools/gen_tos_part_img.py $< $@

$(OUTSYS): $(ALLMODULE_OBJS) $(EXTRA_OBJS) $(MONLIB)
	@echo partial linking $@
	$(NOECHO)$(LD) -r $(ALLMODULE_OBJS) $(EXTRA_OBJS) $(MONLIB) -o $@

$(OUTELF).sym: $(OUTELF)
	@echo generating symbols: $@
	$(NOECHO)$(OBJDUMP) -t $< | $(CPPFILT) > $@

$(OUTELF).lst: $(OUTELF)
	@echo generating listing: $@
	$(NOECHO)$(OBJDUMP) -Mreg-names-raw -d $< | $(CPPFILT) > $@

$(OUTELF).debug.lst: $(OUTELF)
	@echo generating listing: $@
	$(NOECHO)$(OBJDUMP) -Mreg-names-raw -S $< | $(CPPFILT) > $@

$(OUTELF).size: $(OUTELF)
	@echo generating size map: $@
	$(NOECHO)$(NM) -S --size-sort $< > $@

#include arch/$(ARCH)/compile.mk

