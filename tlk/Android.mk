#
# Copyright (c) 2013-2015, NVIDIA CORPORATION. All rights reserved
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

# Define module tlk.
# tlk: Compile the tlk kernel and generate tos.img.
ifeq (tlk,$(SECURE_OS_BUILD))

# tos.img is considered to be 32-bit
ifneq (,$(TARGET_2ND_ARCH))
LOCAL_2ND_ARCH_VAR_PREFIX := $(TARGET_2ND_ARCH_VAR_PREFIX)
endif

ifeq (t124,$(TARGET_TEGRA_VERSION))
# Don't pull in the module, but instead pull in the static library
MONBIN :=
MONLIB := $(call intermediates-dir-for,STATIC_LIBRARIES,libmonitor,,,$(LOCAL_2ND_ARCH_VAR_PREFIX))/libmonitor.a
else
MONBIN := $(call intermediates-dir-for,EXECUTABLES,monitor.bin,,,$(LOCAL_2ND_ARCH_VAR_PREFIX))/monitor.bin
MONLIB :=
endif # (t124,$(TARGET_TEGRA_VERSION))

LOCAL_PATH := $(call my-dir)


# Enable storage rollback protection for desired devices.
ifeq ($(BOARD_SUPPORT_ROLLBACK_PROTECTION),true)
	WITH_ROLLBACK_PROTECTION := 1
else
	WITH_ROLLBACK_PROTECTION := 0
endif

# Local module is tos.img, we also define the tlk target
include $(CLEAR_VARS)
LOCAL_MODULE := tos
LOCAL_MODULE_SUFFIX := .img
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_MODULE_PATH := $(PRODUCT_OUT)

TLK_INTERMEDIATES := $(call intermediates-dir-for,$(LOCAL_MODULE_CLASS),$(LOCAL_MODULE),,,$(LOCAL_2ND_ARCH_VAR_PREFIX))
TLK_PROJECT := tegra

LK_BIN := $(TLK_INTERMEDIATES)/build-$(TLK_PROJECT)/lk.bin
tlk: tos.img
TOSIMAGE := $(TLK_INTERMEDIATES)/tos.img

# List of task modules to link with tlk
TASK_MODULES := \
	trusted_app \
	trusted_app2 \
	secure_otf \
	oemcrypto_secure_service \
	hdcp_secure_service \
	crypto_service \
	storage_service \
	hwkeystore_task \
	tlkstoragedemo_task \
	secure_rtc

ifeq ($(filter t210,$(TARGET_TEGRA_VERSION)), $(TARGET_TEGRA_VERSION))
TASK_MODULES += \
	tsec_service
endif

# daemon to handle storage requests in Android user space
DAEMON := tlk_daemon

# keystorage client running in Android user space
KEYSTORE_CLIENT := keystore.tegra

# Get actual task executables from list of task modules
TASK_EXECUTABLES := \
	$(foreach task,$(TASK_MODULES), \
		$(call intermediates-dir-for,EXECUTABLES,$(task),,,$(LOCAL_2ND_ARCH_VAR_PREFIX))/$(task))

ifeq ($(TARGET_ARCH),arm64)
LK_TOOLCHAIN_PREFIX := prebuilts/gcc/$(HOST_PREBUILT_TAG)/arm/arm-eabi-4.8/bin/arm-eabi-
LK_TOOLCHAIN_PREFIX64 := $(TARGET_TOOLS_PREFIX)
else
LK_TOOLCHAIN_PREFIX := $(ARM_EABI_TOOLCHAIN)/arm-eabi-
LK_TOOLCHAIN_PREFIX64 := $(ARM_EABI_TOOLCHAIN)/../../../aarch64/aarch64-linux-android-4.8/bin/aarch64-linux-android-
endif

# Generate lk.bin with PRIVATE_CUSTOM_TOOL
# Call make in lk directory
$(LK_BIN): PRIVATE_CUSTOM_TOOL_ARGS := PROJECT=$(TLK_PROJECT) \
		TARGET=$(TARGET_TEGRA_VERSION) \
		TOOLCHAIN_PREFIX=$(abspath $(LK_TOOLCHAIN_PREFIX)) \
		TOOLCHAIN_PREFIX64=$(abspath $(LK_TOOLCHAIN_PREFIX64)) \
		PREFIX=$(abspath $(TLK_INTERMEDIATES)) \
		TASKS="$(foreach task,$(TASK_EXECUTABLES), $(abspath $(task)))" \
		MONBIN=$(abspath $(MONBIN)) \
		MONLIB=$(abspath $(MONLIB)) \
		TOSIMAGE=$(abspath $(TOSIMAGE)) \
		WITH_ROLLBACK_PROTECTION=$(WITH_ROLLBACK_PROTECTION) \
		-C $(LOCAL_PATH)
$(LK_BIN): PRIVATE_MODULE := $(LOCAL_MODULE)
# Depend on tasks when we are doing a full build.
# For one shot builds, (mm, mmm) do not.
ifeq (,$(ONE_SHOT_MAKEFILE))
$(LK_BIN): $(TASK_EXECUTABLES) $(DAEMON) $(KEYSTORE_CLIENT) $(MONLIB) $(MONBIN)
endif
$(LK_BIN):
	@echo "target Generated: $(PRIVATE_MODULE)"
	@mkdir -p $(dir $@)
	$(hide) $(MAKE) $(PRIVATE_CUSTOM_TOOL_ARGS)

$(TOSIMAGE): $(LK_BIN)

.PHONY: $(LK_BIN)

ifeq ($(LOCAL_2ND_ARCH_VAR_PREFIX),)
ALL_NVIDIA_MODULES += $(LOCAL_MODULE)
else
ALL_NVIDIA_MODULES += $(LOCAL_MODULE)_32
endif

include $(NVIDIA_BASE)
include $(BUILD_SYSTEM)/base_rules.mk
include $(NVIDIA_POST)

# Clean variables
TLK_INTERMEDIATES :=
TLK_PROJECT :=
LK_BIN :=
TOSIMAGE :=
TASK_MODULES :=
TASK_EXECUTABLES :=
DAEMON :=
KEYSTORE_CLIENT :=
MONBIN :=
MONLIB :=
LK_TOOLCHAIN_PREFIX :=
LK_TOOLCHAIN_PREFIX64 :=

endif # SECURE_OS_BUILD == tlk
