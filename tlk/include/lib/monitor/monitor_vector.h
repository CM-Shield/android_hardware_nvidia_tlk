/*
 * Copyright (c) 2014-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __MONITOR_VECTOR_H
#define __MONITOR_VECTOR_H

#ifndef ASSEMBLY
#define UL(x)	(x##UL)
#else
#define UL(x)	(x)
#endif

#if !defined(ASSEMBLY)

uint32_t mon_cpu_id(void);
void mon_atomic_or(volatile uint32_t *ptr, uint32_t bits);
void mon_atomic_and(volatile uint32_t *ptr, uint32_t bits);

/* holds arguments/return value during fastcalls */
struct fastcall_frame {
	uint64_t r[8];  /* r0-r7 */
};

#if defined(WITH_MONITOR_BIN)

/*
 * Exported monitor data structures and functions which can be
 * referenced by routines also linked into the monitor binary.
 */
extern uintptr_t __mon_cpu_return_addr;
extern uintptr_t __mon_cpu_reset_vector;
extern uintptr_t __mon_phys_base;
extern uintptr_t __mon_phys_size;

paddr_t mon_virt_to_phys(void *vaddr);
void *mon_phys_to_virt(uint64_t paddr);

int mon_mmu_map_mmio(uintptr_t vaddr, uint64_t paddr, uint32_t length);
void mon_mmu_map_uncached(uintptr_t vaddr, uint64_t paddr, uint32_t length);
void mon_mmu_unmap(uintptr_t vaddr, uint32_t length);

uint32_t mon_get_cpu_id(void);

/*
 * CPU power down sequence as per A57/A53 TRM
 *
 * l2_flush = indicates if L2 flush is required
 *
 */
void mon_cpu_power_down(int l2_flush);

/*
 * CPU debug register save before powering down
 *
 * this function clobbers registers x0, x1, and x4.
 */
void mon_cpu_dbg_save(void);

#endif	// WITH_MONITOR_BIN

#endif	// !ASSEMBLY


#define SMC_STDCALL			(UL(0) << 31)
#define SMC_FASTCALL			(UL(1) << 31)

#define SMC_CALLING_CONVENTION_32	(UL(0) << 30)
#define SMC_CALLING_CONVENTION_64	(UL(1) << 30)

#define SMC_OWNER_MASK			0x3F
#define SMC_OWNER_SHIFT			24

#define SMC_OWNER_ARM_ARCH		0x0
#define SMC_OWNER_CPU_SERVICE		0x1
#define SMC_OWNER_SIP_SERVICE		0x2
#define SMC_OWNER_OEM_SERVICE		0x3
#define SMC_OWNER_ARM_STD		0x4

#define SMC_OWNER_TRUSTED_BASE		0x30
#define SMC_OWNER_TRUSTED_SERVICE	0x5

#define SMC_MUST_BE_ZERO_MASK		0xFF
#define SMC_MUST_BE_ZERO_SHIFT		16

/* legacy when fastcall & MBZ field is all 1s */
#define SMC_IS_LEGACY	\
	(SMC_FASTCALL | (SMC_MUST_BE_ZERO_MASK << SMC_MUST_BE_ZERO_SHIFT))

/* Silicon Partner issued SMCs */
#define SMC_SIP_CALL			(SMC_OWNER_SIP_SERVICE << SMC_OWNER_SHIFT)
#define SMC_SIP_AARCH_SWITCH		(SMC_FASTCALL | SMC_SIP_CALL | 0x4)
#define SMC_SIP_GET_FIQ_REGS		(SMC_FASTCALL | SMC_SIP_CALL | 0x6)

/* Trusted OS issued SMC (i.e. generated from the TLK kernel) */
#define SMC_TOS_CALL			(0x32 << SMC_OWNER_SHIFT)

#if ARCH_ARM
/* TOS 32bit secure fastcalls */
#define SMC_TOS_SECURE	(SMC_FASTCALL | SMC_CALLING_CONVENTION_32 | \
			 SMC_TOS_CALL)
#endif
#if ARCH_ARM64
/* TOS 64bit secure fastcalls */
#define SMC_TOS_SECURE	(SMC_FASTCALL | SMC_CALLING_CONVENTION_64 \
			 SMC_TOS_CALL)
#endif

/* low byte used as jump table idx */
#define	SMC_TOS_FUNC_ID_MASK		0xFF

/* TOS issued SMCs (update MAX_FUNC_IDX when adding new calls) */
#define	SMC_TOS_COMPLETION		(SMC_TOS_SECURE | 0x1)
#define	SMC_TOS_PREEMPTED		(SMC_TOS_SECURE | 0x2)
#define	SMC_TOS_INITIAL_NS_RETURN	(SMC_TOS_SECURE | 0x3)
#define	SMC_TOS_ADDR_TRANSLATE		(SMC_TOS_SECURE | 0x4)
#define	SMC_TOS_INIT_SHARED_ADDR	(SMC_TOS_SECURE | 0x5)
#define	SMC_TOS_MAX_FUNC_IDX		0x5

/* restart pre-empted SMC handling */
#define SMC_TOS_RESTART			(60 << 24)

/* informs the NS world that we were pre-empted by an irq */
#define SMC_ERR_PREEMPT_BY_IRQ		0xFFFFFFFD
#define SMC_ERR_PREEMPT_BY_FS		0xFFFFFFFE

#endif
