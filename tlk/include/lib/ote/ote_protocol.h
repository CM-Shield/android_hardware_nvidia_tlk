/*
 * Copyright (c) 2012-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __OTE_PROTOCOL_H
#define __OTE_PROTOCOL_H

/*
 * Holds implementation structs/defines to be used only by the secure os
 * the trusted application interface library (and should not be included
 * by the trusted application directly).
 */
#include <stddef.h>
#include "ote_types.h"
#include <common/ote_common.h>
#include <common/ote_error.h>
#include <common/ote_nv_uuid.h>
#include <service/ote_service.h>
#include <service/ote_manifest.h>
#include <ext_nv/ote_ext_nv.h>
#include <service/ote_task_load.h>

/*
 * Get property info
 */
enum {
	OTE_PROP_DATA_TYPE_UUID		= 1,
	OTE_PROP_DATA_TYPE_IDENTITY	= 2,
};

#define DEVICE_UID_SIZE_WORDS	4

typedef struct {
	uint32_t id[DEVICE_UID_SIZE_WORDS];
} te_device_id_args_t;

#endif /* __OTE_PROTOCOL_H */
