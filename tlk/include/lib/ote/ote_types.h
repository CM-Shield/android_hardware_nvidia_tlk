/*
 * Copyright (c) 2012-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __OTE_TYPES_H__
#define __OTE_TYPES_H__

#include <sys/types.h>
#include <common/ote_common.h>

// Session types
struct __te_object_handle;
typedef uint32_t te_handle;
typedef te_handle te_ta_session_handle;
typedef struct __te_object_handle* te_object_handle;

#define TE_HANDLE_NULL		0
#define TE_ATTR_VAL		1 << 29
#define TE_ATTR_PUB		1 << 28

/*
 * Property set types
 */
typedef uint32_t te_prop_set_handle;

/*
 * Return origin types
 */
typedef uint32_t te_origin;

#endif //__OTE_TYPES_H__
