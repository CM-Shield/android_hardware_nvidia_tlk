/*
 * Copyright (c) 2012-2013, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __ARCH_OUTERCACHE_H
#define __ARCH_OUTERCACHE_H

#if ARM_WITH_OUTER_CACHE
struct outer_cache_fns {
	void (*init)(void);
	void (*flush_all)(void);
	void (*flush_range)(paddr_t paddr, size_t len);
	void (*clean_all)(void);
	void (*clean_range)(paddr_t paddr, size_t len);
	void (*inv_all)(void);
	void (*inv_range)(paddr_t paddr, size_t len);
	void (*disable)(void);
	void (*enable)(void);
	void (*sync)(void);
};

extern struct outer_cache_fns outer_cache;

static inline void outer_init(void)
{
	if (outer_cache.init)
		outer_cache.init();
}

static inline void outer_flush_all(void)
{
	if (outer_cache.flush_all)
		outer_cache.flush_all();
}

static inline void outer_flush_range(paddr_t paddr, size_t len)
{
	if (outer_cache.flush_range)
		outer_cache.flush_range(paddr, len);
}

static inline void outer_clean_all(void)
{
	if (outer_cache.clean_all)
		outer_cache.clean_all();
}

static inline void outer_clean_range(paddr_t paddr, size_t len)
{
	if (outer_cache.clean_range)
		outer_cache.clean_range(paddr, len);
}

static inline void outer_inv_all(void)
{
	if (outer_cache.inv_all)
		outer_cache.inv_all();
}

static inline void outer_inv_range(paddr_t paddr, size_t len)
{
	if (outer_cache.inv_range)
		outer_cache.inv_range(paddr, len);
}

static inline void outer_disable(void)
{
	if (outer_cache.disable)
		outer_cache.disable();
}

static inline void outer_enable(void)
{
	if (outer_cache.enable)
		outer_cache.enable();
}

static inline void outer_sync(void)
{
	if (outer_cache.sync)
		outer_cache.sync();
}
#endif

#endif
