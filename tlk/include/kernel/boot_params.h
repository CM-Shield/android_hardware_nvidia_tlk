/*
 * Copyright (c) 2012-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __BOOT_PARAMS_H
#define __BOOT_PARAMS_H

#include <sys/types.h>
#include <kernel/task.h>

#define BOOT_PARAM_HEADER	'TFBP'
#define CMDLINE_START		"[Global]"
#define CMDLINE_DRAM_RANGE	"mem"
#define CMDLINE_OS_BOOT_ADDR	"normalOS.ColdBoot.PA"
#define CMDLINE_DEBUG_UART_ID	"config.register.uart.id"
#define CMDLINE_TFKEY_ADDR	"config.s.mem.1.address"
#define CMDLINE_TFKEY_SIZE	"config.s.mem.1.size"
#define CMDLINE_DEVICE_UID0	"config.uid0"
#define CMDLINE_DEVICE_UID1	"config.uid1"
#define CMDLINE_DEVICE_UID2	"config.uid2"
#define CMDLINE_DEVICE_UID3	"config.uid3"
#define CMDLINE_TSEC_CARVEOUT	"tsec"
#define CMDLINE_DTB_ADDR	"dtbaddr"

#define DEVICE_UID_SIZE_WORDS	4
#define PARAM_NAME_LEN		64		// in bytes

#define TLK_BOOT_PARAMS_MAX_STRING	1024
#define EKS_MAXIMUM_CODE_SIZE		0x600

typedef struct {
	uint32_t	encrypted_key_sz;
	char		encrypted_keys[EKS_MAXIMUM_CODE_SIZE];		// encrypted keys
} key_params;

/* Structure for boot params */
typedef struct boot_params {
	uint32_t version;
	uint32_t uart_id;
	uint32_t chip_uid[4];
	uint64_t pmem;
	uint64_t pmem_size;
	uint64_t emem;
	uint64_t emem_size;
	uint64_t tsec_carveout;
	uint64_t tsec_size;
	uint64_t dtb_load_addr;
} boot_params_t;

void save_boot_params(unsigned int, uint32_t);
void get_boot_params(void);
int get_boot_args(task_t *task, uint32_t *args);

#endif
