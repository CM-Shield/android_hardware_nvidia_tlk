/*
 * Copyright (c) 2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __KERNEL_TASK_UNLOAD_H
#define __KERNEL_TASK_UNLOAD_H

#include <kernel/task.h>

/*!
 * Define this nonzero if unloading of tasks is allowed at all.
 * If defined zero: no tasks can be unloaded.
 */
#ifndef HAVE_UNLOAD_TASKS
#define HAVE_UNLOAD_TASKS	1
#endif

/*!
 * Define this nonzero if unloading of static tasks is allowed.
 * If defined zero: only post-loaded tasks can be unloaded.
 *
 * The static task image memory is not reclaimed/reused after unloading.
 * [ For static tasks the memory is not allocated from TLK heap but rather
 *   baked into the TLK image itself ]
 */
#ifndef HAVE_UNLOAD_STATIC_TASKS
#define HAVE_UNLOAD_STATIC_TASKS	1
#endif

void task_unload_init(void);

/*! Remove a dead thread from the thread list of a task.
 *
 * Called by thread cleanup (if the thread belongs to a task)
 * before the thread resources are deallocated.
 */
struct thread;

void task_thread_killed(struct thread *thread);

/*! Initiate unloading of a task.
 *
 * Called by task load syscall handler.
 */
status_t task_unload(task_t **taskp_p);

/*! Get number of tasks unloaded since boot.
 */
uint32_t task_get_unload_counter();

#endif /* __KERNEL_TASK_UNLOAD_H */
