/*
 * Copyright (c) 2012-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __KERNEL_TASK_H
#define __KERNEL_TASK_H

/* offsets within task_t */
#define TASK_CONTEXTID		0x24
#define TASK_PAGETABLE		0x28

#ifndef ASSEMBLY

#include <sys/types.h>
#include <arch/task.h>
#include <list.h>
#include <kernel/elf.h>

#include <lib/ote/ote_protocol.h>

#define TASK_START_ADDR		0x8000
#define TASK_STACK_ADDR		MAX_TASK_SIZE

typedef enum {
	TM_UX		= (1 << 0),
	TM_UW		= (1 << 1),
	TM_UR		= (1 << 2),
	TM_PHYS_CONTIG	= (1 << 20),
	TM_NS_MEM 	= (1 << 21),
	TM_NS_MEM_PRIV	= (1 << 22),
	TM_SEC_VA	= (1 << 23),
	TM_KERN_VA    	= (1 << 24),
	TM_PHYS_ALLOCATED = (1 << 28),
	TM_KERN_SEC_VA	= (1 << 29),
	TM_IO		= (1 << 30),
} tmflags_t;

/* first 3 flag bits equate to prog header PF_* flags */
#define PF_FLAG_MASK	(TM_UR | TM_UW | TM_UX)

typedef struct task_map
{
	/* either contiguous physaddr or pagelist */
	union {
		paddr_t contig;
		paddr_t *pagelist;
	} u_phys;
	addr_t vaddr;
	u_int  offset;
	u_int  size;
	u_int  id;

	tmflags_t flags;
	void *map_attrs;

	struct list_node node;
} task_map_t;

typedef struct
{
	te_service_id_t uuid;
	u_int    multi_instance;
	u_int    min_stack_size;
	u_int    min_heap_size;
	u_int    map_io_mem_cnt;
	u_int    restrict_access;
	u_int    manifest_exists;
	u_int    authorizations;
	u_int    initial_state;
	u_int    config_entry_cnt;
	u_int    *config_blob;
} task_props_t;

/* task lifecycle states
 */
typedef enum {
	TASK_STATE_UNKNOWN = 0,
	TASK_STATE_INIT,
	TASK_STATE_STARTING,
	TASK_STATE_ACTIVE,
	TASK_STATE_BLOCKED,
	TASK_STATE_TERMINATED
} task_state_t;

typedef enum {
	TASK_TYPE_UNKNOWN = 0,
	TASK_TYPE_STATIC,
	TASK_TYPE_LOADED
} task_type_t;

typedef struct task
{
	addr_t entry;

	addr_t start_code;
	addr_t end_code;
	addr_t start_data;
	addr_t end_data;
	addr_t end_bss;
	addr_t start_brk;
	addr_t curr_brk;
	addr_t end_brk;

	/* field offset hard coded */
	u_int context_id;

	/* field offset hard coded */
	paddr_t page_table;

	struct list_node map_list;

	task_map_t *stack_map;
	vaddr_t	sp;

	struct list_node thread_node;
	task_props_t props;
	u_int te_instances;

	struct list_node *valloc_list;
	u_int valloc_start;
	u_int valloc_end;

	Elf32_Ehdr *elf_hdr;

	task_state_t task_state;
	task_type_t task_type;
	u_int task_index;
	char task_name[OTE_TASK_NAME_MAX_LENGTH];
	unsigned char task_private_data[OTE_TASK_PRIVATE_DATA_LENGTH];
	uint32_t task_size;
	struct list_node node;

	/* bitfield of events task has registered for.  */
	uint32_t ta_events_mask;
} task_t;

void task_init();
void task_early_init();

addr_t task_find_address_space(task_t *taskp, u_int size, u_int align);
void task_add_mapping(task_t *taskp, task_map_t *mptr);
void task_delete_mapping(task_t *taskp, task_map_t *mptr);
task_map_t *task_find_mapping(task_t *taskp, addr_t vaddr, u_int size);
task_map_t *task_find_mapping_by_id(task_t *taskp, u_int id);
status_t task_get_physaddr(task_t *taskp, addr_t vaddr, paddr_t *paddr);
bool task_valid_address(vaddr_t addr, u_int size);
task_t *task_find_task_by_uuid(te_service_id_t *uuid);

/*! Lookup task by its task slot index in task_list.
 *
 * \param index[in] task index (0..(task_count-1)).
 *
 * \return Return pointer to task or NULL if index is >= task_count.
 *
 * This may return a pointer to inactive task slot.
 */
task_t *task_find_task_by_index(uint32_t index);

/*! Return a pointer to const char C-string PREFIXtasknameSUFFIX if task has a name,
 * otherwise return pointer to "".
 *
 * On entry buf is a pointer to memory area of length buflen characters.
 *
 * \param task[in]	get taskname from here
 * \param prefix[in]	added before task name, if not null
 * \param suffix[in]	added after task name, if not null
 * \param buf[in/out]	if task has a name, will contain "prefixTASKNAMEsuffix" C-string
 * \param buflen[in]	buf length (must be long enough to contain prefix, task name, suffix and nul)
 */
const char *task_get_name_str(const task_t *task, const char *prefix, const char *suffix,
			      char *buf, uint32_t buflen);

/*! Log the uuid && name of taskp at dprintf() level with optional prefix string.
 *
 * \param level  defines dprintf() level to conditionally print at.
 * \param prefix is an optional string prefix (or NULL).
 * \param taskp  is the task which uuid is to be printed in "hex-dash" format
 */
void task_print_id(uint32_t level, const char *prefix, const task_t *taskp);

/*! Log the uuid at dprintf() level.
 *
 * \param level	defines dprintf() level to conditionally print at.
 * \param uuid	is an UUID to log.
 */
void task_print_uuid(uint32_t level, const te_service_id_t *uuid);

status_t task_prepare(char *task_addr, u_int task_size, task_t *taskp,
		      Elf32_Shdr **bss_pad_shdr_p, task_type_t task_type);

status_t task_init_one_task(task_t *task);

/*! Atomically commit task (and set it's index) to the known task list
 * unless there is an error.
 *
 * UUID duplicates are atomically checked here.
 *
 * \param task_p[in] task object pointer.
 *
 * \return NO_ERROR on success and on failure:
 *	   ERR_TOO_MANY_TASKS if not more tasks can be installed (out of memory)
 *	   ERR_ALREADY_EXISTS if a task with matching uuid already exists
 *	   ERR_INVALID_ARGS if parameters are invalid.
 */
status_t task_register(task_t **task_p);

/*! return a const pointer to task_list header node */
const struct list_node *task_get_task_list(void);

/*! return the number of currently existing active tasks */
u_int task_get_active_count(void);

/*! return the number of registered tasks since boot */
u_int task_get_count(void);

/*! set the requested callbacks for ta events */
status_t task_register_ta_events(task_t *task, uint32_t events_mask);

/*! schedule tasks that have requested callbacks for event.  */
status_t task_signal_ta_event(enum ta_event_id event, void *arg);

#endif /* ASSEMBLY */
#endif /* __KERNEL_TASK_H */
