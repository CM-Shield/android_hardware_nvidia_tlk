/*
 * Copyright (c) 2013-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __KERNEL_TASK_LOAD_H
#define __KERNEL_TASK_LOAD_H

#include <kernel/task.h>
#include <kernel/task_unload.h>

/*!
 * Privileged "installer TA" starting the task may request
 * some overrides for the task being started.
 *
 * Currently only supports overriding the max size
 * of the stack or heap configured in the manifest and the
 * task name.
 *
 * Other types of overrides / restrictions may be supported later.
 *
 * te_task_restrictions_t is compatible with this.
 */
typedef struct {
	/* currently supported overrides; all-zero values ignored */
	uint32_t min_stack_size;
	uint32_t min_heap_size;
	char	 task_name[OTE_TASK_NAME_MAX_LENGTH];
} task_restrictions_t;

/*!
 * Contains copy of information in the task manifest fixed fields.
 * te_task_info_t is compatible with this
 *
 * Most manifest fields and some task_t fields are copied here.
 */
typedef struct {
	te_service_id_t uuid;
	u_int    manifest_exists;
	u_int    multi_instance;
	u_int    min_stack_size;
	u_int    min_heap_size;
	u_int    map_io_mem_cnt;
	u_int    restrict_access;
	u_int    authorizations;
	u_int    initial_state;
	char	 task_name[OTE_TASK_NAME_MAX_LENGTH];
	unsigned char task_private_data[OTE_TASK_PRIVATE_DATA_LENGTH];
} task_info_t;

void task_load_config(vaddr_t vaddr_begin, vaddr_t *vaddr_end_p);

void task_load_init(void);

void task_set_name(task_t *task, const char *name);

u_int task_allowed_to_load_tasks(task_t *taskp);

/*!
 * Called from syscall ioctl handler to perform task loading and
 * related operations.
 */
struct te_task_request_args_s;

int task_request_handler(struct te_task_request_args_s *args);

/*! Free the memory allocated from TLK heap for task image when it was
 * loaded to the system.
 *
 * VADDR is the TLK virtual address of the allocated TASK_BYTE_SIZE size object.
 */
void task_dealloc_memory(vaddr_t vaddr, u_int task_byte_size);

#endif /* __KERNEL_TASK_LOAD_H */
