/*
 * Copyright (c) 2012-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __OTE_INTF_H__
#define __OTE_INTF_H__

#include <list.h>
#include <platform/platform_p.h>

void te_intf_init(void);

te_error_t te_handle_open_session(te_request_t *req, bool task_issued);
te_error_t te_handle_close_session(te_request_t *req, bool task_issued);
te_error_t te_handle_launch_op(te_request_t *req, bool task_issued);
void te_get_completed_cmd(te_request_t *req, bool task_issuea);

te_error_t te_handle_ta_event(enum ta_event_id event);

int te_handle_ta_message(uint32_t fd, void *msg, uint32_t msgsize, bool read);
int te_get_current_ta_property(te_get_property_args_t *args);
int te_get_current_client_property(te_get_property_args_t *args);
int te_get_implementation_property(te_get_property_args_t *args);

struct thread;
void te_session_cancel_thread(struct thread *t);

#endif
