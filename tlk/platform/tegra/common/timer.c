/*
 * Copyright (c) 2008 Travis Geiselbrecht
 * Copyright (c) 2012-2013, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <sys/types.h>
#include <err.h>
#include <reg.h>
#include <debug.h>
#include <kernel/thread.h>
#include <platform.h>
#include <platform/interrupts.h>
#include <platform/timer.h>
#include <platform/memmap.h>
#include <platform/platform_p.h>

static lk_time_t tick_interval_ms;
static platform_timer_callback t_callback;
static void *callback_arg;

/* timer 10 */
static const unsigned int system_timer_irq = 157;

#define MSEC_PER_SEC		1000000

#define TIMER_PTV		0x0
#define TIMER_PCR		0x4

#define RTC_SECONDS		0x08
#define RTC_MILLISECONDS	0x10
#define RTC_SHADOW_SECONDS	0x0C

#define TIMERUS_CNTR_1US	0x10

static inline void write_timer_reg(uint32_t reg, unsigned int data)
{
	*(volatile uint32_t *)(TEGRA_TMR0_BASE + (reg)) = data;
}

static inline uint32_t read_timer_reg(uint32_t reg)
{
	return *(volatile uint32_t *)(TEGRA_TMR0_BASE + (reg));
}

status_t platform_set_periodic_timer(platform_timer_callback callback, void *arg, lk_time_t interval_ms)
{
	enter_critical_section();

	t_callback = callback;
	callback_arg = arg;
	tick_interval_ms = interval_ms;

	write_timer_reg(TIMER_PTV, 0);
	write_timer_reg(TIMER_PTV, 0xC0000000 | (interval_ms*1000));

	unmask_interrupt(system_timer_irq);

	exit_critical_section();

	return NO_ERROR;
}

lk_time_t current_time(void)
{
	uint32_t ms = *(volatile uint32_t *)(TEGRA_RTC_BASE + RTC_MILLISECONDS);
	uint32_t s = *(volatile uint32_t *)(TEGRA_RTC_BASE + RTC_SHADOW_SECONDS);

	return (lk_time_t)((uint64_t)s * MSEC_PER_SEC + ms);
}

lk_bigtime_t current_time_hires(void)
{
	static uint32_t upper;
	static uint32_t prev, now;
	uint64_t retval;

	now = *(volatile uint32_t *)(TEGRA_TMR1_BASE + TIMERUS_CNTR_1US);
	if (now < prev) {
		upper++;
	}
	prev = now;
	retval = ((uint64_t)upper << 32) | now;

	return (lk_bigtime_t)retval;
}

static enum handler_return os_timer_tick(void *arg)
{
	write_timer_reg(TIMER_PCR, 1 << 30);
	return t_callback(callback_arg, current_time());
}

void platform_init_timer(void)
{
	write_timer_reg(TIMER_PTV, 0);
	register_int_handler(system_timer_irq, os_timer_tick, NULL);
}

void platform_halt_timers(void)
{
	uint32_t timer_val = read_timer_reg(TIMER_PTV);
	write_timer_reg(TIMER_PTV, timer_val & ~0x80000000);
}
