/*
 * Copyright (c) 2012-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <err.h>
#include <debug.h>
#include <platform.h>
#include <platform/memmap.h>
#include <reg.h>
#include <string.h>

#include <platform/platform_p.h>
#include <kernel/boot_params.h>

/* clock enable for TZRAM */
#define CLK_OUT_ENB_V			0x360
#define CLK_OUT_ENB_TZRAM_SHIFT		30
#define CLK_OUT_ENB_TZRAM_ENABLE	1

__WEAK void platform_setup_keys(void)
{
	boot_params_t *boot_params_ptr_args;
	key_params *keys_ptr;
	uint32_t reg, offset;
	extern uint32_t __bootarg_addr;

	/* keys immediately follow the boot params in DRAM */
	boot_params_ptr_args = (boot_params_t *) __bootarg_addr;
	offset = sizeof(boot_params_t);
	keys_ptr = (key_params *)((uintptr_t)boot_params_ptr_args + offset);

	/* ensure TZRAM clock is enabled */
	reg  = readl(TEGRA_CLK_RESET_BASE + CLK_OUT_ENB_V);
	reg |= (CLK_OUT_ENB_TZRAM_ENABLE << CLK_OUT_ENB_TZRAM_SHIFT);
	writel(reg, TEGRA_CLK_RESET_BASE + CLK_OUT_ENB_V);

	/* copy keys to TZRAM */
	memcpy((void *)TEGRA_TZRAM_BASE,
		keys_ptr->encrypted_keys, keys_ptr->encrypted_key_sz);
}
