/*
 * Copyright (c) 2012-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <err.h>
#include <errno.h>
#include <debug.h>
#include <assert.h>
#include <malloc.h>
#include <string.h>
#include <arch.h>
#include <stdlib.h>
#include <arch/arm.h>
#include <arch/arm/mmu.h>
#include <platform/platform_p.h>
#include <platform/platform_ta.h>
#include <platform/platform_tos.h>
#include <ote_intf.h>

#define TZ_UNSUPPORTED_PARAM	0xDEADBEEF

extern unsigned long _jump_to_ns_irq_addr;

extern vaddr_t platform_vaspace_ptr;
extern vaddr_t platform_vaspace_end;
extern unsigned int ote_logger_enabled;

#if !defined(WITH_MONITOR_BIN)
extern struct tz_monitor_frame *go_nonsecure(uint32_t smc_type, struct tz_monitor_frame *);
#endif

/* location in NS of req/param structs */
static vaddr_t tz_shared_req_param_buf;
static uint32_t tz_shared_req_param_size;

/* valid DRAM ranges as specified by the bootloader */
#define MAX_NUM_DRAM_RANGES	8
struct dram_range {
	paddr_t dram_base;
	paddr_t dram_size;
};
static struct dram_range tz_dram_range[MAX_NUM_DRAM_RANGES];
static uint32_t tz_dram_range_count;

void tz_add_dram_range(paddr_t base, paddr_t size)
{
	ASSERT(tz_dram_range_count < MAX_NUM_DRAM_RANGES);

	tz_dram_range[tz_dram_range_count].dram_base = base;
	tz_dram_range[tz_dram_range_count].dram_size = size;
	tz_dram_range_count++;
}

static bool valid_dram_address(paddr_t addr, paddr_t size)
{
	uint32_t i;

	for (i = 0; i < tz_dram_range_count; i++) {
		if ((tz_dram_range[i].dram_base <= addr) &&
			((tz_dram_range[i].dram_base + tz_dram_range[i].dram_size) >= (addr + size)))
			return true;
	}

	dprintf(CRITICAL, "%s: illegal addr 0x%08llx, size 0x%08llx reference\n",
		__func__, (uint64_t)addr, (uint64_t)size);
	return false;
}

vaddr_t tz_map_shared_mem(nsaddr_t ns_addr, uint32_t size)
{
	vaddr_t vaddr, vsize;
	uint32_t offset;
	paddr_t *pagelist;
	uint32_t pg, npages;
	task_map_t mptr;

	offset = ns_addr & PAGE_MASK;
	size = ROUNDUP(offset + size, PAGE_SIZE);
	npages = size / PAGE_SIZE;

	/* referencing NS memory, so setup proper vaddr alignment */
	vaddr = ROUNDUP(platform_vaspace_ptr, NS_VIRT_ADDR_ALIGN);
	vsize = ROUNDUP(size, NS_VIRT_ADDR_ALIGN);
	if ((vaddr + vsize) > platform_vaspace_end)
		return NULL;

	pagelist = malloc(npages * sizeof(paddr_t));
	if (pagelist == NULL)
		return NULL;

	mptr.flags = TM_NS_MEM_PRIV;
	mptr.size = size;

	arm_mmu_translate_range(ns_addr, pagelist, &mptr);

	/* verify all pages reside in DRAM */
	for (pg = 0; pg < npages; pg++) {
                ASSERT(!(pagelist[pg] & PAGE_MASK));

		if (valid_dram_address(pagelist[pg], PAGE_SIZE) == false) {
			free(pagelist);
			return NULL;
		}
	}

	/* map pages and TLB invalidate by addr */
	for (pg = 0; pg < npages; pg++) {
		vaddr_t map_addr;

		map_addr = vaddr + (pg * PAGE_SIZE);
		arm_mmu_map_kpage(map_addr, pagelist[pg], &mptr);
		arm_invalidate_tlb_byaddr(map_addr);
	}

	free(pagelist);

	platform_vaspace_ptr = (vaddr + vsize);
	return vaddr + offset;
}

struct tz_monitor_frame *tz_switch_to_ns(uint32_t smc_type, struct tz_monitor_frame *frame)
{
	struct tz_monitor_frame *incoming_smc;

	enter_critical_section();

	if (s_vfp_hw_context && s_vfp_hw_context->valid) {
		/*
		 * Save the secure world vfp state on the way out and mark
		 * the buffer as invalid as on entry we'll disable fpexc,
		 * so we can save out the NS copy and fault in the new one.
		 */
		arch_vfp_save(s_vfp_hw_context);
		s_vfp_hw_context->fpexc = 0x0;
		s_vfp_hw_context->valid = false;
	}

	if (ns_vfp_hw_context && ns_vfp_hw_context->valid) {
		/*
		 * Restore the non-secure world vfp state if during our
		 * running we context switched away from what was loaded in
		 * the HW (NS version of fpexc is always saved/restored).
		 */
		arch_vfp_restore(ns_vfp_hw_context);
		ns_vfp_hw_context->valid = false;
		arm_set_vfp_fpexc(ns_vfp_hw_context->fpexc);
	}

#if defined(WITH_MONITOR_BIN)
	/* go to monitor, for return to NS */
	incoming_smc = monitor_send_receive(smc_type, frame);
#else
	/* go to NS, on return there'll be a new SMC */
	incoming_smc = go_nonsecure(smc_type, frame);
#endif

	exit_critical_section();

	/* on entry, save NS fpexc and disable to detect vfp usage */
	if (ns_vfp_hw_context && ns_vfp_hw_context->valid)
		ns_vfp_hw_context->fpexc = arm_get_vfp_fpexc();

	arm_set_vfp_fpexc(0x0);


	return incoming_smc;
}

status_t tz_register_req_param_buf(struct tz_monitor_frame *frame)
{
	uint32_t size;
	nsaddr_t ns_addr;

	ns_addr = frame->r[1];
	size = frame->r[2];

	if (!size) {
		/* probing only for supported SMC */
		return NO_ERROR;
	}

	tz_shared_req_param_buf = tz_map_shared_mem(ns_addr, size);
	if (tz_shared_req_param_buf == NULL)
		return ERR_GENERIC;

	tz_shared_req_param_size = size;
	return NO_ERROR;
}

/*
 * System related SMCs handled on the current idle stack.
 * These should be simple operations that can't block.
 */
void tz_handle_system_smc(struct tz_monitor_frame *frame)
{
	int error = 0;
	nsaddr_t _ns_cb_struct_addr = NULL;

	switch (frame->r[0]) {

		case SMC_TOS_SS_REGISTER_HANDLER:
		case SMC_TOS_SS_REGISTER_HANDLER_LEGACY:
			error = platform_ss_register_handler(frame);
			break;

		case SMC_TOS_NS_REG_REQPARAM_BUF:
		case SMC_TOS_NS_REG_REQPARAM_BUF_LEGACY:
			error = tz_register_req_param_buf(frame);
			break;

		case SMC_TOS_INIT_LOGGER:
		case SMC_TOS_INIT_LOGGER_LEGACY:
			_ns_cb_struct_addr = frame->r[1];

			if (!_ns_cb_struct_addr) {
				ote_logger_enabled = 0;
				dputs(CRITICAL, early_logbuf);
				error = ERR_NOT_SUPPORTED;
				break;
			}

			/* physical address of the circular buffer */
			if (set_log_phy_addr(_ns_cb_struct_addr)) {
				ote_logger_enabled = 0;
				dputs(CRITICAL, early_logbuf);
				error = ERR_NOT_SUPPORTED;
				break;
			}

			/* copy early prints into the shared buffer */
			dprintf(CRITICAL, "%s", early_logbuf);

			break;
	}

	frame->r[0] = error;
}

static uint32_t tz_copyin_params(te_request_t *req, te_oper_param_t *in_params)
{
	uint32_t i, len;
	uint32_t extent;
	te_oper_param_t *req_params;

	if (req->params_size == 0)
		return OTE_SUCCESS;

	if (in_params == NULL)
		return OTE_ERROR_BAD_PARAMETERS;

	len = req->params_size * sizeof(te_oper_param_t);
	extent = len + (vaddr_t)in_params;

	if ((len >= tz_shared_req_param_size) ||
	    (extent >= (tz_shared_req_param_buf + tz_shared_req_param_size))) {
		dprintf(CRITICAL,
			"%s: nparams (0x%08x) exceeds map size (0x%08x)\n",
			__func__, req->params_size, tz_shared_req_param_size);
		return OTE_ERROR_BAD_PARAMETERS;
	}

	req_params = calloc(1, len);
	if (req_params == NULL)
		return OTE_ERROR_OUT_OF_MEMORY;

	memcpy(req_params, in_params, len);

	dprintf(SPEW, "%s: params %p len 0x%x\n", __func__, in_params, len);

	for (i = 0; i < req->params_size; i++) {
		switch (req_params[i].type) {
		case TE_PARAM_TYPE_INT_RO:
			dprintf(SPEW, "%s: %d: INT_RO: val 0x%x\n",
				__func__, i, req_params[i].u.Int.val);
			break;
		case TE_PARAM_TYPE_INT_RW:
			dprintf(SPEW, "%s: %d: INT_RW: val 0x%x\n",
				__func__, i, req_params[i].u.Int.val);
			break;
		case TE_PARAM_TYPE_MEM_RO:
			dprintf(SPEW, "%s: %d: MEM_RO: len 0x%x\n",
				__func__, i, req_params[i].u.Mem.len);
			break;
		case TE_PARAM_TYPE_MEM_RW:
			dprintf(SPEW, "%s: %d: MEM_RW: len 0x%x\n",
				__func__, i, req_params[i].u.Mem.len);
			break;
		case TE_PARAM_TYPE_PERSIST_MEM_RO:
			dprintf(SPEW, "%s: %d: PERSIST_MEM_RO: len 0x%x\n",
				__func__, i, req_params[i].u.Mem.len);
			break;
		case TE_PARAM_TYPE_PERSIST_MEM_RW:
			dprintf(SPEW, "%s: %d: PERSIST_MEM_RW: len 0x%x\n",
				__func__, i, req_params[i].u.Mem.len);
			break;
		default:
			dprintf(INFO, "%s: unhandled param type 0x%x\n",
				__func__, req_params[i].type);
			break;
		}
	}
	req->params = (uintptr_t)req_params;
	return OTE_SUCCESS;
}

static void tz_copyout_params(te_oper_param_t *out_params, te_request_t *req)
{
	uint32_t i;
	te_oper_param_t *req_params;

	if (out_params == NULL || req->params_size == 0)
		return;

	req_params = (te_oper_param_t *)(uintptr_t)req->params;
	for (i = 0; i < req->params_size; i++) {
		switch (out_params[i].type) {
		case TE_PARAM_TYPE_INT_RO:
			dprintf(SPEW, "%s: %d: INT_RO: val 0x%x\n",
				__func__, i, req_params[i].u.Int.val);
			break;
		case TE_PARAM_TYPE_INT_RW:
			dprintf(SPEW, "%s: %d: INT_RW: val 0x%x\n",
				__func__, i, req_params[i].u.Int.val);
			out_params[i].u.Int.val = req_params[i].u.Int.val;
			break;
		case TE_PARAM_TYPE_MEM_RO:
			dprintf(SPEW, "%s: %d: MEM_RO: len 0x%x\n",
				__func__, i, req_params[i].u.Mem.len);
			break;
		case TE_PARAM_TYPE_MEM_RW:
			dprintf(SPEW, "%s: %d: MEM_RW: len 0x%x\n",
				__func__, i, req_params[i].u.Mem.len);
			out_params[i].u.Mem.len = req_params[i].u.Mem.len;
			break;
		case TE_PARAM_TYPE_PERSIST_MEM_RO:
			dprintf(SPEW, "%s: %d: PERSIST_MEM_RO: len 0x%x\n",
				__func__, i, req_params[i].u.Mem.len);
			break;
		case TE_PARAM_TYPE_PERSIST_MEM_RW:
			dprintf(SPEW, "%s: %d: PERSIST_MEM_RW: len 0x%x\n",
				__func__, i, req_params[i].u.Mem.len);
			out_params[i].u.Mem.len = req_params[i].u.Mem.len;
			break;
		default:
			dprintf(INFO, "%s: unhandled param type 0x%x\n",
				__func__, out_params[i].type);
			break;
		}
	}
	free(req_params);
}

/*
 * TE related SMCs that require some setup and will
 * trigger a TA thread to run before the SMC completes.
 */
static void tz_handle_trusted_app_smc(struct tz_monitor_frame *frame)
{
	te_request_t *caller_req;
	te_oper_param_t *caller_params;
	te_request_t *req;
	uint32_t req_off, param_off;
	te_error_t result = OTE_SUCCESS;

	if (!tz_shared_req_param_buf || !tz_shared_req_param_size) {
		frame->r[0] = OTE_ERROR_GENERIC;
		return;
	}

	req_off = (uint32_t)frame->r[1];
	param_off = (uint32_t)frame->r[2];

	/* check req/param offsets are within the mapped buffer */
	if ((req_off + sizeof(te_request_t)) >= tz_shared_req_param_size) {
		dprintf(CRITICAL,
			"%s: req offset (0x%08x) beyond map size (0x%08x)\n",
			__func__, req_off, tz_shared_req_param_size);
		frame->r[0] = OTE_ERROR_BAD_PARAMETERS;
		return;
	}
	if ((param_off + sizeof(te_oper_param_t)) >= tz_shared_req_param_size) {
		dprintf(CRITICAL,
			"%s: param offset (0x%08x) beyond map size (0x%08x)\n",
			__func__, param_off, tz_shared_req_param_size);
		frame->r[0] = OTE_ERROR_BAD_PARAMETERS;
		return;
	}

	/* save caller request and param block addresses */
	caller_req = (te_request_t *)(tz_shared_req_param_buf + req_off);
	caller_params = (te_oper_param_t *)(tz_shared_req_param_buf + param_off);

	/* copy caller request info to new buffer */
	req = calloc(1, sizeof(te_request_t));
	if (req == NULL) {
		frame->r[0] = OTE_ERROR_OUT_OF_MEMORY;
		return;
	}
	memcpy(req, caller_req, sizeof(te_request_t));

	/* move optional parameters into request struct */
	result = tz_copyin_params(req, caller_params);
	if (result != OTE_SUCCESS) {
		free(req);
		frame->r[0] = result;
		return;
	}

	switch (frame->r[0]) {
		case SMC_TA_OPEN_SESSION:
		case SMC_TA_OPEN_SESSION_LEGACY:
			result = te_handle_open_session(req, false);
			break;
		case SMC_TA_CLOSE_SESSION:
		case SMC_TA_CLOSE_SESSION_LEGACY:
			result = te_handle_close_session(req, false);
			break;
		case SMC_TA_LAUNCH_OPERATION:
		case SMC_TA_LAUNCH_OPERATION_LEGACY:
			result = te_handle_launch_op(req, false);
			break;
	}
	req->result = result;

	/* consider any failure here to have occured in common TE code */
	if (req->result != OTE_SUCCESS) {
		req->result_origin = OTE_RESULT_ORIGIN_KERNEL;
	}

	te_get_completed_cmd(req, false);

	/* move request results back to caller struct */
	caller_req->result = req->result;
	caller_req->result_origin = req->result_origin;
	if ((frame->r[0] == SMC_TA_OPEN_SESSION) ||
	    (frame->r[0] == SMC_TA_OPEN_SESSION_LEGACY)) {
		caller_req->session_id = req->session_id;
	}

	/* move optional param results back to caller */
	tz_copyout_params(caller_params, req);

	free(req);
	frame->r[0] = result;
}

static void tz_handle_ta_event(struct tz_monitor_frame *frame)
{
	frame->r[0] = te_handle_ta_event(frame->r[1]);
}

void tz_stdcall_handler(struct tz_monitor_frame *frame)
{
	dprintf(SPEW, "%s: 0x%llx, 0x%llx, 0x%llx, 0x%llx\n", __func__,
		frame->r[0], frame->r[1], frame->r[2], frame->r[3]);

	switch (frame->r[0]) {
	case SMC_TOS_SS_REGISTER_HANDLER:
	case SMC_TOS_NS_REG_REQPARAM_BUF:
	case SMC_TOS_INIT_LOGGER:
	case SMC_TOS_SS_REGISTER_HANDLER_LEGACY:
	case SMC_TOS_NS_REG_REQPARAM_BUF_LEGACY:
	case SMC_TOS_INIT_LOGGER_LEGACY:
		tz_handle_system_smc(frame);
		break;

	case SMC_TA_OPEN_SESSION:
	case SMC_TA_CLOSE_SESSION:
	case SMC_TA_LAUNCH_OPERATION:
	case SMC_TA_OPEN_SESSION_LEGACY:
	case SMC_TA_CLOSE_SESSION_LEGACY:
	case SMC_TA_LAUNCH_OPERATION_LEGACY:
		tz_handle_trusted_app_smc(frame);
		break;
	case SMC_TA_EVENT:
		tz_handle_ta_event(frame);
		break;

	default:
		dprintf(CRITICAL, "%s: unhandled function 0x%x\n",
			__func__, (uint32_t)frame->r[0]);
		frame->r[0] = TZ_UNSUPPORTED_PARAM;
		break;
	}
}
