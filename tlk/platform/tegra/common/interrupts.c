/*
 * Copyright (c) 2008 Travis Geiselbrecht
 * Copyright (c) 2012-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <sys/types.h>
#include <assert.h>
#include <debug.h>
#include <err.h>
#include <reg.h>
#include <string.h>
#include <kernel/thread.h>
#include <platform/interrupts.h>
#include <arch/ops.h>
#include <arch/arm.h>
#include <platform/memmap.h>
#include <platform/irqs.h>
#include <platform/platform_p.h>
#include <lib/monitor/monitor_vector.h>

#define GIC_DIST_CTR	0x004
#define GIC_CPU_ICCIAR	0x00c

#define ICTLR_CPU_IEP_VFIQ	0x08
#define ICTLR_CPU_IEP_FIR	0x14
#define ICTLR_CPU_IEP_FIR_SET	0x18
#define ICTLR_CPU_IEP_FIR_CLR	0x1c

#define ICTLR_CPU_IER		0x20
#define ICTLR_CPU_IER_SET	0x24
#define ICTLR_CPU_IER_CLR	0x28
#define ICTLR_CPU_IEP_CLASS	0x2C

#define ICTLR_COP_IER		0x30
#define ICTLR_COP_IER_SET	0x34
#define ICTLR_COP_IER_CLR	0x38
#define ICTLR_COP_IEP_CLASS	0x3c

struct int_handler_struct {
	int_handler handler;
	void *arg;
};

#if DEBUG
/* storage containing state at time of last interrupt */
static struct arm_iframe arm_iframe_last;
#endif

static struct int_handler_struct int_handler_table[NR_IRQS];
static uint32_t tegra_gic_cpu_base = TEGRA_ARM_INT_CPU_BASE;

static uint32_t ictlr_reg_base[] = {
	TEGRA_PRIMARY_ICTLR_BASE,
	TEGRA_SECONDARY_ICTLR_BASE,
	TEGRA_TERTIARY_ICTLR_BASE,
	TEGRA_QUATERNARY_ICTLR_BASE,
	TEGRA_QUINARY_ICTLR_BASE,
};

#define vectorToController(vector) (((vector) - 32) / 32)

status_t mask_interrupt(unsigned int vector)
{
	uint32_t base = ictlr_reg_base[vectorToController(vector)];
	*REG32(base + ICTLR_CPU_IER_CLR) = 1 << (vector & 31);
	return NO_ERROR;
}

void platform_mask_irqs(void)
{
	int i;

	for (i = 0; i < NR_IRQS; i++)
		mask_interrupt(i);
}

status_t unmask_interrupt(unsigned int vector)
{
	uint32_t base = ictlr_reg_base[vectorToController(vector)];
	if (vector >= NR_IRQS)
		return ERR_INVALID_ARGS;
	*REG32(base + ICTLR_CPU_IER_SET) = 1 << (vector & 31);
	return NO_ERROR;
}

enum handler_return platform_irq(struct arm_iframe *iframe)
{
	uint32_t vector;
	struct tz_monitor_frame frame;

#if DEBUG
	/*
	 * debug aid to know where within the secure world the last interrupt
	 * occurred
	 */
	memcpy((void *)&arm_iframe_last, iframe, sizeof(arm_iframe_last));
#endif

	/* read GIC interrupt ack register */
	vector = *REG32(tegra_gic_cpu_base + GIC_CPU_ICCIAR) & 0x3FF;

	if (vector == 1023) {
		/* normal spurious intr, just return */
		return INT_NO_RESCHEDULE;
	} else {
		/* at the moment, NS owns all interrupts */
		ASSERT(vector == 1022);
	}

	memset(&frame, 0, sizeof(struct tz_monitor_frame));
	frame.r[0] = SMC_ERR_PREEMPT_BY_IRQ;
	(void)tz_switch_to_ns(SMC_TOS_PREEMPTED, &frame);

	return INT_NO_RESCHEDULE;
}

void platform_fiq(struct arm_iframe *frame)
{
	PANIC_UNIMPLEMENTED;
}

void register_int_handler(unsigned int vector, int_handler handler, void *arg)
{
	if (vector >= NR_IRQS)
		panic("register_int_handler: vector out of range %d\n", vector);

	enter_critical_section();

	int_handler_table[vector].arg = arg;
	int_handler_table[vector].handler = handler;

	exit_critical_section();
}
