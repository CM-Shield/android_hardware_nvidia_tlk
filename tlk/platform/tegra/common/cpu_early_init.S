/*
 * Copyright (c) 2012-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
.text
.globl __cpu_early_init

__cpu_early_init:

#if ARM_CLUSTER0_INIT_L2
	/* set up L2 for cluster0 */
	mrc	p15, 0, r0, c0, c0, 5
	ubfx	r0, r0, #8, #4			@ get cluster id
	cmp	r0, #0				@ C0 = 0, C1 = 1
	bne	not_on_cluster0

	mrc	p15, 0x1, r0, c9, c0, 0x2	@ read L2CTLR
	bic	r0, r0, #0x7			@ clear data ram latency field
	orr	r0, r0, #0x2			@ pipeline depth of 3
	mcr	p15, 0x1, r0, c9, c0, 0x2	@ write L2CTLR

not_on_cluster0:
#endif
	cpsid	aif, 0x13		@ SVC mode, interrupts disabled
	mrc	p15, 0, r0, c0, c0, 0	@ read main ID register
	and	r1, r0, #0xff000000	@ ARM?
	teq	r1, #0x41000000
	bne	no_wars
	and	r2, r0, #0x00f00000	@ variant
	and	r3, r0, #0x0000000f	@ revision
	orr	r3, r3, r2, lsr #20-4	@ combine variant and revision
	ubfx	r0, r0, #4, #12		@ primary part number

	/* Cortex-A9 Errata */
	ldr	r1, =0x00000c09		@ Cortex-A9 primary part number
	teq	r0, r1
	bne	no_wars
	cmp	r3, #0x10		@ power ctrl reg added r1p0
	mrcge	p15, 0, r1, c15, c0, 0	@ read power control register
	orrge	r1, r1, #1		@ enable dynamic clock gating
	mcrge	p15, 0, r1, c15, c0, 0	@ write power control register

	/* CONFIG_ARM_ERRATA_742230 */
	cmp	r3, #0x22		@ only present up to r2p2
	mrcle	p15, 0, r1, c15, c0, 1	@ read diagnostic register
	orrle	r1, r1, #1 << 4		@ set bit #4
	mcrle	p15, 0, r1, c15, c0, 1	@ write diagnostic register

	/* CONFIG_ARM_ERRATA_743622 */
	teq	r3, #0x20		@ present in r2p0
	teqne	r3, #0x21		@ present in r2p1
	teqne	r3, #0x22		@ present in r2p2
	teqne	r3, #0x27		@ present in r2p7
	teqne	r3, #0x29		@ present in r2p9
	mrceq	p15, 0, r1, c15, c0, 1	@ read diagnostic register
	orreq	r1, r1, #1 << 6		@ set bit #6
	mcreq	p15, 0, r1, c15, c0, 1	@ write diagnostic register

	/* CONFIG_ARM_ERRATA_751472 */
	cmp	r3, #0x30 		@ present prior to r3p0
	mrclt	p15, 0, r1, c15, c0, 1	@ read diagnostic register
	orrlt	r1, r1, #1 << 11	@ set bit #11
	mcrlt	p15, 0, r1, c15, c0, 1	@ write diagnostic register

	/* CONFIG_ARM_ERRATA_752520 */
	cmp	r3, #0x29		@ present prior to r2p9
	mrclt	p15, 0, r1, c15, c0, 1	@ read diagnostic register
	orrlt	r1, r1, #1 << 20 	@ set bit #20
	mcrlt	p15, 0, r1, c15, c0, 1	@ write diagnostic register

no_wars:
#if !defined(WITH_MONITOR_BIN)
	/* enable SCR.FW for CPU0 (with EL3 AARCH64, only writable in EL3) */
	mrc	p15, 0, r0, c0, c0, 5
	and	r0, r0, #0xF
	cmp	r0, #0			@ CPU0?
	bne	ret
	mov	r0, #(1 << 4)		@ SCR.FW
	mcr	p15, 0, r0, c1, c1, 0
	isb
#endif
ret:
	bx	lr			@ return
