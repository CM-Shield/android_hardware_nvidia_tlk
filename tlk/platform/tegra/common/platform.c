/*
 * Copyright (c) 2012-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <assert.h>
#include <errno.h>
#include <err.h>
#include <debug.h>
#include <rand.h>
#include <string.h>
#include <stdlib.h>
#include <libfdt/libfdt.h>
#include <lib/heap.h>
#include <arch/arm/mmu.h>
#include <arch/ops.h>
#include <arch/arm.h>
#include <platform.h>
#include <platform/memmap.h>
#include <platform/irqs.h>
#include <platform/platform_fuse.h>
#include <kernel/task.h>
#include <target/debugconfig.h>
#include <lib/monitor/monitor_vector.h>
#if ARM_WITH_OUTER_CACHE
#include <arch/outercache.h>
#endif
#include <platform/platform_p.h>
#include <ote_intf.h>

#define	MB		(1024 * 1024)

extern unsigned long boot_secondary_cpu_addr;
extern unsigned int coldboot_normal_os;
extern unsigned int normal_os_coldboot_fn;
extern uint32_t device_uid[4];
extern paddr_t dtb_addr;

#if !defined(WITH_MONITOR_BIN)
extern uint32_t __save_boot_regs[9];
extern uint32_t __save_boot_cpsr;
extern uint32_t __jumpback_addr;
#endif

uint32_t debug_uart_id = DEFAULT_DEBUG_PORT;

/* track available kernel VA space */
vaddr_t platform_vaspace_ptr;
vaddr_t platform_vaspace_end;

extern unsigned long cbstruct_addr;
extern unsigned long cbuf_addr;

void platform_early_init(void)
{
#if WITH_LIB_VERSION
	extern char *version;
#endif
	platform_init_debug_port(debug_uart_id);

#if WITH_LIB_VERSION
	dprintf(CRITICAL, "starting platform early init (TLK %s)\n", version);
#endif
}

void platform_idle(void)
{
	struct tz_monitor_frame frame, *smc_frame;
#if ARM_CPU_CORTEX_A9
	uint32_t val;
#endif

#if !defined(WITH_MONITOR_BIN)
	memset(&frame, 0, sizeof(frame));

	ASSERT(__jumpback_addr);
	frame.pc = __jumpback_addr;
	frame.spsr = __save_boot_cpsr;	/* interrupts disabled, in svc */
	memcpy(&frame.r[4], __save_boot_regs, sizeof(__save_boot_regs));
#endif

#if ARM_CPU_CORTEX_A9
	/*
	 * Before going to the NS world for the first time, set ACTLR.FW.
	 * The NSACR.NS_SMP setting granted it the capability to set ACTLR.SMP,
	 * but it doesn't cover NS writing ACTLR.FW.
	 */
	val = arm_read_actlr();
	val |= 0x1;
	arm_write_actlr(val);
#endif

	dputs(CRITICAL, "TLK initialization complete. Jumping to non-secure world\n");

	smc_frame = tz_switch_to_ns(SMC_TOS_INITIAL_NS_RETURN, &frame);
	while (smc_frame) {
		tz_stdcall_handler(smc_frame);
		smc_frame = tz_switch_to_ns(SMC_TOS_COMPLETION, smc_frame);
	}
}

void platform_init(void)
{
	uint32_t reg = 0;

	platform_init_cpu();

	platform_setup_keys();

	platform_fuse_init();

	te_intf_init();

	/*
	 * Set SE_TZRAM_SECURITY sticky bit to respect secure TZRAM accesses.
	 * Note: No need to reprogram it after LP0 exit as it's part of SE's
	 * sticky bits HW LP0 context, so will be restored by the BR.
	 */
	reg = *(volatile uint32_t *)(TEGRA_SE_BASE + 0x4);
	reg &= ~(0x1);
	*(volatile uint32_t *)(TEGRA_SE_BASE + 0x4) = reg;
}

void platform_init_mmu_mappings(void)
{
	extern int _heap_end;
	extern uint32_t __load_phys_size, __early_heap_allocs;

	/*
	 * End of the kernel's heap is the carveout size, reduced by
	 * any early allocations (e.g. pages used for pagetables).
	 */
	_heap_end = (VMEMBASE + __load_phys_size) - __early_heap_allocs;
	_heap_end &= ~PAGE_MASK;

	/* setup available vaspace (starts at end of carveout memory) */
	platform_vaspace_ptr = VMEMBASE + __load_phys_size;
	platform_vaspace_end = platform_vaspace_ptr + (VMEMSIZE - __load_phys_size);
}

uint64_t platform_translate_nsaddr(nsaddr_t vaddr, uint32_t type)
{
#if defined(WITH_MONITOR_BIN)
	struct tz_monitor_frame frame;
	frame.r[0] = vaddr;
	frame.r[1] = type;
	monitor_send_receive(SMC_TOS_ADDR_TRANSLATE, &frame);
	return frame.r[0];
#else
	arm_write_v2p(vaddr, type);
	return arm_read_par();
#endif
}

uint32_t platform_get_rand32(void)
{
	return rand();
}

uint32_t platform_get_time_us(void)
{
	return *(volatile uint32_t *)(TEGRA_TMRUS_BASE);
}

status_t platform_ss_register_handler(struct tz_monitor_frame *frame)
{
	return NO_ERROR;
}

/*
 * Calculate the physical address of the shared buffer that we have got for
 * logging from the linux kernel. All references to the shared buffer from
 * within tlk are made directly to the physical address.
 */
status_t set_log_phy_addr(nsaddr_t _ns_cb_struct_addr)
{
	struct circular_buffer *cbstruct;
	nsaddr_t cbuf;

	cbstruct = (struct circular_buffer *)
			tz_map_shared_mem(_ns_cb_struct_addr, PAGE_SIZE);
	if (cbstruct == NULL) {
		dprintf(CRITICAL, "%s: failed to map cbstruct\n", __func__);
		return ERR_NO_MEMORY;
	}

	cbuf = tz_map_shared_mem(cbstruct->buf, cbstruct->size);
	if (cbuf == NULL) {
		dprintf(CRITICAL, "%s: failed to map cbuf\n", __func__);
		return ERR_NO_MEMORY;
	}

	cbstruct_addr = (unsigned long)cbstruct;
	cbuf_addr = (unsigned long)cbuf;

	return NO_ERROR;
}

int platform_ss_request_handler(void)
{
	struct tz_monitor_frame frame;

	memset(&frame, 0, sizeof(struct tz_monitor_frame));
	frame.r[0] = SMC_ERR_PREEMPT_BY_FS;

	(void)tz_switch_to_ns(SMC_TOS_PREEMPTED, &frame);

	/* check status of fs preempt handler */
	if (frame.r[1] != OTE_SUCCESS)
		return -EIO;

	return 0;
}

#if WITH_ROLLBACK_PROTECTION
static void platform_ss_parse_dtb(uint32_t *ss_config)
{
	void *fdt = dtb_addr;
	char *device_name;
	int node;

	if (fdt == NULL)
		panic("no DTB available\n");

	node = fdt_path_offset(fdt, "/rollback-protection");
	if (node < 0)
		panic("no rollback node available\n");

	/* get status and device_name props */
	device_name = (char *)fdt_getprop(fdt, node, "device-name", NULL);
	if (!device_name)
		panic("no rollback device-name property specified\n");

	/* check for rollback device */
	if (!strcmp(device_name, "sdmmc"))
		*ss_config |= OTE_SS_CONFIG_RPMB_ENABLE;
	else if (!strcmp(device_name, "cpc"))
		*ss_config |= OTE_SS_CONFIG_CPC_ENABLE;
	else
		panic("unknown rollback device-name property specified: %s\n",
			device_name);
}
#endif

void platform_ss_get_config(uint32_t *ss_config)
{
	*ss_config = 0;

#if WITH_ROLLBACK_PROTECTION
	/* no rollback if ODM_PRODUCTION not blown */
	if (!platform_fuse_get(FUSE_ID_ODM_PROD))
		return;

	/* get rollback device info from dtb */
	platform_ss_parse_dtb(ss_config);

	dprintf(SPEW, "%s: *ss_config 0x%x\n", __func__, *ss_config);
#endif
}

void platform_get_device_id(te_device_id_args_t *out)
{
	if (out)
		memcpy(out, device_uid, sizeof(te_device_id_args_t));
}

void platform_clean_invalidate_cache_range(vaddr_t range, uint32_t length)
{
#if defined(ARM_USE_CPU_CACHING)
	arch_clean_invalidate_cache_range(range, length);

#if ARM_WITH_OUTER_CACHE
	outer_clean_range(virtual_to_physical(range), length);
	outer_inv_range(virtual_to_physical(range), length);
#endif
#endif
}
