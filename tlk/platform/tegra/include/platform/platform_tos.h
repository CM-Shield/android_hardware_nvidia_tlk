/*
 * Copyright (c) 2012-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Trusted OS Calls, defined according to ARM_DEN0028A specs.
 *
 * Bit 30 - 1: SMC64 calling convention, 0: SMC32 calling convention
 * Bits 29:24 - (0x32000000 - 0x3F000000) Trusted OS calls
 * Bits 15:0 - Function number
 *
 */
#define SMC_TOS_NS_REG_REQPARAM_BUF		0x72000001
#define SMC_TOS_INIT_LOGGER			0x72000002
#define SMC_TOS_SS_REGISTER_HANDLER		0x72000003
/* restart pre-empted SMC handling */
#define SMC_TOS_RESTART				0x72000100

/* Trusted OS calls (legacy) */
#define SMC_TOS_NS_REG_REQPARAM_BUF_LEGACY	0x32000002
#define SMC_TOS_INIT_LOGGER_LEGACY		0x32000007
#define SMC_TOS_SS_REGISTER_HANDLER_LEGACY	0x32000010
#define SMC_TOS_RESTART_LEGACY			(60 << 24)
