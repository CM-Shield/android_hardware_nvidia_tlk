/*
 * Copyright (c) 2013, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __PLATFORM_TEGRA_MEMMAP_EXT_H
#define __PLATFORM_TEGRA_MEMMAP_EXT_H

#define TEGRA_ARM_PL310_BASE		0x50061000
#define TEGRA_ARM_PL310_SIZE		SZ_4K

#define TEGRA_MC_BASE			0x70019000
#define TEGRA_MC_SIZE			SZ_2K

#define TEGRA_EMC_BASE			0x7001B000
#define TEGRA_EMC_SIZE			SZ_2K

#define TEGRA_ARM_SCU_BASE		TEGRA_ARM_PERIF_BASE
#define TEGRA_ARM_INT_CPU_BASE		(TEGRA_ARM_PERIF_BASE + 0x100)

#endif /*__PLATFORM_TEGRA_MEMMAP_EXT_H */
