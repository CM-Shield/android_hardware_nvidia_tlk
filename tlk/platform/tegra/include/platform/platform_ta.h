/*
 * Copyright (c) 2012-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * Trusted Application Calls, defined according to ARM_DEN0028A specs.
 *
 * Bit 30 - 1: SMC64 calling convention, 0: SMC32 calling convention
 * Bits 29:24 - (0x30000000 - 0x31000000) Trusted Application calls
 * Bits 15:0 - Function number
 *
 */
#define SMC_TA_OPEN_SESSION			0x70000001
#define SMC_TA_CLOSE_SESSION			0x70000002
#define SMC_TA_LAUNCH_OPERATION			0x70000003
#define SMC_TA_EVENT 				0x70000004

/* Trusted Application Calls (legacy) */
#define SMC_TA_OPEN_SESSION_LEGACY		0x30000001
#define SMC_TA_CLOSE_SESSION_LEGACY		0x30000002
#define SMC_TA_LAUNCH_OPERATION_LEGACY		0x30000003
#define SMC_TA_EVENT_LEGACY			0x30000004
