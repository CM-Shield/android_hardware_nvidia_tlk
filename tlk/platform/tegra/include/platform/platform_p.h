/*
 * Copyright (c) 2008 Travis Geiselbrecht
 * Copyright (c) 2012-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __PLATFORM_P_H
#define __PLATFORM_P_H

#include <lib/ote/ote_protocol.h>

/*
 * Note: normal world sp/lr aren't kept in the tz_monitor_frame,
 * but instead are saved/restored from the global nonsecure_state
 * in monitor_vector.S.
 */
struct tz_monitor_frame {
	uint64_t r[13];		/* r0-r12 */
	uint64_t pc;		/* return pc */
	uint64_t spsr;
};
typedef struct tz_monitor_frame tz_monitor_frame_t;

void platform_init_memory(uint32_t sec_base, uint32_t sec_size);
void platform_setup_keys(void);
void platform_init_cpu(void);
void platform_disable_debug_intf(void);
void platform_enable_debug_intf(void);
void platform_set_intr_ready_state(bool, struct tz_monitor_frame *frame);
status_t platform_ss_register_handler(struct tz_monitor_frame *frame);
int platform_ss_request_handler(void);
void platform_ss_get_config(uint32_t *ss_config);
void platform_get_device_id(te_device_id_args_t *args);
uint32_t platform_get_time_us(void);
void platform_clean_invalidate_cache_range(vaddr_t range, uint32_t size);

struct tz_monitor_frame *tz_switch_to_ns(uint32_t smc_type, struct tz_monitor_frame *frame);
void tz_stdcall_handler(struct tz_monitor_frame *frame);
void tz_fastcall_handler(struct tz_monitor_frame *frame);
void tz_handle_smc_l2(unsigned int smc);
void tz_handle_smc_deep_sleep(void);
vaddr_t tz_map_shared_mem(nsaddr_t ns_addr, uint32_t size);
void tz_add_dram_range(paddr_t base, paddr_t size);

uint64_t platform_translate_nsaddr(uint64_t vaddr, uint32_t type);
struct tz_monitor_frame *monitor_send_receive(uint32_t smc_type, struct tz_monitor_frame *frame);

void pm_init(void);
void pm_handle_platform_smc(struct tz_monitor_frame *frame);

#if ARM_WITH_SCU
void cpu_enable_scu(void);
void cpu_enable_scu_access(void);
#endif
void mon_cpu_gic_setup(void);

status_t set_log_phy_addr(nsaddr_t _ns_cb_struct_addr);

void fuse_load_table(uint32_t fuse_id_max, uint32_t *fuse_table);

#endif
