/*
 * Copyright (c) 2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <assert.h>
#include <debug.h>
#include <platform.h>
#include <reg.h>

#include <platform/platform_fuse.h>
#include <platform/platform_p.h>
#include <platform/memmap.h>

/* fuse register information */
#define FUSECTRL_0			(0x0)
#define FUSECTRL_STATE_MASK		(0x1F << 16)
#define FUSECTRL_STATE_IDLE		(0x4 << 16)

#define SECURITY_MODE_0			(0x1a0)
#define SECURITY_MODE_MASK		(0x1 << 0x0)

#define FUSE_READ32(o) 			readl(TEGRA_FUSE_BASE + (o))

/* car register information */
#define CLK_MASK_ARM_0			(0x48)
#define CLK_MASK_ARM_CFG_ALL_VISIBLE	(0x10000000)

#define RST_DEVICES_H_0			(0x8)
#define RST_DEVICES_H_SWR_FUSE_RST	(0x80)

#define CLK_OUT_ENB_H_0			(0x14)
#define CLK_OUT_ENB_H_CLK_ENB_FUSE	(0x80)

#define CAR_WRITE32(o,v) 		writel((v), TEGRA_CLK_RESET_BASE + (o))
#define CAR_READ32(o) 			readl(TEGRA_CLK_RESET_BASE + (o))

static void fuse_wait_for_idle(void)
{
	uint32_t data;

	do {
		spin(1);
		data = FUSE_READ32(FUSECTRL_0);
	} while ((data & FUSECTRL_STATE_MASK) != FUSECTRL_STATE_IDLE);
}

static bool fuse_make_visible(void)
{
	uint32_t clk_mask_arm_data = 0;

	/* for now assume fuse block is out of reset with clock enabled */
	ASSERT(!(CAR_READ32(RST_DEVICES_H_0) & RST_DEVICES_H_SWR_FUSE_RST));
	ASSERT(CAR_READ32(CLK_OUT_ENB_H_0) & CLK_OUT_ENB_H_CLK_ENB_FUSE);

	/* make all fuse registers visible */
	clk_mask_arm_data = CAR_READ32(CLK_MASK_ARM_0);
	if ((clk_mask_arm_data & CLK_MASK_ARM_CFG_ALL_VISIBLE) == 0) {
		CAR_WRITE32(CLK_MASK_ARM_0,
			clk_mask_arm_data | CLK_MASK_ARM_CFG_ALL_VISIBLE);
		return true;
	}

	return false;
}

static void fuse_make_invisible(void)
{
	uint32_t clk_mask_arm_data = 0;

	/* hide fuse registers */
	clk_mask_arm_data = CAR_READ32(CLK_MASK_ARM_0);
	clk_mask_arm_data &= ~CLK_MASK_ARM_CFG_ALL_VISIBLE;
	CAR_WRITE32(CLK_MASK_ARM_0, clk_mask_arm_data);

	return;
}

static te_error_t fuse_read(enum fuse_ids fuse_id, uint32_t *fuse_val)
{
	uint32_t val = 0;
	te_error_t result = OTE_SUCCESS;

	switch (fuse_id) {
		case FUSE_ID_ODM_PROD:
			fuse_wait_for_idle();
			val = FUSE_READ32(SECURITY_MODE_0);
			val &= SECURITY_MODE_MASK;
			break;
		default:
			dprintf(CRITICAL, "%s: unknown fuse id 0x%x\n",
				__func__, fuse_id);
			result = OTE_ERROR_BAD_PARAMETERS;
			break;
	}

	*fuse_val = val;

	return result;
}

void fuse_load_table(uint32_t fuse_id_max, uint32_t *fuse_table)
{
	uint32_t fuse_id, fuse_data;
	bool made_visible;

	/* expose fuse registers for access */
	made_visible = fuse_make_visible();

	/* init fuses */
	for (fuse_id = 0; fuse_id < FUSE_ID_MAX; fuse_id++) {
		if (fuse_read(fuse_id, &fuse_data) != OTE_SUCCESS) {
			dprintf(CRITICAL, "%s: cannot read fuse_id %d\n",
				__func__, fuse_id);
			goto done;
		}
		fuse_table[fuse_id] = fuse_data;

		dprintf(SPEW, "%s: id %d value 0x%x\n",
			__func__, fuse_id, fuse_table[fuse_id]);
	}

done:
	/* move fuses back to invisible if necessary */
	if (made_visible)
		fuse_make_invisible();

	return ;
}

