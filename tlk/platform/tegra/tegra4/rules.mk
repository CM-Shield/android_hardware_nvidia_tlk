# ROMBASE, VMEMBASE, and VMEMSIZE are required for the linker script
VMEMBASE := 0x48000000

ARM_CPU := cortex-a15
ARM_WITH_LPAE := true

DEFINES += \
	WITH_PADDR_T_64BIT=1

ifeq ($(TARGET),t124)
DEFINES += \
	ARM_CLUSTER0_INIT_L2=1
endif

MODULE_SRCS += \
        $(LOCAL_DIR)/tegra4/fuse.c	\
        $(LOCAL_DIR)/tegra4/platform.c
