LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR)

ENABLE_THUMB := false

ARCH := arm
CPU := generic

INCLUDES += \
	-I$(LOCAL_DIR)/include \
	-I$(LOCAL_DIR)/include/platform/$(PLATFORM_SOC) \
	-I$(LOCAL_DIR)/common \
	-Ilib \
	-I../lib \
        -Isecure_monitor/platform/tegra/include

COMMON_DIR := $(LOCAL_DIR)/common
PLATFORM_SOC_DIR := $(LOCAL_DIR)/$(PLATFORM_SOC)

MODULE_SRCS += \
	$(COMMON_DIR)/fuse.c		\
	$(COMMON_DIR)/tz.c		\
	$(COMMON_DIR)/platform.c	\
	$(COMMON_DIR)/timer.c		\
	$(COMMON_DIR)/interrupts.c	\
	$(COMMON_DIR)/memory.c		\
	$(COMMON_DIR)/debug.c		\

ifeq ($(MONITOR_BIN),true)
MODULE_SRCS += \
	$(COMMON_DIR)/cpu_early_init.S
endif

MODULE_DEPS += \
	lib/libfdt \
	lib/version

include $(PLATFORM_SOC_DIR)/rules.mk

# Disable all prints for release builds, only CRITICAL prints will be
# printed in release builds. Change the value of DEBUG in else block
# to 1->INFO and 2->SPEW prints in debug builds.
ifeq ($(TARGET_BUILD_TYPE), release)
	DEBUG := 0
else
	DEBUG := 1
endif

# relocate image to provided physaddr via MMU
ARM_USE_MMU_RELOC := true

# enable use of CPU caching
ARM_USE_CPU_CACHING := true

DEFINES += VMEMBASE=$(VMEMBASE) \
	VMEMSIZE=$(VMEMSIZE) \
	DEBUG=$(DEBUG) \
	WITH_CPU_EARLY_INIT=1 \
	WITH_ROLLBACK_PROTECTION=$(WITH_ROLLBACK_PROTECTION)

# use a two segment memory layout, where all of the read-only sections
# of the binary reside in rom, and the read/write are in memory. The
# ROMBASE, VMEMBASE, and VMEMSIZE make variables are required to be set
# for the linker script to be generated properly.
#
LINKER_SCRIPT += \
	$(BUILDDIR)/system-onesegment.ld

include make/module.mk
