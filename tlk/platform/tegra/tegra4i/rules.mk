# ROMBASE, VMEMBASE, and VMEMSIZE are required for the linker script
VMEMBASE := 0x48000000

ARM_CPU := cortex-a9

DEFINES += \
	ARM_WITH_L2X0=1		\
	ARM_WITH_OUTER_CACHE=1

MODULE_SRCS += \
	$(LOCAL_DIR)/tegra4i/platform.c	\
	$(LOCAL_DIR)/tegra4i/tz.c
