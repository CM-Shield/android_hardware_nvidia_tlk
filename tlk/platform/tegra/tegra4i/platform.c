/*
 * Copyright (c) 2013, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <debug.h>
#include <arch/arm.h>
#include <platform/memmap.h>
#include <arch/arm/cache-l2x0.h>
#include <platform/platform_p.h>

static unsigned int lp_tag_latency = 0x110;
static unsigned int lp_data_latency = 0x331;
static unsigned int g_tag_latency = 0x111;
static unsigned int g_data_latency = 0x441;

#define FLOW_CTRL_CLUSTER_CONTROL	(TEGRA_FLOW_CTRL_BASE + 0x2c)

static bool platform_is_lp_cluster(void)
{
	uint32_t cluster_ctrl;

	cluster_ctrl = *(volatile uint32_t *)(FLOW_CTRL_CLUSTER_CONTROL);

	return ((cluster_ctrl & 0x1) != 0);	/* 0 == G, 1 == LP*/
}

uint32_t platform_l2x0_base(void)
{
	return TEGRA_ARM_PL310_BASE;
}

void platform_init_outer(void)
{
	uint32_t base = TEGRA_ARM_PL310_BASE;
	uint32_t tag_latency_ctrl = base + L2X0_TAG_LATENCY_CTRL;
	uint32_t data_latency_ctrl = base + L2X0_DATA_LATENCY_CTRL;
	uint32_t aux_ctrl;

	if (*(volatile uint32_t *)(base + L2X0_CTRL) & 1) {
		dprintf(INFO, "L2 cache already enabled\n");
			return;
	}

	/* determine cluster ID to set latencies */
	if (platform_is_lp_cluster()) {
		// LP cluster
		*(volatile uint32_t *)tag_latency_ctrl = lp_tag_latency;
		*(volatile uint32_t *)data_latency_ctrl = lp_data_latency;
	} else {
		// G cluster
		*(volatile uint32_t *)tag_latency_ctrl = g_tag_latency;
		*(volatile uint32_t *)data_latency_ctrl = g_data_latency;
	}

	*(volatile uint32_t *)(base + L2X0_PREFETCH_CTRL) = 0x40000007;
	*(volatile uint32_t *)(base + L2X0_POWER_CTRL) =
		L2X0_DYNAMIC_CLK_GATING_EN | L2X0_STNDBY_MODE_EN;

	aux_ctrl = *(volatile uint32_t *)(base + L2X0_AUX_CTRL);
	aux_ctrl |= (uint32_t) ((1 << 0) | // Full line of zeros
		(1 << 22) |  // Shared attribute override enable
		(1 << 26) |  // NS lockdown enable
		(1 << 27) |  // NS int access ctrl
		(1 << 28) |  // data prefetch enable
		(1 << 29) |  // instn prefetch enable
		(1 << 30));  // early BRESP enable
	*(volatile uint32_t *)(base + L2X0_AUX_CTRL) = aux_ctrl;
}

void platform_init_cpu(void)
{
	l2x0_setup();

        cpu_enable_scu();
        cpu_enable_scu_access();

	cpu_gic_setup();
}
