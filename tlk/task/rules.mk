LOCAL_DIR := $(GET_LOCAL_DIR)

TASKDIR := $(BUILDDIR)/tasks

TASK_OBJ := $(TASKDIR)/task.o
TASK_BIN := $(TASKDIR)/task

GENERATED += $(TASK_OBJ) $(TASK_BIN)

#
# Create a task.o from the concatenation of tasks to be made available.
# To control placement in the final LK image, the tasks .data section is
# renamed to .task.data, so it's picked up in a page-aligned part of the
# data section by the linker script (allowing tasks to run, in-place).
#
$(TASK_OBJ): $(TASK_BIN)
	@$(MKDIR)
	@echo generating $@
	cd $(TASKDIR); ../$(LD) -r -b binary -o $(notdir $@) $(notdir $^)
	$(OBJCOPY) --prefix-sections=.task $@

$(TASK_BIN): $(TASKS)
	@$(MKDIR)
	@echo combining tasks $@
	cat $(TASKS) > $@
