LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR)

MODULE_DEPS := \
	lib/libc \
	lib/debug \
	lib/heap

MODULE_SRCS := \
	$(LOCAL_DIR)/debug.c \
	$(LOCAL_DIR)/dpc.c \
	$(LOCAL_DIR)/event.c \
	$(LOCAL_DIR)/main.c \
	$(LOCAL_DIR)/mutex.c \
	$(LOCAL_DIR)/task.c \
	$(LOCAL_DIR)/task_load.c \
	$(LOCAL_DIR)/task_unload.c \
	$(LOCAL_DIR)/thread.c \
	$(LOCAL_DIR)/timer.c \
	$(LOCAL_DIR)/boot.c \
	$(LOCAL_DIR)/semaphore.c \
	$(LOCAL_DIR)/syscall.c \
	$(LOCAL_DIR)/ote_intf.c

include make/module.mk
