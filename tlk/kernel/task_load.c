/*
 * Copyright (c) 2013-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <debug.h>
#include <sys/types.h>
#include <assert.h>
#include <string.h>
#include <malloc.h>
#include <err.h>
#include <errno.h>
#include <stdlib.h>
#include <kernel/task.h>
#include <kernel/thread.h>
#include <kernel/task_load.h>
#include <lib/heap.h>

/* for handling requests from syscall.c */
#include <lib/ote/ote_protocol.h>

#ifndef TASK_HEAP_PERCENTAGE
/*
 * Max 60% of heap can be used for task loading.
 * Define as 0 to disable task loading.
 */
#define TASK_HEAP_PERCENTAGE 60
#endif /* TASK_HEAP_PERCENTAGE */

#ifndef TASK_HEAP_PAGES_RESERVED
/*
 * Task loading will leave this many pages for TLK heap use
 * no matter what the percentage above is or no matter what
 * the heap size is.
 *
 * If the heap is too small, task loading will be disabled.
 */
#define TASK_HEAP_PAGES_RESERVED 50
#endif /* TASK_HEAP_PAGES_RESERVED */

#ifndef TASK_HEAP_PAGES_MIN
/*
 * No point in supporting task loading if there are less pages
 * than this to load tasks to.
 *
 * If the heap is too small, task loading will be disabled.
 */
#define TASK_HEAP_PAGES_MIN	20
#endif /* TASK_HEAP_PAGES_MIN */

/* Pending task-load states (before task is fully loaded)
 *
 * The tp_state_t defines the internal states of the state machine used
 * for the multi-step task loading operation. Once completed or on error
 * the associated task_pending_t node will be deleted and the task be
 * either installed or rejected.
 */
typedef enum {
	TASK_PSTATE_UNDEFINED,	 /* uninitialized */
	TASK_PSTATE_INVALID,	 /* allocated, undefined */
	TASK_PSTATE_MEMBUF,	 /* memory allocated for task */
	TASK_PSTATE_TA_MAPPED,	 /* shared buffer mapped for copying task image */
	TASK_PSTATE_TA_UNMAPPED, /* shared buffer unmapped; task image copied */
	TASK_PSTATE_PREPARED,	 /* task parsed */
	TASK_PSTATE_READY	 /* task started (pending node deallocated) */
} tp_state_t;

/*
 * Each task loading operation will get a pending node associated with it.
 * The node exists only during the load process.
 *
 * Completed/rejected task installs are removed from this list and the pending node
 * gets deleted.
 */
typedef struct {
	uint32_t	tp_handle;
	tp_state_t	tp_state;
	task_t  	tp_task;
	vaddr_t		tp_task_tlk_addr;	/* task virt address in TLK */
	uint32_t	tp_task_size;
	vaddr_t		tp_task_ta_addr;	/* task virt address in TA */
	struct list_node tp_node;
} task_pending_t;

static struct list_node task_pending_list;

/*
 * Count task pages allocated and max number of pages allowed
 * for task loading.
 *
 * TASK_HEAP_PERCENTAGE defines how much of the total TLK heap can
 * be used for this.
 */
static u_int task_pages_heap_used;
static u_int task_pages_heap_max;
static u_int task_heap_percentage = TASK_HEAP_PERCENTAGE;

static vaddr_t original_heap_start;
static vaddr_t original_heap_end;

static uint32_t task_load_counter;

void task_load_config(vaddr_t vaddr_begin, vaddr_t *vaddr_end_p)
{
	u_int heap_pages = 0;

	/* if no memory available, just return */
	if (!vaddr_end_p || *vaddr_end_p == 0)
		goto exit;

	/* save the original limit values for info (heap begin..end) */
	original_heap_start = vaddr_begin;
	original_heap_end   = *vaddr_end_p;

	if ((original_heap_start == 0) ||
	    (original_heap_start >= original_heap_end) ||
	    (task_heap_percentage == 0))
		goto exit;

	heap_pages = (original_heap_end - original_heap_start) / PAGE_SIZE;

	if (heap_pages <= TASK_HEAP_PAGES_RESERVED) {
		dprintf(INFO, "heap too small for task loading (%d pages)\n",
			heap_pages);
		goto exit;
	}

	/*
	 * Max number of heap pages allowed for task loading
	 * This is an upper limit, less heap may be available at runtime.
	 */
	task_pages_heap_max =
		(heap_pages / 100) * task_heap_percentage;

	if (heap_pages - task_pages_heap_max < TASK_HEAP_PAGES_RESERVED)
		task_pages_heap_max = heap_pages - TASK_HEAP_PAGES_RESERVED;

	if (task_pages_heap_max < TASK_HEAP_PAGES_MIN) {
		dprintf(INFO, "Not enough pages for task loading (%d pages)\n",
			task_pages_heap_max);

		task_pages_heap_max = 0;
		goto exit;
	}

	dprintf(INFO, "Max heap %d pages; task load limit %d pages\n",
		heap_pages, task_pages_heap_max);

	if (0) {
	exit:
		dprintf(INFO, "task loading not supported\n");
	}
}

void task_load_init()
{
	list_initialize(&task_pending_list);
}

u_int task_allowed_to_load_tasks(task_t *taskp)
{
	return (taskp && (taskp->props.authorizations & OTE_AUTHORIZE_INSTALL));
}

static void task_add_new_pending(task_pending_t *tp, vaddr_t addr, uint32_t size,
				 tp_state_t pstate)
{
	uint32_t handle = 0;
	int found = 0;

	if (!tp || !size || !addr)
		return;

	tp->tp_task_tlk_addr = addr;	/* task address in TLK */
	tp->tp_task_ta_addr = 0;	/* TA address not yet mapped */
	tp->tp_task_size = size;	/* task byte size */
	tp->tp_state = TASK_PSTATE_INVALID;

	do {
		task_pending_t *tp_scan = NULL;

		found = 0;

		handle = platform_get_rand32(); /* need to push a unique non-zero handle */
		if (!handle)
			handle++;		/* zero means handle unused */

		enter_critical_section();

		list_for_every_entry(&task_pending_list, tp_scan,
				     task_pending_t, tp_node) {
			if (tp_scan->tp_handle == handle) {
				found++;
				break;
			}
		}

		/* new handle found; reserve it */
		if (!found) {
			tp->tp_handle = handle;
			tp->tp_state  = pstate;
			list_add_tail(&task_pending_list, &tp->tp_node);
		}
		exit_critical_section();

	} while (found);
}

/*
 * Atomically search pending task by random handle
 * Found entry is removed from the list so caller must push it back if required.
 */
static task_pending_t *task_find_pending(uint32_t handle)
{
	task_pending_t *tp_scan = NULL;
	int found = 0;

	enter_critical_section();

	list_for_every_entry(&task_pending_list, tp_scan, task_pending_t, tp_node) {
		if (tp_scan->tp_handle == handle) {
			found++;

			list_delete(&tp_scan->tp_node);
			break;
		}
	}

	exit_critical_section();

	if (!found)
		tp_scan = NULL;

	return tp_scan;
}

static void task_push_pending(task_pending_t *tp, tp_state_t pstate)
{
	if (tp) {
		tp->tp_state = pstate;

		enter_critical_section();
		list_add_tail(&task_pending_list, &tp->tp_node);
		exit_critical_section();
	}
}

/*
 * Map the reserved memory virtual address into the target TA
 * task address space.
 */
static status_t task_map_memory_to_ta(task_t *taskp, uint32_t handle,
				      vaddr_t *ta_addr_p)
{
	status_t err = NO_ERROR;
	int flags = TM_UW | TM_UR | TM_KERN_SEC_VA;
	task_map_t *mptr = NULL;
	task_pending_t *tp  = NULL;

	if (!taskp || !ta_addr_p)
		return ERR_INVALID_ARGS;

	tp = task_find_pending(handle);
	if (!tp) {
		dprintf(SPEW, "%s: task handle unknown 0x%x\n",
			__func__, handle);
		err = ERR_NOT_FOUND;
		goto exit;
	}

	if (tp->tp_state != TASK_PSTATE_MEMBUF) {
		dprintf(CRITICAL, "%s: task handle 0x%x in wrong state: %d\n",
			__func__, handle, tp->tp_state);
		err = ERR_TASK_GENERIC;
		goto exit;
	}

	mptr = arch_task_map_memory(taskp, tp->tp_task_tlk_addr,
				    tp->tp_task_size, flags);
	if (mptr == NULL) {
		dprintf(CRITICAL, "TLK vaddr app load memory map failed for 0x%lx (%d bytes))\n",
			(unsigned long)tp->tp_task_tlk_addr, tp->tp_task_size);
		err = ERR_NO_MEMORY;
		goto exit;
	}

	tp->tp_task_ta_addr = mptr->vaddr;
	*ta_addr_p = mptr->vaddr;

	task_push_pending(tp, TASK_PSTATE_TA_MAPPED);

	if (0) {
	exit:
		/* released by caller */
		if (tp)
			task_push_pending(tp, TASK_PSTATE_INVALID);

		if (err == NO_ERROR)
			err = ERR_GENERIC;
	}

	return err;
}

/* @brief Task loading step#1/3, request TLK shared memory mapping to TA space
 *
 * Allocate page aligned memory from the TLK heap for app loading and
 * map it to TA memory. TA then copies the TASK into this memory area
 * and calls tlk_handle_app_prepare.
 */
status_t task_request_app_load_memory(u_int byte_size, uint32_t *handle_p)
{
	status_t err = NO_ERROR;
	u_int	 npages = 0;
	vaddr_t	 load_addr = NULL;
	task_pending_t *tp = NULL;

	if (byte_size <= 0 || !handle_p) {
		err = ERR_INVALID_ARGS;
		goto exit;
	}

	*handle_p = 0;
	npages = ROUNDUP(byte_size, PAGE_SIZE) / PAGE_SIZE;

	dprintf(SPEW, "TASK MEM ALLOCATION REQUEST => %d bytes (rounded to %d pages)\n",
		byte_size, npages);

	load_addr = (vaddr_t)heap_alloc((npages * PAGE_SIZE), PAGE_SIZE);
	if (!load_addr) {
		err = ERR_NO_MEMORY;
		goto exit;
	}
	memset((void *)load_addr, 0, npages * PAGE_SIZE);

	tp = malloc(sizeof(task_pending_t));
	if (!tp) {
		err = ERR_NO_MEMORY;
		goto exit;
	}
	memset(tp, 0, sizeof(task_pending_t));

	/* check for an upper limit for heap allocation for task loading */
	enter_critical_section();

	if (task_pages_heap_used + npages > task_pages_heap_max) {
		err = ERR_TASK_GENERIC;
	} else {
		task_pages_heap_used += npages;
	}

	exit_critical_section();

	/* if not allowed to reserve more pages */
	if (err != NO_ERROR) {
		dprintf(INFO, "TASK MEM => too many pages (%d) used for tasks\n",
			task_pages_heap_used);
		goto exit;
	}

	/* setup a task load pending object with a unique handle */
	task_add_new_pending(tp, load_addr, byte_size, TASK_PSTATE_MEMBUF);

	/* Must not fail after this point... */
	*handle_p = tp->tp_handle;

	dprintf(SPEW, "TASK MEM allocated (handle 0x%x) => app start=0x%x (%d pages) [tasks pages used %d]\n",
		*handle_p, (uint32_t)load_addr, npages, task_pages_heap_used);

	if (0) {
	exit:
		if (tp)
			free(tp);

		if (load_addr)
			heap_free((void *)load_addr);

		if (err == NO_ERROR)
			err = ERR_GENERIC;
	}

	return err;
}

void task_dealloc_memory(vaddr_t vaddr, u_int task_byte_size)
{
	u_int npages = 0;

	ASSERT(vaddr);
	ASSERT(task_byte_size > 0);
	ASSERT(original_heap_start <= vaddr && vaddr <= original_heap_end);

	npages = ROUNDUP(task_byte_size, PAGE_SIZE) / PAGE_SIZE;

	ASSERT(task_pages_heap_used >= npages);

	heap_free((void *)vaddr);

	enter_critical_section();
	task_pages_heap_used -= npages;
	exit_critical_section();
}

static status_t task_dealloc_app_load_memory_tp(task_pending_t *tp)
{
	status_t err = NO_ERROR;
	u_int npages = 0;
	u_int byte_size = 0;
	vaddr_t vaddr = 0;
	u_int used_pages = task_pages_heap_used;

	if (!tp) {
		err = ERR_TASK_GENERIC;
		goto exit;
	}

	vaddr     = tp->tp_task_tlk_addr;
	byte_size = tp->tp_task_size;

	if (!vaddr || byte_size <= 0) {
		err = ERR_TASK_GENERIC;
		goto exit;
	}

	task_dealloc_memory(vaddr, byte_size);

	npages = ROUNDUP(byte_size, PAGE_SIZE) / PAGE_SIZE;
	if (used_pages < npages)
		dprintf(INFO, "TASK MEM (handle 0x%x) dealloc INCONSISTENCY => task addr 0x%x (%d pages) [tasks pages used %d]\n",
			tp->tp_handle, (uint32_t)vaddr, npages, used_pages);
	else
		dprintf(INFO, "TASK MEM (handle 0x%x) dealloc => task addr 0x%x (%d pages) [tasks pages used %d]\n",
			tp->tp_handle, (uint32_t)vaddr, npages, used_pages);

	if (0) {
	exit:
		if (err == NO_ERROR)
			err = ERR_GENERIC;
	}
	return err;
}

static status_t task_dealloc_app_load_memory(uint32_t handle)
{
	status_t err = NO_ERROR;
	task_pending_t *tp = NULL;

	tp = task_find_pending(handle);
	if (!tp) {
		dprintf(SPEW, "%s: task handle unknown 0x%x\n",
			__func__, handle);
		err = ERR_NOT_FOUND;
		goto exit;
	}

	err = task_dealloc_app_load_memory_tp(tp);

	free(tp);
	tp = NULL;

	if (0) {
	exit:
		if (err == NO_ERROR)
			err = ERR_GENERIC;
	}
	return err;
}

/* @brief Override manifest properties unless manifest is immutable.
 *
 * Installer task starting a loaded task may restrict the manifest options
 * dynamically or prevent a task from being run (after verifying the
 * manifest options).
 *
 * Modifications requested to manifest flagged as immutable are ignored.
 */
static status_t task_property_override(task_t *taskp, task_restrictions_t *tr)
{
	status_t err = NO_ERROR;
	uint32_t size;

	if (!taskp || !tr) {
		err = ERR_TASK_GENERIC;
		goto exit;
	}

	if ((taskp->props.initial_state & OTE_MANIFEST_TASK_ISTATE_IMMUTABLE) == 0) {
		if (tr->min_stack_size > 0) {
			size = ROUNDUP(tr->min_stack_size, PAGE_SIZE);
			taskp->props.min_stack_size = size;
			dprintf(INFO, "Loaded task stack size set to %d bytes\n",
				taskp->props.min_stack_size);
		}

		if (tr->min_heap_size > 0) {
			size = ROUNDUP(tr->min_heap_size, PAGE_SIZE);
			taskp->props.min_heap_size = size;
			dprintf(INFO, "Loaded task heap size set to %d bytes\n",
				taskp->props.min_heap_size);
		}

		/* override the task name if set */
		if (tr->task_name[0])
			task_set_name(taskp, tr->task_name);
	}

	if (0) {
	exit:
		if (err == NO_ERROR)
			err = ERR_GENERIC;
	}
	return err;
}

/* @brief unmap task from TA memory at step#2/3 of task loading before
 * the binary is parsed.
 */
static status_t task_unmap_app_mem_from_ta(task_t *taskp, uint32_t handle)
{
	status_t err = NO_ERROR;
	task_pending_t *tp = NULL;
	task_map_t *mptr = NULL;

	if (!taskp) {
		err = ERR_INVALID_ARGS;
		goto exit;
	}

	tp = task_find_pending(handle);

	if (!tp) {
		dprintf(SPEW, "%s: task handle unknown 0x%x\n",
			__func__, handle);
		err = ERR_NOT_FOUND;
		goto exit;
	}

	if (tp->tp_state != TASK_PSTATE_TA_MAPPED) {
		dprintf(CRITICAL, "%s: task handle 0x%x in wrong state: %d\n",
			__func__, handle, tp->tp_state);
		err = ERR_TASK_GENERIC;
		goto exit;
	}

	/*
	 * Unmap the application install buffer from the installer
	 * TA memory virtual address before doing anything with the TA
	 * copied data (for security reasons).
	 */
	mptr = task_find_mapping(taskp, tp->tp_task_ta_addr, tp->tp_task_size);
	if (!mptr) {
		dprintf(CRITICAL, "Can't find mapped memory from task address space\n");
		err = ERR_NOT_FOUND;
		goto exit;
	}

	arch_task_unmap_memory(taskp, mptr);
	dprintf(SPEW,
		"Unmapped task map memory <0%08lx, %d bytes> from the calling task\n",
		tp->tp_task_ta_addr, tp->tp_task_size);

	task_push_pending(tp, TASK_PSTATE_TA_UNMAPPED);

	if (0) {
	exit:
		/* released by caller */
		if (tp)
			task_push_pending(tp, TASK_PSTATE_INVALID);

		if (err == NO_ERROR)
			err = ERR_GENERIC;
	}
	return err;
}

/* @brief Copy task properties to task info.
 */
static void task_get_config(task_info_t *ti, task_t *taskp)
{
	if (ti)
		memset(ti, 0, sizeof(task_info_t));

	if (taskp && ti) {
		memcpy(&ti->uuid, &taskp->props.uuid,
		       sizeof(taskp->props.uuid));

#define TASK_COPY_PROPERTY(to,from,field) (to)->field = (from)->props.field

		TASK_COPY_PROPERTY(ti,taskp,manifest_exists);
		TASK_COPY_PROPERTY(ti,taskp,multi_instance);
		TASK_COPY_PROPERTY(ti,taskp,min_stack_size);
		TASK_COPY_PROPERTY(ti,taskp,min_heap_size);
		TASK_COPY_PROPERTY(ti,taskp,map_io_mem_cnt);
		TASK_COPY_PROPERTY(ti,taskp,restrict_access);
		TASK_COPY_PROPERTY(ti,taskp,authorizations);
		TASK_COPY_PROPERTY(ti,taskp,initial_state);

#undef TASK_COPY_PROPERTY

		memcpy(ti->task_name, taskp->task_name,
		       sizeof(taskp->task_name));

		ti->task_name[sizeof(ti->task_name) - 1] = '\000';

		memcpy(ti->task_private_data, taskp->task_private_data,
		       sizeof(taskp->task_private_data));
	}
}

/* @brief Parse loaded task binary in TLK memory at step#2/3 of task loading.
 */
static status_t task_parse_app(uint32_t handle, task_info_t *ti)
{
	status_t err = NO_ERROR;
	task_pending_t *tp = NULL;

	tp = task_find_pending(handle);
	if (!tp) {
		dprintf(SPEW, "%s: task handle unknown 0x%x\n",
			__func__, handle);
		err = ERR_NOT_FOUND;
		goto exit;
	}

	if (tp->tp_state != TASK_PSTATE_TA_UNMAPPED) {
		dprintf(CRITICAL, "%s: task handle 0x%x in wrong state: %d\n",
			__func__, handle, tp->tp_state);
		err = ERR_TASK_GENERIC;
		goto exit;
	}

	/* prepare task based on static app data in buffer */
	err = task_prepare((char *)tp->tp_task_tlk_addr, tp->tp_task_size,
			   &tp->tp_task, NULL, TASK_TYPE_LOADED);
	if (err != NO_ERROR) {
		dprintf(CRITICAL, "%s: loaded task preparation failed (%d)\n",
			__func__, err);
		goto exit;
	}

	if (!tp->tp_task.props.manifest_exists) {
		dprintf(INFO, "%s: loaded task image has no manifest\n", __func__);
	}

	/* copy selected manifest config options to be passed back to caller */
	task_get_config(ti, &tp->tp_task);

	task_push_pending(tp, TASK_PSTATE_PREPARED);

	if (0) {
	exit:
		/* released by caller */
		if (tp)
			task_push_pending(tp, TASK_PSTATE_INVALID);

		if (err == NO_ERROR)
			err = ERR_GENERIC;
	}
	return err;
}

/* @brief Start loaded application with manifest restrictions at step#3/3.
 */
static status_t task_run_app(uint32_t handle, install_type_t install_type, task_restrictions_t *tr)
{
	status_t err = NO_ERROR;
	task_pending_t *tp = NULL;
	task_t *taskp = NULL;
	int task_registered = 0;
	task_t *update_task = NULL;
	task_state_t update_task_state = TASK_STATE_UNKNOWN;
	te_service_id_t update_task_uuid;

	tp = task_find_pending(handle);
	if (!tp) {
		dprintf(SPEW, "%s: task handle unknown 0x%x\n",
			__func__, handle);
		err = ERR_NOT_FOUND;
		goto exit;
	}

	/* a task can be rejected at any state; this triggers resource cleanup */
	if (!tr || (install_type == INSTALL_TYPE_REJECT)) {
		dprintf(INFO, "%s: task handle 0x%x rejected in state: %d\n",
			__func__, handle, tp->tp_state);

		task_dealloc_app_load_memory_tp(tp);
	} else {
		if (tp->tp_state != TASK_PSTATE_PREPARED) {
			dprintf(CRITICAL, "%s: task handle 0x%x in wrong state: %d\n",
				__func__, handle, tp->tp_state);
			err = ERR_TASK_GENERIC;
			goto exit;
		}

		taskp = &tp->tp_task;

		/*
		 * Any possible task restrictions are enforced here
		 *
		 * If loading an image without manifest is required =>
		 * add support for setting UUID and other manifest
		 * configured options by the property override calls.
		 *
		 * Currently this is NOT SUPPORTED but it is simple to
		 * add to the task_property_override().
		 *
		 * Doing so would enable the installer to control all
		 * aspects of app install (task loading), including
		 * the application identity and all options (e.g.
		 * the task config data modification).
		 *
		 * NOTE: Currently all loaded tasks must contain a
		 * built-in manifest.
		 */
		err = task_property_override(taskp, tr);
		if (err != NO_ERROR) {
			dprintf(CRITICAL, "%s: task install 0x%x rejected by override error %d\n",
				__func__, handle, err);
			goto exit;
		}

		/*
		 * At this point the task must either have manifest loaded from signed image
		 * or contain one created by the property override above.
		 */
		if (!taskp->props.manifest_exists) {
			dprintf(CRITICAL, "%s: Invalid task image\n", __func__);
			err = ERR_NOT_VALID;
			goto exit;
		}

		if (install_type == INSTALL_TYPE_UPDATE) {
			update_task = task_find_task_by_uuid(&taskp->props.uuid);
			if (update_task) {
				if (update_task->props.initial_state & OTE_MANIFEST_TASK_ISTATE_STICKY) {
					task_print_id(INFO, "Sticky task can not be updated ", update_task);
					err = ERR_ALREADY_EXISTS;
					goto exit;
				}

				task_print_id(INFO, "preparing to update ", update_task);

				/* Block existing task, no new connections allowed to it */
				update_task_state = update_task->task_state;
				update_task->task_state = TASK_STATE_BLOCKED;

				/* save UUID in case we need to restore task */
				memcpy(&update_task_uuid, &update_task->props.uuid,
				       sizeof(update_task->props.uuid));

				/* Clear existing task UUID to allow task updating */
				memset(&update_task->props.uuid, 0, sizeof(update_task->props.uuid));
			}
		}

		/*
		 * Atomically commit task (and assign the index) to the known task_list
		 * unless there is an error. TASKP value swapped to track the registered
		 * task header (old object is cleared in the call).
		 *
		 * tp->tp_task is cleared by task_register.
		 */
		err = task_register(&taskp);

		if (err != NO_ERROR) {
			dprintf(CRITICAL, "%s: loaded task %d registration failed (%d)\n",
				__func__, task_get_count(), err);
			goto exit;
		}

		task_registered++;

		arch_clean_cache_range((addr_t)tp->tp_task_tlk_addr, tp->tp_task_size);

		/*
		 * Init task and start it
		 */
		err = task_init_one_task(taskp);
		if (err != NO_ERROR) {
			status_t err2 = NO_ERROR;
			uint32_t tindex = taskp->task_index;

			dprintf(CRITICAL, "Loaded task#%u init failed (0x%x), unloading...\n",
				tindex, err);

			err2 = task_unload(&taskp);
			if (err2 != NO_ERROR) {
				dprintf(CRITICAL, "Unloading the failed task#%u failed: (0x%x)\n",
					tindex, err2);
			}
			goto exit;
		}

		task_load_counter++;
	}

	/*
	 * Already removed from pending task list and now junked
	 * because task is now active.
	 *
	 * Just for completeness, advance the state machine before killing object.
	 */
	tp->tp_state = TASK_PSTATE_READY;
	free(tp);
	tp = NULL;

	if (0) {
	exit:
		if (update_task) {
			/* The updated task must not exist in task list (UUID was cleared) */
			ASSERT(task_find_task_by_uuid(&update_task_uuid) == NULL);

			/* repair original task, loading replacement task failed */
			memcpy(&update_task->props.uuid, &update_task_uuid, sizeof(update_task->props.uuid));
			update_task->task_state = update_task_state;
			update_task = NULL;
		}

		if (tp) {
			if (!task_registered)
				task_dealloc_app_load_memory_tp(tp);

			free(tp);
		}

		if (err == NO_ERROR)
			err = ERR_GENERIC;
	}

	if (update_task) {
		err = task_unload(&update_task);
		if (err != NO_ERROR) {
			task_print_id(INFO, "replaced task unloading failed ", update_task);
		}
	}

	return err;
}

/**
 * @brief Change name of the task
 */
void task_set_name(task_t *task, const char *name)
{
	if (task) {
		if (!name || !name[0])
			task->task_name[0] = '\000';
		else
			strlcpy(task->task_name, name, sizeof(task->task_name));
	}
}

#ifdef DEBUG

/* reference to end of the binary and end of heap */
extern int _end;
extern int _heap_end;

/* debug: print system info to console.
 *
 * For R&D only, should disable from production builds.
 */
void task_system_info(te_system_info_args_t *args)
{
	if (args->si_type == 41) {
		dprintf(INFO, "Kernel thread dump:\n");
		dump_all_threads();
	} else {

#define HEAP_LEN ((u_int)_heap_end - (u_int)&_end)

		dprintf(INFO, "HEAP start (orig): 0x%x\n", (u_int)original_heap_start);
		dprintf(INFO, "HEAP end   (orig): 0x%x\n", (u_int)original_heap_end);

		dprintf(INFO, "HEAP start (end of binary == &_end) %p\n", &_end);
		dprintf(INFO, "HEAP end (end_of_memory == _heap_end) %p\n",
			(void *)_heap_end);
		dprintf(INFO, "HEAP size (_heap_end-&_end)=%u (0x%x) bytes\n",
			HEAP_LEN, HEAP_LEN);

		dprintf(INFO, "Number of active tasks: %d\n",
			task_get_active_count());

		dprintf(INFO, "sizeof(task_t): %d bytes\n", sizeof(task_t));

		dprintf(INFO, "Heap pages used for tasks: %d\n", task_pages_heap_used);

		dprintf(INFO, "Heap pages for tasks: left %d (out of %d)\n",
			task_pages_heap_max - task_pages_heap_used,
			task_pages_heap_max);

		dprintf(INFO, "Tasks loaded since boot: %u\n", task_load_counter);
		dprintf(INFO, "Tasks unloaded since boot: %u\n", task_get_unload_counter());
	}
}
#endif /* DEBUG */

/* handlers for task_request_handler() */

static int tlk_handle_app_memory_request(te_app_load_memory_request_args_t *args)
{
	vaddr_t     ta_vaddr	= NULL;
	uint32_t    handle	= 0;
	task_t     *taskp	= current_thread->arch.task;

	if (!args)
		return -EINVAL;

	dprintf(SPEW, "%s: mem request app size %d bytes\n",
		__func__, args->app_size);

	task_print_id(INFO, "Map task memory request by ", taskp);

	/*
	 * Simple manifest based permission allowing a task to install
	 * other tasks.
	 */
	if (!task_allowed_to_load_tasks(taskp)) {
		task_print_id(CRITICAL, "Client tried to perform "
			      "an operation for which they are "
			      "not permitted ", taskp);
		return -EACCES;
	}

	{
		status_t err = NO_ERROR;

		err = task_request_app_load_memory(args->app_size, &handle);
		if (err != NO_ERROR)  {
			dprintf(CRITICAL, "Failed to allocate app memory (%d bytes, err %d)\n",
				args->app_size, err);
			return ((err == ERR_NO_MEMORY) ? -ENOMEM : -EINVAL);
		}

		dprintf(SPEW, "Handle for app loading 0x%x, %d bytes\n",
			handle, args->app_size);

		err = task_map_memory_to_ta(taskp, handle, &ta_vaddr);
		if (err != NO_ERROR || !ta_vaddr) {
			status_t err2 = task_dealloc_app_load_memory(handle);
			if (err2 != NO_ERROR)  {
				dprintf(CRITICAL, "Failed to dealloc app handle 0x%x, %d bytes, err %d\n",
					handle, args->app_size, err2);
			}
			return -ENOMEM;
		}
	}

	/* pass shared buffer TA vaddr back to the TA */
	args->app_addr   = ta_vaddr;
	/* handle to refer to the pending task load */
	args->app_handle = handle;

	/*
	 * app_size field is unmodified; still contains the requested
	 * mem size but the actual shared buffer size is rounded
	 * to next page boundary.
	 */
	return 0;
}

static void map_task_info_to_te(te_task_info_t *to, task_info_t *from)
{
	if (to)
		memset(to, 0, sizeof(te_task_info_t));

	if (from && to) {
		memcpy(&to->uuid, &from->uuid, sizeof(from->uuid));

#define TASK_COPY_PROPERTY(out,in,field) (out)->field = (in)->field

		TASK_COPY_PROPERTY(to,from,manifest_exists);
		TASK_COPY_PROPERTY(to,from,multi_instance);
		TASK_COPY_PROPERTY(to,from,min_stack_size);
		TASK_COPY_PROPERTY(to,from,min_heap_size);
		TASK_COPY_PROPERTY(to,from,map_io_mem_cnt);
		TASK_COPY_PROPERTY(to,from,restrict_access);
		TASK_COPY_PROPERTY(to,from,authorizations);
		TASK_COPY_PROPERTY(to,from,initial_state);

#undef TASK_COPY_PROPERTY

		memcpy(to->task_name, from->task_name, OTE_TASK_NAME_MAX_LENGTH);
		memcpy(to->task_private_data,
		       from->task_private_data, OTE_TASK_PRIVATE_DATA_LENGTH);
	}
}

static int tlk_handle_app_prepare(te_app_prepare_args_t *args)
{
	status_t err = NO_ERROR;
	task_t *taskp = current_thread->arch.task;
	task_info_t ti;

	if (!args)
		return -EINVAL;

	dprintf(INFO, "Parsing app handle 0x%x\n", args->app_handle);

	if (!task_allowed_to_load_tasks(taskp)) {
		task_print_id(CRITICAL, "Client tried to perform "
			      "an operation for which they are "
			      "not permitted ", taskp);
		return -EACCES;
	}

	/*
	 * Remove the shared memory mapping from installer
	 * memory map. After this only TLK will see the application.
	 */
	err = task_unmap_app_mem_from_ta(taskp, args->app_handle);
	if (err != NO_ERROR) {
		dprintf(INFO, "Could not unmap TA shared memory (%d)\n", err);
		task_dealloc_app_load_memory(args->app_handle);
		return -EINVAL;
	}

	/*
	 * Parse application
	 */
	memset(&ti, 0, sizeof(ti));
	err = task_parse_app(args->app_handle, &ti);
	if (err != NO_ERROR) {
		dprintf(INFO, "Could parse loaded application (%d)\n", err);
		task_dealloc_app_load_memory(args->app_handle);
		return -EINVAL;
	}
	map_task_info_to_te(&args->te_task_info, &ti);

	return 0;
}

static int get_manifest_mapping(task_t *task, te_memory_mapping_t *map)
{
	u_int i = 0;
	u_int found	= 0;
	u_int map_index	= 0;

	if (! task || ! map)
		return -EINVAL;

	for (i = 0; i < task->props.config_entry_cnt; i++) {
		if (task->props.config_blob[i] == OTE_CONFIG_KEY_MAP_MEM) {
			if (map_index == map->map_index) {

				/* found the specified mapping by index */
				map->map_id	= task->props.config_blob[++i];
				map->map_offset	= task->props.config_blob[++i];
				map->map_size	= task->props.config_blob[++i];

				found++;
				break;
			} else {
				/* wrong mapping, skip the 3 fields */
				i += 3;
			}

			map_index++;
		} else {
			/* all other config options take 1 data value */
			i++;
		}
	}

	if (!found)
		return -ESRCH;

	return 0;
}

/*
 * Add an API to fetch the manifest mappings from a pending task using its handle
 * (i.e. when task is being loaded).
 *
 * This is an installer only API to enable decisions if the task
 * needs to be rejected due to unauthorized memory mappings. The caller must
 * reject the task on error, this routine does not do it.
 */
static int tlk_get_pending_mapping(te_get_pending_map_args_t *args)
{
	int rval = 0;
	task_t *taskp = current_thread->arch.task;
	task_pending_t *tp = NULL;

	if (!args)
		return -EINVAL;

	if (!task_allowed_to_load_tasks(taskp)) {
		task_print_id(CRITICAL, "Client tried to perform "
			      "an operation for which they are "
			      "not permitted ", taskp);
		rval = -EACCES;
		goto exit;
	}

	tp = task_find_pending(args->pm_handle);
	if (!tp) {
		dprintf(SPEW, "%s: task handle unknown 0x%x\n",
			__func__, args->pm_handle);
		rval = -ENOENT;
		goto exit;
	}

	/* Do not reject task when fetching a non-existent mapping.
	 * Just return a not found error but allow install to proceed or
	 * installer to reject the task.
	 */
	if (tp->tp_state != TASK_PSTATE_PREPARED) {
		dprintf(INFO, "%s: task handle 0x%x in wrong state: %d\n",
			__func__, args->pm_handle, tp->tp_state);
		rval = -EINVAL;
	} else if (tp->tp_task.props.map_io_mem_cnt < (args->pm_map.map_index + 1)) {
		dprintf(INFO, "Task manifest does not contain %d memory mappings",
			args->pm_map.map_index + 1);
		rval = -ESRCH;
	} else {
		if (get_manifest_mapping(&tp->tp_task, &args->pm_map)) {
			dprintf(INFO, "Task manifest does not contain mapping at index %d",
				args->pm_map.map_index);
			rval = -ESRCH;
		}
	}

	task_push_pending(tp, tp->tp_state);

exit:
	return rval;
}

/*
 * Copy selected non-security related fields that the installer wants
 * to enforce to the task when it starts. Zero field values are not-used.
 *
 * This is just a type mapping, properties are set later in task_property_override.
 */
static void map_task_app_restrictions(task_restrictions_t *to,
				      te_task_restrictions_t *from)
{
	if (to)
		memset(to, 0, sizeof(task_restrictions_t));

	if (from && to) {

		to->min_stack_size = from->min_stack_size;
		to->min_heap_size = from->min_heap_size;

		memcpy(to->task_name, from->task_name, OTE_TASK_NAME_MAX_LENGTH);
	}
}

static int tlk_handle_app_start(te_app_start_args_t *args)
{
	status_t err = NO_ERROR;
	task_t *taskp = current_thread->arch.task;
	task_restrictions_t task_restrictions;
	const char *op_info = "start";

	if (!args)
		return -EINVAL;

	if (!task_allowed_to_load_tasks(taskp)) {
		task_print_id(CRITICAL, "Client tried to perform "
			      "an operation for which they are "
			      "not permitted ", taskp);
		return -EACCES;
	}

	switch (args->app_install_type) {
	case INSTALL_TYPE_REJECT:
		op_info = "reject";
		break;
	case INSTALL_TYPE_UPDATE:
		op_info = "update";
		break;
	case INSTALL_TYPE_NORMAL:
		op_info = "install";
		break;
	default:
		return -EINVAL;
	}

	map_task_app_restrictions(&task_restrictions, &args->app_restrictions);

	dprintf(INFO, "%s app with handle 0x%x\n", op_info, args->app_handle);

	err = task_run_app(args->app_handle, args->app_install_type, &task_restrictions);
	if (err != NO_ERROR) {
		/* Do not log when rejecting a task that no longer exists */
		if ((args->app_install_type != INSTALL_TYPE_REJECT) || (err != ERR_NOT_FOUND))
			dprintf(CRITICAL, "Could not %s loaded application (%d)\n",
				op_info, err);
		return -EINVAL;
	}

	return 0;
}

/*
 * A bit strict: only authorized apps can list which tasks are installed
 * in the system. However, this is a privileged operation also in GP specs.
 *
 * Copy the UUID and TASK_NAME of the indicated task to the arg buf.
 * If the task has no defined name the app_name field is zeroed.
 */
static int tlk_list_apps(te_app_list_args_t *args)
{
	task_t *taskp	= current_thread->arch.task;
	task_t *task	= NULL;
	task_info_t ti;

	if (!args)
		return -EINVAL;

	if (!task_allowed_to_load_tasks(taskp)) {
		task_print_id(CRITICAL, "Client tried to perform "
			      "an operation for which they are "
			      "not permitted ", taskp);
		return -EACCES;
	}

	args->app_type = TASK_TYPE_UNKNOWN;
	args->app_state = TASK_STATE_UNKNOWN;
	memset(&args->app_info, 0, sizeof(args->app_info));

	/* task at this or any larger index does not exist */
	if (args->app_index > task_get_count())
		return -ENOENT;

	task = task_find_task_by_index(args->app_index);
	if (!task) {
		/* task at index does not exist */
		return -ESRCH;
	}

	/*
	 * Otherwise set up the UUID and other info of the specified task to the
	 * IOCTL result.
	 */
	args->app_type = task->task_type;
	args->app_state = task->task_state;

	task_get_config(&ti, task);
	map_task_info_to_te(&args->app_info, &ti);

	return 0;
}

static int tlk_task_get_info(te_get_task_info_t *args)
{
	task_t *taskp	= current_thread->arch.task;
	task_t *task	= NULL;
	task_info_t ti;

	if (!args)
		return -EINVAL;

	/* Any task is authorized to fetch it's own manifest information (SELF).
	 * Installers are authorized to fetch it from any task.
	 */
	if (!task_allowed_to_load_tasks(taskp) &&
	    (args->gti_request_type != OTE_GET_TASK_INFO_REQUEST_SELF)) {
		task_print_id(CRITICAL, "Client tried to perform "
			      "an operation for which they are "
			      "not permitted ", taskp);
		return -EACCES;
	}

	switch (args->gti_request_type) {
	case OTE_GET_TASK_INFO_REQUEST_INDEX:
		/* task at this or any larger index does not exist */
		if (args->gtiu_index > task_get_count())
			return -ENOENT;

		task = task_find_task_by_index(args->gtiu_index);
		break;
	case OTE_GET_TASK_INFO_REQUEST_UUID:
		task = task_find_task_by_uuid(&args->gtiu_uuid);
		break;
	case OTE_GET_TASK_INFO_REQUEST_SELF:
		task = taskp;
		break;
	default:
		task = NULL;
		break;
	}

	if (!task)
		return -ENOENT;

	memset(&ti, 0, sizeof(ti));
	task_get_config(&ti, task);

	args->gti_state = task->task_state;
	map_task_info_to_te(&args->gti_info, &ti);

	return 0;
}

static int tlk_task_get_mapping(te_get_task_mapping_t *args)
{
	task_t *taskp	= current_thread->arch.task;
	task_t *task	= NULL;

	if (!args)
		return -EINVAL;

	/* Any task is authorized to fetch it's own mapping information (SELF).
	 * Installers are authorized to fetch it from any task.
	 */
	if (!task_allowed_to_load_tasks(taskp) &&
	    (args->gmt_request_type != OTE_GET_TASK_INFO_REQUEST_SELF)) {
		task_print_id(CRITICAL, "Client tried to perform "
			      "an operation for which they are "
			      "not permitted ", taskp);
		return -EACCES;
	}

	switch (args->gmt_request_type) {
	case OTE_GET_TASK_INFO_REQUEST_INDEX:
		/* task at this or any larger index does not exist */
		if (args->gmtu_index > task_get_count())
			return -ENOENT;

		task = task_find_task_by_index(args->gmtu_index);
		break;
	case OTE_GET_TASK_INFO_REQUEST_UUID:
		task = task_find_task_by_uuid(&args->gmtu_uuid);
		break;
	case OTE_GET_TASK_INFO_REQUEST_SELF:
		task = taskp;
		break;
	default:
		task = NULL;
		break;
	}

	if (!task)
		return -ENOENT;

	return get_manifest_mapping(task, &args->gmt_map);
}

static int tlk_task_unload(te_app_unload_args_t *args)
{
	status_t err  = NO_ERROR;
	task_t *taskp = current_thread->arch.task;
	task_t *target_task = NULL;

	if (!args)
		return -EINVAL;

	if (!task_allowed_to_load_tasks(taskp)) {
		task_print_id(CRITICAL, "Client tried to perform "
			      "an operation for which they are "
			      "not permitted ", taskp);
		return -EACCES;
	}

	switch (args->tid_type) {
	case OTE_APP_ID_INDEX:
		/* task at this or any larger index does not exist */
		if (args->tid_index > task_get_count())
			return -ENOENT;

		target_task = task_find_task_by_index(args->tid_index);
		break;
	case OTE_APP_ID_UUID:
		target_task = task_find_task_by_uuid(&args->tid_uuid);
		break;
	default:
		return -EINVAL;
	}

	if (!target_task)
		return -ESRCH;

	task_print_id(INFO, "Request to unload task ", target_task);

	err = task_unload(&target_task);
	if (err != NO_ERROR) {
		switch (err) {
		case ERR_NOT_FOUND:   return -ENOENT;
		case ERR_NOT_ALLOWED: return -EACCES;
		default: return -EINVAL;
		}
	}

	return 0;
}

/*
 * Block an active task so it does not accept new sessions.
 *
 * Note that after blocking the last task which has installer privileges
 * you can no longer unblock any blocked tasks in the system.
 */
static int tlk_task_block(te_app_block_args_t *args)
{
	task_t *taskp = current_thread->arch.task;
	task_t *target_task  = NULL;

	if (!args)
		return -EINVAL;

	if (!task_allowed_to_load_tasks(taskp)) {
		task_print_id(CRITICAL, "Client tried to perform "
			      "an operation for which they are "
			      "not permitted ", taskp);
		return -EACCES;
	}

	switch (args->tid_type) {
	case OTE_APP_ID_INDEX:
		/* task at this or any larger index does not exist */
		if (args->tid_index > task_get_count())
			return -ENOENT;

		target_task = task_find_task_by_index(args->tid_index);
		break;
	case OTE_APP_ID_UUID:
		target_task = task_find_task_by_uuid(&args->tid_uuid);
		break;
	default:
		return -EINVAL;
	}

	if (!target_task)
		return -ESRCH;

	switch(target_task->task_state) {
	case TASK_STATE_BLOCKED:
	case TASK_STATE_ACTIVE:
		target_task->task_state = TASK_STATE_BLOCKED;
		task_print_id(INFO, "Blocked task ", target_task);
		break;
	default:
		dprintf(INFO, "Request to block task#%d in state %d -- rejected\n",
			target_task->task_index,
			target_task->task_state);
		return -EINVAL;
	}

	return 0;
}

static int tlk_task_unblock(te_app_block_args_t *args)
{
	task_t *taskp = current_thread->arch.task;
	task_t *target_task  = NULL;

	if (!args)
		return -EINVAL;

	if (!task_allowed_to_load_tasks(taskp)) {
		task_print_id(CRITICAL, "Client tried to perform "
			      "an operation for which they are "
			      "not permitted ", taskp);
		return -EACCES;
	}

	switch (args->tid_type) {
	case OTE_APP_ID_INDEX:
		/* task at this or any larger index does not exist */
		if (args->tid_index > task_get_count())
			return -ENOENT;

		target_task = task_find_task_by_index(args->tid_index);
		break;
	case OTE_APP_ID_UUID:
		target_task = task_find_task_by_uuid(&args->tid_uuid);
		break;
	default:
		return -EINVAL;
	}

	if (!target_task)
		return -ESRCH;

	switch(target_task->task_state) {
	case TASK_STATE_BLOCKED:
		target_task->task_state = TASK_STATE_ACTIVE;
		task_print_id(INFO, "Unblocked task ", target_task);
		break;
	default:
		dprintf(INFO, "Request to unblock task#%d in state %d -- rejected\n",
			target_task->task_index,
			target_task->task_state);
		return -EINVAL;
	}

	return 0;
}

/*
 * Called by syscall OTE_IOCTL_TASK_REQUEST ioctl handler
 */
int task_request_handler(te_task_request_args_t *args)
{
	int retval = 0;

	if (!args) {
		retval = -EINVAL;
		goto exit;
	}

	switch (args->ia_opcode) {
	case OTE_TASK_OP_MEMORY_REQUEST:
		retval = tlk_handle_app_memory_request(&args->ia_load_memory_request);
		break;

	case OTE_TASK_OP_PREPARE:
		retval = tlk_handle_app_prepare(&args->ia_prepare);
		break;

	case OTE_TASK_OP_PENDING_MAPPING:
		retval = tlk_get_pending_mapping(&args->ia_pending_mapping);
		break;

	case OTE_TASK_OP_START:
		retval = tlk_handle_app_start(&args->ia_start);
		break;

	case OTE_TASK_OP_LIST:
		retval = tlk_list_apps(&args->ia_list);
		break;

	case OTE_TASK_OP_GET_TASK_INFO:
		retval = tlk_task_get_info(&args->ia_get_task_info);
		break;

	case OTE_TASK_OP_GET_MAPPING:
		/* much like pending mapping above, but for existing tasks */
		retval = tlk_task_get_mapping(&args->ia_get_task_mapping);
		break;

#ifdef DEBUG
	case OTE_TASK_OP_SYSTEM_INFO:
		task_system_info(&args->ia_system_info);
		break;
#endif

	case OTE_TASK_OP_UNLOAD:
		retval = tlk_task_unload(&args->ia_app_unload);
		break;

	case OTE_TASK_OP_BLOCK:
		retval = tlk_task_block(&args->ia_app_block);
		break;

	case OTE_TASK_OP_UNBLOCK:
		retval = tlk_task_unblock(&args->ia_app_block);
		break;

	default:
		retval = -EINVAL;
		break;
	}

exit:
	return retval;
}
