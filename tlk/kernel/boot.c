/*
 * Copyright (c) 2012-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <debug.h>
#include <kernel/boot_params.h>
#include <platform/platform_p.h>
#include <common/ote_nv_uuid.h>

unsigned int coldboot_normal_os;
unsigned int normal_os_coldboot_fn;
extern uint32_t debug_uart_id;
unsigned int tf_key_addr;
unsigned int tf_key_size;
uint32_t device_uid[DEVICE_UID_SIZE_WORDS];
paddr_t tsec_carveout_size, tsec_carveout_base;
paddr_t dtb_addr;

extern unsigned int __bootarg_addr;

struct cmdline_member {
	char name[PARAM_NAME_LEN];
	unsigned int *container;
};

static int getoffsetof(char *source, char c)
{
	int offset = 0;
	int len;

	if (!source)
		return 0;

	len = strlen(source);

	while (*source++ != c && len-- > 0)
		offset++;

	return offset;
}

void get_boot_params(void)
{
	boot_params_t *boot_params_ptr = (boot_params_t*)__bootarg_addr;
	uint64_t base, size;

	if (!boot_params_ptr)
		return;

	device_uid[0] = boot_params_ptr->chip_uid[0];
	device_uid[1] = boot_params_ptr->chip_uid[1];
	device_uid[2] = boot_params_ptr->chip_uid[2];
	device_uid[3] = boot_params_ptr->chip_uid[3];

	debug_uart_id = boot_params_ptr->uart_id;

	/* Get DRAM range from the structure passed */
	base = boot_params_ptr->pmem;
	size = boot_params_ptr->pmem_size;
	tz_add_dram_range(base << 20, size << 20);

	if (boot_params_ptr->emem_size != 0) {
		base = boot_params_ptr->emem;
		size = boot_params_ptr->emem_size;
		tz_add_dram_range(base << 20, size << 20);
	}

	dtb_addr = boot_params_ptr->dtb_load_addr;

	/* Get TSEC range from the structure passed */
	tsec_carveout_base = boot_params_ptr->tsec_carveout;
	tsec_carveout_size = boot_params_ptr->tsec_size;
	tsec_carveout_size <<= 20;
	tsec_carveout_base <<= 20;

}

int get_boot_args(task_t *task, uint32_t *argv)
{
	int i = 0;
	te_service_id_t hdcp_uuid = SERVICE_SECURE_HDCP_UUID;

	/* pass input params to hdcp service via argv */
	if (!memcmp(&hdcp_uuid, &task->props.uuid, sizeof(te_service_id_t))) {
		argv[i++] = 0x0;		/* argv[0] = progname (NULL) */
		argv[i++] = tsec_carveout_base;	/* argv[1] */
		argv[i++] = tsec_carveout_size;	/* argv[2] */
	}

	return i;
}
