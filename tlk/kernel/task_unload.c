/*
 * Copyright (c) 2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <debug.h>
#include <sys/types.h>
#include <assert.h>
#include <string.h>
#include <err.h>
#include <malloc.h>
#include <arch/arm/mmu.h>
#include <kernel/task.h>
#include <kernel/thread.h>
#include <kernel/task_load.h>
#include <kernel/event.h>
#include <ote_intf.h>

static event_t task_reaper_event;
static uint32_t task_unload_counter;

/*! Release resources of a terminated task.
 *
 * Called by the task_reaper thread.
 */
static void task_free_resources(task_t **taskp_p)
{
	if (taskp_p && *taskp_p) {
		task_map_t *mptr = NULL;
		task_map_t *mptr_tmp = NULL;
		task_t *taskp = *taskp_p;

		ASSERT(taskp->task_state == TASK_STATE_TERMINATED);
		ASSERT(taskp->elf_hdr);
		ASSERT(taskp->task_size > 0);
		ASSERT(list_is_empty(&taskp->thread_node));
		ASSERT(list_in_list(&taskp->node));

		memset(&taskp->props.uuid, 0, sizeof(te_service_id_t));

		list_for_every_entry_safe(&taskp->map_list, mptr, mptr_tmp, task_map_t, node) {
			arch_task_unmap_memory(taskp, mptr);
		}

		/* deallocate the task page tables */
		arm_mmu_dealloc_upagetable(taskp);

		/* clear task image in tlk heap before deallocating it */
		memset(taskp->elf_hdr, 0, taskp->task_size);

		if (taskp->task_type == TASK_TYPE_LOADED) {
			/* dealloc task image memory, if allocated from TLK heap */
			task_dealloc_memory((vaddr_t)taskp->elf_hdr, taskp->task_size);
		}

		enter_critical_section();
		list_delete(&taskp->node);
		exit_critical_section();

		arch_task_killed(taskp);

		/* Clear task header in task_list */
		memset(taskp, 0, sizeof(task_t));

		free(taskp);
		*taskp_p = NULL;

		task_unload_counter++;
	}
}

/*! A reaper thread that gets an event up every time a kernel thread associated
 * with a task gets killed.
 *
 * This scans the task list for any tasks that has no threads left AND
 * is flagged as terminated. This means tasks started without threads or tasks committing
 * suicide will not get reaped, they need to be explicitely unloaded.
 *
 * If it finds such a zombie task it will release its resources.
 */
static int task_reaper(void *arg)
{
	(void)arg;

	while(1) {
		uint32_t index = 0;
		task_t *taskp = NULL;
		u_int task_next_index = 0;

		event_wait(&task_reaper_event);

		enter_critical_section();

		task_next_index = task_get_count();

		/* Scan task list for terminated tasks that have no threads left.
		 *
		 * Scan the list every time from the beginning to find all terminated
		 * tasks since last scan.
		 */
		while(1) {
			if (index >= task_next_index) {
				taskp = NULL;
				break;
			}

			taskp = task_find_task_by_index(index++);
			if (!taskp)
				continue;

			if ((taskp->task_state == TASK_STATE_TERMINATED) &&
			    (list_is_empty(&taskp->thread_node))) {
				/*
				 * taskp has no threads left and in terminated
				 * state => release it...
				 */
				break;
			}
		}

		exit_critical_section();

		if (taskp) {
			/* and then free the resources */
			task_free_resources(&taskp);

			/* Do not unsignal event when dead task found,
			 * instead scan for more.
			 */
			continue;
		}

		event_unsignal(&task_reaper_event);
	}

	return 0;
}

void task_unload_init(void)
{
	event_init(&task_reaper_event, false, 0);

	(void)thread_resume(thread_create("task reaper", &task_reaper,
					  NULL, DEFAULT_PRIORITY,
					  DEFAULT_STACK_SIZE));

	dprintf(SPEW, "task reaper started\n");
}

/*! Remove a dead thread from the thread list of a task.
 *
 * Called by thread cleanup before the thread resources
 * are deallocated when the thread belongs to a task.
 */
void task_thread_killed(thread_t *thread)
{
	task_t *taskp = NULL;

	if (!thread)
		return;

	ASSERT(thread->state == THREAD_DEATH);
	ASSERT(thread->arch.task);
	ASSERT(list_in_list(&thread->task_node));

	arch_task_thread_killed(thread);

	/* remove the thread from TE session lists */
	te_session_cancel_thread(thread);

	taskp = thread->arch.task;

	/* remove dead thread from task's thread list */
	enter_critical_section();
	list_delete(&thread->task_node);
	exit_critical_section();

	/* Was this the last thread of a task?
	 * If so, flag the task terminated and wakeup the Grim Reaper.
	 */
	if (list_is_empty(&taskp->thread_node)) {
		taskp->task_state = TASK_STATE_TERMINATED;
		memset(&taskp->props.uuid, 0, sizeof(te_service_id_t));
		event_signal(&task_reaper_event, false);
	}
}

/*! Initiate unloading of a task.
 *
 *  Called by task load syscall handler.
 *
 * Note that after unloading the last task which has installer privileges
 * you can no longer install anything to the system.
 */
status_t task_unload(task_t **taskp_p)
{
#if HAVE_UNLOAD_TASKS == 0
	dprintf(INFO, "task unloading not enabled\n");
	return ERR_NOT_ALLOWED;
#else
	status_t err = NO_ERROR;
	thread_t *thread = NULL;
	thread_t *th_tmp = NULL;
	int killed_thread_count = 0;
	const char *ch_state = NULL;
	char tp_name[OTE_TASK_NAME_MAX_LENGTH+3];
	task_t *taskp = NULL;

	if (!taskp_p || !*taskp_p) {
		err = ERR_INVALID_ARGS;
		goto exit;
	}
	taskp = *taskp_p;

	task_print_id(INFO, "task unload request ", taskp);

#if HAVE_UNLOAD_STATIC_TASKS == 0
	if (taskp->task_type != TASK_TYPE_LOADED) {
		dprintf(INFO, "Can only unload previously loaded tasks\n");
		err = ERR_NOT_ALLOWED;
		goto exit;
	}
#endif

	switch (taskp->task_state) {
	case TASK_STATE_BLOCKED:
		if (!ch_state)
			ch_state = "blocked";
		/* FALLTHROUGH */

	case TASK_STATE_ACTIVE:
		if (!ch_state)
			ch_state = "active";

		if (taskp->props.initial_state & OTE_MANIFEST_TASK_ISTATE_STICKY) {
			dprintf(INFO, "Sticky %s task#%u%s can not be unloaded\n",
				ch_state,
				taskp->task_index,
				task_get_name_str(taskp, " (", ")", tp_name, sizeof(tp_name)));
			err = ERR_NOT_ALLOWED;
			goto exit;
		}

		taskp->task_state = TASK_STATE_BLOCKED;
		/* FALLTHROUGH */

	case TASK_STATE_STARTING:
		/* Task starting, thread exist but not yet running; error case */
		if (!ch_state)
			ch_state = "STARTING";

		/* The loop kills all kernel threads of the task which will wakeup
		 * task reaper which will reclaim resources when the task dies.
		 */
		list_for_every_entry_safe(&taskp->thread_node, thread, th_tmp, thread_t, task_node) {
			dprintf(INFO, "killing %s task#%u%s thread#%u\n",
				ch_state, taskp->task_index,
				task_get_name_str(taskp, " (", ")", tp_name, sizeof(tp_name)),
				killed_thread_count);

			killed_thread_count++;
			thread_kill(thread);
		}

		/* All active tasks should have at least one thread, but
		 * if there are no threads release the task resources directly.
		 * (Task creation allows this when the EFL binary does not
		 * have an entry point).
		 */
		if (killed_thread_count == 0) {
			/* task had no associated threads so just release resources
			 */
			dprintf(INFO, "releasing resources of %s task#%u%s -- no threads\n",
				ch_state, taskp->task_index,
				task_get_name_str(taskp, " (", ")", tp_name, sizeof(tp_name)));

			taskp->task_state = TASK_STATE_TERMINATED;
			task_free_resources(&taskp);
		}
		break;

	case TASK_STATE_INIT:
		/* No threads have yet been created for a task in INIT state */
		if (!ch_state)
			ch_state = "INIT";
		/* FALLTHROUGH */

	case TASK_STATE_TERMINATED:
		/* all threads must be killed before task enters TERMINATED state */
		if (!ch_state)
			ch_state = "TERMINATED";

		ASSERT(list_is_empty(&taskp->thread_node));

		dprintf(INFO, "releasing resources of %s task#%u%s -- no threads\n",
			ch_state, taskp->task_index,
			task_get_name_str(taskp, " (", ")", tp_name, sizeof(tp_name)));

		taskp->task_state = TASK_STATE_TERMINATED;
		task_free_resources(&taskp);
		break;

	case TASK_STATE_UNKNOWN:
		if (!ch_state)
			ch_state = "UNKNOWN";
		/* FALLTHROUGH */

	default:
		if (!ch_state)
			ch_state = "(unknown)";

		dprintf(INFO, "Attempt to unload task#%u with obscure state %s(%u)?\n",
			taskp->task_index, ch_state, taskp->task_state);
		break;
	}

exit:
	if (taskp_p)
		*taskp_p = taskp;
	return err;
#endif /* HAVE_UNLOAD_TASKS */
}

uint32_t task_get_unload_counter()
{
	return task_unload_counter;
}
