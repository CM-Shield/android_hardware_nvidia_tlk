# can override this in local.mk
ENABLE_THUMB ?= true

ifeq ($(ARM_CPU),cortex-a15)
MODULE_COMPILEFLAGS += -mcpu=$(ARM_CPU)
endif
ifeq ($(ARM_CPU),cortex-a9)
MODULE_COMPILEFLAGS += -mcpu=$(ARM_CPU)
endif
ifeq ($(ARM_CPU),arm926ej-s)
MODULE_COMPILEFLAGS += -mcpu=$(ARM_CPU)
endif


THUMBCFLAGS :=
THUMBINTERWORK :=

ifeq ($(ENABLE_THUMB),true)
THUMBCFLAGS := -mthumb -D__thumb__
THUMBINTERWORK := -mthumb-interwork
endif

# set the default toolchain to arm elf and set a #define
TOOLCHAIN_PREFIX ?= arm-elf-

ifeq ($(TOOLCHAIN_PREFIX),arm-none-linux-gnueabi-)
# XXX test for EABI better than this
# eabi compilers dont need this
THUMBINTERWORK:=
endif

MODULE_COMPILEFLAGS += $(THUMBCFLAGS) $(THUMBINTERWORK)

THUMBCFLAGS :=
THUMBINTERWORK :=
