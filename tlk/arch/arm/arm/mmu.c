/*
 * Copyright (c) 2008-2009 Travis Geiselbrecht
 * Copyright (c) 2012-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <debug.h>
#include <assert.h>
#include <sys/types.h>
#include <err.h>
#include <compiler.h>
#include <malloc.h>
#include <string.h>
#include <arch.h>
#include <arch/arm.h>
#include <arch/arm/mmu.h>
#include <kernel/thread.h>
#include <kernel/task.h>
#include <platform.h>
#include <platform/platform_p.h>

#if ARM_WITH_MMU

void arm_mmu_init(void)
{
#if !defined(ARM_USE_MMU_RELOC)
	/* setup MMU, since wasn't done in early boot */
	arm_mmu_desc_config_mmu();
#else
	/* translation table setup already in start.S */
	platform_init_mmu_mappings();
#endif
}

void arch_disable_mmu(void)
{
	arm_write_sctlr(arm_read_sctlr() & ~(1<<0));
}

void arm_mmu_map_kpage(vaddr_t vaddr, paddr_t paddr, task_map_t *mptr)
{
	ASSERT(mptr->map_attrs);

	mptr->flags |= TM_KERN_VA;
	arm_mmu_desc_map_page(vaddr, paddr, NULL, mptr->flags, mptr->map_attrs);
}

void arm_mmu_map_upage(task_t *taskp, addr_t vaddr, paddr_t paddr,
		       task_map_t *mptr)
{
	ASSERT(mptr->map_attrs);
	arm_mmu_desc_map_page(vaddr, paddr, &taskp->page_table,
			      mptr->flags, mptr->map_attrs);
}

void arm_mmu_unmap_upage(task_t *taskp, addr_t vaddr)
{
	arm_mmu_desc_unmap_page(taskp->page_table, vaddr, taskp->context_id);
}

/* after all pages in a task have been unmapped call this to deallocate the
 * task page tables.
 */
void arm_mmu_dealloc_upagetable(task_t *taskp)
{
	arm_mmu_desc_dealloc_upgtbl(&taskp->page_table);
}

status_t arm_mmu_set_attrs_task_init(task_map_t *mptr)
{
	mptr->map_attrs = calloc(1, sizeof(arm_phys_attrs_t));
	if (mptr->map_attrs == NULL)
		return ERR_NO_MEMORY;
	arm_mmu_desc_set_default_task(mptr->map_attrs);
	return NO_ERROR;
}

status_t arm_mmu_set_attrs_from_mapping(nsaddr_t vaddr, uint32_t type,
					arm_phys_attrs_t *attrs)
{
	uint64_t par;

	enter_critical_section();
	par = platform_translate_nsaddr(vaddr, type);
	exit_critical_section();

	attrs->faulted = !!(par & PAR_ATTR_FAULTED);
	if (attrs->faulted) {
		dprintf(CRITICAL,
			"%s: vaddr 0x%016llx addr translation fault (par = 0x%016llx)\n",
			__func__, (nsaddr_t)vaddr, par);
		attrs->physaddr = INVALID_PHYSADDR;
		return ERR_NOT_VALID;
	}

	if (par & PAR_ATTR_LPAE) {
		attrs->physaddr = PAR_LDESC_ALIGNED_PA(par);
		attrs->inner = PAR_LDESC_ATTR_INNER(par);
		attrs->outer = PAR_LDESC_ATTR_OUTER(par);
		attrs->shareable = PAR_LDESC_ATTR_SHAREABLE(par);
		attrs->is_lpae = true;
	} else {
		attrs->physaddr = PAR_SDESC_ALIGNED_PA(par);
		attrs->inner = PAR_SDESC_ATTR_INNER(par);
		attrs->outer = PAR_SDESC_ATTR_OUTER(par);
		attrs->shareable = PAR_SDESC_ATTR_SHAREABLE(par);
		attrs->is_lpae = false;
	}

#if !ARM_WITH_LPAE
	/* without LPAE enabled, we can't map memory beyond 4GB */
	if (attrs->physaddr >> 32) {
		dprintf(CRITICAL,
			"%s: physaddr (0x%016llx) >= 4GB and LPAE is unsupported\n",
			__func__, attrs->physaddr);
		attrs->physaddr = INVALID_PHYSADDR;
		return ERR_NOT_SUPPORTED;
	}
#endif

	return NO_ERROR;
}

paddr_t arm_mmu_virt_to_phys(nsaddr_t vaddr, bool ns, bool priv)
{
	uint32_t type;
	arm_phys_attrs_t attrs;

	if (ns && priv)
		type = V2POWPR;		/* other world, priv */
	else if (ns && !priv)
		type = V2POWUR;		/* other world, user */
	else if (!ns && priv)
		type = V2PCWPR;		/* curr world, priv */
	else
		type = V2PCWUR;		/* curr world, user */

	if (arm_mmu_set_attrs_from_mapping(vaddr, type, &attrs) != NO_ERROR)
		return INVALID_PHYSADDR;

	/* vtop HW always returns PAGE_MASK aligned phys addrs */
	return (paddr_t)attrs.physaddr + (vaddr & PAGE_MASK);
}

static bool arm_mmu_compare_attrs(arm_phys_attrs_t *attr1, arm_phys_attrs_t *attr2)
{
	if ((attr1->shareable == attr2->shareable) ||
	    (attr1->inner == attr2->inner) || (attr1->outer == attr2->outer))
		return true;
	return false;
}

void arm_mmu_translate_range(nsaddr_t vaddr, paddr_t *pagelist, task_map_t *mptr)
{
	u_int type, pg;

	mptr->map_attrs = malloc(sizeof(arm_phys_attrs_t));
	if (mptr->map_attrs == NULL)
		return;

	if (mptr->flags & TM_KERN_SEC_VA)
		type = V2PCWPW;
	else if (mptr->flags & TM_SEC_VA)
		type = V2PCWUR;
	else if (mptr->flags & TM_NS_MEM_PRIV)
		type = V2POWPR;
	else
		type = V2POWUR;

	for (pg = 0; pg < (mptr->size / PAGE_SIZE); pg++, pagelist++) {
		arm_phys_attrs_t attr;
		status_t retval;

		retval = arm_mmu_set_attrs_from_mapping(vaddr, type, &attr);
		if (retval != NO_ERROR) {
			free(mptr->map_attrs);
			mptr->map_attrs = NULL;
			return;
		}

		/*
		 * Set flags on first page and check for attribute
		 * consistency on subsequent pages
		 */
		if (pg == 0) {
			*(arm_phys_attrs_t *)(mptr->map_attrs) = attr;
		} else if (!arm_mmu_compare_attrs(mptr->map_attrs, &attr)) {
			dprintf(CRITICAL, "%s: attribute inconsistency\n",
				__func__);
			attr.physaddr = INVALID_PHYSADDR;
		}
		*pagelist = attr.physaddr;
		vaddr += PAGE_SIZE;
	}
}

#endif // ARM_WITH_MMU
