/*
 * Copyright (c) 2008 Travis Geiselbrecht
 * Copyright (c) 2012, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __ARM_ARCH_THREAD_H
#define __ARM_ARCH_THREAD_H

/* offsets within thread_t */
#define THREAD_ARCH_KERN_STACK	0x34
#define THREAD_ARCH_TASK	0x38
#define THREAD_ARCH_FPCTX	0x3c

/* offset within fpctx_t */
#define FPCTX_FPEXC		0x0

#ifndef ASSEMBLY

static inline void arch_set_tls(u_int tp_value)
{
	__asm volatile("mcr p15, 0, %0, c13, c0, 3" :: "r" (tp_value));
}

#include <kernel/task.h>

struct arch_fpctx {
	uint32_t fpexc;
	uint64_t dregs[32];
	uint32_t fpscr;
	bool valid;
};
typedef struct arch_fpctx fpctx_t;

struct arch_thread {
	vaddr_t sp;
	task_t *task;
	fpctx_t *fpctx;
	uint32_t tp_value;
	bool initial;
};

extern fpctx_t *s_vfp_hw_context;
extern fpctx_t *ns_vfp_hw_context;

#endif
#endif

