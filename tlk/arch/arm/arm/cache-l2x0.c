/*
 * Copyright (c) 2012-2013, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <sys/types.h>
#include <arch/arm/cache-l2x0.h>
#include <arch/outercache.h>
#include <platform/memmap.h>
#include <platform.h>
#include <debug.h>

#if ARM_WITH_L2X0
#define SZ_1K	1024
#define CACHE_LINE_SIZE		32

struct outer_cache_fns outer_cache;

static uint32_t l2x0_base;
static uint32_t l2x0_way_mask;	/* Bitmask of active ways */
static uint32_t l2x0_size;
static uint32_t l2x0_cache_id;
static uint32_t l2x0_sets;
static uint32_t l2x0_ways;

static inline void write_cachel2x0_reg(unsigned int data, uint32_t reg)
{
	*(volatile uint32_t *)(l2x0_base + reg) = data;
}

static inline uint32_t read_cachel2x0_reg(uint32_t reg)
{
	return *(volatile uint32_t *)(l2x0_base + reg);
}

static inline void l2x0_cache_wait_way(uint32_t reg, unsigned long mask)
{
	/* wait for cache operation by line or way to complete */
	while (read_cachel2x0_reg(reg) & mask)
		__asm__ volatile ("nop");
}

static void l2x0_cache_sync(void)
{
	write_cachel2x0_reg(0, L2X0_CACHE_SYNC);
	/* no cache_wait: operations by line are atomic on PL310 */
}

static void l2x0_flush_all(void)
{
	write_cachel2x0_reg(l2x0_way_mask, L2X0_CLEAN_INV_WAY);
	l2x0_cache_wait_way(L2X0_CLEAN_INV_WAY, l2x0_way_mask);
	l2x0_cache_sync();
}

static inline void l2x0_flush_line(paddr_t addr)
{
	/* no cache_wait: operations by line are atomic on PL310 */
	write_cachel2x0_reg(addr, L2X0_CLEAN_INV_LINE_PA);
}

static void l2x0_flush_range(paddr_t start, size_t len)
{
	paddr_t end = start + len;

	if ((end - start) >= l2x0_size) {
		l2x0_flush_all();
		return;
	}

	start &= ~(CACHE_LINE_SIZE - 1);
	while (start < end) {
		l2x0_flush_line(start);
		start += CACHE_LINE_SIZE;
	}
	l2x0_cache_sync();
}

static void l2x0_clean_all(void)
{
	/* clean all ways */
	write_cachel2x0_reg(l2x0_way_mask, L2X0_CLEAN_WAY);
	l2x0_cache_wait_way(L2X0_CLEAN_WAY, l2x0_way_mask);
	l2x0_cache_sync();
}

static inline void l2x0_clean_line(paddr_t addr)
{
	/* no cache_wait: operations by line are atomic on PL310 */
	write_cachel2x0_reg(addr, L2X0_CLEAN_LINE_PA);
}

static void l2x0_clean_range(paddr_t start, size_t len)
{
	paddr_t end = start + len;

	if (len >= l2x0_size) {
		l2x0_clean_all();
		return;
	}

	start &= ~(CACHE_LINE_SIZE - 1);
	while (start < end) {
		l2x0_clean_line(start);
		start += CACHE_LINE_SIZE;
	}
	l2x0_cache_sync();
}

static void l2x0_inv_all(void)
{
	if (read_cachel2x0_reg(L2X0_CTRL) & 1)
		panic("invalidating when L2 is enabled is a nono\n");
	write_cachel2x0_reg(l2x0_way_mask, L2X0_INV_WAY);
	l2x0_cache_wait_way(L2X0_INV_WAY, l2x0_way_mask);
	l2x0_cache_sync();
}

static inline void l2x0_inv_line(paddr_t addr)
{
	/* no cache_wait: operations by line are atomic on PL310 */
	write_cachel2x0_reg(addr, L2X0_INV_LINE_PA);
}

static void l2x0_inv_range(paddr_t start, size_t len)
{
	paddr_t end = start + len;

	if (start & (CACHE_LINE_SIZE - 1)) {
		start &= ~(CACHE_LINE_SIZE - 1);
		l2x0_flush_line(start);
		start += CACHE_LINE_SIZE;
	}

	if (end & (CACHE_LINE_SIZE - 1)) {
		end &= ~(CACHE_LINE_SIZE - 1);
		l2x0_flush_line(end);
	}

	while (start < end) {
		l2x0_inv_line(start);
		start += CACHE_LINE_SIZE;
	}
	l2x0_cache_sync();
}

/* enables l2x0 after l2x0_disable, does not invalidate */
void l2x0_enable(void)
{
	write_cachel2x0_reg(1, L2X0_CTRL);
}

static void l2x0_disable(void)
{
	uint32_t enb = read_cachel2x0_reg(L2X0_CTRL);
	write_cachel2x0_reg(enb & ~1UL, L2X0_CTRL);
	__asm__ volatile ("dsb");
}

static void l2x0_unlock(uint32_t cache_id)
{
	int lockregs;
	int i;

	cache_id &= L2X0_CACHE_ID_PART_MASK;

	if (cache_id == L2X0_CACHE_ID_PART_L310)
		lockregs = 8;
	else
		/* L210 and unknown types */
		lockregs = 1;

	for (i = 0; i < lockregs; i++) {
		write_cachel2x0_reg(0x0, L2X0_LOCKDOWN_WAY_D_BASE +
			i * L2X0_LOCKDOWN_STRIDE);
		write_cachel2x0_reg(0x0, L2X0_LOCKDOWN_WAY_I_BASE +
			i * L2X0_LOCKDOWN_STRIDE);
	}
}

static void l2x0_init(void)
{
	if (!l2x0_base)
		panic("L2 cache base address undefined\n");

	/* if we're already enabled then we're done */
	if (read_cachel2x0_reg(L2X0_CTRL) & 1) {
		return;
	}

	l2x0_unlock(l2x0_cache_id);
	platform_init_outer();
	l2x0_inv_all();

	l2x0_enable();
}

void l2x0_setup(void)
{
	const char *type;
	uint32_t aux_ctrl;
	unsigned int waysize;

	l2x0_base = platform_l2x0_base();
	if (!l2x0_base)
		panic("L2 cache base address undefined\n");

	l2x0_cache_id = read_cachel2x0_reg(L2X0_CACHE_ID);

	aux_ctrl = read_cachel2x0_reg(L2X0_AUX_CTRL);
	waysize = ((aux_ctrl & L2X0_AUX_CTRL_WAY_SIZE_MASK) >> 17);
	waysize = SZ_1K << (waysize + 3);

	switch (l2x0_cache_id & L2X0_CACHE_ID_PART_MASK)
	{
		case L2X0_CACHE_ID_PART_L210:
			type = "L210";
			l2x0_ways = (aux_ctrl >> 13) & 0xf;
			break;
		case L2X0_CACHE_ID_PART_L220:
			type = "L220";
			l2x0_ways = (aux_ctrl >> 13) & 0xf;
			break;
		case L2X0_CACHE_ID_PART_L310:
			type = "L310";
			l2x0_ways = (aux_ctrl & (1 << 16)) ? 16 : 8;
			break;
		default:
			type = "Unknown";
			l2x0_ways = 0;
			break;
	}

	l2x0_way_mask = (1 << l2x0_ways) - 1;
	l2x0_size = l2x0_ways * waysize;
	l2x0_sets = waysize / CACHE_LINE_SIZE;

	outer_cache.init = l2x0_init;
	outer_cache.sync = l2x0_cache_sync;
	outer_cache.clean_all = l2x0_clean_all;
	outer_cache.clean_range = l2x0_clean_range;
	outer_cache.flush_all = l2x0_flush_all;
	outer_cache.flush_range = l2x0_flush_range;
	outer_cache.inv_all = l2x0_inv_all;
	outer_cache.inv_range = l2x0_inv_range;
	outer_cache.disable = l2x0_disable;
	outer_cache.enable = l2x0_enable;

	aux_ctrl = read_cachel2x0_reg(L2X0_AUX_CTRL);
	dprintf(SPEW, "%s cache controller enabled:\n", type);
	dprintf(SPEW, "  %d ways, CACHE_ID 0x%x, AUX_CTRL 0x%x size = %d B\n",
		l2x0_ways, l2x0_cache_id, aux_ctrl, l2x0_size);
}
#endif
