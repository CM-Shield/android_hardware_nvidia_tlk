LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR)
MODULE_COMPILEFLAGS += -mfpu=neon -mfloat-abi=softfp

INCLUDES += \
	-I$(LOCAL_DIR)/include \
	-I$(LOCAL_DIR)/$(SUBARCH)/include

# shared platform code
MODULE_SRCS += \
	$(LOCAL_DIR)/ops.S

include make/module.mk
