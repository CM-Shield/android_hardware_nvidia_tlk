/*
 * Copyright (c) 2013, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <debug.h>
#include <assert.h>
#include <sys/types.h>
#include <err.h>
#include <compiler.h>
#include <malloc.h>
#include <string.h>
#include <arch.h>
#include <arch/arm.h>
#include <arch/arm/mmu.h>
#include <arch/arm/mmu_sdesc.h>
#include <platform.h>

#if ARM_WITH_MMU

#define MB (1024*1024)

/* the location of the table may be brought in from outside */
#if WITH_EXTERNAL_TRANSLATION_TABLE
#if !defined(MMU_TRANSLATION_TABLE_ADDR)
#error must set MMU_TRANSLATION_TABLE_ADDR in the make configuration
#endif
uint32_t *tt = (void *)MMU_TRANSLATION_TABLE_ADDR;
#else
/* the main translation table */
uint32_t tt[4096] __ALIGNED(16384) __SECTION(".bss.prebss.translation_table");
#endif

typedef enum {
        NONE = 0,
        LEVEL_1_USER,
        LEVEL_1_PRIV,
        LEVEL_2,
} pgtbl_lvl_t;

vaddr_t arm_mmu_desc_get_max_user_space()
{
	return ((MMU_MEMORY_TTBR0_L1_SIZE / 4) * \
			((MMU_MEMORY_TTBR_L2_SIZE / 4) * PAGE_SIZE));
}

void arm_mmu_desc_set_default_kernel(arm_phys_attrs_t *attrs)
{
	attrs->inner = MMU_MEMORY_WRITE_BACK_ALLOCATE;
	attrs->outer = MMU_MEMORY_WRITE_BACK_NO_ALLOCATE;
	attrs->is_lpae = false;
}

void arm_mmu_desc_set_default_task(arm_phys_attrs_t *attrs)
{
	attrs->inner = MMU_MEMORY_WRITE_BACK_ALLOCATE;
	attrs->outer = MMU_MEMORY_WRITE_BACK_NO_ALLOCATE;
	attrs->is_lpae = false;
}

static u_int *arm_mmu_desc_alloc_pgtbl(pgtbl_lvl_t type)
{
	u_int size;
	u_int *pgtable = NULL;

	switch (type) {
	case LEVEL_1_USER:
		size = MMU_MEMORY_TTBR0_L1_SIZE;
		break;
	case LEVEL_1_PRIV:
		size = MMU_MEMORY_TTBR1_L1_SIZE;
		break;
	case LEVEL_2:
		size = MMU_MEMORY_TTBR_L2_SIZE;
		break;
	default:
		dprintf(CRITICAL, "unrecognized pgtbl_type %d\n", type);
		return pgtable;
	}

	pgtable = memalign(size, size);
	if (pgtable)
		memset(pgtable, 0, size);

	return pgtable;
}

static uint32_t arm_mmu_desc_l2_cache_attrs(arm_phys_attrs_t *attrs)
{
	uint32_t l2_attrs = 0;

	/* check if attrs are from a short or long desc */
	if (!attrs->is_lpae) {
		/* short desc -> short desc */
		if (attrs->shareable)
			l2_attrs |= MMU_MEMORY_L2_SHAREABLE;

		/* inner cacheable (cb) */
		l2_attrs |= MMU_MEMORY_SET_L2_INNER(attrs->inner);

		/* outer cacheable (tex) */
		l2_attrs |= (MMU_MEMORY_SET_L2_CACHEABLE_MEM |
			MMU_MEMORY_SET_L2_OUTER(attrs->outer));
	} else {
		/* long desc -> short desc */
		uint32_t cache_attr;

		if (attrs->shareable == 0x3)
			l2_attrs |= MMU_MEMORY_L2_SHAREABLE;

		/* inner cacheable (cb) */
		if ((attrs->inner & 0xC) == 0x1)
			cache_attr = MMU_MEMORY_NON_CACHEABLE;
		else if ((attrs->inner & 0xC) == 0x8)
			cache_attr = MMU_MEMORY_WRITE_THROUGH_NO_ALLOCATE;
		else if ((attrs->inner & 0xF) == 0xE)
			cache_attr = MMU_MEMORY_WRITE_BACK_NO_ALLOCATE;
		else
			cache_attr = MMU_MEMORY_WRITE_BACK_ALLOCATE;

		l2_attrs |= MMU_MEMORY_SET_L2_INNER(cache_attr);

		/* outer cacheable (tex) */
		if ((attrs->inner & 0xC) == 0x1)
			cache_attr = MMU_MEMORY_NON_CACHEABLE;
		else if ((attrs->inner & 0xC) == 0x8)
			cache_attr = MMU_MEMORY_WRITE_THROUGH_NO_ALLOCATE;
		else if ((attrs->inner & 0xF) == 0xE)
			cache_attr = MMU_MEMORY_WRITE_BACK_NO_ALLOCATE;
		else
			cache_attr = MMU_MEMORY_WRITE_BACK_ALLOCATE;

		l2_attrs |= (MMU_MEMORY_SET_L2_CACHEABLE_MEM |
				MMU_MEMORY_SET_L2_OUTER(cache_attr));
	}
	return l2_attrs;
}

void arm_mmu_desc_map_page(vaddr_t vaddr, paddr_t paddr, paddr_t *pgtbl,
			   tmflags_t flags, arm_phys_attrs_t *attrs)
{
	uint32_t *page_table;
	uint32_t *level_2;
	uint32_t idx, l1_flags, l2_flags;
	bool is_kmap;

	is_kmap = (flags & TM_KERN_VA);
	if (is_kmap) {
		page_table = tt;
	} else {
		/* task mapping */
		if (*pgtbl == NULL) {
			page_table = arm_mmu_desc_alloc_pgtbl(LEVEL_1_USER);
			if (page_table == NULL) {
				dprintf(CRITICAL,
					"unable to allocate LEVEL_1 page table\n");
				halt();
			}
			*pgtbl = virtual_to_physical((vaddr_t)page_table);
		} else {
			/* task page tables are kept as phys addrs */
			page_table = (uint *)physical_to_virtual(*pgtbl);
		}
	}

	ASSERT(page_table);

	/* setup level 1 flags */
	l1_flags = (flags & (TM_NS_MEM | TM_NS_MEM_PRIV)) ?
			MMU_MEMORY_L1_NON_SECURE : 0;

	idx = vaddr >> 20;

	level_2 = (u_int *)(page_table[idx] & ~(MMU_MEMORY_TTBR_L2_SIZE - 1));
	if (!level_2) {
		/* alloc level 2 page table */
		level_2 = arm_mmu_desc_alloc_pgtbl(LEVEL_2);
		if (level_2 == NULL) {
			dprintf(CRITICAL, "unable to allocate LEVEL_2 page table\n");
			halt();
		}

		/* install in level_1 */
		page_table[idx]  = (u_int)(virtual_to_physical((vaddr_t)level_2));
		page_table[idx] |= ((MMU_MEMORY_DOMAIN_MEM << 5) | l1_flags | 0x1);
	} else {
		/* convert from a section to page table */
		if ((page_table[idx] & 0x3) == 0x2) {
			ASSERT(is_kmap);
			level_2 = arm_mmu_desc_alloc_pgtbl(LEVEL_2);
			if (level_2 == NULL) {
				dprintf(CRITICAL,
					"unable to allocate LEVEL_2 page table\n");
				halt();
			}
			/* install in level_1 */
			page_table[idx]  = (u_int)(virtual_to_physical((vaddr_t)level_2));
			page_table[idx] |= ((MMU_MEMORY_DOMAIN_MEM << 5) | l1_flags | 0x1);
		} else {
			level_2 = (uint *)physical_to_virtual((paddr_t)level_2);
		}
	}

	idx = vaddr >> 12;
	idx &= MMU_MEMORY_TTBR_L2_INDEX_MASK;

	ASSERT(!level_2[idx]);

	/* setup level 2 flags */
	l2_flags  = (!is_kmap) ? MMU_MEMORY_L2_NON_GLOBAL : 0;
	l2_flags |= (flags & TM_UW) ?
			MMU_MEMORY_L2_AP_P_RW_U_RW : MMU_MEMORY_L2_AP_P_RW_U_RO;

	/* set cache attribs */
	if (flags & TM_IO)
		l2_flags |= MMU_MEMORY_L2_TYPE_STRONGLY_ORDERED;
	else
		l2_flags |= arm_mmu_desc_l2_cache_attrs(attrs);

	/* install level_2 4K entry */
	level_2[idx]  = paddr & ~PAGE_MASK;
	level_2[idx] |= (l2_flags | 0x2);
}

void arm_mmu_desc_unmap_page(paddr_t pgtbl, vaddr_t vaddr, uint32_t asid)
{
        u_int *page_table;
        u_int *level_2;
        u_int idx;

        ASSERT(pgtbl);
        page_table = (u_int *)physical_to_virtual(pgtbl);

        idx = vaddr >> 20;
        idx &= MMU_MEMORY_TTBR0_L1_INDEX_MASK;

        level_2 = (u_int *)(page_table[idx] & ~(MMU_MEMORY_TTBR_L2_SIZE - 1));
        ASSERT(level_2);
        level_2 = (u_int *)physical_to_virtual((paddr_t)level_2);

        idx = vaddr >> 12;
        idx &= MMU_MEMORY_TTBR_L2_INDEX_MASK;

        level_2[idx] = 0;       /* invalid entry */
        arm_invalidate_tlb_byaddr_asid(vaddr, asid);
}

void arm_mmu_desc_dealloc_upgtbl(paddr_t *pgtbl)
{
	u_int idx;
	u_int *page_table;
        u_int *level_2;

	if (pgtbl && *pgtbl) {

		/* L1 table virtual addr */
		page_table = (u_int *)physical_to_virtual(*pgtbl);

		/* free all LEVEL_2 tables referred to in LEVEL_1_USER table */
		for (idx = 0; idx <= MMU_MEMORY_TTBR0_L1_INDEX_MASK; idx++) {
			if (page_table[idx]) {
				level_2 = (u_int *)(page_table[idx] & ~(MMU_MEMORY_TTBR_L2_SIZE - 1));
				free_memalign((void *)physical_to_virtual((paddr_t)level_2));
				page_table[idx] = 0;
			}
		}

		/* free the LEVEL_1_USER table */
		free_memalign((void *)page_table);
		*pgtbl = 0;
	}
}

#if !defined(ARM_USE_MMU_RELOC)
static void arm_mmu_desc_map_section(addr_t paddr, addr_t vaddr, uint flags)
{
	int index;

	/* Get the index into the translation table */
	index = vaddr / MB;

	/* Set the entry value:
	 * (2<<0): Section entry
	 * (0<<5): Domain = 0
	 *  flags: TEX, CB and AP bit settings provided by the caller.
	 */
	tt[index] = (paddr & ~(MB-1)) | (MMU_MEMORY_DOMAIN_MEM << 5) | (2<<0) | flags;

	arm_invalidate_tlb();
}

void arm_mmu_desc_config_mmu()
{
	int i;
	uint32_t reg;

	/* set some mmu specific control bits:
	 * access flag disabled, TEX remap disabled, mmu disabled
	 */
	arm_write_sctlr(arm_read_sctlr() & ~((1<<29)|(1<<28)|(1<<0)));

	/* set up an identity-mapped translation table with
	 * strongly ordered memory type and read/write access.
	 */
	for (i=0; i < 4096; i++) {
		arm_mmu_desc_map_section(i * MB,
					 i * MB,
					 MMU_MEMORY_L1_TYPE_STRONGLY_ORDERED |
					 MMU_MEMORY_L1_AP_P_RW_U_NA);
	}

	platform_init_mmu_mappings();

	/* configure N=7, (kernel uses TTBR1 / user uses TTBR0) */
	arm_write_ttbcr(MMU_MEMORY_TTBCR_N);

	/* set up the translation table base */
#if defined(ARM_USE_CPU_CACHING)
	/* I:wb/alloc O:wb/alloc */
	arm_write_ttbr1((uint32_t)tt | MMU_MEMORY_TTBR1_IRGN_RGN);
#else
	arm_write_ttbr1((uint32_t)tt);
#endif

	/* set up the domain access register */
	arm_write_dacr(0x1 << (MMU_MEMORY_DOMAIN_MEM * 2));

	/* turn on the mmu */
	reg = arm_read_sctlr();
	arm_write_sctlr(reg | 0x1);
}
#endif // ARM_USE_MMU_RELOC

#endif // ARM_WITH_MMU
