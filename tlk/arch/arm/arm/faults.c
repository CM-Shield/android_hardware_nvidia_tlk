/*
 * Copyright (c) 2008 Travis Geiselbrecht
 * Copyright (c) 2012-2013, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <debug.h>
#include <platform.h>
#include <assert.h>
#include <arch.h>
#include <arch/arm.h>
#include <kernel/task.h>

extern unsigned int ote_logger_enabled;
#define MAX_FRAME_STRING_CHARS		64

struct mode_str_ent {
	uint32_t mode;
	char *str;
};

static struct mode_str_ent mode_str[] = {
	{ .mode = MODE_USR, .str = "usr" },
	{ .mode = MODE_FIQ, .str = "fiq" },
	{ .mode = MODE_IRQ, .str = "irq" },
	{ .mode = MODE_SVC, .str = "svc" },
	{ .mode = MODE_MON, .str = "mon" },
	{ .mode = MODE_ABT, .str = "abt" },
	{ .mode = MODE_UND, .str = "und" },
	{ .mode = MODE_SYS, .str = "sys" },
};

static inline char *get_mode_str(uint32_t mode)
{
	uint32_t i;

	for (i = 0; i < sizeof(mode_str) / sizeof(mode_str[0]); i++)
		if (mode == mode_str[i].mode)
			return mode_str[i].str;
	return NULL;
}

static void dump_fault_frame(struct arm_fault_frame *frame)
{
	char str[MAX_FRAME_STRING_CHARS];

	sprintf(str, "r0  0x%08x r1  0x%08x r2  0x%08x r3  0x%08x\n", frame->r[0], frame->r[1], frame->r[2], frame->r[3]);
	dputs(CRITICAL, str);
	sprintf(str, "r4  0x%08x r5  0x%08x r6  0x%08x r7  0x%08x\n", frame->r[4], frame->r[5], frame->r[6], frame->r[7]);
	dputs(CRITICAL, str);
	sprintf(str, "r8  0x%08x r9  0x%08x r10 0x%08x r11 0x%08x\n", frame->r[8], frame->r[9], frame->r[10], frame->r[11]);
	dputs(CRITICAL, str);
	sprintf(str, "r12 0x%08x usp 0x%08x ulr 0x%08x pc  0x%08x\n", frame->r[12], frame->usp, frame->ulr, frame->pc);
	dputs(CRITICAL, str);
	sprintf(str, "spsr 0x%08x\n", frame->spsr);
	dputs(CRITICAL, str);

	struct arm_mode_regs regs;
	arm_save_mode_regs(&regs);

	sprintf(str, "%c%s r13 0x%08x r14 0x%08x\n", ((frame->spsr & MODE_MASK) == MODE_FIQ) ? '*' : ' ', "fiq", regs.fiq_r13, regs.fiq_r14);
	dputs(CRITICAL, str);
	sprintf(str, "%c%s r13 0x%08x r14 0x%08x\n", ((frame->spsr & MODE_MASK) == MODE_IRQ) ? '*' : ' ', "irq", regs.irq_r13, regs.irq_r14);
	dputs(CRITICAL, str);
	sprintf(str, "%c%s r13 0x%08x r14 0x%08x\n", ((frame->spsr & MODE_MASK) == MODE_SVC) ? '*' : ' ', "svc", regs.svc_r13, regs.svc_r14);
	dputs(CRITICAL, str);
	sprintf(str, "%c%s r13 0x%08x r14 0x%08x\n", ((frame->spsr & MODE_MASK) == MODE_MON) ? '*' : ' ', "mon", regs.mon_r13, regs.mon_r14);
	dputs(CRITICAL, str);
	sprintf(str, "%c%s r13 0x%08x r14 0x%08x\n", ((frame->spsr & MODE_MASK) == MODE_UND) ? '*' : ' ', "und", regs.und_r13, regs.und_r14);
	dputs(CRITICAL, str);
	sprintf(str, "%c%s r13 0x%08x r14 0x%08x\n", ((frame->spsr & MODE_MASK) == MODE_SYS) ? '*' : ' ', "sys", regs.sys_r13, regs.sys_r14);
	dputs(CRITICAL, str);

	// dump the bottom of the current stack
	addr_t stack;
	switch (frame->spsr & MODE_MASK) {
		case MODE_FIQ: stack = regs.fiq_r13; break;
		case MODE_IRQ: stack = regs.irq_r13; break;
		case MODE_SVC: stack = regs.svc_r13; break;
		case MODE_UND: stack = regs.und_r13; break;
		case MODE_SYS: stack = regs.sys_r13; break;
		case MODE_USR: stack = frame->usp; break;
		default:
			stack = 0;
	}

	if (stack != 0) {
		sprintf(str, "bottom of stack at 0x%08x:\n", (unsigned int)stack);
		dputs(CRITICAL, str);
		hexdump((void *)stack, 128);
	}
}

static void exception_die(struct arm_fault_frame *frame, int pc_off, const char *msg)
{
	inc_critical_section();
	frame->pc += pc_off;
	dputs(CRITICAL, msg);
	dump_fault_frame(frame);

	halt();
	for(;;);
}

void arm_syscall_handler(struct arm_fault_frame *frame)
{
	ASSERT(in_critical_section() == false);

	/*  enable ints as we enter an exception with ints disabled */
	arch_enable_ints();

	if (platform_syscall_handler((void *)frame->r))
		return;

	exception_die(frame, -4, "unhandled syscall, halting\n");
}

/* Pointers to secure/normal world VFP contexts */
fpctx_t *s_vfp_hw_context = NULL;
fpctx_t *ns_vfp_hw_context = NULL;

void arm_undefined_handler(struct arm_fault_frame *frame)
{
#if ARM_WITH_NEON
	if (arm_get_vfp_fpexc() & 0x40000000)
		goto fail;	/* already enabled? */

	/*
	 * This vfp ctxsw exception should've been triggered by a user mode
	 * access. The current user mode thread should have a non-NULL fpctx
	 * setup during its task init. There also should be a single normal
	 * world vfp context buffer allocated to save/restore that context.
	 */
	ASSERT((frame->spsr & MODE_MASK) == MODE_USR);
	ASSERT(current_thread->arch.fpctx);
	ASSERT(ns_vfp_hw_context);

	/*
	 * First vfp context switch saves to NS world state. Any exceptions
	 * that follow are tasks context switching against each other where
	 * an outgoing context will need to be faulted back in.
	 */
	if (ns_vfp_hw_context->valid == false) {
		/* save NS world state */
		arch_vfp_save(ns_vfp_hw_context);
		ns_vfp_hw_context->valid = true;

		s_vfp_hw_context = current_thread->arch.fpctx;

		/* restore S world state */
		arch_vfp_restore(s_vfp_hw_context);
		s_vfp_hw_context->fpexc = 0x40000000;
		s_vfp_hw_context->valid = true;
	} else {
		ASSERT(s_vfp_hw_context);

		/* save previous S world state (faults when run again) */
		arch_vfp_save(s_vfp_hw_context);
		s_vfp_hw_context->fpexc = 0x0;

		s_vfp_hw_context = current_thread->arch.fpctx;

		/* restore next S world state */
		arch_vfp_restore(s_vfp_hw_context);
		s_vfp_hw_context->fpexc = 0x40000000;
		s_vfp_hw_context->valid = true;
	}

	/* retry instruction */
	if (frame->spsr & EXC_THUMB)
		frame->pc -= 2;		// thumb
	else
		frame->pc -= 4;		// arm
	return;
#endif

fail:
	/* unexpected exception */
	exception_die(frame, -4, "undefined abort, halting\n");
}

void arm_data_abort_handler(struct arm_fault_frame *frame)
{
	uint32_t dfar, dfsr;
	uint32_t mode;

	ote_logger_enabled = 0;
	__asm volatile ("mrc	p15, 0, %0, c6, c0, 0" : "=r" (dfar));
	__asm volatile ("mrc	p15, 0, %0, c5, c0, 0" : "=r" (dfsr));

	dputs(CRITICAL, "TLK data abort handler\n");
	printf("dfar: 0x%08x dfsr: 0x%08x pc: 0x%08x\n", dfar, dfsr, frame->pc);
	mode = frame->spsr & MODE_MASK;
	printf("mode: 0x%02x / %s\n", mode, get_mode_str(mode));

	if (mode == MODE_ABT) {
		dputs(CRITICAL, "faulted in fault handler, just idle\n");
		halt();
		for(;;);
	}

	printf("current thread: %p (%s)\n", current_thread,
		(current_thread) ? current_thread->name : NULL);
	if (current_thread) {
		task_t *task = current_thread->arch.task;
		printf("current task: %p (%s)\n", task,
			(task) ? task->task_name : NULL);
	}
	exception_die(frame, -8, "data abort, halting\n");
}

void arm_prefetch_abort_handler(struct arm_fault_frame *frame)
{
	exception_die(frame, -4, "prefetch abort, halting\n");
}
