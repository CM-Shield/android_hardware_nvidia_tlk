/*
 * Copyright (c) 2008 Travis Geiselbrecht
 * Copyright (c) 2012, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>
#include <debug.h>
#include <assert.h>
#include <kernel/thread.h>
#include <kernel/task.h>
#include <arch/arm.h>
#include <arch/arm/mmu.h>

// #define DEBUG_THREAD_SWITCH

static void arch_thread_bootstrap_stack_switch(thread_t *t)
{
	vaddr_t stack_top;
	vaddr_t curr_sp, new_sp;
	uint32_t stack_used;
	extern int abort_stack_top;

	/* move to allocated stack */
	t->stack_size = DEFAULT_STACK_SIZE;
	t->stack = malloc(t->stack_size);
	if (t->stack == NULL) {
		panic("arch_thread_init: stack alloc failed\n");
	}

	stack_top = (vaddr_t)t->stack + t->stack_size;
	stack_top = ROUNDDOWN(stack_top, 8);

	__asm__ volatile("mov	%0, sp" : "=r" (curr_sp));
	stack_used = (vaddr_t)(&abort_stack_top) - curr_sp;
	new_sp = stack_top - stack_used;

	memcpy((void *)new_sp, (void *)curr_sp, stack_used);
	__asm__ volatile("mov	sp, %0" :: "r" (new_sp));
}

void arch_thread_set_context(thread_t *t)
{
	vaddr_t stack_top;
	struct context_switch_frame *frame;

	ASSERT(t->state == THREAD_SUSPENDED);

	/* create context frame for initial context switch */
	stack_top = (vaddr_t)t->stack + t->stack_size;
	stack_top = ROUNDDOWN(stack_top, 8);	/* for EABI compliance */

	frame = (struct context_switch_frame *) stack_top;
	frame--;

	// fill in initial context */
	memset(frame, 0, sizeof(*frame));

	/* kernel mode thread */
	frame->psr = MODE_SVC;
	frame->sp = stack_top;

	frame->lr = (vaddr_t)&thread_exit;
	frame->r0 = (vaddr_t)t->arg;
	frame->pc = (vaddr_t)t->entry;
	t->arch.sp = (vaddr_t)frame;

	t->arch.initial = true;

#if DEBUG_THREAD_SWITCH
	dprintf(SPEW,
		"arch_thread_set_context: thread %p (%s), frame %p (base 0x%08x, top 0x%08x)\n",
		t, t->name, t->arch.sp, t->stack, (uint32_t)stack_top);
	dump_thread(current_thread);
#endif
}

void arch_thread_initialize(thread_t *thread)
{
	/*
	 * Normally called from thread_create(), but there's some setup and
	 * stack switching needed during the early thread_init() routine to
	 * move the current thread off a statically allocated stack to one
	 * allocated from the heap.
	 */
	if (thread == current_thread)
		arch_thread_bootstrap_stack_switch(thread);
	else
		arch_thread_set_context(thread);
}

void arch_context_switch(thread_t *oldthread, thread_t *newthread)
{
	struct context_switch_frame *frame;

	/*
	 * Whether newthread runs with interrupts enabled or not, depends
	 * on the current conditions, if this is the initial launch of the
	 * thread (as it jumps to its t->entry point).
	 *
	 * If the thread has already started (initial == false), it became
	 * blocked in the scheduler and is where it resumes, so an eventual
	 * exit_critical_section() will set the correct intr state.
	 */
	ASSERT(in_critical_section());

	frame = (struct context_switch_frame *) newthread->arch.sp;
	if (newthread->arch.initial) {
		newthread->arch.initial = false;

		/* exit critical section created in thread_create */
		dec_critical_section();
		if (critical_section_count == 0)
			frame->psr &= ~(1 << 7);	/* clear intr disable */
		else
			frame->psr |= (1 << 7);		/* set intr disable */
	} else {
		/* until it leaves the scheduler (intrs should be disabled) */
		ASSERT(frame->psr & (1 << 7));
	}

#if DEBUG_THREAD_SWITCH
	dprintf(SPEW, "arch_context_switch: old %p (%s), new %p (%s)\n",
		oldthread, oldthread->name, newthread, newthread->name);
	dprintf(SPEW,
		"arch_context_switch: sp 0x%08x lr 0x%08x pc 0x%08x psr 0x%08x frame 0x%08x top 0x%08x\n",
		frame->sp, frame->lr, frame->pc, frame->psr, frame,
		((vaddr_t)newthread->stack + newthread->stack_size));
#endif

	arch_set_tls(newthread->arch.tp_value);
	arm_context_switch(oldthread, newthread);
}
