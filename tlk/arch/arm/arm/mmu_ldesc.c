/*
 * Copyright (c) 2013, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <debug.h>
#include <assert.h>
#include <sys/types.h>
#include <err.h>
#include <compiler.h>
#include <malloc.h>
#include <string.h>
#include <arch.h>
#include <arch/arm.h>
#include <arch/arm/mmu.h>
#include <arch/arm/mmu_ldesc.h>
#include <platform.h>

#if ARM_WITH_MMU

#define MB (1024*1024)

/* the location of the table may be brought in from outside */
#if WITH_EXTERNAL_TRANSLATION_TABLE
#if !defined(MMU_TRANSLATION_TABLE_ADDR)
#error must set MMU_TRANSLATION_TABLE_ADDR in the make configuration
#endif
uint32_t *tt = (void *)MMU_TRANSLATION_TABLE_ADDR;
#else
/* the main translation table */
uint64_t tt[2048] __ALIGNED(16384) __SECTION(".bss.prebss.translation_table");
/* first level page table base (with TTBCR.T1SZ = 0, only need 32 byte alignment) */
uint64_t tt_level1[4] __ALIGNED(32) __SECTION(".bss.prebss.translation_table");
#endif

typedef enum {
        NONE = 0,
        LEVEL_2_USER,
        LEVEL_2_PRIV,
        LEVEL_3,
} pgtbl_lvl_t;

vaddr_t arm_mmu_desc_get_max_user_space()
{
	return (1 << (32 - MMU_MEMORY_TTBCR_T0SZ));
}

void arm_mmu_desc_set_default_kernel(arm_phys_attrs_t *attrs)
{
	attrs->inner = MMU_MEMORY_WRITE_BACK_ALLOCATE;
	attrs->outer = MMU_MEMORY_WRITE_BACK_NO_ALLOCATE;
	attrs->is_lpae = true;
}

void arm_mmu_desc_set_default_task(arm_phys_attrs_t *attrs)
{
	attrs->inner = MMU_MEMORY_WRITE_BACK_ALLOCATE;
	attrs->outer = MMU_MEMORY_WRITE_BACK_NO_ALLOCATE;
	attrs->is_lpae = true;
}

static uint64_t *arm_mmu_desc_alloc_pgtbl(pgtbl_lvl_t type)
{
	uint32_t size;
	uint64_t *pgtable = NULL;

	switch (type) {
	case LEVEL_2_USER:
	case LEVEL_2_PRIV:
		size = MMU_MEMORY_TTBR_L2_SIZE;
		break;
	case LEVEL_3:
		size = MMU_MEMORY_TTBR_L3_SIZE;
		break;
	default:
		dprintf(CRITICAL, "unrecognized pgtbl_type %d\n", type);
		return pgtable;
	}

	pgtable = memalign(size, MMU_MEMORY_TABLE_ADDR_ALIGN);
	if (pgtable)
		memset(pgtable, 0, size);

	return pgtable;
}

static int32_t arm_mmu_desc_find_mair_index(uint32_t attr)
{
	uint32_t index;
	uint32_t free_index = 0;
	uint32_t mem_attrs;

	/* read MAIR0 */
        __asm__ volatile("mrc p15, 0, %0, c10, c2, 0": "=r" (mem_attrs));

	for (index = 0; index < 4; index++) {
		if ((attr & 0xFF) == (mem_attrs & 0xFF))
			return index;	/* match */
		/* don't replace SO index (first index and holds 0) */
		if (index && !(mem_attrs & 0xFF)) {
			free_index = index;
			break;
		}
		mem_attrs >>= 8;
	}

	if (free_index) {
		/* insert in MAIR0 */
		__asm__ volatile("mrc p15, 0, %0, c10, c2, 0" : "=r" (mem_attrs));
		mem_attrs |= ((attr & 0xFF) << (free_index * 8));
		__asm__ volatile("mcr p15, 0, %0, c10, c2, 0" :: "r" (mem_attrs));
		return free_index;
	}

	/* read MAIR1 (no match or free index in the first set) */
        __asm__ volatile("mrc p15, 0, %0, c10, c2, 1": "=r" (mem_attrs));

	for (index = 4; index < 8; index++) {
		if ((attr & 0xFF) == (mem_attrs & 0xFF))
			return index;	/* match */
		if (!(mem_attrs & 0xFF)) {
			free_index = index;
			break;
		}
		mem_attrs >>= 8;
	}

	if (free_index) {
		/* insert in MAIR1 */
		__asm__ volatile("mrc p15, 0, %0, c10, c2, 1" : "=r" (mem_attrs));
		mem_attrs |= ((attr & 0xFF) << ((free_index - 4) * 8));
		__asm__ volatile("mcr p15, 0, %0, c10, c2, 1" :: "r" (mem_attrs));
		return free_index;
	}

	dprintf(CRITICAL, "%s: unable to map attr (0x%02x) to MAIR0/1 index\n",
		__func__, attr);
	halt();

	return free_index;
}

static uint64_t arm_mmu_desc_l2_cache_attrs(arm_phys_attrs_t *attrs)
{
	uint64_t mem_attrs = 0;
	uint32_t attr_val;
	uint32_t attr_indx;

	if (!attrs->is_lpae) {
		dprintf(CRITICAL,
			"%s: mapping non-LPAE attribs to LPAE is unsupported\n",
			__func__);
		halt();
	}

	if (attrs->shareable)
		mem_attrs |= MMU_MEMORY_SH_INNER_SHAREABLE;

	/* find mem attr index */
	attr_val = (attrs->outer << 4) | (attrs->inner);
	attr_indx = arm_mmu_desc_find_mair_index(attr_val);

	mem_attrs |= MMU_MEMORY_SET_ATTR_IDX(attr_indx);
	return mem_attrs;
}

void arm_mmu_desc_map_page(vaddr_t vaddr, paddr_t paddr, paddr_t *pgtbl,
			   tmflags_t flags, arm_phys_attrs_t *attrs)
{
	uint64_t *page_table;
	uint64_t l3_flags = 0;
	uint64_t *level_3;
	uint32_t idx;
	bool is_kmap;

	is_kmap = (flags & TM_KERN_VA);
	if (is_kmap) {
		/* always start with second level */
		page_table = tt;
	} else {
		/* task mapping */
		if (*pgtbl == NULL) {
			page_table = arm_mmu_desc_alloc_pgtbl(LEVEL_2_USER);
			if (page_table == NULL) {
				dprintf(CRITICAL,
					"unable to allocate LEVEL_2 page table\n");
				halt();
			}
			*pgtbl = virtual_to_physical((vaddr_t)page_table);
		} else {
			/* task page tables are saved as its physaddr */
			page_table = (uint64_t *)physical_to_virtual(*pgtbl);
		}
	}

	ASSERT(page_table);

	idx = vaddr >> MMU_MEMORY_TTBR_L2_VADDR_SHIFT;

	level_3 = (uint64_t *)(addr_t)(page_table[idx] & MMU_MEMORY_TABLE_ADDR_MASK);
	if (!level_3) {
		/* alloc level 3 page table */
		level_3 = arm_mmu_desc_alloc_pgtbl(LEVEL_3);
		if (level_3 == NULL) {
			dprintf(CRITICAL, "unable to allocate LEVEL_3 page table\n");
			halt();
		}

		/* install in level_2 */
		page_table[idx]  = (uint64_t)(virtual_to_physical((vaddr_t)level_3));
		page_table[idx] |= 0x3;
	} else {
		/* convert from a level 2 block to page table desc */
		if ((page_table[idx] & 0x3) == 0x1) {
			uint32_t i;
			ASSERT(is_kmap);

			level_3 = arm_mmu_desc_alloc_pgtbl(LEVEL_3);
			if (level_3 == NULL) {
				dprintf(CRITICAL,
					"unable to allocate LEVEL_3 page table\n");
				halt();
			}

			/* initialize level 3 to map what level 2 had */
			for (i = 0; i < (1 << MMU_MEMORY_TTBR_L3_INDEX_BITS); i++)
				level_3[i] = page_table[idx] | (i << MMU_L3_SIZE_SHIFT) | 0x3;

			/* install in level_2 */
			page_table[idx]  = (uint64_t)(virtual_to_physical((vaddr_t)level_3));
			page_table[idx] |= 0x3;
		} else {
			level_3 = (uint64_t *)physical_to_virtual((addr_t)level_3);
		}
	}

	idx = vaddr >> MMU_MEMORY_TTBR_L3_VADDR_SHIFT;
	idx &= MMU_MEMORY_TTBR_L3_INDEX_MASK;

	/* setup level 3 flags */
	if (is_kmap) {
		l3_flags = MMU_MEMORY_AP_P_RW_U_NA;
	} else {
		l3_flags = (flags & TM_UW) ?
			MMU_MEMORY_AP_P_RW_U_RW : MMU_MEMORY_AP_P_RO_U_RO;
		l3_flags |= MMU_MEMORY_NON_GLOBAL;
	}
	l3_flags |= (flags & (TM_NS_MEM | TM_NS_MEM_PRIV)) ? MMU_MEMORY_NON_SECURE : 0;
	l3_flags |= MMU_MEMORY_ACCESS_FLAG;	/* avoid an access fault */

	/* set cache attribs */
	if (flags & TM_IO)
		l3_flags |= MMU_MEMORY_SET_ATTR_IDX(MMU_MEMORY_STRONGLY_ORDERED);
	else
		l3_flags |= arm_mmu_desc_l2_cache_attrs(attrs);

	/* install level_3 4K entry */
	level_3[idx]  = paddr & ~PAGE_MASK;
	level_3[idx] |= (l3_flags | 0x3);
}

void arm_mmu_desc_unmap_page(paddr_t pgtbl, vaddr_t vaddr, uint32_t asid)
{
	uint64_t *page_table;
	uint64_t *level_3;
	u_int idx;

	ASSERT(pgtbl);
	page_table = (uint64_t *)physical_to_virtual(pgtbl);

	idx = vaddr >> MMU_MEMORY_TTBR_L2_VADDR_SHIFT;

	level_3 = (uint64_t *)(addr_t)(page_table[idx] & MMU_MEMORY_TABLE_ADDR_MASK);
	ASSERT(level_3);
	level_3 = (uint64_t *)physical_to_virtual((addr_t)level_3);

	idx = vaddr >> MMU_MEMORY_TTBR_L3_VADDR_SHIFT;
	idx &= MMU_MEMORY_TTBR_L3_INDEX_MASK;

	level_3[idx] = 0;       /* invalid entry */
	arm_invalidate_tlb_byaddr_asid(vaddr, asid);
}

void arm_mmu_desc_dealloc_upgtbl(paddr_t *pgtbl)
{
	u_int idx;
	uint64_t *page_table;
	paddr_t level3;

	if (pgtbl && *pgtbl) {
		page_table = (uint64_t *)physical_to_virtual(*pgtbl);

		/* free LEVEL_3 tables */
		for (idx = 0; idx <= MMU_MEMORY_TTBR_L2_INDEX_MASK; idx++) {
			if (page_table[idx]) {
				level3 = page_table[idx] & MMU_MEMORY_TABLE_ADDR_MASK;
				free_memalign((void *)physical_to_virtual(level3));
				page_table[idx] = 0;
			}
		}

		/* free the LEVEL_2_USER table */
		free_memalign((void *)page_table);
		*pgtbl = 0;
	}
}

#if !defined(ARM_USE_MMU_RELOC)
static void arm_mmu_desc_map_block(addr_t paddr, addr_t vaddr, uint flags)
{
	int index;

	/* Get the index into the translation table */
	index = vaddr / (2 * MB);

	/* Set the entry value:
	 * (1<<0): Block entry
	 *  flags: TEX, CB and AP bit settings provided by the caller.
	 */
	tt[index] = (paddr & ~(2*MB-1)) | (1<<0) | flags;

	arm_invalidate_tlb();
}

void arm_mmu_desc_config_mmu()
{
	int i;
	uint32_t reg;

	/* set some mmu specific control bits:
	 * access flag disabled, TEX remap disabled, mmu disabled
	 */
	arm_write_sctlr(arm_read_sctlr() & ~((1<<29)|(1<<28)|(1<<0)));

	/* set up an identity-mapped translation table with
	 * strongly ordered memory type and read/write access.
	 */
	for (i=0; i < 2048; i++) {
		arm_mmu_desc_map_block((i * (2 * MB)),
					 (i * (2 * MB)),
					 MMU_MEMORY_SET_ATTR_IDX(MMU_MEMORY_STRONGLY_ORDERED) |
					 MMU_MEMORY_AP_P_RW_U_NA);
	}

	platform_init_mmu_mappings();

	/* configure N=7, (kernel uses TTBR1 / user uses TTBR0) */
	arm_write_ttbcr(MMU_TTBCR_FLAGS);

	/* set up the translation table base */
	arm_write_ttbr1((uint32_t)tt_level1);

	/* turn on the mmu (EAE and enable) */
	reg = arm_read_sctlr();
	reg |= ((1 << 31) | 1);
	arm_write_sctlr(reg);
}
#endif // ARM_USE_MMU_RELOC

#endif // ARM_WITH_MMU
