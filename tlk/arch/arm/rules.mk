LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR)

# default to the regular arm subarch
SUBARCH := arm

DEFINES += \
	ARM_CPU_$(ARM_CPU)=1

# do set some options based on the cpu core
HANDLED_CORE := false

ifeq ($(ARM_CPU),cortex-a15)
DEFINES += \
	ARM_WITH_CP15=1		\
	ARM_WITH_MMU=1		\
	ARM_ISA_ARMv7=1		\
	ARM_ISA_ARMv7A=1	\
	ARM_WITH_VFP=1		\
	ARM_WITH_NEON=1		\
	ARM_WITH_THUMB=1	\
	ARM_WITH_THUMB2=1	\
	ARM_WITH_CACHE=1	\
	ARM_WITH_SCU=0		\
	ARM_WITH_L2=0
HANDLED_CORE := true
#CFLAGS += -mfpu=neon -mfloat-abi=softfp
MODULE_DEPS += $(LOCAL_DIR)/arm/neon
endif
ifeq ($(ARM_CPU),cortex-a9)
DEFINES += \
	ARM_WITH_CP15=1		\
	ARM_WITH_MMU=1		\
	ARM_ISA_ARMv7=1		\
	ARM_ISA_ARMv7A=1	\
	ARM_WITH_VFP=1		\
	ARM_WITH_NEON=1		\
	ARM_WITH_THUMB=1	\
	ARM_WITH_THUMB2=1	\
	ARM_WITH_CACHE=1	\
	ARM_WITH_SCU=1		\
	ARM_WITH_L2=0
HANDLED_CORE := true
#CFLAGS += -mfpu=neon -mfloat-abi=softfp
MODULE_DEPS += $(LOCAL_DIR)/arm/neon
endif

ifeq ($(ARM_CPU),arm926ej-s)
DEFINES += \
	ARM_WITH_CP15=1 \
	ARM_WITH_MMU=1 \
	ARM_ISA_ARMv5E=1 \
	ARM_WITH_THUMB=1 \
	ARM_WITH_CACHE=1 \
	ARM_CPU_ARM9=1 \
	ARM_CPU_ARM926=1
HANDLED_CORE := true
endif

ifneq ($(HANDLED_CORE),true)
$(warning $(LOCAL_DIR)/rules.mk doesnt have logic for arm core $(ARM_CPU))
$(warning this is likely to be broken)
endif

INCLUDES += \
	-I$(LOCAL_DIR)/include \
	-I$(LOCAL_DIR)/$(SUBARCH)/include

ifeq ($(SUBARCH),arm)
MODULE_SRCS += \
	$(LOCAL_DIR)/arm/start.S \
	$(LOCAL_DIR)/arm/asm.S \
	$(LOCAL_DIR)/arm/cache-ops.S \
	$(LOCAL_DIR)/arm/cache.c \
	$(LOCAL_DIR)/arm/ops.S \
	$(LOCAL_DIR)/arm/exceptions.S \
	$(LOCAL_DIR)/arm/faults.c \
	$(LOCAL_DIR)/arm/mmu.c \
	$(LOCAL_DIR)/arm/task.c \
	$(LOCAL_DIR)/arm/thread.c \
	$(LOCAL_DIR)/arm/dcc.S \
	$(LOCAL_DIR)/arm/cache-l2x0.c

# The monitor support is provided either as a separate binary when
# WITH_MONITOR_BIN = true, or as part of the secureos image when
# it's false.
#
# When used with a separate binary, monitor_interface.S is used to
# pass back/forth between secureos and the monitor, otherwise the
# monitor support is part of the image by including monitor_vectors.S
# in the build.

ifeq ($(MONITOR_BIN),true)
# interface support
MODULE_SRCS += \
	$(LOCAL_DIR)/arm/monitor_interface.S
endif

MODULE_ARM_OVERRIDE_SRCS := \
	$(LOCAL_DIR)/arm/arch.c

# using either long / short MMU desc support
ifeq ($(ARM_WITH_LPAE),true)
DEFINES += \
	ARM_WITH_LPAE=1
MODULE_SRCS += \
	$(LOCAL_DIR)/arm/mmu_ldesc.c
else
MODULE_SRCS += \
	$(LOCAL_DIR)/arm/mmu_sdesc.c
endif

DEFINES += \
	ARCH_DEFAULT_STACK_SIZE=4096
endif

# If platform sets ARM_USE_MMU_RELOC the image will be built based on
# VMEMBASE and will create page table entries in start.S to the physmem
# it's been given (avoiding relocation by copying the image).

ifeq ($(ARM_USE_MMU_RELOC),true)
DEFINES += \
	ARM_USE_MMU_RELOC=1
endif

ifeq ($(ARM_USE_CPU_CACHING),true)
DEFINES += \
	ARM_USE_CPU_CACHING=1
endif

# make sure some bits were set up
MEMVARS_SET := 0
ifeq ($(ARM_USE_MMU_RELOC),true)
ifneq ($(VMEMBASE),)
MEMVARS_SET := 1
endif
ifneq ($(VMEMSIZE),)
MEMVARS_SET := 1
endif
ifeq ($(MEMVARS_SET),0)
$(error missing VMEMBASE or VMEMSIZE variable, please set in target rules.mk)
endif
else
ifneq ($(MEMBASE),)
MEMVARS_SET := 1
endif
ifneq ($(MEMSIZE),)
MEMVARS_SET := 1
endif
DEFINES += \
        VMEMBASE=$(MEMBASE)
endif
ifeq ($(MEMVARS_SET),0)
$(error missing MEMBASE or MEMSIZE variable, please set in target rules.mk)
endif

LIBGCC := $(shell $(TOOLCHAIN_PREFIX)gcc $(MODULE_COMPILEFLAGS) -print-libgcc-file-name)
$(info LIBGCC = $(LIBGCC))

$(info ARCH_COMPILEFLAGS = $(MODULE_COMPILEFLAGS))

# potentially generated files that should be cleaned out with clean make rule
GENERATED += \
	$(BUILDDIR)/system-onesegment.ld \
	$(BUILDDIR)/system-twosegment.ld

# rules for generating the linker scripts

ifeq ($(ARM_USE_MMU_RELOC),true)
$(BUILDDIR)/system-onesegment.ld: $(LOCAL_DIR)/system-onesegment.ld $(CONFIGHEADER)
	@echo generating $@
	@$(MKDIR)
	$(NOECHO)sed "s/%MEMBASE%/$(VMEMBASE)/;s/%MEMSIZE%/$(VMEMSIZE)/" < $< > $@

$(BUILDDIR)/system-twosegment.ld: $(LOCAL_DIR)/system-twosegment.ld $(CONFIGHEADER)
	@echo generating $@
	@$(MKDIR)
	$(NOECHO)sed "s/%ROMBASE%/$(ROMBASE)/;s/%MEMBASE%/$(VMEMBASE)/;s/%MEMSIZE%/$(VMEMSIZE)/" < $< > $@
else
$(BUILDDIR)/system-onesegment.ld: $(LOCAL_DIR)/system-onesegment.ld $(CONFIGHEADER)
	@echo generating $@
	@$(MKDIR)
	$(NOECHO)sed "s/%MEMBASE%/$(MEMBASE)/;s/%MEMSIZE%/$(MEMSIZE)/" < $< > $@

$(BUILDDIR)/system-twosegment.ld: $(LOCAL_DIR)/system-twosegment.ld $(CONFIGHEADER)
	@echo generating $@
	@$(MKDIR)
	$(NOECHO)sed "s/%ROMBASE%/$(ROMBASE)/;s/%MEMBASE%/$(MEMBASE)/;s/%MEMSIZE%/$(MEMSIZE)/" < $< > $@
endif

include make/module.mk
