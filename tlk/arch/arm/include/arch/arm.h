/*
 * Copyright (c) 2008 Travis Geiselbrecht
 * Copyright (c) 2012-2013, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __ARCH_ARM_H
#define __ARCH_ARM_H

/* defines used in asm */
#define MODE_USR	0x10
#define MODE_FIQ	0x11
#define MODE_IRQ	0x12
#define MODE_SVC	0x13
#define MODE_MON	0x16
#define MODE_ABT	0x17
#define MODE_UND	0x1b
#define MODE_SYS	0x1f
#define MODE_MASK	0x1f

#define EXC_ARM		(0 << 5)
#define EXC_THUMB	(1 << 5)

/* offsets in context_switch_frame */
#define CSF_OFFSET_R0	0x0
#define CSF_OFFSET_R1	0x4
#define CSF_OFFSET_R2	0x8
#define CSF_OFFSET_R3	0xc
#define CSF_OFFSET_R4	0x10
#define CSF_OFFSET_R5	0x14
#define CSF_OFFSET_R6	0x18
#define CSF_OFFSET_R7	0x1c
#define CSF_OFFSET_R8	0x20
#define CSF_OFFSET_R9	0x24
#define CSF_OFFSET_R10	0x28
#define CSF_OFFSET_R11	0x2c
#define CSF_OFFSET_R12	0x30
#define CSF_OFFSET_SP	0x34
#define CSF_OFFSET_LR	0x38
#define CSF_OFFSET_PC	0x3c
#define CSF_OFFSET_PSR	0x40
#define CSF_SIZE	(CSF_OFFSET_PSR + 0x4)

#ifndef ASSEMBLY

#include <sys/types.h>
#include <arch/arm/cores.h>
#include <kernel/thread.h>

#if defined(__cplusplus)
extern "C" {
#endif

#define DSB __asm__ volatile("dsb" ::: "memory")
#define ISB __asm__ volatile("isb" ::: "memory")

void arm_context_switch(thread_t *old_sp, thread_t *new_sp);

static inline uint32_t read_cpsr() {
	uint32_t cpsr;

	__asm__ volatile("mrs   %0, cpsr" : "=r" (cpsr));
	return cpsr;
}

struct arm_iframe {
	uint32_t r[13];
	uint32_t lr;	/* lr_svc */
	uint32_t usp;	/* sp_usr */
	uint32_t ulr;	/* lr_usr */
	uint32_t pc;
	uint32_t spsr;	/* spsr_irq */
};

struct arm_fault_frame {
	uint32_t spsr;
	uint32_t usp;
	uint32_t ulr;
	uint32_t r[13];
	uint32_t pc;
};

struct context_switch_frame {
	uint32_t r0;
	uint32_t r1;
	uint32_t r2;
	uint32_t r3;
	uint32_t r4;
	uint32_t r5;
	uint32_t r6;
	uint32_t r7;
	uint32_t r8;
	uint32_t r9;
	uint32_t r10;
	uint32_t r11;
	uint32_t r12;
	uint32_t sp;	/* sp_usr or sp_svc */
	uint32_t lr;	/* lr_usr or lr_svc */
	uint32_t pc;	/* restart pc */
	uint32_t psr;	/* previous mode */
};

struct arm_mode_regs {
	uint32_t fiq_r13, fiq_r14;
	uint32_t irq_r13, irq_r14;
	uint32_t svc_r13, svc_r14;
	uint32_t mon_r13, mon_r14;
	uint32_t abt_r13, abt_r14;
	uint32_t und_r13, und_r14;
	uint32_t sys_r13, sys_r14;
};

void arm_save_mode_regs(struct arm_mode_regs *regs);

uint32_t arm_read_sctlr(void);
void arm_write_sctlr(uint32_t val);
uint32_t arm_read_actlr(void);
void arm_write_actlr(uint32_t val);
uint32_t arm_read_ttbr0(void);
void arm_write_ttbr0(uint32_t val);
uint32_t arm_read_contextidr(void);
void arm_write_contextidr(uint32_t val);
uint32_t arm_read_ttbr1(void);
void arm_write_ttbr1(uint32_t val);
uint32_t arm_read_ttbcr(void);
void arm_write_ttbcr(uint32_t val);
uint32_t arm_read_dacr(void);
void arm_write_dacr(uint32_t val);
void arm_invalidate_tlb(void);
void arm_invalidate_tlb_byaddr(addr_t addr);
void arm_invalidate_tlb_byaddr_asid(addr_t addr, uint32_t asid);

uint32_t arm_get_tls(void);
void arm_set_tls(uint32_t val);
uint32_t arm_read_vbar(void);
void arm_write_vbar(uint32_t val);

/* virt -> phys address translation args */
enum {
	V2PCWPR,
	V2PCWPW,
	V2PCWUR,
	V2PCWUW,
	V2POWPR,
	V2POWPW,
	V2POWUR,
	V2POWUW
};
void arm_write_v2p(uint32_t vaddr, uint32_t type);
uint64_t arm_read_par(void);

#if ARM_WITH_NEON
uint32_t arm_get_vfp_fpexc(void);
void arm_set_vfp_fpexc(uint32_t val);
uint32_t arm_get_vfp_fpscr(void);
void arm_set_vfp_fpscr(uint32_t val);
void arm_save_vfp_dregs(addr_t ctx);
void arm_restore_vfp_dregs(addr_t ctx);
#endif

#if defined(__cplusplus)
}
#endif
#endif

#endif
