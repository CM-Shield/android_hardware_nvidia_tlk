/*
 * Copyright (c) 2013, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <config.h>
#include <arch/arm/mmu_sdesc.h>

#define MMU_PAGE_TABLE		tt

#define MMU_PTE_L1_MMIO_FLAGS	\
	((MMU_MEMORY_L1_TYPE_STRONGLY_ORDERED |	\
	  MMU_MEMORY_L1_AP_P_RW_U_NA) | 0x2)

#define MMU_PTE_L1_KERN_FLAGS	\
	((MMU_MEMORY_SET_L1_INNER(MMU_MEMORY_WRITE_BACK_ALLOCATE) |	\
	  MMU_MEMORY_SET_L1_OUTER(MMU_MEMORY_WRITE_BACK_NO_ALLOCATE) |	\
	  MMU_MEMORY_SET_L1_CACHEABLE_MEM |	\
	  MMU_MEMORY_L1_AP_P_RW_U_NA) | 0x2)

#define MMU_TTBR1_IWBA_OWBA	\
	(MMU_MEMORY_TTBR1_IRGN_RGN)


/* loads 32-bit value into a register */
.macro mov32, reg, val
	movw	\reg, #:lower16:\val
	movt	\reg, #:upper16:\val
.endm

/* sections require 1MB alignment */
.macro mmu_desc_phy_align, base, size, tmp
	mov32	\tmp, 0xFFFFF
	tst	\base, \tmp
	bne	.		@ base not aligned
	cmp	\size, #0
	beq	.		@ size is zero
	tst	\size, \tmp
	bne	.		@ size not aligned
.endm

/* fill in pagetable */
.macro mmu_desc_setup_pt, pt, pte, idx, tmp, tmp2
	/* NULL entry */
	ldr	\pt, =MMU_PAGE_TABLE		@ virt pt addr
	adr	\tmp, __load_phys_base
	ldr	\tmp, [\tmp]
	mov32	\idx, VMEMBASE
	sub	\idx, \tmp, \idx
	add	\pt, \idx, \pt			@ phys pt addr
	mov	\pte, #0
	mov	\idx, #0
	str	\pte, [\pt, \idx, lsl #2]	@ NULL ptr access fault

	/* create identity entries */
	mov32	\tmp, MMU_PTE_L1_MMIO_FLAGS
	add	\idx, #1
ident_loop:
	add	\pte, \tmp, \idx, lsl #20
	str	\pte, [\pt, \idx, lsl #2]	@ identity mapping
	add	\idx, #1
	cmp	\idx, #0x1000			@ number of 1MB mappings
	blt	ident_loop

	/* map VMEMBASE for physize entries */
	adr	\tmp, __load_phys_base
	ldr	\tmp, [\tmp]
	mov32	\pte, MMU_PTE_L1_KERN_FLAGS
	add	\pte, \pte, \tmp

	adr	\tmp, __load_phys_size
	ldr	\tmp, [\tmp]
	mov32	\idx, (VMEMBASE >> 20)
	add	\tmp, \idx, \tmp, lsr #20	@ last VMEM index

vtop_loop:
	str	\pte, [\pt, \idx, lsl #2]	@ VMEMBASE mapping
	add	\idx, #1
	add	\pte, \pte, #(1 << 20)
	cmp	\idx, \tmp
	blt	vtop_loop

	/* map phys alias with same KERN_FLAG ptes */
	adr	\tmp, __load_phys_base
	ldr	\tmp, [\tmp]
	mov32	\pte, MMU_PTE_L1_KERN_FLAGS
	mov	\idx, \tmp, lsr #20
	add	\pte, \pte, \tmp

	adr	\tmp, __load_phys_size
	ldr	\tmp, [\tmp]
	add	\tmp, \idx, \tmp, lsr #20	@ last alias index

alias_loop:
	str	\pte, [\pt, \idx, lsl #2]	@ phys alias mapping
	add	\idx, #1
	add	\pte, \pte, #(1 << 20)
	cmp	\idx, \tmp
	blt	alias_loop
.endm

/* init MMU registers and enable */
.macro mmu_desc_init_mmu, tmp, tmp2
	mov	\tmp, #MMU_MEMORY_TTBCR_N
	mcr	p15, 0, \tmp, c2, c0, 2		@ TTBCR

	adr	\tmp, __load_phys_base
	ldr	\tmp, [\tmp]
	mov32	\tmp2, VMEMBASE
	sub	\tmp2, \tmp, \tmp2		@ vtop offset

	ldr	\tmp, =MMU_PAGE_TABLE		@ virt pt addr
	add	\tmp, \tmp2, \tmp		@ phys pt addr

	mov32	\tmp2, MMU_TTBR1_IWBA_OWBA
	orr	\tmp, \tmp2
	mcr	p15, 0, \tmp, c2, c0, 1		@ TTBR1

	mov	\tmp, #1
	mcr	p15, 0, \tmp, c3, c0, 0		@ DACR

	mrc	p15, 0, \tmp, c1, c0, 0
	orr	\tmp, \tmp, #1
	mcr	p15, 0, \tmp, c1, c0, 0		@ SCTLR
.endm

.macro mmu_desc_switch_pt, new, asid, tmp, tmp2
	/*
	 * This is a thread that's part of a task, so switch the setup
	 * user mode context (i.e. make sure the user mode pagetables
	 * are loaded and update the VFP state).
	 *
	 * When switching user-mode translation tables we need to be sure that
	 * no pages are prefetched from the outgoing table.  To do this we
	 * we must first switch to the global translation table (ttbr1)
	 * using the following sequence:
	 *
	 *   1- switch ttbr0 to global translation table ttbr1
	 *   2- ISB
	 *   3- change state (i.e. contextidr)
	 *   4- DSB
	 *   5- switch ttbr0 to new translation table for this thread
	 */
	mrc	p15, 0, \tmp, c2, c0, 1		@ get ttbr1
	mcr	p15, 0, \tmp, c2, c0, 0		@ load into ttbr0
	isb
	dsb					@ ARM_ERRATA_754322
	mcr	p15, 0, \asid, c13, c0, 1	@ contextidr
	isb
#if ARM_USE_CPU_CACHING
	/* I:wb/alloc O:wb/alloc */
	orr	\new, \new, #0x48
#endif
	mcr	p15, 0, \new, c2, c0, 0		@ ttbr0
.endm

#define MMU_DESC_CONTEXT_INTS	5

/* save MMU context */
.macro mmu_desc_save_context, base, tmp, tmp2
	mrc	p15, 0, \tmp, c2, c0, 2		@ TTBCR
	str	\tmp, [\base], #4
	mrc	p15, 0, \tmp, c2, c0, 0		@ TTBR0
	str	\tmp, [\base], #4
	mrc	p15, 0, \tmp, c13, c0, 1	@ CONTEXTIDR
	str	\tmp, [\base], #4
	mrc	p15, 0, \tmp, c2, c0, 1		@ TTBR1
	str	\tmp, [\base], #4
	mrc	p15, 0, \tmp, c3, c0, 0		@ DACR
	str	\tmp, [\base], #4
.endm


/* restore MMU context */
.macro mmu_desc_restore_context, base, tmp, tmp2
	ldr	\tmp, [\base], #4
	mcr	p15, 0, \tmp, c2, c0, 2		@ TTBCR
	ldr	\tmp, [\base], #4
	mcr	p15, 0, \tmp, c2, c0, 0		@ TTBR0
	ldr	\tmp, [\base], #4
	mcr	p15, 0, \tmp, c13, c0, 1	@ CONTEXTIDR
	ldr	\tmp, [\base], #4
	mcr	p15, 0, \tmp, c2, c0, 1		@ TTBR1
	ldr	\tmp, [\base], #4
	mcr	p15, 0, \tmp, c3, c0, 0		@ DACR
.endm
