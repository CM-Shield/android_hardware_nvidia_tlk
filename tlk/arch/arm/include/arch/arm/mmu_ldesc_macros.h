/*
 * Copyright (c) 2013-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <config.h>
#include <arch/arm/mmu_ldesc.h>

#define MMU_PTE_L2_BLOCK_MMIO_LOWER	\
	(MMU_MEMORY_SET_ATTR_IDX(MMU_MEMORY_STRONGLY_ORDERED) | \
	 MMU_MEMORY_ACCESS_FLAG |	\
	 MMU_MEMORY_AP_P_RW_U_NA | 0x1)

/* no execute for bits for EL1/EL0 */
#define MMU_PTE_L2_BLOCK_MMIO_UXN	(1 << 54)
#define MMU_PTE_L2_BLOCK_MMIO_PXN	(1 << 53)

#define MMU_PTE_L2_BLOCK_MMIO_UPPER	\
	(MMU_PTE_L2_BLOCK_MMIO_UXN | MMU_PTE_L2_BLOCK_MMIO_PXN)

#define MMU_PTE_L2_BLOCK_KERN_FLAGS	\
	(MMU_MEMORY_SET_ATTR_IDX(MMU_MEMORY_WB_OUTER_NO_ALLOC_INNER_ALLOC) | \
	 MMU_MEMORY_ACCESS_FLAG |	\
	 MMU_MEMORY_AP_P_RW_U_NA | 0x1)

#define MMU_PTE_L3_KERN_FLAGS	\
	(MMU_MEMORY_SET_ATTR_IDX(MMU_MEMORY_WB_OUTER_NO_ALLOC_INNER_ALLOC) | \
	 MMU_MEMORY_ACCESS_FLAG |	\
	 MMU_MEMORY_AP_P_RW_U_NA | 0x3)

#define MMU_TTBCR_FLAGS		\
	(MMU_MEMORY_TTBCR_EAE | MMU_MEMORY_TTBCR_A1 | \
	 MMU_MEMORY_TTBCR_OUTER_RGN1(MMU_MEMORY_RGN_WRITE_BACK_ALLOCATE) | \
	 MMU_MEMORY_TTBCR_OUTER_RGN0(MMU_MEMORY_RGN_WRITE_BACK_ALLOCATE) | \
	 MMU_MEMORY_TTBCR_INNER_RGN1(MMU_MEMORY_RGN_WRITE_BACK_ALLOCATE) | \
	 MMU_MEMORY_TTBCR_INNER_RGN0(MMU_MEMORY_RGN_WRITE_BACK_ALLOCATE) | \
	 MMU_MEMORY_TTBCR_T1SZ | MMU_MEMORY_TTBCR_T0SZ)

/* loads 32-bit value into a register */
.macro mov32, reg, val
	movw	\reg, #:lower16:\val
	movt	\reg, #:upper16:\val
.endm

/* size/align check of carveout */
.macro mmu_desc_phy_align, base, size, tmp
	mov32	\tmp, (MMU_L2_BLOCK_SIZE - 1)
	tst	\base, \tmp
	bne	.		@ base not aligned
	cmp	\size, #0
	beq	.		@ size is zero
	tst	\size, \tmp
	bne	.		@ size not aligned
.endm

/* create NULL 2MB entry to guarantee a NULL ptr fault */
.macro mmu_desc_create_null_entry, pt, tmp
	mov	\tmp, #0
	str	\tmp, [\pt], #4
	str	\tmp, [\pt], #4		@ write PTE and upper 0
.endm

/* create identity 2MB entries for MMIO */
.macro mmu_desc_create_ident_mapping, pt, pte, idx, upper
	/* create second level identity entries */
	mov32	\pte, MMU_PTE_L2_BLOCK_MMIO_LOWER
	mov	\idx, #1
	mov	\upper, #(MMU_PTE_L2_BLOCK_MMIO_UPPER >> 32)
1:
	add	\pte, \pte, #(1 << MMU_L2_BLOCK_SHIFT)
	stmia	\pt!, {\pte, \upper}	@ write PTE and upper 0
	add	\idx, #1
	cmp	\idx, #0x800		@ # of 2MB mappings in 32bit VA
	blt	1b
.endm

/* create identity 2MB entries for TZ carveout */
.macro mmu_desc_setup_carveout_ident, pt, addr, len, pte, tmp
	/* create second level identity entries */
	mov32	\pte, MMU_PTE_L2_BLOCK_MMIO_LOWER
	lsr	\addr, #MMU_L2_BLOCK_SHIFT
	mov	\tmp, \addr
	lsl	\addr, #MMU_L2_BLOCK_SHIFT
	orr	\pte, \addr
	add	\pt, \tmp, lsl #3

	/* roundup length to L2 blocks */
	mov32   \tmp, ((1 << MMU_L2_BLOCK_SHIFT) - 1)
	add	\tmp, \len, \tmp
	lsr	\len, \tmp, #MMU_L2_BLOCK_SHIFT		@ L2 block count
	mov	\tmp, #0
1:
	stmia	\pt!, {\pte, \tmp}    @ write PTE and upper 0
	add	\pte, \pte, #(1 << MMU_L2_BLOCK_SHIFT)
	subs	\len, \len, #0x1
	bne	1b
.endm

/* map phys carveout with L3 entries */
.macro mmu_desc_map_phys_l3, virt, phys, len, pt, l2, tmp, tmp2, zero
	/* setup L2 entries */
	mov32	\tmp, ((1 << MMU_L3_MAP_SHIFT) - 1)
	add	\tmp2, \len, \tmp
	lsr	\tmp2, \tmp2, #MMU_L3_MAP_SHIFT			@ roundup to L3 map size
	lsl	\tmp2, \tmp2, #MMU_L3_SIZE_SHIFT		@ bytes of L3 tables
	lsr	\tmp2, \tmp2, #MMU_MEMORY_TTBR_L3_VADDR_SHIFT	@ # of L2 entries

	lsr	\tmp, \virt, #MMU_MEMORY_TTBR_L2_VADDR_SHIFT
	add	\l2, \tmp, lsl #MMU_ENTRY_SHIFT		@ starting L2 entry
	orr	\tmp, \pt, #0x3
2:
	stmia	\l2!, {\tmp, \zero}		@ write PTE for level 3
	add	\tmp, \tmp, #(1 << MMU_MEMORY_TTBR_L3_VADDR_SHIFT)
	subs	\tmp2, \tmp2, #0x1
	bne	2b

	/* setup L3 entries */
	mov32	\tmp, MMU_PTE_L3_KERN_FLAGS
	add	\phys, \phys, \tmp			@ setup PTE
	lsr	\virt, \virt, #MMU_MEMORY_TTBR_L3_VADDR_SHIFT
	mov32	\tmp2, MMU_MEMORY_TTBR_L3_INDEX_MASK
	and	\virt, \virt, \tmp2
	add	\pt, \virt, lsl #MMU_ENTRY_SHIFT	@ starting PT entry
	mov32	\tmp2, (1 << MMU_MEMORY_TTBR_L3_VADDR_SHIFT)
	mov	\tmp, \len
3:
	stmia	\pt!, {\phys, \zero}			@ write PTE and upper 0
	add	\phys, \phys, \tmp2
	subs	\tmp, \tmp, #(1 << MMU_MEMORY_TTBR_L3_VADDR_SHIFT)
	bne	3b

	/* clear what remains of the page */
	mov32	\tmp, (MMU_MEMORY_TTBR_L3_SIZE - 1)
	ands	\tmp, \pt, \tmp
	beq	5f				@ done, ended on a page

	mov32	\tmp2, MMU_MEMORY_TTBR_L3_SIZE
	sub	\tmp, \tmp2, \tmp		@ bytes until end of page
	mov	\tmp2, \zero
4:
	stmia	\pt!, {\tmp2, \zero}		@ write PTE and upper 0
	subs	\tmp, \tmp, #(1 << MMU_ENTRY_SHIFT)
	bne	4b
5:
.endm

/* map phys carveout with L2 block entries */
.macro mmu_desc_map_phys_l2, virt, phys, len, pt, tmp
	mov32	\tmp, MMU_PTE_L2_BLOCK_KERN_FLAGS
	add	\phys, \phys, \tmp
	lsr	\virt, \virt, #(MMU_L2_BLOCK_SHIFT - MMU_ENTRY_SHIFT)
	add	\pt, \pt, \virt
	add	\len, \phys, \len
	mov	\tmp, #0
1:
	stmia	\pt!, {\phys, \tmp}	@ write PTE and upper 0
	add	\phys, \phys, #(1 << MMU_L2_BLOCK_SHIFT)
	cmp	\phys, \len
	bne	1b
.endm

/* map phys carveout in L1 */
.macro mmu_desc_map_phys_l1, phys, pt, zero
	orr	\phys, \phys, #0x3
	/* write first level entries (with the 4 parts of the main tt) */
	stmia	\pt!, {\phys, \zero}		@ index 0
	add	\phys, \phys, #(1 << 12)
	stmia	\pt!, {\phys, \zero}		@ index 1
	add	\phys, \phys, #(1 << 12)
	stmia	\pt!, {\phys, \zero}		@ index 2
	add	\phys, \phys, #(1 << 12)
	stmia	\pt!, {\phys, \zero}		@ index 3
.endm

/* init MMU registers and enable */
.macro mmu_desc_init_mmu, pt1, pt2, tmp
	/* init kernel referenced mem attribs */
	mov32	\tmp, MMU_MEMORY_ATTR_INDIR_0
	mcr	p15, 0, \tmp, c10, c2, 0	@ MAIR0
	mov32	\tmp, MMU_MEMORY_ATTR_INDIR_1
	mcr	p15, 0, \tmp, c10, c2, 1	@ MAIR1

	mov32	\tmp, MMU_TTBCR_FLAGS
	mcr	p15, 0, \tmp, c2, c0, 2		@ TTBCR

	mov	\tmp, #0
	mcrr	p15, 1, \pt1, \tmp, c2		@ TTBR1 64bit write (tmp | pt1)

	mrc	p15, 0, \tmp, c1, c0, 0
	orr	\tmp, \tmp, #1
	mcr	p15, 0, \tmp, c1, c0, 0		@ SCTLR
.endm

.macro mmu_desc_switch_pt, new, asid, tmp, tmp2
	/*
	 * This is a thread that's part of a task, so switch the setup
	 * user mode context (i.e. make sure the user mode pagetables
	 * are loaded and update the VFP state).
	 *
	 * When switching user-mode translation tables we need to be sure that
	 * no pages are prefetched from the outgoing table.  To do this we
	 * we must first switch to the global translation table (ttbr1)
	 * using the following sequence:
	 *
	 *   1- switch ttbr0 to global translation table ttbr1
	 *   2- ISB
	 *   4- DSB
	 *   5- switch ttbr0 to new translation table for this thread
	 */
	mrrc	p15, 1, \tmp, \tmp2, c2		@ get ttbr1
	mcrr	p15, 0, \tmp, \tmp2, c2		@ load into ttbr0
	isb
	dsb					@ ARM_ERRATA_754322
	lsl	\asid, \asid, #16		@ asid is bits[55:48]
	mcrr	p15, 0, \new, \asid, c2		@ load into ttbr0
.endm

#define MMU_DESC_CONTEXT_INTS	7

/* save MMU context */
.macro mmu_desc_save_context, base, tmp, tmp2
	mrc	p15, 0, \tmp, c10, c2, 0	@ MAIR0
	str	\tmp, [\base], #4
	mrc	p15, 0, \tmp, c10, c2, 1	@ MAIR1
	str	\tmp, [\base], #4
	mrc	p15, 0, \tmp, c2, c0, 2		@ TTBCR
	str	\tmp, [\base], #4
	mrrc	p15, 0, \tmp, \tmp2, c2		@ TTBR0 64bit
	str	\tmp, [\base], #4
	str	\tmp2, [\base], #4
	mrrc	p15, 1, \tmp, \tmp2, c2		@ TTBR1 64bit
	str	\tmp, [\base], #4
	str	\tmp2, [\base], #4
.endm

/* restore MMU context */
.macro mmu_desc_restore_context, base, tmp, tmp2
	ldr	\tmp, [\base], #4
	mcr	p15, 0, \tmp, c10, c2, 0	@ MAIR0
	ldr	\tmp, [\base], #4
	mcr	p15, 0, \tmp, c10, c2, 1	@ MAIR1
	ldr	\tmp, [\base], #4
	mcr	p15, 0, \tmp, c2, c0, 2		@ TTBCR
	ldr	\tmp, [\base], #4
	ldr	\tmp2, [\base], #4
	mcrr	p15, 0, \tmp, \tmp2, c2		@ TTBR0 64bit
	ldr	\tmp, [\base], #4
	ldr	\tmp2, [\base], #4
	mcrr	p15, 1, \tmp, \tmp2, c2		@ TTBR1 64bit
.endm
