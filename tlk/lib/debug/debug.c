/*
 * Copyright (c) 2008-2015 Travis Geiselbrecht
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <ctype.h>
#include <debug.h>
#include <stdlib.h>
#include <printf.h>
#include <list.h>
#include <string.h>
#include <arch/ops.h>
#include <platform.h>
#include <platform/debug.h>
#include <kernel/thread.h>

/* Physical address of cbuf */
unsigned long cbuf_addr;
unsigned long cbstruct_addr;

unsigned int ote_logger_enabled = 1;

/* Internal TLK buffer used to store TLK prints before we get
 * a shared buffer from linux.
 * At one point, we'll push all this to the shared buffer.
 */
char early_logbuf[EARLYBUF_SIZE];

/* Char count for early prints */
int early_char_count;

void spin(uint32_t usecs)
{
	lk_bigtime_t start = current_time_hires();

	while ((current_time_hires() - start) < usecs)
		;
}

void halt(void)
{
	enter_critical_section(); // disable ints
	platform_halt();
}

void _panic(void *caller, const char *fmt, ...)
{
	ote_logger_enabled = 0;

	dprintf(ALWAYS, "panic (caller %p): ", caller);

	va_list ap;
	va_start(ap, fmt);
	_dvprintf(fmt, ap);
	va_end(ap);

	halt();
}

void task_panic(char *task_name, char *msg)
{
	ote_logger_enabled = 0;

	dprintf(ALWAYS, "panic (task %s): %s\n", task_name, msg);
	halt();
}

#if !DISABLE_DEBUG_OUTPUT

int _dputs(const char *str)
{
	while(*str != 0) {
		_dputc(*str++);
	}

	return 0;
}

static void cbuf_write(char c)
{
	struct circular_buffer *cb = (struct circular_buffer *)cbstruct_addr;
	char *cbuf = (char*)cbuf_addr;

	cbuf[cb->end] = c;
	cb->end = (cb->end + 1) % cb->size;

	if (cb->end == cb->start) {
		cb->overflow = 1;
		cb->start = (cb->start + 1) % cb->size;
	}
}

static int _dprintf_output_func(char c, void *state)
{
	/* in case we dont get the shared memory buffer from the NS world */
	if (!ote_logger_enabled) {
		_dputc(c);
		goto out;
	}

	/* If physical address has not been initialized, means we have
	 * not yet received shared buffer from linux.
	 * Buffer logs in TLK internal buffer for now.
	 */
	if (cbstruct_addr == 0) {

		if (c == '\0')
			return INT_MAX;

		*(char *)(early_logbuf + early_char_count) = c;
		*(char *)(early_logbuf + early_char_count + 1) = '\0';
		early_char_count++;
		if (early_char_count > (EARLYBUF_SIZE - 2)) {
#if DEBUGLEVEL > INFO
			_dputs("\nTLK early buffer overflow, \
				overwriting data!\n");
#endif
			early_char_count = 0;
		}
	} else {

		/* We have received shared buffer. Write to it's physical address */
		if (c == '\0')
			return INT_MAX;

		cbuf_write(c);
	}

out:
	return INT_MAX;
}

int _dprintf(const char *fmt, ...)
{
	int err;

	va_list ap;
	va_start(ap, fmt);
	err = _printf_engine(&_dprintf_output_func, NULL, fmt, ap);
	va_end(ap);

	return err;
}

int _dvprintf(const char *fmt, va_list ap)
{
	int err;

	err = _printf_engine(&_dprintf_output_func, NULL, fmt, ap);

	return err;
}

void hexdump(const void *ptr, size_t len)
{
	addr_t address = (addr_t)ptr;
	size_t count;
	int i;

	for (count = 0 ; count < len; count += 16) {
		printf("0x%08lx: ", address);
		printf("%08x %08x %08x %08x |", *(const uint32_t *)address, *(const uint32_t *)(address + 4), *(const uint32_t *)(address + 8), *(const uint32_t *)(address + 12));
		for (i=0; i < 16; i++) {
			char c = *(const char *)(address + i);
			if (isalpha(c)) {
				printf("%c", c);
			} else {
				printf(".");
			}
		}
		printf("|\n");
		address += 16;
	}
}

void hexdump8(const void *ptr, size_t len)
{
	addr_t address = (addr_t)ptr;
	size_t count;
	int i;

	for (count = 0 ; count < len; count += 16) {
		printf("0x%08lx: ", address);
		for (i=0; i < 16; i++) {
			printf("0x%02hhx ", *(const uint8_t *)(address + i));
		}
		printf("\n");
		address += 16;
	}
}

#endif // !DISABLE_DEBUG_OUTPUT

