LOCAL_DIR := $(GET_LOCAL_DIR)

INCLUDES += \
	-I$(LOCAL_DIR)/include

PLATFORM := tegra
PLATFORM_SOC := tegra4

# using phys (MAP_NOR_BASE -> MAP_NOR_LIMIT) as VA space
VMEMSIZE := 0x8000000	# 128MB

#
# As part of the build, also generate a separate mon.bin from
# lib/monitor of the specified architecture. This binary will
# an address space of 2^MONBITS in size linked at MONBASE.
#
MONITOR_BIN := true

# base of GIC dist/cpu registers
ARM_GIC_DIST_BASE := 0x50041000
ARM_GIC_CPU_BASE  := 0x50042000

# frequency of ARM generic timer
ARM_SYSTEM_COUNTER_FREQ := 12000000

DEFINES += \
	ARM_GIC_DIST_BASE=$(ARM_GIC_DIST_BASE) \
	ARM_GIC_CPU_BASE=$(ARM_GIC_CPU_BASE) \
	ARM_SYSTEM_COUNTER_FREQ=$(ARM_SYSTEM_COUNTER_FREQ) \
	WITH_PLATFORM_IDLE=1
